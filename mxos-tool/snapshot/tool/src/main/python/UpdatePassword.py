import sys
import argparse
import urllib.parse
import urllib.request

parser = argparse.ArgumentParser()
url = 'http://localhost:8080/mxos/mailbox/v2/'

#-e EMAIL -p PASSWORD -t PASSWORD_TYPE -c HASHCOUNT

parser.add_argument("-e", "--email", dest = "email",  help="Email", default='')
parser.add_argument("-p", "--password",dest = "password", help="Password", default='')
parser.add_argument("-t", "--passwordType",dest ="passwordType", help="Password Type", default='')
parser.add_argument("-d", "--preEncrypt",dest = "preEncrypt", help="preEncrypt", default='true')
	
args = parser.parse_args()

print("\n\n\nPlease verify your input parameters before proceeding\n")
print("------------------------------------------------------------")
print("Email                   : " + args.email)
print("Password                : " + args.password)
print("Password Type           : " + args.passwordType)
print("Password Pre-Encrypted  : " + args.preEncrypt)
print("------------------------------------------------------------")

var = input("\n\nDo you want to continue (y/n): ")

if(not (var == 'y' or var == 'Y')):
	sys.exit("\nAborting password update")


print ('\nUpdating password...')

url = url + args.email + '/credentials'
params = {'email' : args.email, 'password' : args.password, 'passwordStoreType' : args.passwordType, 'preEncryptedPasswordAllowed' : args.preEncrypt}
data = urllib.parse.urlencode(params)
req = urllib.request.Request(url, bytes(data, 'UTF-8'))
req.get_method = lambda: 'POST'
response = urllib.request.urlopen(req)
the_page = response.read()
print ("\nPassword updated successfully")