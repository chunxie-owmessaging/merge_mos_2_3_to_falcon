/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.error;

/**
 * Class contains MxOS errors for SMS API's.
 *
 * @author mxos-dev
 */
public enum CustomError {

    MBX_UNABLE_TO_GET_CUSTOM_STATUS,
    MBX_UNABLE_TO_SET_CUSTOM_STATUS,
    FIONA_FLOW_INVALID_MSISDN,
    FIONA_FLOW_INVALID_CURRENT_MSISDN,
    FIONA_FLOW_INVALID_NEW_MSISDN,
    MAA_CONNECTION_ERROR,
    SEND_SMS_INVALID_MESSAGE,
    DEPOSIT_MAIL_MISSING_PARAMS,

    //Send SMS
    SMS_INVALID_SMS_TYPE,
    SEND_SMS_INVALID_FROM_ADDRESS,
    SEND_SMS_INVALID_TO_ADDRESS,

    //AuthorizeMSISDN
    AUTHORIZE_MSISDN_INVALID_MSISDN,

    //Log to External Entity
    LOG_TO_EXTERNAL_ENTITY_INVALID_APICALL,
    LOG_TO_EXTERNAL_ENTITY_INVALID_TIMESTAMP,
    LOG_TO_EXTERNAL_ENTITY_INVALID_KEY,
    LOG_TO_EXTERNAL_ENTITY_INVALID_PAYLOAD,
    LOG_TO_EXTERNAL_ENTITY_INVALID_RESULTCODE,
    LOG_TO_EXTERNAL_ENTITY_INVALID_CRUD,
    LOG_TO_EXTERNAL_ENTITY_INVALID_GATEWAY_CONFIGURED,

    //Deposit Mail
    DEPOSIT_MAIL_INVALID_FROM_ADDRESS,
    DEPOSIT_MAIL_INVALID_TO_ADDRESS,
    DEPOSIT_MAIL_INVALID_MESSAGE,

    //Send Mail
    SEND_MAIL_INVALID_FROM_ADDRESS,
    SEND_MAIL_INVALID_TO_ADDRESS,
    SEND_MAIL_INVALID_MESSAGE,

    //API Missing Parameters
    SEND_SMS_MISSING_PARAMS,
    AUTHORIZE_MSISDN_MISSING_PARAMS,
    LOG_TO_EXTERNAL_ENTITY_MISSING_PARAMS,
    SEND_MAIL_MISSING_PARAMS,

    //Errors returned to Client
    SEND_SMS_TEMPORARY_ERROR,
    SEND_SMS_PERM_SNMP_ERROR,
    SEND_SMS_PERMANENT_ERROR,
    MSISDN_AUTHORIZE_TEMPORARY_ERROR,
    MSISDN_AUTHORIZE_PERMANENT_ERROR,
    LOG_TO_EXTERNAL_ENTITY_ERROR,
    SEND_MAIL_ERROR,
    SEND_MAIL_AUTHENTICATION_ERROR,
    SEND_MAIL_CONNECT_ERROR,

    // MEG(MySQL) Errors
    MEG_CONNECTION_ERROR,
    MEG_CREATE_ERROR,
    MEG_READ_ERROR,
    MEG_UPDATE_ERROR,
    MEG_DELETE_ERROR,
}
