/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.rmi;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RMIClientSocketFactory;
import java.util.HashSet;
import java.util.Random;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import org.apache.log4j.Logger;

import com.openexchange.admin.rmi.OXContextInterface;
import com.openexchange.admin.rmi.dataobjects.Context;
import com.openexchange.admin.rmi.dataobjects.Credentials;
import com.openexchange.admin.rmi.dataobjects.User;
import com.openexchange.admin.rmi.exceptions.ContextExistsException;
import com.openexchange.admin.rmi.exceptions.InvalidCredentialsException;
import com.openexchange.admin.rmi.exceptions.InvalidDataException;
import com.openexchange.admin.rmi.exceptions.NoSuchContextException;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.crud.IRMIMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPBackendState;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPMailboxProperty;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

public class AppSuiteRMIMailboxCRUD implements IRMIMailboxCRUD {

    private static Logger logger = Logger
            .getLogger(AppSuiteRMIMailboxCRUD.class);

    private static final String DEFAULT_RMI_CLIENT_TIMEOUT = "2000";
    private static final String DEFAULT_RMI_READ_TIMEOUT = "2000";
    private static final String DEFAULT_MAX_RETRY_COUNT = "3";
    private static final String DEFAULT_RETRY_INTERVAL = "10";
    private static final long DEFAULT_OX_MAX_QUOTA = 1000L;
    private static final String DEFAULT_OX_TIME_ZONE = "EST5EDT";
    private static final String DEFAULT_OX_LOCALE = "en_US";
    private static final String DEFAULT_IMAP_MOS_URL = "mos://localhost:8081";
    private static final String DEFAULT_SMTP_MOS_URL = "mos://localhost:8081";
    private static final String DEFAULT_OX_FIRST_NAME = "_";
    private static final String DEFAULT_OX_LAST_NAME = "Myself";
    private static final String DEFAULT_OX_USER_ACCESS = "opwvall";
    private OXContextInterface contextInterface = null;
    private RMIClientSocketFactoryWithTimeout socketFactory;
    private Registry registry;
    private int readTimeout;
    private int connectionTimeout;
    private String host;
    private int port;
    private int maxRetryCount;
    private int retryInterval;

    /**
     * Constructor.
     * 
     * @param baseURL - base url of RMI registry server.
     * @throws NotBoundException
     * @throws IOException
     */
    public AppSuiteRMIMailboxCRUD(String host, int port) throws MxOSException {
        this.host = host;
        this.port = port;
        connectionTimeout = Integer.parseInt(System.getProperty(
                SystemProperty.rmiClientTimeout.name(),
                DEFAULT_RMI_CLIENT_TIMEOUT));
        maxRetryCount = Integer
                .parseInt(System.getProperty(
                        SystemProperty.oxRMIRetryCount.name(),
                        DEFAULT_MAX_RETRY_COUNT));
        retryInterval = Integer.parseInt(System.getProperty(
                SystemProperty.oxRMIRetryInterval.name(),
                DEFAULT_RETRY_INTERVAL));
        readTimeout = Integer
                .parseInt(System.getProperty(
                        SystemProperty.rmiReadTimeout.name(),
                        DEFAULT_RMI_READ_TIMEOUT));

        int retryCount = 0;
        do {
            try {
                getNewContextInterface();
                break;
            } catch (Exception e) {
                if (retryCount <= maxRetryCount) {
                    retryCount++;
                    logger.info("Error in RMI connection, retrying... retryCount: "
                            + retryCount);
                    logger.info("Thread sleep before retry retryInterval: "
                            + retryInterval);
                    try {
                        Thread.sleep(retryInterval);
                    } catch (InterruptedException ie) {
                        logger.warn("InterruptedException while thread sleep.");
                    }
                } else {
                    logger.error("Error while initializing RMI CRUD. Exceeded oxRMIRetryCount "
                            + maxRetryCount);
                    throw new ApplicationException(
                            ErrorCode.RMI_CONNECTION_ERROR.name(),
                            "Failed to connect to OX");
                }
            }
        } while (retryCount <= maxRetryCount);
    }
    
    /**
     * getNewContextInterface - creates a new socket and sets the contextInterface
     * 
     */
    private void getNewContextInterface() throws Exception {
        try {
            if (socketFactory == null) {
                socketFactory = new RMIClientSocketFactoryWithTimeout(
                        connectionTimeout, readTimeout);
            }
            socketFactory.createSocket(host, port);
            if (registry == null) {
                registry = LocateRegistry.getRegistry(host, port, socketFactory);
            }
            contextInterface = (OXContextInterface) registry
                    .lookup(OXContextInterface.RMI_NAME);
        } catch (Exception e) {
            logger.warn("Exception received while creating RMI socket: "
                    + e.getMessage());
            throw e;
        }
    }

    public void close() {
        // do nothing as RMI remote object will be
        // destroyed by DGC after its lease expires.
    }

    @Override
    public void commit() throws ApplicationException {
        // not supported
    }

    @Override
    public void createMailbox(MxOSRequestState mxosRequestState)
            throws MxOSException {

        IBackendState backendState = mxosRequestState.getBackendState().get(
                MailboxDBTypes.ldap.name());
        final Attributes attrs = ((LDAPBackendState) backendState).attributes;

        final String email = mxosRequestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        String name = null;
        String firstName = null;
        String lastName = null;
        String password = null;
        Long quota = null;
        Integer oxMaxFileUploadSize = null;
        String variant = null;
        String locale = null;
        String timeZone = null;
        String oxImapUrl = null;
        String oxSmtpUrl = null;
        String mailboxId = null;
        String oxSetMaxFileUploadSize = null;
        Boolean enableSpamFilter = null;
        try {
            name = (String) attrs.get(LDAPMailboxProperty.maillogin.name())
                    .get();
            password = (String) attrs.get(
                    LDAPMailboxProperty.mailpassword.name()).get();
            if (attrs.get(LDAPMailboxProperty.mailboxid.name()) != null) {
                mailboxId = String.valueOf(attrs.get(
                        LDAPMailboxProperty.mailboxid.name()).get());
            }
            firstName = (String) attrs.get(LDAPMailboxProperty.cn.name()).get();
            lastName = (String) attrs.get(LDAPMailboxProperty.sn.name()).get();
            // Read quota
            try {
                String quotaString = System
                        .getProperty(SystemProperty.oxMaxQuota.name());
                quota = Long.parseLong(quotaString);
            } catch (NumberFormatException nfe) {
                logger.warn(new StringBuilder("Error parsing ")
                        .append(SystemProperty.oxMaxQuota)
                        .append(" property, using default value [")
                        .append(DEFAULT_OX_MAX_QUOTA).append("]"));
                quota = DEFAULT_OX_MAX_QUOTA;
            }
            logger.debug("oxMaxQuota = " + quota);
            // Read oxSetMaxFileUploadSize
            try {
                oxSetMaxFileUploadSize = System.getProperty(
                        SystemProperty.oxSetMaxFileUploadSize.name(), "false");
            } catch (NumberFormatException e) {
                logger.warn("Invalid oxMaxFileUploadSize provided, taking default value");
                oxSetMaxFileUploadSize = "false";
            }
            logger.debug("oxSetMaxFileUploadSize = " + oxSetMaxFileUploadSize);
            // Read oxMaxFileUploadSize
            try {
                oxMaxFileUploadSize = Integer.parseInt(System.getProperty(
                        SystemProperty.oxMaxFileUploadSize.name(), "10"));
            } catch (NumberFormatException e) {
                logger.warn("Invalid oxMaxFileUploadSize provided, taking default value");
                oxMaxFileUploadSize = 10;
            }
            logger.debug("oxMaxFileUploadSize = " + oxMaxFileUploadSize);
            // read variant
            if (attrs.get(LDAPMailboxProperty.netmailvariant.name()) != null) {
                variant = (String) attrs.get(
                        LDAPMailboxProperty.netmailvariant.name()).get();
            }
            logger.debug("netmailvariant = " + variant);
            // read locale
            if (attrs.get(LDAPMailboxProperty.netmaillocale.name()) != null) {
                locale = (String) attrs.get(
                        LDAPMailboxProperty.netmaillocale.name()).get();
            } else {
                locale = System.getProperty(
                        SystemProperty.oxDefaultLocale.name(),
                        DEFAULT_OX_LOCALE);
            }
            logger.debug("locale = " + locale);
            // read timeZone
            if (attrs.get(LDAPMailboxProperty.msgtimezone.name()) != null) {
                timeZone = (String) attrs.get(
                        LDAPMailboxProperty.msgtimezone.name()).get();
            } else {
                timeZone = System.getProperty(
                        SystemProperty.oxDefaultTimezone.name(),
                        DEFAULT_OX_TIME_ZONE);
            }
            logger.debug("timeZone = " + timeZone);
            // read URLs
            oxImapUrl = System.getProperty(SystemProperty.oxImapUrl.name(),
                    DEFAULT_IMAP_MOS_URL);
            logger.debug("oxImapUrl = " + oxImapUrl);
            oxSmtpUrl = System.getProperty(SystemProperty.oxSmtpUrl.name(),
                    DEFAULT_SMTP_MOS_URL);
            logger.debug("oxSmtpUrl = " + oxSmtpUrl);
            enableSpamFilter = Boolean.parseBoolean(System.getProperty(
                    SystemProperty.oxSpamFilterEnabled.name(),
                    MxOSConstants.TRUE));
            logger.debug("enableSpamFilter = " + enableSpamFilter);
        } catch (NamingException e) {
            logger.warn("Invalid Data provided", e);
            throw new InvalidRequestException(ErrorCode.GEN_BAD_REQUEST.name(),
                    e);
        }
        Context context = new Context();
        context.setName(name);
        context.addLoginMapping(mailboxId);
        context.setMaxQuota(quota);
        
        String filestoreId = System.getProperty(SystemProperty.oxFilestoreId
                .name());
        if (null != filestoreId && !filestoreId.equals("")) {
            String[] fileStoreList = filestoreId.split(",");
            Random random = new Random();
            context.setFilestoreId(Integer
                    .valueOf(fileStoreList[random.nextInt(fileStoreList.length)]));
        }
        logger.debug("filestoreId = " + filestoreId);
        
      
        final boolean oxSSOEnabled = Boolean.parseBoolean(System.getProperty(
                SystemProperty.oxSSOEnabled.name(), MxOSConstants.FALSE));
        logger.debug("oxSSOEnabled = " + oxSSOEnabled);

        User user = new User();
        user.setName(name);
        if (oxSSOEnabled) {
            // if SSO Enabled at OX
            user.setDisplay_name(name);
            firstName = System.getProperty(
                    SystemProperty.oxDefaultFirstName.name(),
                    DEFAULT_OX_FIRST_NAME);
            logger.debug("firstName = " + firstName);
            
            user.setGiven_name(firstName);
            lastName = System.getProperty(
                    SystemProperty.oxDefaultLastName.name(),
                    DEFAULT_OX_LAST_NAME);
            logger.debug("lastName = " + lastName);
            user.setGiven_name(firstName);
            user.setSur_name(lastName);
            user.setCellular_telephone1(name);
        } else {
            user.setDisplay_name(new StringBuilder(firstName)
                    .append(MxOSConstants.SPACE).append(lastName).toString());
            user.setGiven_name(firstName);
            user.setSur_name(lastName);
        }
        //password is always set as mailLogin
        user.setPassword(name);
        user.setPrimaryEmail(email);
        user.setEmail1(email);
        user.setImapLogin(name);
        user.setUserAttribute("owm", "userSettingStatus", "retry");

        if (Boolean.parseBoolean(oxSetMaxFileUploadSize)) {
            user.setUploadFileSizeLimit(oxMaxFileUploadSize);
        }

        final boolean oxPLMNAddressEnabled = Boolean
                .parseBoolean(System.getProperty(
                        SystemProperty.oxPLMNAddressEnabled.name(), "true"));
        logger.debug("oxPLMNAddressEnabled = " + oxPLMNAddressEnabled);
        if (oxPLMNAddressEnabled) {
            if (logger.isDebugEnabled())
                logger.debug("Defalut Sender Address conversion for:" + name);
            try {
                Long.parseLong(name);
                if (logger.isDebugEnabled())
                    logger.debug("Converted Defalut Sender Address is:<" + name
                            + "/TYPE=PLMN>");
                user.setDefaultSenderAddress(name + "/TYPE=PLMN");
                final HashSet<String> aliases = new HashSet<String>();
                aliases.add(name);
                aliases.add(name + "/TYPE=PLMN");
                user.setAliases(aliases);
            } catch (NumberFormatException e) {
                logger.warn("Mail Login is not numeric so not changing defaultSenderAddress: "
                        + name);
            }
        }
        logger.info(" Inside mOS Authenticator: getUser, for user:" + name);
        user.setLanguage(locale);
        // LDAP msgtimezone attribute is always in POSIX format
        // TimeZones calculations not required
        user.setTimezone(timeZone);

        user.setImapServer(oxImapUrl);
        user.setSmtpServer(oxSmtpUrl);
        user.setGui_spam_filter_enabled(enableSpamFilter);

        final Credentials auth = new Credentials();
        auth.setLogin(System.getProperty(SystemProperty.oxAdminMaster.name()));
        auth.setPassword(System
                .getProperty(SystemProperty.oxAdminMasterPassword.name()));

        final String oxUserAccess = System.getProperty(
                SystemProperty.oxUserAccess.name(), DEFAULT_OX_USER_ACCESS);
        logger.debug("oxUserAccess = " + oxUserAccess);

        int retryCount = 0;
        do {
            try {
                if (contextInterface == null) {
                    getNewContextInterface();
                }
                contextInterface.create(context, user, oxUserAccess, auth);
                logger.info("Create Context success in OX : " + name);
                break;
            } catch (InvalidCredentialsException e) {
                logger.warn("Invalid admin credentials", e);
                throw new InvalidRequestException(
                        MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
            } catch (InvalidDataException e) {
                logger.warn("Invalid Data provided", e);
                throw new InvalidRequestException(
                        MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
            } catch (ContextExistsException e) {
                logger.warn("Mailbox already exists", e);
                throw new InvalidRequestException(
                        MailboxError.MBX_ALREADY_EXISTS.name(), e);
            } catch (Exception e) {
                contextInterface = null;
                retryCount++;
                if (retryCount <= maxRetryCount) {
                    logger.info("Retrying to createMailbox: " + retryCount);
                    try {
                        Thread.sleep(retryInterval);
                    } catch (InterruptedException e1) {
                        logger.warn("InterruptedException during thread sleep",
                                e1);
                    }
                } else {
                    logger.error("Failed to create context in OX. Retried for "
                            + maxRetryCount + " times... giving up!");
                    throw new ApplicationException(
                            MailboxError.MBX_UNABLE_TO_CREATE.name(),
                            "Failed to create context in OX");
                }
            }
        } while (retryCount <= maxRetryCount);
    }
    
    private String getTheme(String variant) {
        String theme = "";
        // TODO May be some scope for improvement in the logic below
        // For now the objective is to have some value in user's theme
        String themeDef = System
                .getProperty(SystemProperty.oxOpenwaveVariantPrefix.name()
                        + "." + variant);
        if (themeDef != null) {
            String[] themeParts = themeDef.split(",");
            if (themeParts != null && themeParts.length == 2) {
                theme = "\"theme\":{\"name\":\"" + themeParts[0]
                        + "\",\"path\":\"" + themeParts[1] + "\"}";
            }
        }
        return theme;
    }

    @Override
    public void deleteMailbox(MxOSRequestState mxosRequestState)
            throws MxOSException {

        final String email = mxosRequestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        // Optional params
        String userNameAsEmail = System
                .getProperty(SystemProperty.storeUserNameAsEmail.name());

        final String[] token = email.split(MxOSConstants.AT_THE_RATE);
        final String userName = ActionUtils.getUserName(email);
        final String domain = token[1];
        if (!ActionUtils.isDomainValid(email, domain)) {
            throw new InvalidRequestException(
                    DomainError.DMN_INVALID_NAME.name());
        }

        final Credentials auth = new Credentials();
        auth.setLogin(System.getProperty(SystemProperty.oxAdminMaster.name()));
        auth.setPassword(System
                .getProperty(SystemProperty.oxAdminMasterPassword.name()));

        int retryCount = 0;
        do {
            try {
                if (contextInterface == null) {
                    getNewContextInterface();
                }
                Context[] context = contextInterface.list(userName, auth);
                if (context.length == 1) {
                    contextInterface.delete(context[0], auth);
                    logger.info("Delete Context success in OX : " + email);
                } else {
                    logger.warn("None OR Multiple context found in OX, delete aborted.");
                }
                break;
            } catch (InvalidCredentialsException e) {
                logger.warn("Invalid admin credentials", e);
                throw new InvalidRequestException(
                        MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
            } catch (InvalidDataException e) {
                logger.warn("Invalid Data provided", e);
                throw new InvalidRequestException(
                        MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
            } catch (NoSuchContextException e) {
                logger.warn("Context does not exist", e);
                throw new InvalidRequestException(
                        MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
            } catch (Exception e) {
                contextInterface = null;
                retryCount++;
                if (retryCount <= maxRetryCount) {
                    logger.info("Retrying to deleteMailbox: " + retryCount);
                    try {
                        Thread.sleep(retryInterval);
                    } catch (InterruptedException e1) {
                        logger.warn("InterruptedException during thread sleep",
                                e1);
                    }
                } else {
                    logger.error("Failed to delete Context in OX. Retried for "
                            + maxRetryCount + " times... giving up!");
                    throw new ApplicationException(
                            MailboxError.MBX_UNABLE_TO_DELETE.name(),
                            "Failed to delete context in OX");
                }
            }
        } while (retryCount <= maxRetryCount);
    }

    @Override
    public void readMailbox(MxOSRequestState mxOSRequestStatus, Base base)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void rollback() throws ApplicationException {
        // not supported
    }

    @Override
    public void updateMailbox(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub
    }
}

class RMIClientSocketFactoryWithTimeout implements RMIClientSocketFactory,
        Serializable {
    /**
     * Generated serial id
     */
    private static final long serialVersionUID = -7715892777748778406L;
    private int connectionTimeout;
    private int readTimeout;
    private Socket s;

    public RMIClientSocketFactoryWithTimeout(int connectionTimeout,
            int readTimeout) {
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException {
        s = new Socket();
        s.connect(new InetSocketAddress(host, port), connectionTimeout);
        s.setSoTimeout(readTimeout);
        return s;
    }
}
