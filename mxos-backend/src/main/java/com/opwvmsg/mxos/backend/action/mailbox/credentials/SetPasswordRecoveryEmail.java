/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.credentials;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update password.
 * 
 * @author mxos-dev
 */
public class SetPasswordRecoveryEmail implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetPasswordRecoveryEmail.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("SetPasswordRecoveryEmail action start.");
        }
        String passRecoveryEmail = requestState.getInputParams()
                .get(MailboxProperty.passwordRecoveryEmail.name()).get(0);
        
        String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        // if passRecoveryEmail is same as the primary email address,throw
        // MBX_INVALID_PASSWORD_RECOVERY_EMAIL error code
        if (null != passRecoveryEmail
                && passRecoveryEmail.equalsIgnoreCase(email)) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_PASSWORD_RECOVERY_EMAIL.name());
        }

        /*
         * passRecoveryEmail should not use the primary email address or alias
         * email address. so we search dir server for mailbox base with the
         * passRecoveryEmail, if we can search this mailbox base and the
         * searched base's email equal the entered mail address, throw
         * MBX_INVALID_PASSWORD_RECOVERY_EMAIL error code
         */
       
        if (null != passRecoveryEmail) {
            Base recoveryBase = null;
            try {

                recoveryBase = ActionUtils
                        .getUserInfoFromLDAP(passRecoveryEmail);

            } catch (MxOSException e) {
                if (logger.isDebugEnabled()) {
                    logger.debug("SetPasswordRecoveryEmail: can't find the passRecoveryEmail from dir server");
                }
            }
            if (null != recoveryBase) {

                Base primaryBase = null;
                
                /**
                 * When create a new mailbox ,the email address does not exits,
                 * so we do nothing in the catch block <br>
                 * When update a mailbox credential, we will search the mailbox's
                 * Base object, then compare the Base's email address with
                 * password recovery email address
                 */

                try {

                    primaryBase = ActionUtils.getUserInfoFromLDAP(email);

                } catch (MxOSException e) {
                    logger.info("SetPasswordRecoveryEmail: can't find the email from dir server");
                }

                if (primaryBase != null && recoveryBase.getEmail().equalsIgnoreCase(
                        primaryBase.getEmail())) {

                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_PASSWORD_RECOVERY_EMAIL
                                    .name());
                }

            }

        }

             
        if (passRecoveryEmail != null) {
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.passwordRecoveryEmail,
                            passRecoveryEmail);
        } else {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_PASSWORD_RECOVERY_EMAIL.name());
        }
        if (logger.isDebugEnabled()) {
            logger.debug("SetPasswordRecoveryEmail action end.");
        }
    }
}
