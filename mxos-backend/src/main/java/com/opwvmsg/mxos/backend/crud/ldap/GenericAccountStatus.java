/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.HashMap;
import java.util.Map;

import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;

/**
 * Class for AccountStatus.
 *
 * @author mxos-dev
 */
public enum GenericAccountStatus {

    active("A"), maintenance("M"), suspended("S"), locked("L"),
    deleted("D"), proxy("P");

    private static final Map<String, GenericAccountStatus> ACCOUNT_STATUS_MAP =
        new HashMap<String, GenericAccountStatus>();

    private String key;

    static {
        ACCOUNT_STATUS_MAP.put("A", active);
        ACCOUNT_STATUS_MAP.put("M", maintenance);
        ACCOUNT_STATUS_MAP.put("S", suspended);
        ACCOUNT_STATUS_MAP.put("L", locked);
        ACCOUNT_STATUS_MAP.put("D", deleted);
        ACCOUNT_STATUS_MAP.put("P", proxy);
    }

    /**
     * Default Constructor.
     *
     * @param key key
     */
    private GenericAccountStatus(final String key) {
        this.key = key;
    }

    /**
     * Method to get Enum of given short code (key).
     *
     * @param key - short code
     * @return AccountStatus - enum
     * @throws InvalidDataException - if enum not found
     */
    public static GenericAccountStatus getEnumByKey(final String key)
            throws InvalidRequestException {
        if (key == null) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_MAILBOX_STATUS.name(),
                    "Invalid account status:" + key);
        } else {
            if (ACCOUNT_STATUS_MAP.get(key) != null) {
                return ACCOUNT_STATUS_MAP.get(key);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_MAILBOX_STATUS.name(),
                        "Unsupported account status:" + key);
            }
        }
    }

    /**
     * Method to get Enum from given value.
     *
     * @param value - value
     * @return AccountStatus - enum
     * @throws InvalidDataException - if enum not found
     */
    public static GenericAccountStatus getEnumByValue(final String value)
            throws InvalidRequestException {
        if (value == null) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_MAILBOX_STATUS.name(),
                    "Invalid account status:" + value);
        }
        for (GenericAccountStatus status: values()) {
            if (status.name().equalsIgnoreCase(value)) {
                return status;
            }
        }
        throw new InvalidRequestException(
                MailboxError.MBX_INVALID_MAILBOX_STATUS.name(),
                "Unsupported account status type:" + value);
    }

    /**
     * Method to get key as String.
     *
     * @return the key - short code
     */
    public String getKey() {
        return key;
    }
}
