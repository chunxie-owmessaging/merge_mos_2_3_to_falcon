/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.text.SimpleDateFormat;

import java.util.Date;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;

import com.opwvmsg.mxos.backend.crud.IMailboxHelper;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.DomainType;
import com.opwvmsg.mxos.data.enums.MxosEnums.Status;
import com.opwvmsg.mxos.data.enums.MailboxDBTypes;
import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.utils.config.DeploymentMode;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

public class LDAPMailboxHelper implements IMailboxHelper {

    @Override
    public void setAttribute(final MxOSRequestState mxosRequestState,
            final String key, final String value)
            throws InvalidRequestException {
        getAttributes(mxosRequestState).put(key, value);
    }

    @Override
    public void setAttribute(final MxOSRequestState mxosRequestState,
            final DomainProperty key, final Object value)
            throws InvalidRequestException {
        if (key == DomainProperty.type) {
            final String dbDomainType = LDAPDomainType
                    .toLdap((DomainType) value);
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Domain.get(key).name(), dbDomainType);
        } else {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Domain.get(key).name(), value);
        }
    }

    @Override
    public void setAttribute(MxOSRequestState mxosRequestState,
            DomainProperty key, String[] values) throws InvalidRequestException {
        BasicAttribute attr = new BasicAttribute(LDAPUtils.JSONToLDAP_Domain
                .get(key).name());
        for (String value : values) {
            attr.add(value);
        }
        getAttributes(mxosRequestState).put(attr);
    }

    @Override
    public void setAttribute(final MxOSRequestState mxosRequestState,
            final DataMap.Property key, final Object value)
            throws InvalidRequestException {
        String temp = null;
        if (key == MailboxProperty.cosId) {
            // TODO: hack for cosId.
            final String ldapCosId = LDAPUtils.getLDAPCosId((String) value);
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Mailbox.get(key).name(), ldapCosId);
        } else if (key == MailboxProperty.passwordStoreType) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Mailbox.get(key).name(),
                    PasswordType.getPasswordTypeKey((String) value));
        } else if (key == MailboxProperty.autoReplyMode) {
            if (value != null && !value.equals("")) {
                getAttributes(mxosRequestState).put(
                        LDAPUtils.JSONToLDAP_Mailbox.get(key).name(),
                        AutoReplyMode.getAutoReplyModeKey((String) value));
            } else {
                getAttributes(mxosRequestState).put(
                        LDAPUtils.JSONToLDAP_Mailbox.get(key).name(), value);
            }
        } else if (key == MailboxProperty.email) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Mailbox.get(key).name(), value);
            /*
             * TODO: This is hack for Cn and Sn. Cn is Common name and Sn is
             * Surname. Need to check whether they are required or not. At
             * present made them similar to imldapsh (EmailMx) perl script. If
             * this is something independent to LDAP then move below two
             * attributes to Action.
             */
            getAttributes(mxosRequestState).put(LDAPConstants.CN, value);
            getAttributes(mxosRequestState).put(LDAPConstants.SN, value);
        } else if (key == MailboxProperty.status) {
            // for account status.
            final Status s = Status.fromValue((String) value);
            DeploymentMode mode = MxOSConfig.getDeploymentMode();
            GenericAccountStatus ldapStatus = null;
            if (DeploymentMode.generic.equals(mode)) {
                ldapStatus = getGenericStatus(s);
            }
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Mailbox.get(key).name(),
                    ldapStatus.getKey());
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.mailStatusChangeDate.name(),
                    LDAPUtils.getLdapDate(null));
        } else if (key == MailboxProperty.lastStatusChangeDate) {
            // for account status.
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.mailStatusChangeDate.name(),
                    LDAPUtils.getLdapDate((String) value));
        } else if (key == MailboxProperty.lastSuccessfulLoginDate) {
            if (null != value && !"".equals(value)) {
                getAttributes(mxosRequestState).put(
                        LDAPMailboxProperty.mailLastSuccessfulLoginDate.name(),
                        LDAPUtils.getLdapDate((String) value));
            }
        } else if (key == MailboxProperty.notificationMailSentStatus) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Mailbox.get(key).name(),
                    MxosEnums.NotificationMailSentStatus.fromValue(
                            value.toString()).ordinal()
                            + "");
        } else if (key == MailboxProperty.lastLoginAttemptDate) {
            if (value != null) {
                getAttributes(mxosRequestState).put(
                        LDAPMailboxProperty.mailLastLoginAttemptDate.name(),
                        LDAPUtils.getLdapDate((String) value));
            } else {
                String currentLDAPDate = new SimpleDateFormat(
                        System.getProperty(SystemProperty.userDateFormat.name()))
                        .format(new Date(System.currentTimeMillis()));
                getAttributes(mxosRequestState).put(
                        LDAPMailboxProperty.mailLastLoginAttemptDate.name(),
                        LDAPUtils.getLdapDate(currentLDAPDate));
            }
        } else if (key == MailboxProperty.msisdnStatus) {
            if (value != null && ((String) value).equalsIgnoreCase("activated")) {
                getAttributes(mxosRequestState).put(
                        LDAPUtils.JSONToLDAP_Mailbox.get(key).name(), "1");
            } else {
                getAttributes(mxosRequestState).put(
                        LDAPUtils.JSONToLDAP_Mailbox.get(key).name(), "0");
            }
            // for account msisdn status change date.
            getAttributes(mxosRequestState)
                    .put(LDAPMailboxProperty.smsCredentialMSISDNStatusChangeDate
                            .name(), LDAPUtils.getLdapDate(null));
        } else if (key == MailboxProperty.lastMsisdnStatusChangeDate) {
            // for account msisdn status change date.
            getAttributes(mxosRequestState)
                    .put(LDAPMailboxProperty.smsCredentialMSISDNStatusChangeDate
                            .name(), LDAPUtils.getLdapDate((String) value));
        } else if (key == MailboxProperty.locale) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Mailbox.get(key).name(), value);
            getAttributes(mxosRequestState).put( 
                    LDAPMailboxProperty.msglocale.name(), value);
        } else if (key == MailboxProperty.passwordRecoveryMsisdnStatus) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Mailbox.get(key).name(), value);
            getAttributes(mxosRequestState)
                    .put(LDAPMailboxProperty.smspasswordrecoverymsisdnstatuschangedate
                            .name(),
                            new SimpleDateFormat(MxOSConstants.LDAP_DATE_FORMAT)
                                    .format(new Date()));
        } else if (key == MailboxProperty.lastPasswordRecoveryMsisdnStatusChangeDate) {
            String ldapDateFormat = new SimpleDateFormat(
                    MxOSConstants.LDAP_DATE_FORMAT).format(value);
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Mailbox.get(key).name(),
                    ldapDateFormat);
        } else if (key == MailboxProperty.spamAction) {
            // TODO: hack for spamAction.
            String operetionName = mxosRequestState.getServiceName().name();
            if (operetionName
                    .equalsIgnoreCase(ServiceEnum.MailReceiptBMIFiltersService
                            .name())) {
                getAttributes(mxosRequestState)
                        .put(LDAPMailboxProperty.mailbmimailwallspamactionverb
                                .name(),
                                value);

            } else if (operetionName
                    .equalsIgnoreCase(ServiceEnum.MailReceiptCommtouchFiltersService
                            .name())) {
                getAttributes(mxosRequestState)
                        .put(LDAPMailboxProperty.mailcommtouchinboundspamaction
                                .name(),
                                value);
            } else if (operetionName
                    .equalsIgnoreCase(ServiceEnum.MailReceiptCloudmarkFiltersService
                            .name())) {
                getAttributes(mxosRequestState).put(
                        LDAPMailboxProperty.asfdeliveryactiondirty.name(),
                        value);
            }
        } else if (key == MailboxProperty.virusAction) {
            // TODO: hack for spamAction.
            String operetionName = mxosRequestState.getServiceName().name();
            if (operetionName
                    .equalsIgnoreCase(ServiceEnum.MailReceiptCommtouchFiltersService
                            .name())) {
                getAttributes(mxosRequestState)
                        .put(LDAPMailboxProperty.mailcommtouchinboundvirusaction
                                .name(), value);

            } else if (operetionName
                    .equalsIgnoreCase(ServiceEnum.MailReceiptMcAfeeFiltersService
                            .name())) {
                getAttributes(mxosRequestState)
                        .put(LDAPMailboxProperty.mailmcafeeinboundvirusaction
                                .name(),
                                value);
            }
        } else if (key == MailboxProperty.smsServicesMsisdnStatus) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Mailbox.get(key).name(), value);
            // Need to update smsServicesMSISDNStatusChangeDate when there is a
            // change in status
            getAttributes(mxosRequestState)
                    .put(LDAPMailboxProperty.smsServicesMSISDNStatusChangeDate
                            .name(),
                            LDAPUtils.getLdapDate(null));
        } else if (key == MailboxProperty.lastSmsServicesMsisdnStatusChangeDate) {
            getAttributes(mxosRequestState)
                    .put(LDAPMailboxProperty.smsServicesMSISDNStatusChangeDate
                            .name(),
                            LDAPUtils.getLdapDate((String) value));
        } else if (key == MailboxProperty.lastFullFeaturesConnectionDate) {
            getAttributes(mxosRequestState)
                    .put(LDAPMailboxProperty.netmailLastFullFeaturesConnectionDate
                            .name(), LDAPUtils.getLdapDate((String) value, null));
        } else if (key == MailboxProperty.allowPasswordChange) {
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.mailwebmailchangepassword.name(), value);
            // WebMailFeatures.addressbook
        } else if (key == MailboxProperty.maxContacts) {
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.mailWebMailAddressBookLimit.name(), value);
        } else if (key == MailboxProperty.maxGroups) {
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.netmailAddressbookContactListMax.name(), value);
        } else if (key == MailboxProperty.maxContactsPerGroup) {
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.mailWebMailAddressBookListLimit.name(), value);
        } else if (key == MailboxProperty.maxContactsPerPage) {
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.netMailAbEntriesPerPage.name(), value);
        } else if (key == MailboxProperty.createContactsFromOutgoingEmails) {
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.netMailAutoCollect.name(), value);
        } else if (key == MailboxProperty.interManagerAccessEnabled) {
            temp = (String) value;
            if (temp.length() > 0) {
                temp = BooleanType.getByValue(temp).ordinal() + "";
            }
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.mailintermanager.name(), temp);
        } else if (key == MailboxProperty.interManagerSSLAccessEnabled) {
            temp = (String) value;
            if (temp.length() > 0) {
                temp = BooleanType.getByValue(temp).ordinal() + "";
            }
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.mailintermanagerssl.name(), temp);
        } else if (key == MailboxProperty.voiceMailAccessEnabled) {
            temp = (String) value;
            if (temp.length() > 0) {
                temp = BooleanType.getByValue(temp).ordinal() + "";
            }
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.netmailenablevoice.name(), temp);
        } else if (key == MailboxProperty.faxAccessEnabled) {
            temp = (String) value;
            if (temp.length() > 0) {
                temp = BooleanType.getByValue(temp).ordinal() + "";
            }
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.netmailenablefax.name(), temp);
        } else if (key == MailboxProperty.imapProxyAuthenticationEnabled) {
            temp = (String) value;
            if (temp.length() > 0) {
                temp = BooleanType.getByValue(temp).getBoolValue();
            }
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.imapproxyauthenticate.name(), temp);
        } else if (key == MailboxProperty.selfCareAccessEnabled) {
            temp = (String) value;
            if (temp.length() > 0) {
                temp = BooleanType.getByValue(temp).ordinal() + "";
            }
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.mailselfcare.name(), temp);
        } else if (key == MailboxProperty.selfCareSSLAccessEnabled) {
            temp = (String) value;
            if (temp.length() > 0) {
                temp = BooleanType.getByValue(temp).ordinal() + "";
            }
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.mailselfcaressl.name(), temp);
        } else if (key == MailboxProperty.remoteCallTracingEnabled) {
            temp = (String) value;
            if (temp.length() > 0) {
                temp = BooleanType.getByValue(temp).getBoolValue();
            }
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.remotecalltracingenabled.name(), temp);
        } else if (key == MailboxProperty.eventRecordingEnabled) {
            temp = (String) value;
            if (temp.length() > 0) {
                temp = EventRecordingType.getByValue(temp).getShortValue();
            }
            getAttributes(mxosRequestState).put(
                    LDAPMailboxProperty.mersenabled.name(), temp);
        } else if (key == MailboxProperty.numDelayedDeliveryMessagesPending) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Mailbox.get(key).name(), value);
        } else if (key == MailboxProperty.adminRejectInfo) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_GroupAdminMailbox.get(key).name(),
                    value);
        } else if (key == MailboxProperty.adminRejectAction) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_GroupAdminMailbox.get(key).name(),
                    value);
        } else if (key == MailboxProperty.adminDefaultDisposition) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_GroupAdminMailbox.get(key).name(),
                    value);
        } else if (key == MailboxProperty.maxAdminStorageSizeKB) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_GroupAdminMailbox.get(key).name(),
                    value);
        } else if (key == MailboxProperty.maxAdminUsers) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_GroupAdminMailbox.get(key).name(),
                    value);            
        } else if (key == MailboxProperty.maxNumForwardingAddresses) {
            getAttributes(mxosRequestState).put(
                    LDAPUtils.JSONToLDAP_Mailbox.get(key).name(),
                     value.toString()
                            + "");
        } else {
            if (LDAPUtils.JSONToLDAP_Mailbox.containsKey(key)) {
                getAttributes(mxosRequestState).put(
                        LDAPUtils.JSONToLDAP_Mailbox.get(key).name(), value);
            } else {
                getAttributes(mxosRequestState).put(key.name(), value);
            }
        }
    }

    @Override
    public void setAttribute(MxOSRequestState mxosRequestState,
            MailboxProperty key, String[] values)
            throws InvalidRequestException {
        if (key == MailboxProperty.numDelayedDeliveryMessagesPending) {
            values = LDAPUtils
                    .getFormattedDateArrayForDeliveryMessagesPending(values);
        } 
        BasicAttribute attr;
        if (key == MailboxProperty.adminApprovedSendersList
                || key == MailboxProperty.adminBlockedSendersList) {
            attr = new BasicAttribute(LDAPUtils.JSONToLDAP_GroupAdminMailbox
                    .get(key).name());
        } else {
            attr = new BasicAttribute(LDAPUtils.JSONToLDAP_Mailbox.get(key)
                    .name());
        }
        for (String value : values) {
            attr.add(value);
        }
        getAttributes(mxosRequestState).put(attr);
    }

    private Attributes getAttributes(MxOSRequestState requestState) {
        IBackendState backendState = requestState.getBackendState().get(
                MailboxDBTypes.ldap.name());
        if (backendState == null) {
            backendState = new LDAPBackendState();
            requestState.getBackendState().put(MailboxDBTypes.ldap.name(),
                    backendState);
        }
        return ((LDAPBackendState) backendState).attributes;
    }

    private static GenericAccountStatus getGenericStatus(Status s) {
        GenericAccountStatus ldapStatus = null;
        try {
            ldapStatus = GenericAccountStatus.getEnumByValue(s.name());
        } catch (InvalidRequestException e) {
            ldapStatus = GenericAccountStatus.proxy;
        }
        return ldapStatus;
    }

}
