/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to get MailSend object.
 * 
 * @author mxos-dev
 * 
 */
public class GetMailSend implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetMailSend.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetMailSend action start."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxSearchCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            final String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            final MailSend ms = mailboxCRUD.readMailSend(email);

            if (!ServiceEnum.MailSendDeliveryMessagesPendingService
                    .equals(requestState.getServiceName())
                    && !ServiceEnum.MailSendSignatureService
                            .equals(requestState.getServiceName())) {
                metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
                metaCRUD = metaCRUDPool.borrowObject();
                metaCRUD.readMailbox(requestState, null);
                DataMap dataMap = requestState.getAdditionalParams();
                if (dataMap != null) {
                    String signature = dataMap
                            .getProperty(MailboxProperty.signature);
                    if (logger.isDebugEnabled()) {
                        logger.debug(new StringBuffer(" Signature : ")
                                .append(signature));
                    }
                    if (signature != null && !signature.equals("")) {
                        ms.setSignature(signature);
                    }
                }
            }
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.mailSend, ms);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while get mail send.", e);
            throw new ApplicationException(
                    MailboxError.MBX_MAILSEND_UNABLE_TO_GET.name(), e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Provisioning CRUD pool:"
                                    + e.getMessage());
                }
            }
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Meta CRUD pool:" + e.getMessage());
                }
            }
        }

        // Read signatures from OX if backend is set to OX
        String appSuiteIntegrated = System
                .getProperty(SystemProperty.appSuiteIntegrated.name());
        if (appSuiteIntegrated != null
                && appSuiteIntegrated.trim().equalsIgnoreCase(
                        MxOSConstants.TRUE)) {
            if (MxOSConfig.getAddressBookBackend().equals(
                    AddressBookDBTypes.ox.name())) {
                ICRUDPool<ISettingsCRUD> oxSettingsCRUDPool = null;
                ISettingsCRUD oxSettingsCRUD = null;

                try {
                    // call http update methods
                    oxSettingsCRUDPool = MxOSApp.getInstance()
                            .getOXSettingsHttpCRUD();

                    oxSettingsCRUD = oxSettingsCRUDPool.borrowObject();
                    oxSettingsCRUD.readSetting(requestState);
                    List<Signature> signatures = (List<Signature>) requestState
                            .getDbPojoMap().getPropertyAsObject(
                                    MxOSPOJOs.allSignatures);
                    MailSend ms = (MailSend) requestState.getDbPojoMap()
                            .getPropertyAsObject(MxOSPOJOs.mailSend);
                    ms.setSignatures(signatures);
                    requestState.getDbPojoMap().setProperty(MxOSPOJOs.mailSend,
                            ms);
                    if (logger.isDebugEnabled()) {
                        logger.debug(new StringBuffer(
                                "UpdateMailbox Http update done..!"));
                    }
                } catch (final MxOSException e) {
                    throw e;
                } catch (Exception e) {
                    logger.error("Error while read signature in OX.", e);
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                } finally {
                    try {
                        if (oxSettingsCRUDPool != null
                                && oxSettingsCRUD != null) {
                            oxSettingsCRUDPool.returnObject(oxSettingsCRUD);
                        }
                    } catch (final MxOSException e) {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                    }
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetMailSend action end."));
        }
    }
}
