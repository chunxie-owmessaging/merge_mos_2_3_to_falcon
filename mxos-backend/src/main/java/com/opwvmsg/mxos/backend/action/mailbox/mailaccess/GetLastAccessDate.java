/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailaccess;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to get Last Access Date of the user.
 * 
 * @author mxos-dev
 * 
 */
public class GetLastAccessDate implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetLastAccessDate.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetLastAccessDate action start."));
        }
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        long lastLoginTime = -1;

        try {
            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();
            lastLoginTime = metaCRUD.getLastLoginTime(requestState);
            metaCRUD.commit();
        } catch (final MxOSException e) {
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while get last access date : ", e);
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_GET_LAST_ACCESS_DATE.name(), e);
        } finally {
            if (metaCRUDPool != null && metaCRUDPool != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Provisioning CRUD pool:"
                                    + e.getMessage());
                }
            }
        }

        if (lastLoginTime != -1) {
            InternalInfo internalInfo = (InternalInfo) requestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.internalInfo);
            internalInfo.setLastAccessDate(new SimpleDateFormat(System
                    .getProperty(SystemProperty.userDateFormat.name()))
                    .format(new Date(lastLoginTime * 1000)));
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetLastAccessDate action end."));
        }
    }
}
