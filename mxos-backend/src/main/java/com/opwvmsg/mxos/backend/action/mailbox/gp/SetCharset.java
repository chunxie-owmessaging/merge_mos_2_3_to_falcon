/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.gp;

import java.nio.charset.Charset;
import java.util.SortedMap;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set locale.
 * 
 * @author mxos-dev
 */
public class SetCharset implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetCharset.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetCharset action start."));
        }
        final String charset = requestState.getInputParams()
                .get(MailboxProperty.charset.name()).get(0);
        if (!validateCharset(charset)) {
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_CHARSET.name());
        }
        if (charset != null) {
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.charset,
                            charset);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetCharset action end."));
        }
    }

    private static boolean validateCharset(String charset) {
        SortedMap<String, Charset> charSetMap = Charset.availableCharsets();
        if (charSetMap.containsKey(charset)) {
            return true;
        }
        return false;
    }
}
