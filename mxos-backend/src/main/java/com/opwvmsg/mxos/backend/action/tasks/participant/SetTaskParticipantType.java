package com.opwvmsg.mxos.backend.action.tasks.participant;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.exception.MxOSException;

public class SetTaskParticipantType implements MxOSBaseAction{
	private static Logger logger = Logger.getLogger(SetTaskParticipantType.class);
	@Override
	public void run(MxOSRequestState requestState) throws MxOSException {
		// TODO Auto-generated method stub
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action start."));
		}

		int participantTypeCount = requestState.getInputParams()
		.get(TasksProperty.participantType.name()).size();

		StringBuilder participantTypes = new StringBuilder();
		for(int i=0;i<participantTypeCount; i++){
			participantTypes.append(requestState.getInputParams()
					.get(TasksProperty.participantType.name()).get(i));
			participantTypes.append(",");
		}
		
		MxOSApp.getInstance()
		.getTasksHelper()
		.setAttribute(requestState, TasksProperty.participantType,
				participantTypes.toString());
		
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action end."));
		}
	}
}
