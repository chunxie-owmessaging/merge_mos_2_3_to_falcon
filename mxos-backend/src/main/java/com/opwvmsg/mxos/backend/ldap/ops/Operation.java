/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.ldap.ops;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;

import org.apache.log4j.Logger;

/**
 * Abstract class for LDAP operations
 * 
 * @author mxos-dev
 * 
 */
public abstract class Operation {
    protected Logger logger = Logger.getLogger(getClass());

    protected DirContext ldapContext;

    // retry policy for one operation
    private boolean retryWhenOperationFailed = false;
    private List<String> retryExceptions = new ArrayList<String>();
    private int maxRetryCount = 0;
    private int retryInterval = 0;

    public void execute() throws NamingException {
        boolean success = true;
        int retryCount = 0;

        // Invoke operation
        do {
            try {
                if (!success) {
                    // sleep retryInterval time
                    try {
                        Thread.sleep(retryInterval);
                    } catch (Exception e) {
                        // do nothing
                    }
                    retryCount++;
                    logger.info("This is the " + retryCount
                            + "th time retry, remaining " + retryCount
                            + " times.");
                }

                logger.info("Start to invoke LDAP operation: "
                        + getOperationName());
                invoke();
                success = true;
                logger.info("LDAP operation " + getOperationName()
                        + " is invoked successfully.");
            } catch (NamingException e) {
                success = false;
                logger.error("Failed to execute LDAP operation: "
                        + getOperationName(), e);

                // decide if to retry this operation
                if (retryWhenOperationFailed) {
                    if (needToRetry(e) && retryCount < maxRetryCount) {
                        logger.info("Retry LDAP operation: "
                                + getOperationName() + "after " + retryInterval
                                + " ms");
                        continue;
                    } else {
                        throw e;
                    }
                } else {
                    throw e;
                }
            }
        } while (!success);
    }

    protected abstract void invoke() throws NamingException;

    public boolean needToRetry(NamingException e) {
        return false;
    }

    public String getOperationName() {
        return getClass().getSimpleName();
    }

    public DirContext getLdapContext() {
        return ldapContext;
    }

    public void setLdapContext(DirContext ldapContext) {
        this.ldapContext = ldapContext;
    }

    public boolean isRetryWhenOperationFailed() {
        return retryWhenOperationFailed;
    }

    public List<String> getRetryExceptions() {
        return retryExceptions;
    }

    public int getMaxRetryCount() {
        return maxRetryCount;
    }

    public int getRetryInterval() {
        return retryInterval;
    }

    public void retryWhenOperationFailed(List<String> retryExceptions,
            int maxRetryCount, int retryInterval) {
        this.retryWhenOperationFailed = true;
        this.retryExceptions = retryExceptions;
        this.maxRetryCount = maxRetryCount;
        this.retryInterval = retryInterval;
    }

}
