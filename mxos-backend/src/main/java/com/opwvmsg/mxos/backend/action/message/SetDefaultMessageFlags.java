/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.message;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.message.pojos.Flags;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.utils.paf.intermail.mail.Msg.MssFlagOrder;

/**
 * Action class to set default flags from Configuration
 * 
 * @author mxos-dev
 */
public class SetDefaultMessageFlags implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetDefaultMessageFlags.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("SetDefaultMessageFlags action start.");
        }

        try {
            String flagsStr = System.getProperty(SystemProperty.messageFlags
                    .name());

            Flags flags = new Flags();
            if (flagsStr == null)
                flagsStr = MxOSConstants.DEFAULT_MESSAGEFLAGS;
            else if ((flagsStr.length() != MssFlagOrder.values().length))
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(),
                        "Invalid Value configured for "
                                + SystemProperty.messageFlags.name());

            flags.setFlagRecent(getFlag(flagsStr, MssFlagOrder.flagrecent));
            flags.setFlagSeen(getFlag(flagsStr, MssFlagOrder.flagseen));
            flags.setFlagAns(getFlag(flagsStr, MssFlagOrder.flagans));
            flags.setFlagFlagged(getFlag(flagsStr, MssFlagOrder.flagflagged));
            flags.setFlagDraft(getFlag(flagsStr, MssFlagOrder.flagdraft));
            flags.setFlagDel(getFlag(flagsStr, MssFlagOrder.flagdel));
            flags.setFlagUnread(!getFlag(flagsStr, MssFlagOrder.flagseen));

            requestState.getDbPojoMap().setProperty(MxOSPOJOs.messageFlags,
                    flags);

            // We are reading "MxOSPOJOs.messageFlagsMap" property in all
            // SetFlagXXX actions.
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.messageFlagsMap,
                    new HashMap<String, Boolean>());
        } catch (final Exception e) {
            logger.error("Error while setting the default message flag."
                    + e.getMessage());
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_PERFORM_CREATE.name(),
                    e.getMessage());
        }
        if (logger.isDebugEnabled()) {
            logger.debug("SetDefaultMessageFlags action end.");
        }
    }

    public boolean getFlag(String strFlags, MssFlagOrder mssFlagOrder) {
        if (strFlags != null
                && ((strFlags.length() - 1) >= mssFlagOrder.ordinal())) {
            char flag = strFlags.charAt(mssFlagOrder.ordinal());
            return (flag == 'T' || flag == 't');
        } else {
            return false;
        }
    }
}
