/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.cos.info;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;

/**
 * Action class to get InternalInfo object using a COS.
 * 
 * @author mxos-dev
 * 
 */
public class GetCosInternalInfo implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetCosInternalInfo.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("GetCosInternalInfo action start.");
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxSearchCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            final String cosId = requestState.getInputParams()
                    .get(MailboxProperty.cosId.name()).get(0);
            final InternalInfo object = mailboxCRUD
                    .readCosInternalInfo(cosId);
            if (object == null) {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name(),
                        "Cos not found");
            }
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.internalInfo,
                    object);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while get cos internal info.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Provisioning CRUD pool:"
                                    + e.getMessage());
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("GetCosInternalInfo action end.");
        }
    }
}
