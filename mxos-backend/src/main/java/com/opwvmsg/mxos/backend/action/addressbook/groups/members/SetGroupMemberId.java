/*
/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.addressbook.groups.members;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set group member id.
 * 
 * @author mxos-dev
 */
public class SetGroupMemberId implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetGroupMemberId.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetGroupMemberId action start."));
        }

        if (mxosRequestState.getInputParams().containsKey(
                AddressBookProperty.memberId.name())) {
            if (mxosRequestState.getInputParams().containsKey(
                    AddressBookProperty.memberList.name())) {
                // Set Multiple Group Member Id
                Object[] memberNames = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.memberId.name()).toArray();

                MxOSApp.getInstance()
                        .getAddressBookHelper()
                        .setAttributes(mxosRequestState,
                                AddressBookProperty.memberId, memberNames);
            } else {
                // Set single Group Member Id
                String memberName = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.memberId.name()).get(0);

                MxOSApp.getInstance()
                        .getAddressBookHelper()
                        .setAttribute(mxosRequestState,
                                AddressBookProperty.memberId, memberName);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetGroupMemberId action end."));
        }
    }
}
