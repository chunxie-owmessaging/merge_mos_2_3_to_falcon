package com.opwvmsg.mxos.backend.crud.http;

import java.io.IOException;
import java.net.URI;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.log4j.Logger;

public class GenericHttpClient extends DefaultHttpClient {

    private static final Logger logger = Logger
            .getLogger(GenericHttpClient.class);
    
    /* instance variables */
    private final HttpPost httpPost = new HttpPost();
    private HttpResponse response =  null;
    
    /**
     * Sends HttpPost request towards given url with passed message in given 
     * content type format.
     * 
     * @param url - to be set in HttpPost url
     * @param message - data for post body
     * @param contentType - org.apache.http.entity.ContentType
     * @return org.apache.http.HttpResponse
     * @throws ClientProtocolException
     * @throws IOException
     */
    public HttpResponse executePost(URI url, String message,
            ContentType contentType)
            throws ClientProtocolException, IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("Executing HttpPost to " + url);
        }

        // Remove all the old headers.
        Header[] h1 = httpPost.getAllHeaders();
        for (int i = 0; i < h1.length; i++) {
            httpPost.removeHeaders(h1[i].getName());
        }

        httpPost.setURI(url);
        httpPost.addHeader(HTTP.CONTENT_TYPE, contentType.toString());

        /* preparing HttpPost with given message */
        final StringEntity se = new StringEntity(message, contentType);
        httpPost.setEntity(se);

        response = super.execute(httpPost);
        return response;
    }
    
    /**
     * closes the InputStream on the HttpConnection for making the same
     * connection reusable for next request.
     * 
     * @throws IllegalStateException
     * @throws IOException
     */
    void cleanup() throws IllegalStateException, IOException {
        if (response != null) {
            final HttpEntity en = response.getEntity();
            if (en != null) {
                en.getContent().close();
            }
            response = null;
        }
    }
    
}
