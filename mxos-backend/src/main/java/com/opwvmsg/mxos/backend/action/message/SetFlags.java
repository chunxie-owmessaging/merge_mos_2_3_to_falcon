/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.message;

import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.message.pojos.Flags;
import com.opwvmsg.utils.paf.intermail.mail.Msg.MssFlagOrder;

/**
 * Action class to construct the message flags based on the configuration and
 * input parameters.
 * 
 * @author mxos-dev
 */
public class SetFlags implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetFlags.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("SetFlags action start.");
        }
        Map<String, Boolean> flagsMap = (Map<String, Boolean>) requestState
                .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.messageFlagsMap);

        Flags flags = (Flags) requestState.getDbPojoMap().getPropertyAsObject(
                MxOSPOJOs.messageFlags);

        for (Map.Entry<String, Boolean> entry : flagsMap.entrySet()) {
            MssFlagOrder msgFlag = MssFlagOrder.valueOf(entry.getKey()
                    .toLowerCase());
            switch (msgFlag) {
            case flagrecent:
                flags.setFlagRecent(entry.getValue());
                break;
            case flagseen:
                flags.setFlagSeen(entry.getValue());
                break;
            case flagans:
                flags.setFlagAns(entry.getValue());
                break;
            case flagflagged:
                flags.setFlagFlagged(entry.getValue());
                break;
            case flagdraft:
                flags.setFlagDraft(entry.getValue());
                break;
            case flagdel:
                flags.setFlagDel(entry.getValue());
                break;
            }
        }

        requestState.getDbPojoMap().setProperty(MxOSPOJOs.messageFlags, flags);
        Flags flags1 = (Flags) requestState.getDbPojoMap().getPropertyAsObject(
                MxOSPOJOs.messageFlags);

        if (logger.isDebugEnabled()) {
            logger.debug("SetFlags action end.");
        }
    }
}
