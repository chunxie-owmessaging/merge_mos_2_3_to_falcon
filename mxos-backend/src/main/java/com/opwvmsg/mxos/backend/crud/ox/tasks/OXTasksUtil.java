package com.opwvmsg.mxos.backend.crud.ox.tasks;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.OXTasksProperty;
import com.opwvmsg.mxos.data.enums.TasksProperty;

public class OXTasksUtil {

    public static EnumMap<TasksProperty, OXTasksProperty> JSONToOX_Tasks = new EnumMap<TasksProperty, OXTasksProperty>(
            TasksProperty.class);
    public static EnumMap<TasksProperty, OXTasksProperty> JSONToOX_TasksFolders = new EnumMap<TasksProperty, OXTasksProperty>(
            TasksProperty.class);
    public static Map<String, String> Tasks_Id = new HashMap<String, String>();
    public static Map<String, String> Folders_Id = new HashMap<String, String>();

    static {
        JSONToOX_Tasks.put(TasksProperty.userId, OXTasksProperty.name);
        // task base property mapping
        JSONToOX_Tasks.put(TasksProperty.taskId, OXTasksProperty.id);
        JSONToOX_Tasks.put(TasksProperty.folderId, OXTasksProperty.folder);
        JSONToOX_Tasks.put(TasksProperty.folderId, OXTasksProperty.folder_id);
        JSONToOX_Tasks.put(TasksProperty.name, OXTasksProperty.title);
        JSONToOX_Tasks.put(TasksProperty.owner, OXTasksProperty.organizer);
        JSONToOX_Tasks.put(TasksProperty.priority, OXTasksProperty.priority);
        JSONToOX_Tasks.put(TasksProperty.status, OXTasksProperty.status);
        JSONToOX_Tasks.put(TasksProperty.name, OXTasksProperty.title);
        JSONToOX_Tasks.put(TasksProperty.isPrivate, OXTasksProperty.private_flag);
        JSONToOX_Tasks.put(TasksProperty.colorLabel, OXTasksProperty.color_label);
        JSONToOX_Tasks.put(TasksProperty.categories, OXTasksProperty.categories);
        JSONToOX_Tasks.put(TasksProperty.notes, OXTasksProperty.note);
        JSONToOX_Tasks.put(TasksProperty.createdDate, OXTasksProperty.creation_date);
        JSONToOX_Tasks.put(TasksProperty.updatedDate, OXTasksProperty.last_modified);
        JSONToOX_Tasks.put(TasksProperty.startDate, OXTasksProperty.start_date);
        JSONToOX_Tasks.put(TasksProperty.dueDate, OXTasksProperty.end_date);
        JSONToOX_Tasks.put(TasksProperty.completedDate, OXTasksProperty.date_completed);
        JSONToOX_Tasks.put(TasksProperty.reminderDate, OXTasksProperty.alarm);
        JSONToOX_Tasks.put(TasksProperty.progress, OXTasksProperty.percent_completed);
        
        // task recurrence property mapping
        JSONToOX_Tasks.put(TasksProperty.type, OXTasksProperty.recurrence_type);
        JSONToOX_Tasks.put(TasksProperty.daysInWeek, OXTasksProperty.days);
        JSONToOX_Tasks.put(TasksProperty.dayInMonth, OXTasksProperty.day_in_month);
        JSONToOX_Tasks.put(TasksProperty.monthInYear, OXTasksProperty.month);
        JSONToOX_Tasks.put(TasksProperty.recurAfterInterval, OXTasksProperty.interval);
        JSONToOX_Tasks.put(TasksProperty.endDate, OXTasksProperty.until);
        JSONToOX_Tasks.put(TasksProperty.notification, OXTasksProperty.notification);
        JSONToOX_Tasks.put(TasksProperty.occurrences, OXTasksProperty.occurrences);
        
        // task participant property mapping
        JSONToOX_Tasks.put(TasksProperty.participantId, OXTasksProperty.userId);
        JSONToOX_Tasks.put(TasksProperty.participantType, OXTasksProperty.type);
        JSONToOX_Tasks.put(TasksProperty.participantName, OXTasksProperty.display_name);
        JSONToOX_Tasks.put(TasksProperty.participantEmail, OXTasksProperty.mail);
        JSONToOX_Tasks.put(TasksProperty.participantStatus, OXTasksProperty.confirmation);
        JSONToOX_Tasks.put(TasksProperty.participantMessage, OXTasksProperty.confirm_message);
        
        // task details property mapping
        JSONToOX_Tasks.put(TasksProperty.actualCost, OXTasksProperty.actual_costs);
        JSONToOX_Tasks.put(TasksProperty.actualWork, OXTasksProperty.actual_duration);
        JSONToOX_Tasks.put(TasksProperty.billing, OXTasksProperty.billing_information);
        JSONToOX_Tasks.put(TasksProperty.targetCost, OXTasksProperty.target_costs);
        JSONToOX_Tasks.put(TasksProperty.targetWork, OXTasksProperty.target_duration);
        JSONToOX_Tasks.put(TasksProperty.currency, OXTasksProperty.currency);
        JSONToOX_Tasks.put(TasksProperty.mileage, OXTasksProperty.trip_meter);
        JSONToOX_Tasks.put(TasksProperty.companies, OXTasksProperty.companies);
        
        JSONToOX_Tasks.put(TasksProperty.sortKey, OXTasksProperty.sort);
        JSONToOX_Tasks.put(TasksProperty.sortOrder, OXTasksProperty.order);
        JSONToOX_Tasks.put(TasksProperty.moveToFolderId, OXTasksProperty.folder);
        
        // tasks folder property mapping
        JSONToOX_TasksFolders.put(TasksProperty.userId, OXTasksProperty.name);
        JSONToOX_TasksFolders.put(TasksProperty.folderId, OXTasksProperty.folder_id);
        JSONToOX_TasksFolders.put(TasksProperty.folderName, OXTasksProperty.title);
        JSONToOX_TasksFolders.put(TasksProperty.module, OXTasksProperty.module);
        JSONToOX_TasksFolders.put(TasksProperty.type, OXTasksProperty.type);
        JSONToOX_TasksFolders.put(TasksProperty.haveSubFolders, OXTasksProperty.subFolders);
        JSONToOX_TasksFolders.put(TasksProperty.ownRights, OXTasksProperty.own_rights);
        JSONToOX_TasksFolders.put(TasksProperty.description, OXTasksProperty.summary);
        JSONToOX_TasksFolders.put(TasksProperty.isDefault, OXTasksProperty.standard_folder);
        JSONToOX_TasksFolders.put(TasksProperty.numTotalObjects, OXTasksProperty.total);
        JSONToOX_TasksFolders.put(TasksProperty.numNewObjects, OXTasksProperty.newObjects);
        JSONToOX_TasksFolders.put(TasksProperty.numUnreadObjects, OXTasksProperty.unread);
        JSONToOX_TasksFolders.put(TasksProperty.numDeletedObjects, OXTasksProperty.deleted);
        JSONToOX_TasksFolders.put(TasksProperty.capabilities, OXTasksProperty.capabilities);
        JSONToOX_TasksFolders.put(TasksProperty.standardFolderType, OXTasksProperty.standard_folder_type);
        JSONToOX_TasksFolders.put(TasksProperty.supportedCapabilities, OXTasksProperty.supported_capabilities);
        JSONToOX_TasksFolders.put(TasksProperty.permissiosns, OXTasksProperty.permissions);
        JSONToOX_TasksFolders.put(TasksProperty.subscriptionFlag, OXTasksProperty.subscriptionFlag);
        JSONToOX_TasksFolders.put(TasksProperty.publicationFlag, OXTasksProperty.publicationFlag);
        JSONToOX_TasksFolders.put(TasksProperty.hidenSubFoldersFlag, OXTasksProperty.subscr_subflds);
        JSONToOX_TasksFolders.put(TasksProperty.hidenFlag, OXTasksProperty.subscribed);
        

        // Task Base columns
        Tasks_Id.put(TasksProperty.taskId.name(), "1");
        Tasks_Id.put(TasksProperty.folderId.name(), "20");
        Tasks_Id.put(TasksProperty.name.name(), "200");
        Tasks_Id.put(TasksProperty.owner.name(), "224");
        Tasks_Id.put(TasksProperty.priority.name(), "309");
        Tasks_Id.put(TasksProperty.status.name(), "300");
        Tasks_Id.put(TasksProperty.isPrivate.name(), "101");
        Tasks_Id.put(TasksProperty.colorLabel.name(), "102");
        Tasks_Id.put(TasksProperty.categories.name(), "100");
        Tasks_Id.put(TasksProperty.notes.name(), "203");
        
        Tasks_Id.put(TasksProperty.createdDate.name(), "4");
        Tasks_Id.put(TasksProperty.updatedDate.name(), "5");

        Tasks_Id.put(TasksProperty.startDate.name(), "201");
        Tasks_Id.put(TasksProperty.dueDate.name(), "202");
        Tasks_Id.put(TasksProperty.completedDate.name(), "315");
        Tasks_Id.put(TasksProperty.reminderDate.name(), "204");
        Tasks_Id.put(TasksProperty.progress.name(), "301");
        
        // Task Folders columns
        Folders_Id.put(TasksProperty.folderId.name(), "20");
        Folders_Id.put(TasksProperty.name.name(), "300");
        Folders_Id.put(TasksProperty.module.name(), "301");
        Folders_Id.put(TasksProperty.type.name(), "302");
        Folders_Id.put(TasksProperty.haveSubFolders.name(), "304");
        Folders_Id.put(TasksProperty.rights.name(), "305");
        Folders_Id.put(TasksProperty.description.name(), "307");
        Folders_Id.put(TasksProperty.isDefault.name(), "308");
        Folders_Id.put(TasksProperty.numTotalObjects.name(), "309");
        
        Folders_Id.put(TasksProperty.numNewObjects.name(), "310");
        Folders_Id.put(TasksProperty.numUnreadObjects.name(), "311");

        Folders_Id.put(TasksProperty.numDeletedObjects.name(), "312");
        Folders_Id.put(TasksProperty.capabilities.name(), "313");
        Folders_Id.put(TasksProperty.standardFolderType.name(), "316");
        Folders_Id.put(TasksProperty.supportedCapabilities.name(), "317");
        
        Folders_Id.put(TasksProperty.permissiosns.name(), "312");
        Folders_Id.put(TasksProperty.hidenFlag.name(), "314");
        Folders_Id.put(TasksProperty.hidenSubFoldersFlag.name(), "315");
        Folders_Id.put(TasksProperty.subscriptionFlag.name(), "3010");
        Folders_Id.put(TasksProperty.publicationFlag.name(), "3020");
    }

    /**
     * Default private constructor.
     */
    private OXTasksUtil() {

    }
}
