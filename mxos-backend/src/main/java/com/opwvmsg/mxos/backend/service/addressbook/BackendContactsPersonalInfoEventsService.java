/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Event;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoEventsService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Contacts Personal Info Events service exposed to client which is responsible 
 * for doing basic activities e.g create, read, update, delete
 * 
 * @author mxos-dev
 */
public class BackendContactsPersonalInfoEventsService implements
        IContactsPersonalInfoEventsService {

    private static Logger logger = Logger
            .getLogger(BackendContactsPersonalInfoEventsService.class);

    /**
     * Default Constructor.
     */
    public BackendContactsPersonalInfoEventsService() {
        logger.info("BackendContactsPersonalInfoEventsService Service created...");
    }

    @Override
    public void create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.PUT);

            ActionUtils.setAddressBookBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.PUT, t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.PUT, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.DELETE, t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.DELETE, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.DELETEALL);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.DELETEALL, t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.DELETEALL, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public Event read(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.GET, t0, StatStatus.pass);

            return ((Event) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.contactsPersonalInfoEvents));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.GET, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public List<Event> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.GETALL);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.GETALL, t0, StatStatus.pass);

            return ((List<Event>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(
                            MxOSPOJOs.contactsPersonalInfoAllEvents));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.GETALL, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.POST);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.POST, t0, StatStatus.pass);

        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsPersonalInfoEventsService,
                    Operation.POST, t0, StatStatus.fail);
            throw e;
        }
    }

}
