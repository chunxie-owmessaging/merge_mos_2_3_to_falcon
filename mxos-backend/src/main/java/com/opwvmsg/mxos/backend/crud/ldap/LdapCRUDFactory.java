/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ldap;

import javax.naming.directory.DirContext;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * LDAP connection pool factory to access LDAP Data such as Mailbox Profile,
 * Domain and COS. This is required by Apache object pool.
 * 
 * @author mxos-dev
 */
class LdapCRUDFactory extends BasePoolableObjectFactory<LdapMailboxCRUD> {
    private static Logger logger = Logger.getLogger(LdapCRUDFactory.class);

    @Override
    public LdapMailboxCRUD makeObject() throws Exception {
        
        logger.info("Connecting to host: "
                + LdapConnectionPool.getEnv().get(DirContext.PROVIDER_URL));
        return new LdapMailboxCRUD(MxOSConstants.LDAPMAILBOXCRUD);
    }

    @Override
    public void destroyObject(final LdapMailboxCRUD ldapCRUD) throws Exception {
        ldapCRUD.close();
    }

    @Override
    public boolean validateObject(final LdapMailboxCRUD ldapCRUD) {
        boolean status;
        try {
            status = ldapCRUD.isConnected();
            if (logger.isDebugEnabled()) {
                logger.info("LDAP CRUD status: " + status);
            }
        } catch (MxOSException e1) {
            return false;
        }
        return status;
    }
}
