/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ox.response.addressbook;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;


import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.addressbook.pojos.Communication;
import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.addressbook.pojos.Event;
import com.opwvmsg.mxos.addressbook.pojos.ExternalMember;
import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.addressbook.pojos.Image;
import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.addressbook.pojos.Name;
import com.opwvmsg.mxos.addressbook.pojos.PersonalInfo;
import com.opwvmsg.mxos.addressbook.pojos.WorkInfo;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.ox.addressbook.OXAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ox.addressbook.OXAddressBookCRUD.Action;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.MaritalStatus;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;

/**
 * AddressBook <-> OX mapper
 * 
 * @author mxos-dev
 * 
 */
public class JsonToAddressBookMapper {

    private static Logger logger = Logger.getLogger(OXAddressBookCRUD.class);
    private final static String ACTION_STRING = "action";
    private final static String NEW_STRING = "new";
    private final static String DELETE_STRING = "delete";
    private final static String MODULE_STRING = "module";
    private final static String CONTACTS_STRING = "contacts";
    private final static String DATA_STRING = "data";

    private static void addExistingExternalMembers(
            List<ExternalMember> extMemberList, ObjectMapper mapper,
            ArrayNode arrayNode) {
        for (ExternalMember existingMember : extMemberList) {
            addExternalMember(mapper, arrayNode, existingMember);
        }
    }

    private static void addExistingMembers(Map<String, String> params,
            List<Member> existingMemberList, ObjectMapper mapper,
            ArrayNode arrayNode) {
        for (Member existingMember : existingMemberList) {
            addMember(params, mapper, arrayNode, existingMember);
        }
    }

    private static void addExternalMember(ObjectMapper mapper,
            ArrayNode arrayNode, ExternalMember existingMember) {
        ObjectNode node = mapper.createObjectNode();
        node.put(OXContactsProperty.display_name.name(),
                existingMember.getMemberName());
        node.put(OXContactsProperty.mail.name(),
                existingMember.getMemberEmail());
        if (existingMember.getMailField() != null) {
            node.put(OXContactsProperty.mail_field.name(),
                    existingMember.getMailField());
        } else {
            node.put(OXContactsProperty.mail_field.name(),
                    OXAddressBookCRUD.EXTERNAL_MEMBER_FIELD);
        }
        arrayNode.add(node);
    }

    private static void addMember(Map<String, String> params,
            ObjectMapper mapper, ArrayNode arrayNode, Member existingMember) {
        ObjectNode node = mapper.createObjectNode();
        node.put(OXContactsProperty.id.name(), existingMember.getMemberId());
        node.put(OXContactsProperty.mail.name(),
                params.get(OXContactsProperty.mail.name()));
        if (existingMember.getMailField() != null) {
            node.put(OXContactsProperty.mail_field.name(),
                    existingMember.getMailField());
        } else {
            node.put(OXContactsProperty.mail_field.name(),
                    OXAddressBookCRUD.MEMBER_FIELD);
        }
        if (params.get(OXContactsProperty.display_name.name()) != null
                && params.get(OXContactsProperty.mail.name()) != null) {
            node.put(OXContactsProperty.display_name.name(),
                    existingMember.getDisplayName());
            node.put(OXContactsProperty.mail.name(), existingMember.getEmail());
        }
        arrayNode.add(node);
    }

    private static void addNewExternalMember(Map<String, String> params,
            ObjectMapper mapper, ArrayNode arrayNode) {
        if (params.get(OXContactsProperty.display_name.name()) != null
                && params.get(OXContactsProperty.mail.name()) != null) {
            ObjectNode node = mapper.createObjectNode();
            node.put(OXContactsProperty.display_name.name(),
                    params.get(OXContactsProperty.display_name.name()));
            node.put(OXContactsProperty.mail.name(),
                    params.get(OXContactsProperty.mail.name()));

            if (params.get(OXContactsProperty.mail_field.name()) != null) {
                node.put(OXContactsProperty.mail_field.name(),
                        params.get(OXContactsProperty.mail_field.name()));
            } else {
                node.put(OXContactsProperty.mail_field.name(),
                        OXAddressBookCRUD.EXTERNAL_MEMBER_FIELD);
            }
            arrayNode.add(node);
        }
    }

    private static void addNewMember(Map<String, String> params,
            ObjectMapper mapper, ArrayNode arrayNode) {
        if (params.get(OXContactsProperty.memberId.name()) != null) {
            ObjectNode node = mapper.createObjectNode();
            node.put(OXContactsProperty.id.name(),
                    params.get(OXContactsProperty.memberId.name()));
            if (params.get(OXContactsProperty.display_name.name()) != null
                    && params.get(OXContactsProperty.mail.name()) != null) {
                node.put(OXContactsProperty.folder_id.name(),
                        params.get(OXContactsProperty.memberFolderId.name()));
                node.put(OXContactsProperty.display_name.name(),
                        params.get(OXContactsProperty.display_name.name()));
                node.put(OXContactsProperty.mail.name(),
                        params.get(OXContactsProperty.mail.name()));
                if (params.get(OXContactsProperty.mail_field.name()) != null) {
                    node.put(OXContactsProperty.mail_field.name(),
                            params.get(OXContactsProperty.mail_field.name()));
                } else {
                    node.put(OXContactsProperty.mail_field.name(),
                            OXAddressBookCRUD.MEMBER_FIELD_ALTERNATE);
                }
            } else {
                node.put(OXContactsProperty.mail.name(),
                        params.get(OXContactsProperty.mail.name()));
                if (params.get(OXContactsProperty.mail_field.name()) != null) {
                    node.put(OXContactsProperty.mail_field.name(),
                            params.get(OXContactsProperty.mail_field.name()));
                } else {
                    node.put(OXContactsProperty.mail_field.name(),
                            OXAddressBookCRUD.MEMBER_FIELD);
                }
            }
            arrayNode.add(node);
        }
    }

    private static void deleteExistingExternalMember(
            Map<String, String> params, List<ExternalMember> extMemberList,
            ObjectMapper mapper, ArrayNode arrayNode) {
        for (ExternalMember existingMember : extMemberList) {
            if (!params.get(OXContactsProperty.display_name.name()).equals(
                    existingMember.getMemberName())) {
                addExternalMember(mapper, arrayNode, existingMember);
            }
        }
    }

    private static void deleteExistingMember(Map<String, String> params,
            List<Member> existingMemberList, ObjectMapper mapper,
            ArrayNode arrayNode) {
        for (Member existingMember : existingMemberList) {
            if (!params.get(OXContactsProperty.memberId.name()).equals(
                    existingMember.getMemberId())) {
                addMember(params, mapper, arrayNode, existingMember);
            }
        }
    }

    public static String mapFromAllGroupsBase(Map<String, String> params,
            String folder, List<GroupBase> groupBaseList)
            throws JsonGenerationException, JsonMappingException, IOException,
            ParseException {

        List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();

        for (GroupBase grp : groupBaseList) {

            Map<String, String> map = new HashMap<String, String>();
            mapFromAllGroupsBaseFields(params, map);

            if (grp.getFolderId() != null) {
                map.put(OXContactsProperty.folder.name(), grp.getFolderId());
            }
            map.put(OXContactsProperty.id.name(), grp.getGroupId());

            mapList.add(map);
        }
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(mapList);
    }

    private static void mapFromAllGroupsBaseFields(
            Map<String, String> attributes, Map<String, String> map)
            throws ParseException {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

    }

    /**
     * Map from Contact Base to String.
     * 
     * @param base resp.
     * @throws JsonGenerationException.
     * @throws JsonMappingException.
     * @throws IOException.
     * @throws ParseException.
     */
    public static String mapFromContact(Map<String, String> params,
            String folder) throws JsonGenerationException,
            JsonMappingException, IOException, ParseException {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Map<String, String> map = new HashMap<String, String>();

        mapFromContactBaseFields(params, map);

        mapFromContactNameFields(params, map);

        mapFromContactPersonalInfoFields(params, map);

        mapFromContactPersonalInfoAddressFields(params, map);

        mapFromContactPersonalInfoCommunicationFields(params, map);

        mapFromContactPersonalInfoEventFields(params, map);

        mapFromContactWorkInfoFields(params, map);

        mapFromContactWorkInfoAddressFields(params, map);

        mapFromContactWorkInfoCommunicationFields(params, map);

        map.put(OXContactsProperty.folder_id.name(), folder);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(map);
    }

    public static String mapFromContactBase(Map<String, String> params,
            String folder) throws JsonGenerationException,
            JsonMappingException, IOException, ParseException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromContactBaseFields(params, map);

        if (folder != null) {
            map.put(OXContactsProperty.folder.name(), folder);
        }

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(map);
    }

    private static void mapFromContactBaseFields(
            Map<String, String> attributes, Map<String, String> map)
            throws ParseException {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (attributes.get(OXContactsProperty.id.name()) != null) {
            map.put(OXContactsProperty.id.name(),
                    attributes.get(OXContactsProperty.id.name()));
        }
        if (attributes.get(OXContactsProperty.private_flag.name()) != null) {
            Boolean privateFlag = false;
            if (attributes.get(OXContactsProperty.private_flag.name())
                    .equalsIgnoreCase(BooleanType.YES.name())) {
                privateFlag = true;
            }
            if (attributes.get(OXContactsProperty.private_flag.name())
                    .equalsIgnoreCase(BooleanType.NO.name())) {
                privateFlag = false;
            }
            map.put(OXContactsProperty.private_flag.name(),
                    privateFlag.toString());
        }
        if (attributes.get(OXContactsProperty.color_label.name()) != null) {
            map.put(OXContactsProperty.color_label.name(),
                    attributes.get(OXContactsProperty.color_label.name()));
        }
        if (attributes.get(OXContactsProperty.categories.name()) != null) {
            map.put(OXContactsProperty.categories.name(),
                    attributes.get(OXContactsProperty.categories.name()));
        }
        if (attributes.get(OXContactsProperty.note.name()) != null) {
            map.put(OXContactsProperty.note.name(),
                    attributes.get(OXContactsProperty.note.name()));
        }
        if (attributes.get(OXContactsProperty.telephone_pager.name()) != null) {
            map.put(OXContactsProperty.telephone_pager.name(),
                    attributes.get(OXContactsProperty.telephone_pager.name()));
        }
        if (attributes.get(OXContactsProperty.yomiFirstName.name()) != null) {
            map.put(OXContactsProperty.yomiFirstName.name(),
                    attributes.get(OXContactsProperty.yomiFirstName.name()));
        }
        if (attributes.get(OXContactsProperty.yomiLastName.name()) != null) {
            map.put(OXContactsProperty.yomiLastName.name(),
                    attributes.get(OXContactsProperty.yomiLastName.name()));
        }
        if (attributes.get(OXContactsProperty.yomiCompany.name()) != null) {
            map.put(OXContactsProperty.yomiCompany.name(),
                    attributes.get(OXContactsProperty.yomiCompany.name()));
        }
        if (attributes.get(OXContactsProperty.email3.name()) != null) {
            map.put(OXContactsProperty.email3.name(),
                    attributes.get(OXContactsProperty.email3.name()));
        }
        if (attributes.get(OXContactsProperty.userfield19.name()) != null) {
            map.put(OXContactsProperty.userfield19.name(),
                    attributes.get(OXContactsProperty.userfield19.name()));
        }
        if (attributes.get(OXContactsProperty.id.name()) != null) {
            if (attributes.get(OXContactsProperty.uid.name()) != null) {
                map.put(OXContactsProperty.uid.name(),
                        attributes.get(OXContactsProperty.uid.name()));
            }
        }
		if (attributes.get(OXContactsProperty.telephone_other.name()) != null) {
			map.put(OXContactsProperty.telephone_other.name(),
					attributes.get(OXContactsProperty.telephone_other.name()));
		}
    }

    public static String mapFromContactName(Map<String, String> params)
            throws JsonGenerationException, JsonMappingException, IOException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromContactNameFields(params, map);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(map);
    }

    private static void mapFromContactNameFields(
            Map<String, String> attributes, Map<String, String> map) {
        if (attributes.get(OXContactsProperty.first_name.name()) != null) {
            map.put(OXContactsProperty.first_name.name(),
                    attributes.get(OXContactsProperty.first_name.name()));
        }

        if (attributes.get(OXContactsProperty.second_name.name()) != null) {
            map.put(OXContactsProperty.second_name.name(),
                    attributes.get(OXContactsProperty.second_name.name()));
        }
        if (attributes.get(OXContactsProperty.last_name.name()) != null) {
            map.put(OXContactsProperty.last_name.name(),
                    attributes.get(OXContactsProperty.last_name.name()));
        }
        if (attributes.get(OXContactsProperty.display_name.name()) != null) {
            map.put(OXContactsProperty.display_name.name(),
                    attributes.get(OXContactsProperty.display_name.name()));
        }
        if (attributes.get(OXContactsProperty.title.name()) != null) {
            map.put(OXContactsProperty.title.name(),
                    attributes.get(OXContactsProperty.title.name()));
        }
        if (attributes.get(OXContactsProperty.suffix.name()) != null) {
            map.put(OXContactsProperty.suffix.name(),
                    attributes.get(OXContactsProperty.suffix.name()));
        }
        if (attributes.get(OXContactsProperty.nickname.name()) != null) {
            map.put(OXContactsProperty.nickname.name(),
                    attributes.get(OXContactsProperty.nickname.name()));
        }
    }

    public static String mapFromContactPersonalInfo(Map<String, String> params)
            throws JsonGenerationException, JsonMappingException, IOException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromContactPersonalInfoFields(params, map);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(map);
    }

    public static String mapFromContactPersonalInfoAddress(
            Map<String, String> params) throws JsonGenerationException,
            JsonMappingException, IOException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromContactPersonalInfoAddressFields(params, map);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(map);
    }

    private static void mapFromContactPersonalInfoAddressFields(
            Map<String, String> attributes, Map<String, String> map) {
        if (attributes.get(OXContactsProperty.street_home.name()) != null) {
            map.put(OXContactsProperty.street_home.name(),
                    attributes.get(OXContactsProperty.street_home.name()));
        }
        if (attributes.get(OXContactsProperty.city_home.name()) != null) {
            map.put(OXContactsProperty.city_home.name(),
                    attributes.get(OXContactsProperty.city_home.name()));
        }
        if (attributes.get(OXContactsProperty.state_home.name()) != null) {
            map.put(OXContactsProperty.state_home.name(),
                    attributes.get(OXContactsProperty.state_home.name()));
        }
        if (attributes.get(OXContactsProperty.postal_code_home.name()) != null) {
            map.put(OXContactsProperty.postal_code_home.name(),
                    attributes.get(OXContactsProperty.postal_code_home.name()));
        }
        if (attributes.get(OXContactsProperty.country_home.name()) != null) {
            map.put(OXContactsProperty.country_home.name(),
                    attributes.get(OXContactsProperty.country_home.name()));
        }
    }

    public static String mapFromContactPersonalInfoCommunication(
            Map<String, String> params) throws JsonGenerationException,
            JsonMappingException, IOException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromContactPersonalInfoCommunicationFields(params, map);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(map);
    }

    private static void mapFromContactPersonalInfoCommunicationFields(
            Map<String, String> attributes, Map<String, String> map) {
        if (attributes.get(OXContactsProperty.telephone_home1.name()) != null) {
            map.put(OXContactsProperty.telephone_home1.name(),
                    attributes.get(OXContactsProperty.telephone_home1.name()));
        }
        if (attributes.get(OXContactsProperty.telephone_home2.name()) != null) {
            map.put(OXContactsProperty.telephone_home2.name(),
                    attributes.get(OXContactsProperty.telephone_home2.name()));
        }
        if (attributes.get(OXContactsProperty.cellular_telephone2.name()) != null) {
            map.put(OXContactsProperty.cellular_telephone2.name(), attributes
                    .get(OXContactsProperty.cellular_telephone2.name()));
        }
        if (attributes.get(OXContactsProperty.fax_home.name()) != null) {
            map.put(OXContactsProperty.fax_home.name(),
                    attributes.get(OXContactsProperty.fax_home.name()));
        }
        if (attributes.get(OXContactsProperty.email2.name()) != null) {
            map.put(OXContactsProperty.email2.name(),
                    attributes.get(OXContactsProperty.email2.name()));
        }
        if (attributes.get(OXContactsProperty.instant_messenger2.name()) != null) {
            map.put(OXContactsProperty.instant_messenger2.name(), attributes
                    .get(OXContactsProperty.instant_messenger2.name()));
        }
        if (attributes.get(OXContactsProperty.telephone_ip.name()) != null) {
            map.put(OXContactsProperty.telephone_ip.name(),
                    attributes.get(OXContactsProperty.telephone_ip.name()));
        }
    }

    public static String mapFromContactPersonalInfoEvent(
            Map<String, String> params) throws JsonGenerationException,
            JsonMappingException, IOException, ParseException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromContactPersonalInfoEventFields(params, map);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(map);
    }

    private static void mapFromContactPersonalInfoEventFields(
            Map<String, String> attributes, Map<String, String> map)
            throws ParseException {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (attributes.get(OXContactsProperty.birthday.name()) != null) {

            if (!"".equals(attributes.get(OXContactsProperty.birthday.name()))) {
                Date date = simpleDateFormat.parse(attributes
                        .get(OXContactsProperty.birthday.name()));
                map.put(OXContactsProperty.birthday.name(),
                        Long.toString(date.getTime()));
            } else {
                map.put(OXContactsProperty.birthday.name(), null);
            }
        }
        if (attributes.get(OXContactsProperty.anniversary.name()) != null) {

            if (!"".equals(attributes.get(OXContactsProperty.anniversary.name()))) {
                Date date = simpleDateFormat.parse(attributes
                        .get(OXContactsProperty.anniversary.name()));
                map.put(OXContactsProperty.anniversary.name(),
                        Long.toString(date.getTime()));
            } else {
                map.put(OXContactsProperty.anniversary.name(), null);
            }

        }

    }

    private static void mapFromContactPersonalInfoFields(
            Map<String, String> attributes, Map<String, String> map) {
        if (attributes.get(OXContactsProperty.marital_status.name()) != null) {
            map.put(OXContactsProperty.marital_status.name(),
                    attributes.get(OXContactsProperty.marital_status.name()));
        }
        if (attributes.get(OXContactsProperty.spouse_name.name()) != null) {
            map.put(OXContactsProperty.spouse_name.name(),
                    attributes.get(OXContactsProperty.spouse_name.name()));
        }
    }

    public static String mapFromContactWorkInfo(Map<String, String> params)
            throws JsonGenerationException, JsonMappingException, IOException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromContactWorkInfoFields(params, map);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(map);
    }

    public static String mapFromContactWorkInfoAddress(
            Map<String, String> params) throws JsonGenerationException,
            JsonMappingException, IOException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromContactWorkInfoAddressFields(params, map);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(map);
    }

    private static void mapFromContactWorkInfoAddressFields(
            Map<String, String> attributes, Map<String, String> map) {
        if (attributes.get(OXContactsProperty.street_business.name()) != null) {
            map.put(OXContactsProperty.street_business.name(),
                    attributes.get(OXContactsProperty.street_business.name()));
        }
        if (attributes.get(OXContactsProperty.city_business.name()) != null) {
            map.put(OXContactsProperty.city_business.name(),
                    attributes.get(OXContactsProperty.city_business.name()));
        }
        if (attributes.get(OXContactsProperty.state_business.name()) != null) {
            map.put(OXContactsProperty.state_business.name(),
                    attributes.get(OXContactsProperty.state_business.name()));
        }
        if (attributes.get(OXContactsProperty.postal_code_business.name()) != null) {
            map.put(OXContactsProperty.postal_code_business.name(), attributes
                    .get(OXContactsProperty.postal_code_business.name()));
        }
        if (attributes.get(OXContactsProperty.country_business.name()) != null) {
            map.put(OXContactsProperty.country_business.name(),
                    attributes.get(OXContactsProperty.country_business.name()));
        }
        if (attributes.get(OXContactsProperty.addressBusiness.name()) != null) {
            map.put(OXContactsProperty.addressBusiness.name(),
                    attributes.get(OXContactsProperty.addressBusiness.name()));
        }
    }

    public static String mapFromContactWorkInfoCommunication(
            Map<String, String> params) throws JsonGenerationException,
            JsonMappingException, IOException {
        Map<String, String> map = new HashMap<String, String>();
        mapFromContactWorkInfoCommunicationFields(params, map);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(map);
    }

    private static void mapFromContactWorkInfoCommunicationFields(
            Map<String, String> attributes, Map<String, String> map) {
        if (attributes.get(OXContactsProperty.telephone_business1.name()) != null) {
            map.put(OXContactsProperty.telephone_business1.name(), attributes
                    .get(OXContactsProperty.telephone_business1.name()));
        }
        if (attributes.get(OXContactsProperty.telephone_business2.name()) != null) {
            map.put(OXContactsProperty.telephone_business2.name(), attributes
                    .get(OXContactsProperty.telephone_business2.name()));
        }
        if (attributes.get(OXContactsProperty.cellular_telephone1.name()) != null) {
            map.put(OXContactsProperty.cellular_telephone1.name(), attributes
                    .get(OXContactsProperty.cellular_telephone1.name()));
        }
        if (attributes.get(OXContactsProperty.fax_business.name()) != null) {
            map.put(OXContactsProperty.fax_business.name(),
                    attributes.get(OXContactsProperty.fax_business.name()));
        }
        if (attributes.get(OXContactsProperty.email1.name()) != null) {
            map.put(OXContactsProperty.email1.name(),
                    attributes.get(OXContactsProperty.email1.name()));
        }
        if (attributes.get(OXContactsProperty.instant_messenger1.name()) != null) {
            map.put(OXContactsProperty.instant_messenger1.name(), attributes
                    .get(OXContactsProperty.instant_messenger1.name()));
        }
    }

    private static void mapFromContactWorkInfoFields(
            Map<String, String> attributes, Map<String, String> map) {
        if (attributes.get(OXContactsProperty.profession.name()) != null) {
            map.put(OXContactsProperty.profession.name(),
                    attributes.get(OXContactsProperty.profession.name()));
        }
        if (attributes.get(OXContactsProperty.company.name()) != null) {
            map.put(OXContactsProperty.company.name(),
                    attributes.get(OXContactsProperty.company.name()));
        }
        if (attributes.get(OXContactsProperty.department.name()) != null) {
            map.put(OXContactsProperty.department.name(),
                    attributes.get(OXContactsProperty.department.name()));
        }
        if (attributes.get(OXContactsProperty.position.name()) != null) {
            map.put(OXContactsProperty.position.name(),
                    attributes.get(OXContactsProperty.position.name()));
        }
        if (attributes.get(OXContactsProperty.manager_name.name()) != null) {
            map.put(OXContactsProperty.manager_name.name(),
                    attributes.get(OXContactsProperty.manager_name.name()));
        }
        if (attributes.get(OXContactsProperty.assistant_name.name()) != null) {
            map.put(OXContactsProperty.assistant_name.name(),
                    attributes.get(OXContactsProperty.assistant_name.name()));
        }
        if (attributes.get(OXContactsProperty.telephone_assistant.name()) != null) {
            map.put(OXContactsProperty.telephone_assistant.name(), attributes
                    .get(OXContactsProperty.telephone_assistant.name()));
        }
        if (attributes.get(OXContactsProperty.employee_type.name()) != null) {
            map.put(OXContactsProperty.employee_type.name(),
                    attributes.get(OXContactsProperty.employee_type.name()));
        }
        if (attributes.get(OXContactsProperty.url.name()) != null) {
            map.put(OXContactsProperty.url.name(),
                    attributes.get(OXContactsProperty.url.name()));
        }
    }

    /**
     * Json from Contact Id and Folder to String.
     * 
     * @param folder
     * @param contactIdObj
     * @throws JSONException
     */
    public static void mapFromDeleteAddressBook(String contactId,
            String folder, ArrayNode arrayNode, ObjectMapper objMapper)
            throws JsonGenerationException, JsonMappingException, IOException,
            ParseException {

        ObjectNode objNode = objMapper.createObjectNode();

        objNode.put(OXContactsProperty.id.name(), contactId);
        objNode.put(OXContactsProperty.folder.name(), folder);

        arrayNode.add(objNode);
    }

    /**
     * Map from Group to String.
     * 
     * @param base resp.
     * @throws JsonGenerationException.
     * @throws JsonMappingException.
     * @throws IOException.
     * @throws ParseException.
     */
    public static String mapFromGroup(Map<String, String> params, String folder)
            throws JsonGenerationException, JsonMappingException, IOException,
            ParseException {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Map<String, String> map = new HashMap<String, String>();

        mapFromGroupFields(params, map);

        map.put(OXContactsProperty.folder_id.name(), folder);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(map);
    }

    private static void mapFromGroupFields(Map<String, String> attributes,
            Map<String, String> map) {

        if (attributes.get(OXContactsProperty.display_name.name()) != null) {
            map.put(OXContactsProperty.display_name.name(),
                    attributes.get(OXContactsProperty.display_name.name()));
        }
        map.put(OXContactsProperty.mark_as_distributionlist.name(),
                MxOSConstants.TRUE);

    }

    /**
     * Map from Group to String.
     * 
     * @param base resp.
     * @throws JsonGenerationException.
     * @throws JsonMappingException.
     * @throws IOException.
     * @throws ParseException.
     */
    public static String mapFromGroupMembers(Map<String, String> params,
            String folder, List<Member> existingMemberList,
            List<ExternalMember> extMemberList, Action action)
            throws JsonGenerationException, JsonMappingException, IOException,
            ParseException {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        ObjectNode objNode = mapper.createObjectNode();

        ArrayNode arrayNode = mapper.createArrayNode();

        switch (action) {
        case ADD_GROUP_MEMBER:
            addExistingMembers(params, existingMemberList, mapper, arrayNode);
            addNewMember(params, mapper, arrayNode);
            addExistingExternalMembers(extMemberList, mapper, arrayNode);
            break;

        case ADD_EXTERNAL_GROUP_MEMBER:
            addExistingMembers(params, existingMemberList, mapper, arrayNode);
            addExistingExternalMembers(extMemberList, mapper, arrayNode);
            addNewExternalMember(params, mapper, arrayNode);
            break;

        case DELETE_GROUP_MEMBER:
            deleteExistingMember(params, existingMemberList, mapper, arrayNode);
            addExistingExternalMembers(extMemberList, mapper, arrayNode);
            break;

        case DELETE_EXTERNAL_GROUP_MEMBER:
            addExistingMembers(params, existingMemberList, mapper, arrayNode);
            deleteExistingExternalMember(params, extMemberList, mapper,
                    arrayNode);
            break;

        case DELETE_ALL_GROUP_MEMBER:
            addExistingExternalMembers(extMemberList, mapper, arrayNode);
            break;

        case DELETE_ALL_EXTERNAL_GROUP_MEMBER:
            addExistingMembers(params, existingMemberList, mapper, arrayNode);
            break;
        }
        objNode.put(OXContactsProperty.distribution_list.name(), arrayNode);
        return mapper.writeValueAsString(objNode);
    }

    /**
     * Map from Group to String.
     * 
     * @param base resp.
     * @throws JsonGenerationException.
     * @throws JsonMappingException.
     * @throws IOException.
     * @throws ParseException.
     */
    public static String mapFromMultipleGroupMembers(
            List<Map<String, String>> paramsList,
            String folder, List<Member> existingMemberList,
            List<ExternalMember> extMemberList)
            throws JsonGenerationException, JsonMappingException, IOException,
            ParseException {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        ObjectNode objNode = mapper.createObjectNode();

        ArrayNode arrayNode = mapper.createArrayNode();
        boolean isFirst = true;
    
        for (Map<String, String> params : paramsList) {
            if (isFirst) {
                addExistingMembers(params, existingMemberList, mapper, arrayNode);
                addNewMember(params, mapper, arrayNode);
                addExistingExternalMembers(extMemberList, mapper, arrayNode);
                isFirst = false;
            } else {
                addNewMember(params, mapper, arrayNode);
            }
        }
        objNode.put(OXContactsProperty.distribution_list.name(), arrayNode);
        return mapper.writeValueAsString(objNode);
    }

    public static String mapFromGroupsBase(Map<String, String> params,
            String folder) throws JsonGenerationException,
            JsonMappingException, IOException, ParseException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromGroupsBaseFields(params, map);

        if (folder != null) {
            map.put(OXContactsProperty.folder.name(), folder);
        }

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        return mapper.writeValueAsString(map);
    }

    private static void mapFromGroupsBaseFields(Map<String, String> attributes,
            Map<String, String> map) throws ParseException {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (attributes.get(OXContactsProperty.id.name()) != null) {
            map.put(OXContactsProperty.id.name(),
                    attributes.get(OXContactsProperty.id.name()));
        }

        if (attributes.get(OXContactsProperty.display_name.name()) != null) {
            map.put(OXContactsProperty.display_name.name(),
                    attributes.get(OXContactsProperty.display_name.name()));
        }
    }

    /**
     * Map from Contact Base to String.
     * 
     * @param base resp.
     * @throws JsonGenerationException.
     * @throws JsonMappingException.
     * @throws IOException.
     * @throws ParseException.
     */
    public static void mapFromMultipleContact(Map<String, String> params,
            String folder, ArrayNode arrNode, ObjectMapper objMapper)
            throws JsonGenerationException, JsonMappingException, IOException,
            ParseException {

        ObjectNode objNode = objMapper.createObjectNode();

        objNode.put(ACTION_STRING, NEW_STRING);
        objNode.put(MODULE_STRING, CONTACTS_STRING);

        Map<String, String> map = new HashMap<String, String>();
        mapFromContactBaseFields(params, map);

        mapFromContactNameFields(params, map);

        mapFromContactPersonalInfoFields(params, map);

        mapFromContactPersonalInfoAddressFields(params, map);

        mapFromContactPersonalInfoCommunicationFields(params, map);

        mapFromContactPersonalInfoEventFields(params, map);

        mapFromContactWorkInfoFields(params, map);

        mapFromContactWorkInfoAddressFields(params, map);

        mapFromContactWorkInfoCommunicationFields(params, map);

        map.put(OXContactsProperty.folder_id.name(), folder);

        objNode.put(DATA_STRING, objMapper.convertValue(map, JsonNode.class));
        arrNode.add(objNode);

    }

    /**
     * Map from Contact Base to String.
     * 
     * @param base resp.
     * @throws JsonGenerationException.
     * @throws JsonMappingException.
     * @throws IOException.
     * @throws ParseException.
     */
    public static void mapFromMultipleDeleteContact(String contactId,
            String folder, ArrayNode arrNode, ObjectMapper objMapper)
            throws JsonGenerationException, JsonMappingException, IOException,
            ParseException {
        ObjectNode objNode = objMapper.createObjectNode();

        objNode.put(ACTION_STRING, DELETE_STRING);
        objNode.put(MODULE_STRING, CONTACTS_STRING);

        // Add timestamp
        Long updateTime = System.nanoTime();
        objNode.put(OXContactsProperty.timestamp.name(), updateTime.toString());

        Map<String, String> map = new HashMap<String, String>();
        map.put(OXContactsProperty.folder.name(), folder);
        map.put(OXContactsProperty.id.name(), contactId);

        objNode.put(DATA_STRING, objMapper.convertValue(map, JsonNode.class));
        arrNode.add(objNode);

    }

    /**
     * Map from Contact Base to String.
     * 
     * @param base resp.
     * @throws JsonGenerationException.
     * @throws JsonMappingException.
     * @throws IOException.
     * @throws ParseException.
     */
    public static void mapFromMultipleGroup(Map<String, String> params,
            String folder, ArrayNode arrayNode, ObjectMapper objMapper)
            throws JsonGenerationException, JsonMappingException, IOException,
            ParseException {

        ObjectNode objNode = objMapper.createObjectNode();

        objNode.put(ACTION_STRING, NEW_STRING);
        objNode.put(MODULE_STRING, CONTACTS_STRING);

        Map<String, String> map = new HashMap<String, String>();
        mapFromGroupFields(params, map);

        map.put(OXContactsProperty.folder_id.name(), folder);

        // objNode.put(DATA_STRING, objMapper.valueToTree(map));
        objNode.put(DATA_STRING, objMapper.convertValue(map, JsonNode.class));
        arrayNode.add(objNode);

    }

    public static List<ContactBase> mapToAllContactBase(JsonNode root) {

        List<ContactBase> contactBaseList = new ArrayList<ContactBase>();

        Iterator<JsonNode> arrayIter = root.getElements();
        while (arrayIter.hasNext()) {
            JsonNode tempArrayElement = arrayIter.next();

            Iterator<JsonNode> tempNodeIter = tempArrayElement.getElements();

            JsonNode isContactNode = tempNodeIter.next();

            if (!isContactNode.asBoolean()) {
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
                simpleDateFormat.applyLocalizedPattern(System
                        .getProperty(SystemProperty.userDateFormat.name()));
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

                ContactBase contactBase = new ContactBase();

                JsonNode contactIdNode = tempNodeIter.next();
                if (contactIdNode.isInt()) {
                    contactBase.setContactId(String.valueOf(contactIdNode
                            .asInt()));
                }
                Date date = null;
                JsonNode contactIdCreatedDate = tempNodeIter.next();
                if (contactIdCreatedDate.isLong()) {
                    Long creationDate = contactIdCreatedDate.asLong();
                    date = new Date(creationDate);
                    contactBase.setCreated(simpleDateFormat.format(date));
                }
                JsonNode contactIdModifiedDate = tempNodeIter.next();
                if (contactIdModifiedDate.isLong()) {
                    Long modifiedDate = contactIdModifiedDate.asLong();
                    date = new Date(modifiedDate);
                    contactBase.setUpdated(simpleDateFormat.format(date));
                }

                JsonNode folderId = tempNodeIter.next();
                if (folderId.isInt()) {
                    contactBase.setFolderId(folderId.asText());
                }

                JsonNode contactNotesNode = tempNodeIter.next();
                if (contactNotesNode.isTextual()) {
                    contactBase.setNotes(contactNotesNode.asText());
                }
                StringBuilder customFields = new StringBuilder();

                try {
                    JsonNode pagerNode = tempNodeIter.next();
                    if (pagerNode.isTextual()) {
                        String pager = pagerNode.asText();
                        String pagerEncoded = URLEncoder.encode(pager,
                                MxOSConstants.UTF8);
						if (!pagerEncoded.isEmpty()) {
							customFields
									.append(AddressBookProperty.pager.name())
									.append("=").append(pagerEncoded)
									.append(",");
						}
                    }

                    JsonNode yomiFirstNameNode = tempNodeIter.next();
                    if (yomiFirstNameNode.isTextual()) {
                        String yomiFirstName = yomiFirstNameNode.asText();
                        String yomiFirstNameEncoded = URLEncoder.encode(
                                yomiFirstName, MxOSConstants.UTF8);
						if (!yomiFirstNameEncoded.isEmpty()) {
							customFields
									.append(OXContactsProperty.yomiFirstName
											.name()).append("=")
									.append(yomiFirstNameEncoded).append(",");
						}
                    }

                    JsonNode yomiLastNameNode = tempNodeIter.next();
                    if (yomiLastNameNode.isTextual()) {
                        String yomiLastName = yomiLastNameNode.asText();
                        String yomiLastNameEncoded = URLEncoder.encode(
                                yomiLastName, MxOSConstants.UTF8);
						if (!yomiLastNameEncoded.isEmpty()) {
							customFields
									.append(OXContactsProperty.yomiLastName
											.name()).append("=")
									.append(yomiLastNameEncoded).append(",");
						}
                    }

                    JsonNode yomiCompanyNode = tempNodeIter.next();
                    if (yomiCompanyNode.isTextual()) {
                        String yomiCompany = yomiCompanyNode.asText();
                        String yomiCompanyEncoded = URLEncoder.encode(
                                yomiCompany, MxOSConstants.UTF8);
						if (!yomiCompanyEncoded.isEmpty()) {
							customFields
									.append(OXContactsProperty.yomiCompany
											.name()).append("=")
									.append(yomiCompanyEncoded).append(",");
						}
                    }

                    JsonNode email3Node = tempNodeIter.next();
                    if (email3Node.isTextual()) {
                        String email3 = email3Node.asText();
                        String email3Encoded = URLEncoder.encode(email3,
                                MxOSConstants.UTF8);
						if (!email3Encoded.isEmpty()) {
							customFields
									.append(OXContactsProperty.email3.name())
									.append("=").append(email3Encoded)
									.append(",");
						}
                    }

                    JsonNode fileNameNode = tempNodeIter.next();
                    if (fileNameNode.isTextual()) {
                        String fileName = fileNameNode.asText();
                        String fileNameEncoded = URLEncoder.encode(fileName,
                                MxOSConstants.UTF8);
						if (!fileNameEncoded.isEmpty()) {
							customFields
									.append(OXContactsProperty.userfield19
											.name()).append("=")
									.append(fileNameEncoded).append(",");
						}
                    }

                    JsonNode otherPhoneNode = tempNodeIter.next();
                    if (otherPhoneNode.isTextual()) {
                        String otherPhone = otherPhoneNode.asText();
                        String otherPhoneEncoded = URLEncoder.encode(otherPhone,
                                MxOSConstants.UTF8);
						if (!otherPhoneEncoded.isEmpty()) {
							customFields
									.append(AddressBookProperty.otherPhone
											.name()).append("=")
									.append(otherPhoneEncoded).append(",");
						}
                    }
                    
                    JsonNode uidNode = tempNodeIter.next();
                    if (uidNode.isTextual()) {
                        String uid = uidNode.asText();
                        String uidEncoded = URLEncoder.encode(uid,
                                MxOSConstants.UTF8);
						if (!uidEncoded.isEmpty()) {
							customFields.append(OXContactsProperty.uid.name())
									.append("=").append(uidEncoded);
						}
                    }
                } catch (UnsupportedEncodingException ex) {
                    // this should never happen as UTF-8 will always be
                    // supported
                    logger.error("The URL encoder was unable to handle the UTF-8 encoding format");
                    throw new RuntimeException(ex);
                }
                String custom = customFields.toString();
                if ((custom.length() > 0)
                        && (custom.charAt(custom.length() - 1) == ',')) {
                    custom = custom.substring(0, custom.length() - 1);
                }

                contactBase.setCustomFields(custom);

                contactBaseList.add(contactBase);
            }
        }

        return contactBaseList;
    }

    public static List<Contact> mapToAllContacts(JsonNode root)
            throws AddressBookException {

        List<Contact> contactsList = new ArrayList<Contact>();

        Iterator<JsonNode> arrayIter = root.getElements();

        while (arrayIter.hasNext()) {
            JsonNode tempArrayElement = arrayIter.next();

            Iterator<JsonNode> tempNodeIter = tempArrayElement.getElements();
            JsonNode isContactNode = tempNodeIter.next();

            if (!isContactNode.asBoolean()) {

                Contact contact = new Contact();
                ContactBase base = new ContactBase();
                Name name = new Name();
                PersonalInfo personalInfo = new PersonalInfo();
                Address personalInfoAddress = new Address();
                Communication personalInfoCommunication = new Communication();
                WorkInfo workInfo = new WorkInfo();
                Address workInfoAddress = new Address();
                Communication workInfoCommunication = new Communication();
                Image image = new Image();

                // Set name
                JsonNode displayName = tempNodeIter.next();
                if (displayName.isTextual()) {
                    name.setDisplayName(displayName.asText());
                }
                JsonNode firstName = tempNodeIter.next();
                if (firstName.isTextual()) {
                    name.setFirstName(firstName.asText());
                }
                JsonNode lastName = tempNodeIter.next();
                if (lastName.isTextual()) {
                    name.setLastName(lastName.asText());
                }
                JsonNode middleName = tempNodeIter.next();
                if (middleName.isTextual()) {
                    name.setMiddleName(middleName.asText());
                }
                JsonNode suffix = tempNodeIter.next();
                if (suffix.isTextual()) {
                    name.setSuffix(suffix.asText());
                }
                JsonNode prefix = tempNodeIter.next();
                if (prefix.isTextual()) {
                    name.setPrefix(prefix.asText());
                }
                JsonNode nickName = tempNodeIter.next();
                if (nickName.isTextual()) {
                    name.setNickName(nickName.asText());
                }
                // Set base
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
                simpleDateFormat.applyLocalizedPattern(System
                        .getProperty(SystemProperty.userDateFormat.name()));
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                JsonNode contactId = tempNodeIter.next();
                if(contactId.isInt()){ 
                Integer id = contactId.asInt();
                base.setContactId(String.valueOf(id));
                }
                Date date = null;
                JsonNode createdDate = tempNodeIter.next();
                if (createdDate.isLong()) {
                    date = new Date(createdDate.asLong());
                    base.setCreated(simpleDateFormat.format(date));
                }
                JsonNode updatedDate = tempNodeIter.next();
                if (updatedDate.isLong()) {    
                    date = new Date(updatedDate.asLong());
                    base.setUpdated(simpleDateFormat.format(date));
                }
                JsonNode folderId = tempNodeIter.next();
                if (folderId.isInt()) {
                    base.setFolderId(folderId.asText());
                }
                JsonNode categories = tempNodeIter.next();
                if (categories.isTextual()) {
                    base.setCategories(categories.asText());
                }
                JsonNode isPrivate = tempNodeIter.next();
                if (isPrivate.isBoolean()) {
                    base.setIsPrivate(isPrivate.asBoolean() ? BooleanType.YES
                            : BooleanType.NO);
                }
                JsonNode colorLabel = tempNodeIter.next();
                if (colorLabel.isInt()) {
                    base.setColorLabel(colorLabel.asInt());
                }
                JsonNode notes = tempNodeIter.next();
                if (notes.isTextual()) {
                    base.setNotes(notes.asText());
                }

                StringBuilder customFields = new StringBuilder();

                try {
                    JsonNode pagerNode = tempNodeIter.next();
                    if (pagerNode.isTextual()) {
                        String pager = pagerNode.asText();
                        String pagerEncoded = URLEncoder.encode(pager,
                                MxOSConstants.UTF8);
						if (!pagerEncoded.isEmpty()) {
							customFields
									.append(AddressBookProperty.pager.name())
									.append("=").append(pagerEncoded)
									.append(",");
						}
                    }
                    JsonNode yomiFirstNameNode = tempNodeIter.next();
                    if (yomiFirstNameNode.isTextual()) {
                        String yomiFirstName = yomiFirstNameNode.asText();
                        String yomiFirstNameEncoded = URLEncoder.encode(
                                yomiFirstName, MxOSConstants.UTF8);
						if (!yomiFirstNameEncoded.isEmpty()) {
							customFields
									.append(OXContactsProperty.yomiFirstName
											.name()).append("=")
									.append(yomiFirstNameEncoded).append(",");
						}
                    }
                    
                    JsonNode yomiLastNameNode = tempNodeIter.next();
                    if (yomiLastNameNode.isTextual()) {
                        String yomiLastName = yomiLastNameNode.asText();
                        String yomiLastNameEncoded = URLEncoder.encode(
                                yomiLastName, MxOSConstants.UTF8);
						if (!yomiLastNameEncoded.isEmpty()) {
							customFields
									.append(OXContactsProperty.yomiLastName
											.name()).append("=")
									.append(yomiLastNameEncoded).append(",");
						}
                    }
                    
                    JsonNode yomiCompanyNode = tempNodeIter.next();
                    if (yomiCompanyNode.isTextual()) {
                        String yomiCompany = yomiCompanyNode.asText();
                        String yomiCompanyEncoded = URLEncoder.encode(
                                yomiCompany, MxOSConstants.UTF8);
						if (!yomiCompanyEncoded.isEmpty()) {
							customFields
									.append(OXContactsProperty.yomiCompany
											.name()).append("=")
									.append(yomiCompanyEncoded).append(",");
						}
                    }

                    JsonNode email3Node = tempNodeIter.next();
                    if (email3Node.isTextual()) {
                        String email3 = email3Node.asText();
                        String email3Encoded = URLEncoder.encode(email3,
                                MxOSConstants.UTF8);
						if (!email3Encoded.isEmpty()) {
							customFields
									.append(OXContactsProperty.email3.name())
									.append("=").append(email3Encoded)
									.append(",");
						}
                    }


                    JsonNode fileNameNode = tempNodeIter.next();
                    if (fileNameNode.isTextual()) {
                        String fileName = fileNameNode.asText();
                        String fileNameEncoded = URLEncoder.encode(fileName,
                                MxOSConstants.UTF8);
						if (!fileNameEncoded.isEmpty()) {
							customFields
									.append(AddressBookProperty.fileName.name())
									.append("=").append(fileNameEncoded)
									.append(",");
						}
                    }

                    JsonNode otherPhoneNode = tempNodeIter.next();
                    if (otherPhoneNode.isTextual()) {
                        String otherPhone = otherPhoneNode.asText();
                        String otherPhoneEncoded = URLEncoder.encode(otherPhone,
                                MxOSConstants.UTF8);
						if (!otherPhoneEncoded.isEmpty()) {
							customFields
									.append(AddressBookProperty.otherPhone
											.name()).append("=")
									.append(otherPhoneEncoded).append(",");
						}
                    }
                    
                    JsonNode uidNode = tempNodeIter.next();
					if (uidNode.isTextual()) {
						String uid = uidNode.asText();
						String uidEncoded = URLEncoder.encode(uid,
								MxOSConstants.UTF8);
						if (!uidEncoded.isEmpty()) {
							customFields.append(OXContactsProperty.uid.name())
									.append("=").append(uidEncoded);
						}
					}
                } catch (UnsupportedEncodingException ex) {
                    // this should never happen as UTF-8 will always be
                    // supported
                    logger.error("The URL encoder was unable to handle the UTF-8 encoding format");
                    throw new RuntimeException(ex);
                }
                String custom = customFields.toString();

                if ((custom.length() > 0)
                        && (custom.charAt(custom.length() - 1) == ',')) {
                    custom = custom.substring(0, custom.length() - 1);
                }

                base.setCustomFields(custom);

                // Set personalInfo
                try {
                    JsonNode maritalStatusNode = tempNodeIter.next();
                    if (maritalStatusNode.isTextual()) {
                        String maritalStatus = maritalStatusNode.asText();
                        personalInfo.setMaritalStatus(MaritalStatus
                                .fromValue(maritalStatus));
                    }
                } catch (IllegalArgumentException ile) {
                    logger.error("Value for marital status is not correct");
                    personalInfo.setMaritalStatus(null);
                }
                JsonNode spouseName = tempNodeIter.next();
                if (spouseName.isTextual()) {
                    personalInfo.setSpouseName(spouseName.asText());
                }
                // Set personalInfo address
                JsonNode street = tempNodeIter.next();
                if (street.isTextual()) {
                    personalInfoAddress.setStreet(street.asText());
                }
                JsonNode postalCode = tempNodeIter.next();
                if (postalCode.isTextual()) {
                    personalInfoAddress.setPostalCode(postalCode.asText());
                }
                JsonNode city = tempNodeIter.next();
                if (city.isTextual()) {
                    personalInfoAddress.setCity(city.asText());
                }
                JsonNode stateOrProvince = tempNodeIter.next();
                if (stateOrProvince.isTextual()) {
                    personalInfoAddress.setStateOrProvince(stateOrProvince
                            .asText());
                }
                JsonNode country = tempNodeIter.next();
                if (country.isTextual()) {
                    personalInfoAddress.setCountry(country.asText());
                }
                // Set personalInfo communication
                JsonNode phone1 = tempNodeIter.next();
                if (phone1.isTextual()) {
                    personalInfoCommunication.setPhone1(phone1.asText());
                }
                JsonNode phone2 = tempNodeIter.next();
                if (phone2.isTextual()) {
                    personalInfoCommunication.setPhone2(phone2.asText());
                }
                JsonNode fax = tempNodeIter.next();
                if (fax.isTextual()) {
                    personalInfoCommunication.setFax(fax.asText());
                }
                JsonNode mobile = tempNodeIter.next();
                if (mobile.isTextual()) {
                    personalInfoCommunication.setMobile(mobile.asText());
                }
                JsonNode email = tempNodeIter.next();
                if (email.isTextual()) {
					if (!email.asText().isEmpty()) {
						personalInfoCommunication.setEmail(email.asText());
					}
                }
                JsonNode imAddress = tempNodeIter.next();
                if (imAddress.isTextual()) {
                    personalInfoCommunication.setImAddress(imAddress.asText());
                }
                JsonNode voip = tempNodeIter.next();
                if (voip.isTextual()) {
                    personalInfoCommunication.setVoip(voip.asText());
                }
                // Set events
                List<Event> events = new ArrayList<Event>();

                JsonNode birthDate = tempNodeIter.next();
                if (birthDate.isLong()) {
                    Event birthday = new Event();
                    birthday.setType(Event.Type.BIRTHDAY);
                    date = new Date(birthDate.asLong());
                    birthday.setDate(simpleDateFormat.format(date));
                    events.add(birthday);
                }

                JsonNode anniversaryDate = tempNodeIter.next();
                if (anniversaryDate.isLong()) {
                    Event anniversary = new Event();
                    anniversary.setType(Event.Type.ANNIVERSARY);
                    date = new Date(anniversaryDate.asLong());
                    anniversary.setDate(simpleDateFormat.format(date));
                    events.add(anniversary);
                }

                personalInfo.setEvents(events);
                personalInfo.setAddress(personalInfoAddress);
                personalInfo.setCommunication(personalInfoCommunication);

                // Set workInfo
                JsonNode profession = tempNodeIter.next();
                if (profession.isTextual()) {
                    workInfo.setProfession(profession.asText());
                }
                JsonNode companyName = tempNodeIter.next();
                if (companyName.isTextual()) {
                    workInfo.setCompanyName(companyName.asText());
                }
                JsonNode department = tempNodeIter.next();
                if (department.isTextual()) {
                    workInfo.setDepartment(department.asText());
                }
                JsonNode title = tempNodeIter.next();
                if (title.isTextual()) {
                    workInfo.setTitle(title.asText());
                }
                JsonNode employeeId = tempNodeIter.next();
                if (employeeId.isTextual()) {
                    workInfo.setEmployeeId(employeeId.asText());
                }
                JsonNode manager = tempNodeIter.next();
                if (manager.isTextual()) {
                    workInfo.setManager(manager.asText());
                }
                JsonNode assistant = tempNodeIter.next();
                if (assistant.isTextual()) {
                    workInfo.setAssistant(assistant.asText());
                }

                // Set workInfo address
                JsonNode workInfoStreet = tempNodeIter.next();
                if (workInfoStreet.isTextual()) {
                    workInfoAddress.setStreet(workInfoStreet.asText());
                }
                JsonNode workInfoPostalCode = tempNodeIter.next();
                if (workInfoPostalCode.isTextual()) {
                    workInfoAddress.setPostalCode(workInfoPostalCode.asText());
                }
                JsonNode workInfoCity = tempNodeIter.next();
                if (workInfoCity.isTextual()) {
                    workInfoAddress.setCity(workInfoCity.asText());
                }
                JsonNode workInfoStateOrProvince = tempNodeIter.next();
                if (workInfoStateOrProvince.isTextual()) {
                    workInfoAddress.setStateOrProvince(workInfoStateOrProvince
                            .asText());
                }
                JsonNode workInfoCountry = tempNodeIter.next();
                if (workInfoCountry.isTextual()) {
                    workInfoAddress.setCountry(workInfoCountry.asText());
                }
                // Set workInfo communication next
                JsonNode workInfoPhone1 = tempNodeIter.next();
                if (workInfoPhone1.isTextual()) {
                    workInfoCommunication.setPhone1(workInfoPhone1
                            .asText());
                }
                JsonNode workInfoPhone2 = tempNodeIter.next();
                if (workInfoPhone2.isTextual()) {
                    workInfoCommunication.setPhone2(workInfoPhone2
                            .asText());
                }
                JsonNode workInfoFax = tempNodeIter.next();
                if (workInfoFax.isTextual()) {
                    workInfoCommunication.setFax(workInfoFax.asText());
                }
                JsonNode workInfoMobile = tempNodeIter.next();
                if (workInfoMobile.isTextual()) {
                    workInfoCommunication.setMobile(workInfoMobile
                            .asText());
                }
                JsonNode workInfoEmail = tempNodeIter.next();
                if (workInfoEmail.isTextual()) {
					if (!workInfoEmail.asText().isEmpty()) {
						workInfoCommunication.setEmail(workInfoEmail.asText());
					}
                }

                workInfo.setAddress(workInfoAddress);
                workInfo.setCommunication(workInfoCommunication);

                contact.setContactBase(base);
                contact.setName(name);
                contact.setPersonalInfo(personalInfo);
                contact.setWorkInfo(workInfo);
                contact.setImage(image);

                contactsList.add(contact);
            }
        }

        return contactsList;
    }

    public static List<ExternalMember> mapToAllExternalGroupMembers(
            JsonNode root) {

        List<ExternalMember> memberList = new ArrayList<ExternalMember>();

        if (root.path(OXContactsProperty.distribution_list.name()) != null) {

            JsonNode memberListNode = root
                    .path(OXContactsProperty.distribution_list.name());
            Iterator<JsonNode> tempNodeIter = memberListNode.getElements();
            while (tempNodeIter.hasNext()) {
                JsonNode t = tempNodeIter.next();
                if (t.path(OXContactsProperty.mail_field.name()).asInt() == Integer
                        .valueOf(OXAddressBookCRUD.EXTERNAL_MEMBER_FIELD)) {
                    ExternalMember m = new ExternalMember();
                    if (t.path(OXContactsProperty.display_name.name())
                            .isTextual()) {
                        m.setMemberName(t.path(
                                OXContactsProperty.display_name.name())
                                .asText());
                    }
                    if (t.path(OXContactsProperty.mail.name()).isTextual()) {
                        m.setMemberEmail(t.path(OXContactsProperty.mail.name())
                                .asText());
                    }
                    if (t.path(OXContactsProperty.mail_field.name()).isInt()) {
                        Integer mail_field = t.path(
                                OXContactsProperty.mail_field.name()).asInt();
                        m.setMailField(String.valueOf(mail_field));
                    }
                    memberList.add(m);
                }
            }
        }
        return memberList;
    }

    public static List<GroupBase> mapToAllGroupBase(JsonNode root) {

        List<GroupBase> groupBaseList = new ArrayList<GroupBase>();

        Iterator<JsonNode> arrayIter = root.getElements();

        while (arrayIter.hasNext()) {
            JsonNode tempArrayElement = arrayIter.next();

            Iterator<JsonNode> tempNodeIter = tempArrayElement.getElements();

            JsonNode isGroupNode = tempNodeIter.next();

            if (isGroupNode.asBoolean()) {
                GroupBase groupBase = new GroupBase();

                JsonNode groupIdNode = tempNodeIter.next();
                if (groupIdNode.isInt()) {
                    groupBase.setGroupId(String.valueOf(groupIdNode.asInt()));
                }
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
                simpleDateFormat.applyLocalizedPattern(System
                        .getProperty(SystemProperty.userDateFormat.name()));
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

                JsonNode groupCreatedDate = tempNodeIter.next();
                Date date = null;
                if (groupCreatedDate.isLong()) {
                    Long creationDate = groupCreatedDate.asLong();
                    date = new Date(creationDate);
                    groupBase.setCreated(simpleDateFormat.format(date));
                }
                JsonNode groupModifiedDate = tempNodeIter.next();
                if (groupModifiedDate.isLong()) {
                    Long modifiedDate = groupModifiedDate.asLong();
                    date = new Date(modifiedDate);
                    groupBase.setUpdated(simpleDateFormat.format(date));
                }
                JsonNode folderId = tempNodeIter.next();
                if (folderId.isInt()) {
                    groupBase.setFolderId(folderId.asText());
                }
           
                JsonNode groupNameNode = tempNodeIter.next();
                if (groupNameNode.isTextual()) {
                    groupBase.setGroupName(groupNameNode.asText());
                }
                StringBuilder customFields = new StringBuilder();

                JsonNode uidNode = tempNodeIter.next();
                if (uidNode.isTextual()) {
                    String uid = uidNode.asText();
                    customFields.append(OXContactsProperty.uid.name())
                            .append("=").append(uid);
                }
                String custom = customFields.toString();
                groupBase.setCustomFields(custom);

                groupBaseList.add(groupBase);
            }
        }

        return groupBaseList;
    }

    public static List<Member> mapToAllGroupMembers(JsonNode root) {

        List<Member> memberList = new ArrayList<Member>();

        String id = String.valueOf(root.path(OXContactsProperty.id.name())
                .asInt());
        if (root.path(OXContactsProperty.distribution_list.name()) != null) {

            JsonNode memberListNode = root
                    .path(OXContactsProperty.distribution_list.name());
            Iterator<JsonNode> tempNodeIter = memberListNode.getElements();
            while (tempNodeIter.hasNext()) {
                JsonNode t = tempNodeIter.next();
                int memberField = t.path(OXContactsProperty.mail_field.name())
                        .asInt();
                if ((memberField == Integer
                        .valueOf(OXAddressBookCRUD.MEMBER_FIELD)
                        || memberField == Integer
                                .valueOf(OXAddressBookCRUD.MEMBER_FIELD_ALTERNATE) || memberField == Integer
                        .valueOf(OXAddressBookCRUD.MEMBER_FIELD_OTHER))
                        && (t.path(OXContactsProperty.id.name()).asInt() != Integer
                                .valueOf(id))) {
                    Member m = new Member();
                    m.setMemberId(String.valueOf(t.path(
                            OXContactsProperty.id.name()).asInt()));
                    m.setFolderId(String.valueOf(t.path(
                            OXContactsProperty.folder_id.name()).asInt()));
                    m.setEmail(String.valueOf(t.path(
                            OXContactsProperty.mail.name()).asText()));
                    m.setDisplayName(String.valueOf(t.path(
                            OXContactsProperty.display_name.name()).asText()));

                    Integer mail_field = t.path(
                            OXContactsProperty.mail_field.name()).asInt();
                    m.setMailField(String.valueOf(mail_field));
                    memberList.add(m);
                }
            }
        }
        return memberList;
    }

    public static ContactBase mapToContactBase(JsonNode root) {

        ContactBase contactBase = new ContactBase();

        mapToContactBaseFields(root, contactBase);

        return contactBase;
    }

    private static void mapToContactBaseFields(JsonNode root,
            ContactBase contactBase) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (root.path(OXContactsProperty.id.name()).isInt()) {
            Integer id = root.path(OXContactsProperty.id.name()).asInt();
            contactBase.setContactId(String.valueOf(id));
        }

        if (root.path(OXContactsProperty.creation_date.name()).isLong()) {
            Long creationDate = root.path(
                    OXContactsProperty.creation_date.name()).asLong();
            Date date = new Date(creationDate);

            contactBase.setCreated(simpleDateFormat.format(date));
        }

        if (root.path(OXContactsProperty.last_modified.name()).isLong()) {
            Long lastModified = root.path(
                    OXContactsProperty.last_modified.name()).asLong();
            Date date = new Date(lastModified);
            contactBase.setUpdated(simpleDateFormat.format(date));
        }

        if (root.path(OXContactsProperty.folder_id.name()).isInt()) {
            contactBase.setFolderId(root.path(
                    OXContactsProperty.folder_id.name()).asText());
        }

        if (root.path(OXContactsProperty.private_flag.name()).isBoolean()) {
            Boolean isPrivate = root.path(
                    OXContactsProperty.private_flag.name()).asBoolean();
            contactBase.setIsPrivate(isPrivate ? BooleanType.YES
                    : BooleanType.NO);
        } else {
            contactBase.setIsPrivate(BooleanType.NO);
        }

        if (root.path(OXContactsProperty.color_label.name()).isInt()) {
            contactBase.setColorLabel(root.path(
                    OXContactsProperty.color_label.name()).asInt());
        }
        
        if (root.path(OXContactsProperty.categories.name()).isTextual()) {
            contactBase.setCategories(root.path(
                    OXContactsProperty.categories.name()).asText());
        }
        if (root.path(OXContactsProperty.note.name()).isTextual()) {
            contactBase.setNotes(root.path(OXContactsProperty.note.name())
                    .asText());
        }
        
        StringBuilder customFields = new StringBuilder();
       
        try {
            if (root.path(OXContactsProperty.telephone_pager.name())
                    .isTextual()) {
                String pager = root.path(
                        OXContactsProperty.telephone_pager.name()).asText();
                String pagerEncoded = URLEncoder.encode(pager,
                        MxOSConstants.UTF8);
				if (!pagerEncoded.isEmpty()) {
					customFields.append(AddressBookProperty.pager.name())
							.append("=").append(pagerEncoded).append(",");
				}
            }

            if (root.path(OXContactsProperty.yomiFirstName.name()).isTextual()) {
                String yomiFirstName = root.path(
                        OXContactsProperty.yomiFirstName.name()).asText();
                String yomiFirstNameEncoded = URLEncoder.encode(yomiFirstName,
                        MxOSConstants.UTF8);
				if (!yomiFirstNameEncoded.isEmpty()) {
					customFields
							.append(OXContactsProperty.yomiFirstName.name())
							.append("=").append(yomiFirstNameEncoded)
							.append(",");
				}
            }

            if (root.path(OXContactsProperty.yomiLastName.name()).isTextual()) {
                String yomiLastName = root.path(
                        OXContactsProperty.yomiLastName.name()).asText();
                String yomiLastNameEncoded = URLEncoder.encode(yomiLastName,
                        MxOSConstants.UTF8);
				if (!yomiLastNameEncoded.isEmpty()) {
					customFields.append(OXContactsProperty.yomiLastName.name())
							.append("=").append(yomiLastNameEncoded)
							.append(",");
				}
            }

            if (root.path(OXContactsProperty.yomiCompany.name()).isTextual()) {
                String yomiCompany = root.path(
                        OXContactsProperty.yomiCompany.name()).asText();
                String yomiCompanyEncoded = URLEncoder.encode(yomiCompany,
                        MxOSConstants.UTF8);
				if (!yomiCompanyEncoded.isEmpty()) {
					customFields.append(OXContactsProperty.yomiCompany.name())
							.append("=").append(yomiCompanyEncoded).append(",");
				}
            }

            if (root.path(OXContactsProperty.email3.name()).isTextual()) {
                String email3 = root.path(OXContactsProperty.email3.name())
                        .asText();
                String email3Encoded = URLEncoder.encode(email3,
                        MxOSConstants.UTF8);
				if (!email3Encoded.isEmpty()) {
					customFields.append(OXContactsProperty.email3.name())
							.append("=").append(email3Encoded).append(",");
				}
            }

            if (root.path(OXContactsProperty.userfield19.name()).isTextual()) {
                String fileName = root.path(
                        OXContactsProperty.userfield19.name()).asText();
                String fileNameEncoded = URLEncoder.encode(fileName,
                        MxOSConstants.UTF8);
				if (!fileNameEncoded.isEmpty()) {
					customFields.append(AddressBookProperty.fileName.name())
							.append("=").append(fileNameEncoded).append(",");
				}
            }

            if (root.path(OXContactsProperty.telephone_other.name()).isTextual()) {
                String otherPhone = root.path(
                        OXContactsProperty.telephone_other.name()).asText();
                String otherPhoneEncoded = URLEncoder.encode(otherPhone,
                        MxOSConstants.UTF8);
				if (!otherPhoneEncoded.isEmpty()) {
					customFields.append(AddressBookProperty.otherPhone.name())
							.append("=").append(otherPhoneEncoded).append(",");
				}
            }
            
            if (root.path(OXContactsProperty.uid.name()).isTextual()) {
                String uid = root.path(OXContactsProperty.uid.name()).asText();
                String uidEncoded = URLEncoder.encode(uid, MxOSConstants.UTF8);
				if (!uidEncoded.isEmpty()) {
					customFields.append(OXContactsProperty.uid.name())
							.append("=").append(uidEncoded);
				}
            }
        } catch (UnsupportedEncodingException ex) {
            // this should never happen as UTF-8 will always be
            // supported
            logger.error("The URL encoder was unable to handle the UTF-8 encoding format");
            throw new RuntimeException(ex);
        }

        String custom = customFields.toString();
        if ((custom.length() > 0)
                && (custom.charAt(custom.length() - 1) == ',')) {
            custom = custom.substring(0, custom.length() - 1);
        }

        contactBase.setCustomFields(custom);

    }

    public static Image mapToContactImage(JsonNode root)
            throws AddressBookException {

        Image image = new Image();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (root.path(OXContactsProperty.last_modified.name()).isLong()) {
            Long lastModified = root.path(
                    OXContactsProperty.last_modified.name()).asLong();
            Date date = new Date(lastModified);
            image.setUpdated(simpleDateFormat.format(date));
        }
        // pre pend the host name with the image1_url
        if (root.path(OXContactsProperty.image1_url.name()).isTextual()) {
            String imageUrl = root.path(OXContactsProperty.image1_url.name())
                    .asText();
            imageUrl = new StringBuilder("http://")
                    .append(System.getProperty(SystemProperty.mxosHost.name()))
                    .append(":")
                    .append(System.getProperty(SystemProperty.mxosPort.name()))
                    .append("/mxos/addressBook/v2/")
                    .append(AddressBookProperty.userId.name())
                    .append("/")
                    .append("contacts/")
                    .append(String.valueOf(root.path(
                            OXContactsProperty.id.name()).asInt())).append("/")
                    .append("actualImage").toString();
            image.setImageUrl(imageUrl);
        }
        return image;
    }

    public static Name mapToContactName(JsonNode root) {
        Name name = new Name();
        mapToContactNameFields(root, name);

        return name;
    }

    private static void mapToContactNameFields(JsonNode root, Name name) {
        if (root.path(OXContactsProperty.first_name.name()).isTextual()) {
            name.setFirstName(root.path(OXContactsProperty.first_name.name())
                    .asText());
        }
        if (root.path(OXContactsProperty.second_name.name()).isTextual()) {
            name.setMiddleName(root.path(OXContactsProperty.second_name.name())
                    .asText());
        }
        if (root.path(OXContactsProperty.last_name.name()).isTextual()) {
            name.setLastName(root.path(OXContactsProperty.last_name.name())
                    .asText());
        }
        if (root.path(OXContactsProperty.display_name.name()).isTextual()) {
            name.setDisplayName(root.path(
                    OXContactsProperty.display_name.name()).asText());
        }
        if (root.path(OXContactsProperty.suffix.name()).isTextual()) {
            name.setSuffix(root.path(OXContactsProperty.suffix.name()).asText());
        }
        if (root.path(OXContactsProperty.title.name()).isTextual()) {
            name.setPrefix(root.path(OXContactsProperty.title.name()).asText());
        }
        if (root.path(OXContactsProperty.nickname.name()).isTextual()) {
            name.setNickName(root.path(OXContactsProperty.nickname.name())
                    .asText());
        }
    }

    public static PersonalInfo mapToContactPersonalInfo(JsonNode root) {
        PersonalInfo info = new PersonalInfo();

        if (root.path(OXContactsProperty.spouse_name.name()).isTextual()) {
            info.setSpouseName(root.path(OXContactsProperty.spouse_name.name())
                    .asText());
        }
        if (root.path(OXContactsProperty.marital_status.name()).isTextual()) {
            MaritalStatus maritalStatus = MaritalStatus.fromValue(root.path(
                    OXContactsProperty.marital_status.name()).asText());
            info.setMaritalStatus(maritalStatus);
        }

        info.setAddress(mapToContactPersonalInfoAddress(root));
        info.setCommunication(mapToContactPersonalInfoCommunication(root));
        info.setEvents(mapToContactsPersonalInfoEvents(root));
        return info;
    }

    /**
     * Map from Json node to Address.
     * 
     * @param root. *
     */
    public static Address mapToContactPersonalInfoAddress(JsonNode root) {
        Address address = new Address();

        if (root.path(OXContactsProperty.street_home.name()).isTextual()) {
            address.setStreet(root.path(OXContactsProperty.street_home.name())
                    .asText());
        }
        if (root.path(OXContactsProperty.city_home.name()).isTextual()) {
            address.setCity(root.path(OXContactsProperty.city_home.name())
                    .asText());
        }
        if (root.path(OXContactsProperty.state_home.name()).isTextual()) {
            address.setStateOrProvince(root.path(
                    OXContactsProperty.state_home.name()).asText());
        }
        if (root.path(OXContactsProperty.postal_code_home.name()).isTextual()) {
            address.setPostalCode(root.path(
                    OXContactsProperty.postal_code_home.name()).asText());
        }
        if (root.path(OXContactsProperty.country_home.name()).isTextual()) {
            address.setCountry(root
                    .path(OXContactsProperty.country_home.name()).asText());
        }

        return address;
    }

    /**
     * Map from Json node to Communication.
     * 
     * @param root root.
     */
    public static Communication mapToContactPersonalInfoCommunication(
            JsonNode root) {
        Communication communication = new Communication();

        if (root.path(OXContactsProperty.telephone_home1.name()).isTextual()) {
            communication.setPhone1(root.path(
                    OXContactsProperty.telephone_home1.name()).asText());
        } 
        if (root.path(OXContactsProperty.telephone_home2.name()).isTextual()) {
            communication.setPhone2(root.path(
                    OXContactsProperty.telephone_home2.name()).asText());
        } 
        if (root.path(OXContactsProperty.telephone_ip.name()).isTextual()) {
            communication.setVoip(root.path(
                    OXContactsProperty.telephone_ip.name()).asText());
        } 
        if (root.path(OXContactsProperty.cellular_telephone2.name()).isTextual()) {
            communication.setMobile(root.path(
                    OXContactsProperty.cellular_telephone2.name()).asText());
        } 
        if (root.path(OXContactsProperty.fax_home.name()).isTextual()) {
            communication.setFax(root.path(OXContactsProperty.fax_home.name())
                    .asText());
        }
        if (root.path(OXContactsProperty.email2.name()).isTextual()) {
			if (!root.path(OXContactsProperty.email2.name()).asText().isEmpty()) {
				communication.setEmail(root.path(
						OXContactsProperty.email2.name()).asText());
			}
        } 
        if (root.path(OXContactsProperty.instant_messenger2.name()).isTextual()) {
            communication.setImAddress(root.path(
                    OXContactsProperty.instant_messenger2.name()).asText());
        } 
        return communication;
    }

    public static List<Event> mapToContactsPersonalInfoEvents(JsonNode root) {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        List<Event> eventList = new ArrayList<Event>();
        Event birthday = new Event();
        if (root.path(OXContactsProperty.birthday.name()).isLong()) {
            birthday.setType(Event.Type.BIRTHDAY);

            Long birthDate = root.path(OXContactsProperty.birthday.name())
                    .asLong();
            Date date = new Date(birthDate);
            birthday.setDate(simpleDateFormat.format(date));
            eventList.add(birthday);
        }

        Event anniversary = new Event();
        if (root.path(OXContactsProperty.anniversary.name()).isLong()) {
            anniversary.setType(Event.Type.ANNIVERSARY);

            Long anniversaryDate = root.path(
                    OXContactsProperty.anniversary.name()).asLong();
            Date date = new Date(anniversaryDate);
            anniversary.setDate(simpleDateFormat.format(date));
            eventList.add(anniversary);
        }
        return eventList;

    }

    public static WorkInfo mapToContactWorkInfo(JsonNode root) {
        WorkInfo info = new WorkInfo();
        if (root.path(OXContactsProperty.profession.name()).isTextual()) {
            info.setProfession(root.path(OXContactsProperty.profession.name())
                    .asText());
        }
        if (root.path(OXContactsProperty.company.name()).isTextual()) {
            info.setCompanyName(root.path(OXContactsProperty.company.name())
                    .asText());
        }
        if (root.path(OXContactsProperty.department.name()).isTextual()) {
            info.setDepartment(root.path(OXContactsProperty.department.name())
                    .asText());
        }
        if (root.path(OXContactsProperty.position.name()).isTextual()) {
            info.setTitle(root.path(OXContactsProperty.position.name())
                    .asText());
        }
        if (root.path(OXContactsProperty.manager_name.name()).isTextual()) {
            info.setManager(root.path(OXContactsProperty.manager_name.name())
                    .asText());
        }
        if (root.path(OXContactsProperty.assistant_name.name()).isTextual()) {
            info.setAssistant(root.path(
                    OXContactsProperty.assistant_name.name()).asText());
        }
        if (root.path(OXContactsProperty.telephone_assistant.name()).isTextual()) {
            info.setAssistantPhone(root.path(
                    OXContactsProperty.telephone_assistant.name()).asText());
        }
        if (root.path(OXContactsProperty.employee_type.name()).isTextual()) {
            info.setEmployeeId(root.path(
                    OXContactsProperty.employee_type.name()).asText());
        }
        if (root.path(OXContactsProperty.url.name()).isTextual()) {
            info.setWebPage(root.path(OXContactsProperty.url.name()).asText());
        }
        info.setAddress(mapToContactWorkInfoAddress(root));
        info.setCommunication(mapToContactWorkInfoCommunication(root));
        return info;
    }

    public static Address mapToContactWorkInfoAddress(JsonNode root) {
        Address address = new Address();
        if (root.path(OXContactsProperty.street_business.name()).isTextual()) {
        address.setStreet(root.path(OXContactsProperty.street_business.name())
                .asText());
        }
        if (root.path(OXContactsProperty.city_business.name()).isTextual()) {
        address.setCity(root.path(OXContactsProperty.city_business.name())
                .asText());
        }
        if (root.path(OXContactsProperty.state_business.name()).isTextual()) {
        address.setStateOrProvince(root.path(
                OXContactsProperty.state_business.name()).asText());
        }
        if (root.path(OXContactsProperty.postal_code_business.name()).isTextual()) {
        address.setPostalCode(root.path(
                OXContactsProperty.postal_code_business.name()).asText());
        }
        if (root.path(OXContactsProperty.country_business.name()).isTextual()) {
        address.setCountry(root
                .path(OXContactsProperty.country_business.name()).asText());
        }
        if (root.path(OXContactsProperty.addressBusiness.name()).isTextual()) {
            address.setUnstructuredAddress(root
                    .path(OXContactsProperty.addressBusiness.name()).asText());
        }
        return address;
    }

    public static Communication mapToContactWorkInfoCommunication(JsonNode root) {
        Communication communication = new Communication();
        if (root.path(OXContactsProperty.telephone_business1.name())
                .isTextual()) {
            communication.setPhone1(root.path(
                    OXContactsProperty.telephone_business1.name()).asText());
        }
        if (root.path(OXContactsProperty.telephone_business2.name())
                .isTextual()) {
            communication.setPhone2(root.path(
                    OXContactsProperty.telephone_business2.name()).asText());
        }
        if (root.path(OXContactsProperty.cellular_telephone1.name())
                .isTextual()) {
            communication.setMobile(root.path(
                    OXContactsProperty.cellular_telephone1.name()).asText());
        }
        if (root.path(OXContactsProperty.fax_business.name()).isTextual()) {
            communication.setFax(root.path(
                    OXContactsProperty.fax_business.name()).asText());
        }
        if (root.path(OXContactsProperty.email1.name()).isTextual()) {
			if (!root.path(OXContactsProperty.email1.name()).asText().isEmpty()) {
				communication.setEmail(root.path(
						OXContactsProperty.email1.name()).asText());
			}
        }
        if (root.path(OXContactsProperty.instant_messenger1.name()).isTextual()) {
            communication.setImAddress(root.path(
                    OXContactsProperty.instant_messenger1.name()).asText());
        }
        return communication;
    }

    public static GroupBase mapToGroupBase(JsonNode root) {

        GroupBase groupBase = new GroupBase();

        mapToGroupBaseFields(root, groupBase);

        return groupBase;
    }

    private static void mapToGroupBaseFields(JsonNode root, GroupBase groupBase) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (root.path(OXContactsProperty.id.name()).isInt()) {
            groupBase.setGroupId(String.valueOf(root.path(
                    OXContactsProperty.id.name()).asInt()));
        }

        if (root.path(OXContactsProperty.display_name.name()) != null) {
            groupBase.setGroupName(root.path(
                    OXContactsProperty.display_name.name()).asText());
        }

        if (root.path(OXContactsProperty.creation_date.name()) != null) {
            Long creationDate = root.path(
                    OXContactsProperty.creation_date.name()).asLong();
            Date date = new Date(creationDate);

            groupBase.setCreated(simpleDateFormat.format(date));
        }

        if (root.path(OXContactsProperty.last_modified.name()) != null) {
            Long lastModified = root.path(
                    OXContactsProperty.last_modified.name()).asLong();
            Date date = new Date(lastModified);
            groupBase.setUpdated(simpleDateFormat.format(date));
        }
        
        if (root.path(OXContactsProperty.folder_id.name()).isInt()) {
            groupBase.setFolderId(root
                    .path(OXContactsProperty.folder_id.name()).asText());
        }

        StringBuilder customFields = new StringBuilder();
        if (root.path(OXContactsProperty.uid.name()).isTextual()) {
            String uid = root.path(OXContactsProperty.uid.name()).asText();
            customFields.append(OXContactsProperty.uid.name()).append("=")
                    .append(uid);
        }

        String custom = customFields.toString();
        groupBase.setCustomFields(custom);

    }

    public static void mapToMultipleContactsAndGroups(JsonNode root,
            List<String> contactIdList) throws AddressBookException {

        Iterator<JsonNode> arrayIter = root.getElements();
        String message = null;
        String category = null;
        String code = null;
        String errorId = null;
        boolean exceptionFound = false;
        int errorIndex = 0;
        int noOfErrors = 0;

        // index of the element
        int index = 0;

        while (arrayIter.hasNext()) {
            JsonNode tempArrayElement = arrayIter.next();

            if (!tempArrayElement.path(OXContactsProperty.data.name())
                    .isMissingNode()) {
                JsonNode temp = tempArrayElement.path(OXContactsProperty.data
                        .name());

                if (temp.path(OXContactsProperty.id.name()).isInt()) {
                    Integer id = temp.path(OXContactsProperty.id.name())
                            .asInt();
                    contactIdList.add(String.valueOf(id));
                }
            } else if (!tempArrayElement.path(OXContactsProperty.error.name())
                    .isMissingNode()) {

                int i = 0;
                JsonNode errorParams = tempArrayElement
                        .path(OXContactsProperty.error_params.name());
                Object[] parameters = new Object[errorParams.size()];

                for (JsonNode node : errorParams) {
                    parameters[i++] = node.asText();
                }

                if (tempArrayElement.path(OXContactsProperty.error.name())
                        .isTextual()) {
                    message = tempArrayElement.path(
                            OXContactsProperty.error.name()).asText();

                    message = String.format(message.replaceAll("\\$d", "\\$s"),
                            parameters);
                }
                
                if (tempArrayElement.path(OXContactsProperty.category.name())
                        .isTextual()) {
                    category = tempArrayElement.path(
                            OXContactsProperty.category.name()).asText();
                }
                if (tempArrayElement.path(OXContactsProperty.code.name())
                        .isTextual()) {
                    code = tempArrayElement
                            .path(OXContactsProperty.code.name()).asText();
                }
                if (tempArrayElement.path(OXContactsProperty.error_id.name())
                        .isTextual()) {
                    errorId = tempArrayElement.path(
                            OXContactsProperty.error_id.name()).asText();
                }
                exceptionFound = true;
                errorIndex = index;
                noOfErrors++;
            }

            index++;
        }

        if (exceptionFound) {
            throw new AddressBookException("Creation of row : " + errorIndex
                    + " failed with " + message, category, code, errorId);
        }

    }
}
