/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.credentials;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update password.
 * 
 * @author mxos-dev
 */
public class SetPasswordRecoveryAllowed implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetPasswordRecoveryAllowed.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetPasswordRecoveryAllowed action start."));
        }
        String passRecoveryAllowed = requestState.getInputParams()
                .get(MailboxProperty.passwordRecoveryAllowed.name()).get(0);
        if (passRecoveryAllowed != null) {
            if (passRecoveryAllowed.equalsIgnoreCase(MxosEnums.BooleanType.YES
                    .name())) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(
                                requestState,
                                MailboxProperty.passwordRecoveryAllowed,
                                Integer.toString(MxosEnums.BooleanType.YES
                                        .ordinal()));
            } else if (passRecoveryAllowed
                    .equalsIgnoreCase(MxosEnums.BooleanType.NO.name())) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(
                                requestState,
                                MailboxProperty.passwordRecoveryAllowed,
                                Integer.toString(MxosEnums.BooleanType.NO
                                        .ordinal()));
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_PASSWORD_RECOVERY_ALLOWED
                                .name());
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetPasswordRecoveryAllowed action end."));
        }
    }
}
