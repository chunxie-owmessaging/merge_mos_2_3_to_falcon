/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ox.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

import com.opwvmsg.mxos.backend.crud.ITasksCRUD;
import com.opwvmsg.mxos.backend.crud.exception.ComponentException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.backend.crud.ox.OXAbstractHttpCRUD;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.JsonToTasksMapper;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.LoginResponse;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.ParticipantResponse;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.RecurrenceResponse;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.Response;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.TaskDetailsResponse;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.TaskFolderResponse;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.TaskResponse;
import com.opwvmsg.mxos.backend.crud.ox.response.tasks.UserResponse;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.ExternalProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.opwvmsg.mxos.data.enums.OXTasksProperty;
import com.opwvmsg.mxos.data.enums.TasksDBTypes;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.error.TasksError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.task.pojos.Folder;
import com.opwvmsg.mxos.task.pojos.Participant;
import com.opwvmsg.mxos.task.pojos.Details;
import com.opwvmsg.mxos.task.pojos.Recurrence;
import com.opwvmsg.mxos.task.pojos.Task;
import com.opwvmsg.mxos.task.pojos.TaskBase;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Class to implement methods OX specific methods for communicating and parsing
 * the response
 * 
 * @author mxos-dev
 * 
 */
public class OXTasksCRUD extends OXAbstractHttpCRUD implements ITasksCRUD {

    enum SortOrder {
        ascending("asc"), descending("desc");

        private final String value;

        SortOrder(String value) {
            this.value = value;
        }

        public String convert() {
            return value;
        }
    }

    private final String TASKS_STRING = "tasks";
    private final String GET_STRING = "get";
    private final String ALL_STRING = "all";
    private final String UPDATE_STRING = "update";
    private final String DELETE_STRING = "delete";
    private final String FOLDER_STRING = "folders";
    private final String NEW_STRING = "new";
    private static final String TASKS_BASE_COLUMNS = "1,20,200,309";
    private static final String FOLDERS_COLUMNS="1,308,302,300,301";
    private static final String LINE_REGEXP = "\\r?\\n";
    private final String MULTIPLE_STRING = "multiple";
    private final String TREE_STRING = "1";

    /**
     * Constructor.
     * 
     * @param baseURL - base url of mxos
     */
    public OXTasksCRUD(String baseURL) {
        webResource = getWebResource(baseURL);
    }

    @Override
    public String confirmTask(MxOSRequestState mxosRequestState)
            throws TasksException {
        return null;
    }

    @Override
    public List<String> createMultipleTasks(MxOSRequestState mxosRequestState)
            throws TasksException {
        Map<String, List<String>> inputParam = mxosRequestState
                .getInputParams();

        String input = inputParam.get(TasksProperty.tasksList.name()).get(0);
        int size = input.split(LINE_REGEXP).length;

        List<Map<String, String>> attributesList = ((OXTasksBackendState) mxosRequestState
                .getBackendState().get(TasksDBTypes.ox.name())).attributesList;
        final String subUrl = MULTIPLE_STRING;

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(inputParam.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3
                .add(inputParam.get(TasksProperty.cookieString.name()).get(0));

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();
        paramsNew.put(ExternalProperty.session.name(), paramList2);
        paramsNew.put(ExternalProperty.cookieString.name(), paramList3);
        ObjectMapper objMapper = new ObjectMapper();
        ArrayNode arrNode = objMapper.createArrayNode();
        
        addHeaderUserId(mxosRequestState, paramsNew);

        List<String> taskIds = new ArrayList<String>();
        try {
            for (int i = 0; i < size; i++) {
                Map<String, String> attribute = attributesList.get(i);
                JsonToTasksMapper.mapFromMultipleTask(attribute,
                        getFolderId(inputParam), arrNode, objMapper);
            }
            final ClientResponse r = put(subUrl, paramsNew,
                    objMapper.writeValueAsString(arrNode));
            new TaskResponse().getMultipleTaskId(r, taskIds);

        } catch (final TasksException e) {
            deleteMultipleTasks(mxosRequestState, taskIds);
            throw new TasksException(e);
        } catch (final Exception e) {
            throw new TasksException(e);
        }

        return taskIds;
    }

    private void deleteMultipleTasks(MxOSRequestState mxosRequestState,
            List<String> taskIds) throws TasksException {

        Map<String, List<String>> inputParam = mxosRequestState
                .getInputParams();

        final String subUrl = MULTIPLE_STRING;

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(inputParam.get(TasksProperty.sessionId.name())
                .get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(inputParam.get(TasksProperty.cookieString.name())
                .get(0));

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();
        paramsNew.put(ExternalProperty.session.name(), paramList2);
        paramsNew.put(ExternalProperty.cookieString.name(), paramList3);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        ArrayNode arrNode = mapper.createArrayNode();

        try {
            for (String id : taskIds) {
                JsonToTasksMapper.mapFromMultipleTask(id,
                        getFolderId(inputParam), arrNode, mapper);
            }
            final ClientResponse r = put(subUrl, paramsNew,
                    mapper.writeValueAsString(arrNode));
            Response.validateResponse(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }

    }

    @Override
    public String createTask(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(NEW_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(TasksProperty.cookieString.name()).get(
                0));

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(ExternalProperty.session.name(), paramList2);
        paramsNew.put(ExternalProperty.cookieString.name(), paramList3);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {

            Map<String, String> attributes = ((OXTasksBackendState) mxosRequestState
                    .getBackendState().get(TasksDBTypes.ox.name())).attributes;

            String jsonString = JsonToTasksMapper.mapFromTaskBase(
                    attributes, getFolderId(params));

            final ClientResponse r = put(subUrl, paramsNew, jsonString);
            TaskBase taskBase = new TaskResponse().getTaskBase(r);
            return taskBase.getTaskId();
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public void deleteTask(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(DELETE_STRING);
        // Add timestamp
        final List<String> paramList2 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList2.add(updateTime.toString());

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.timestamp.name(), paramList2);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {
            final List<String> taskIds = new ArrayList<String>();
            taskIds.add(params.get(TasksProperty.taskId.name()).get(0));
            String jsonString = JsonToTasksMapper.mapFromTaskIds(
                    taskIds, getFolderId(params));

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }

    }

    @Override
    public void deleteTasks(MxOSRequestState mxosRequestState, final List<String> taskIds)
            throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(DELETE_STRING);
        // Add timestamp
        final List<String> paramList2 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList2.add(updateTime.toString());

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.timestamp.name(), paramList2);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {
            
            String jsonString = JsonToTasksMapper.mapFromTaskIds(
                    taskIds, getFolderId(params));

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }

    }

    private ClientResponse get(final String subURL,
            final Map<String, List<String>> params) throws MxOSException,
            TasksException {
        try {
            return getOX(subURL, params);
        } catch (ComponentException e) {
            throw new TasksException(e);
        }
    }

    private String getFolderId(Map<String, List<String>> params) {
        String folderId = MxOSConstants.OX_DEFAULT_TASKS_FOLDER_ID;
        if (params.get(TasksProperty.folderId.name()) != null
                && !params.get(TasksProperty.folderId.name()).get(0)
                        .isEmpty()) {
            folderId = params.get(TasksProperty.folderId.name()).get(0);
        }
        return folderId;
    }

    @Override
    protected ExternalSession getSessionFromResponse(final ClientResponse r)
            throws TasksException {
        return new LoginResponse().getSession(r);
    }

    @Override
    protected String getUserIdFromResponse(ClientResponse resp)
            throws TasksException {
        return new UserResponse().getUserId(resp);
    }

    @Override
    public ExternalSession login(MxOSRequestState mxosRequestState)
            throws TasksException {
        try {
            return loginOX(mxosRequestState);
        } catch (ComponentException e) {
            throw new TasksException(e);
        }
    }

    @Override
    public void logout(MxOSRequestState mxosRequestState) throws TasksException {
        try {
            logoutOX(mxosRequestState);
        } catch (ComponentException e) {
            throw new TasksException(e);
        }
    }

    private ClientResponse put(final String subURL,
            final Map<String, List<String>> params, final String jsonString)
            throws MxOSException, TasksException {
        try {
            return putOX(subURL, params, jsonString);
        } catch (ComponentException e) {
            throw new TasksException(e);
        }
    }

    @Override
    public List<String> readAllTaskIds(MxOSRequestState mxosRequestState)
            throws TasksException {

        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(ALL_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(TASKS_BASE_COLUMNS);

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(getFolderId(params));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.columns.name(), paramList2);
        paramsNew.put(OXTasksProperty.folder.name(), paramList3);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new TaskResponse().getAllTaskIds(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public List<String> readAllFolderIds(MxOSRequestState mxosRequestState)
            throws TasksException {

        final String subUrl = FOLDER_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add("allVisible");

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(FOLDERS_COLUMNS);

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(getFolderId(params));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.columns.name(), paramList2);
        //paramsNew.put(OXTasksProperty.folder.name(), paramList3);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        final List<String> paramList7 = new ArrayList<String>();
        if (params.get(TasksProperty.module.name()).get(0)
                .equalsIgnoreCase("task"))
            paramList7.add(TASKS_STRING);
        else if (params.get(TasksProperty.module.name()).get(0)
                .equalsIgnoreCase("addressBook"))
            paramList7.add("contacts");
        paramsNew.put("content_type", paramList7);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new TaskFolderResponse().getAllFolderIds(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public Task readTask(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(TasksProperty.taskId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.folder.name(), paramList2);
        paramsNew.put(OXTasksProperty.id.name(), paramList3);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new TaskResponse().getTask(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }
    
    @Override
    public List<Task> readAllTasks(MxOSRequestState mxosRequestState)
            throws TasksException {


        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(ALL_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(TASKS_BASE_COLUMNS);

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(getFolderId(params));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.columns.name(), paramList2);
        paramsNew.put(OXTasksProperty.folder.name(), paramList3);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new TaskResponse().getAllTasks(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }    
    }

    @Override
    public List<TaskBase> listTaskBase(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = TASKS_STRING;
        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(ALL_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(TASKS_BASE_COLUMNS);

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(getFolderId(params));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.columns.name(), paramList2);
        paramsNew.put(OXTasksProperty.folder.name(), paramList3);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new TaskResponse().getListTaskBase(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }    
    }

    @Override
    public TaskBase readTasksBase(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(TasksProperty.taskId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.folder.name(), paramList2);
        paramsNew.put(OXTasksProperty.id.name(), paramList3);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new TaskResponse().getTaskBase(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public  Participant readTasksParticipant(MxOSRequestState requestState)
    throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> paramsMandatory = requestState.getInputParams();
        Map<String, List<String>> paramsOptional = new HashMap<String, List<String>>();

        final List<String> actionParamList = new ArrayList<String>();
        actionParamList.add(GET_STRING);

        final List<String> folderParamList = new ArrayList<String>();
        folderParamList.add(getFolderId(paramsMandatory));

        final List<String> taskIdParamList = new ArrayList<String>();
        taskIdParamList.add(paramsMandatory.get(TasksProperty.taskId.name()).get(0));

        final List<String> participantIdParamList = new ArrayList<String>();
        participantIdParamList.add(paramsMandatory.get(TasksProperty.participantId.name()).get(0));

        // Add timestamp
        final List<String> timeStampParamList = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        timeStampParamList.add(updateTime.toString());

        final List<String> sessionIdParamList = new ArrayList<String>();
        sessionIdParamList.add(paramsMandatory.get(TasksProperty.sessionId.name()).get(0));

        final List<String> cookieParamList = new ArrayList<String>();
        cookieParamList.add(paramsMandatory.get(TasksProperty.cookieString.name()).get(0));

        final List<String> utcParamList = new ArrayList<String>();
        utcParamList.add("UTC");

        paramsOptional.put(ACTION_STRING, actionParamList);
        paramsOptional.put(OXTasksProperty.folder.name(), folderParamList);
        paramsOptional.put(OXTasksProperty.id.name(), taskIdParamList);
        paramsOptional.put(OXTasksProperty.userId.name(), participantIdParamList);
        paramsOptional.put(OXContactsProperty.timestamp.name(), timeStampParamList);
        paramsOptional.put(OXTasksProperty.session.name(), sessionIdParamList);
        paramsOptional.put(TasksProperty.cookieString.name(), cookieParamList);
        paramsOptional.put(OXTasksProperty.timezone.name(), utcParamList);
        
        addHeaderUserId(requestState, paramsOptional);

        try {
            final ClientResponse r = get(subUrl, paramsOptional);

            String participantEmail = requestState.getInputParams()
			.get(TasksProperty.participantId.name()).get(0);
			boolean memberExist = false;
			List<Participant> memberList = new ParticipantResponse().getAllParticipants(r);;
			Participant retParticipant=null;
			for (Participant participant : memberList ) {
				if (participant.getParticipantEmail().equalsIgnoreCase(participantEmail)) {
					requestState.getDbPojoMap().setProperty(
							MxOSPOJOs.participant, participant);
					memberExist = true;
					retParticipant = participant;
				}
			}

			if (!memberExist) {
				throw new TasksException(
						TasksError.TSK_PARTICIPANT_MEMBER_NOT_FOUND.name(),
						ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
						TasksError.TSK_PARTICIPANT_MEMBER_NOT_FOUND.name(), "");
			}
			return retParticipant;
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public Details readTasksDetails(MxOSRequestState requestState)
    throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> params = requestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(TasksProperty.taskId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.folder.name(), paramList2);
        paramsNew.put(OXTasksProperty.id.name(), paramList3);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        
        addHeaderUserId(requestState, paramsNew);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new TaskDetailsResponse().getTaskDetails(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public Recurrence readTasksRecurrence(MxOSRequestState requestState)
    throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> paramsMandatory = requestState.getInputParams();
        Map<String, List<String>> paramsOptional = new HashMap<String, List<String>>();

        final List<String> actionParamList = new ArrayList<String>();
        actionParamList.add(GET_STRING);

        final List<String> folderParamList = new ArrayList<String>();
        folderParamList.add(getFolderId(paramsMandatory));

        final List<String> taskIdParamList = new ArrayList<String>();
        taskIdParamList.add(paramsMandatory.get(TasksProperty.taskId.name()).get(0));

        // Add timestamp
        final List<String> timeStampParamList = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        timeStampParamList.add(updateTime.toString());

        final List<String> sessionIdParamList = new ArrayList<String>();
        sessionIdParamList.add(paramsMandatory.get(TasksProperty.sessionId.name()).get(0));

        final List<String> cookieParamList = new ArrayList<String>();
        cookieParamList.add(paramsMandatory.get(TasksProperty.cookieString.name()).get(0));

        final List<String> utcParamList = new ArrayList<String>();
        utcParamList.add("UTC");

        paramsOptional.put(ACTION_STRING, actionParamList);
        paramsOptional.put(OXTasksProperty.folder.name(), folderParamList);
        paramsOptional.put(OXTasksProperty.id.name(), taskIdParamList);
        paramsOptional.put(OXContactsProperty.timestamp.name(), timeStampParamList);
        paramsOptional.put(OXTasksProperty.session.name(), sessionIdParamList);
        paramsOptional.put(TasksProperty.cookieString.name(), cookieParamList);
        paramsOptional.put(OXTasksProperty.timezone.name(), utcParamList);
        
        addHeaderUserId(requestState, paramsOptional);

        try {
            final ClientResponse r = get(subUrl, paramsOptional);
            return new RecurrenceResponse().getRecurrence(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public void rollback() throws ApplicationException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateTasksBase(MxOSRequestState requestState)
    throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> paramsMandatory = requestState.getInputParams();
        Map<String, List<String>> paramsOptional = new HashMap<String, List<String>>();

        final List<String> actionParamList = new ArrayList<String>();
        actionParamList.add(UPDATE_STRING);

        final List<String> folderParamList = new ArrayList<String>();
        folderParamList.add(getFolderId(paramsMandatory));

        final List<String> taskIdParamList = new ArrayList<String>();
        taskIdParamList.add(paramsMandatory.get(TasksProperty.taskId.name()).get(0));

        // Add timestamp
        final List<String> timeStampParamList = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        timeStampParamList.add(updateTime.toString());

        final List<String> sessionIdParamList = new ArrayList<String>();
        sessionIdParamList.add(paramsMandatory.get(TasksProperty.sessionId.name()).get(0));

        final List<String> cookieParamList = new ArrayList<String>();
        cookieParamList.add(paramsMandatory.get(TasksProperty.cookieString.name()).get(0));

        final List<String> utcParamList = new ArrayList<String>();
        utcParamList.add("UTC");

        paramsOptional.put(ACTION_STRING, actionParamList);
        paramsOptional.put(OXTasksProperty.folder.name(), folderParamList);
        paramsOptional.put(OXTasksProperty.id.name(), taskIdParamList);
        paramsOptional.put(OXContactsProperty.timestamp.name(), timeStampParamList);
        paramsOptional.put(OXTasksProperty.session.name(), sessionIdParamList);
        paramsOptional.put(TasksProperty.cookieString.name(), cookieParamList);
        paramsOptional.put(OXTasksProperty.timezone.name(), utcParamList);
        
        addHeaderUserId(requestState, paramsOptional);

        try {
            Map<String, String> attributes = ((OXTasksBackendState) requestState
                    .getBackendState().get(TasksDBTypes.ox.name())).attributes;

            String jsonString = JsonToTasksMapper.mapFromTaskBase(
                    attributes, getFolderId(paramsMandatory));

            final ClientResponse resp = put(subUrl, paramsOptional, jsonString);
            Response.validateResponse(resp);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public void updateTasksExternalParticipant(MxOSRequestState requestState)
            throws TasksException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateTasksParticipant(MxOSRequestState requestState)
    throws TasksException {
        final String subUrl = TASKS_STRING;
        Map<String, List<String>> paramsMandatory = requestState.getInputParams();
        Map<String, List<String>> paramsOptional = new HashMap<String, List<String>>();

        final List<String> actionParamList = new ArrayList<String>();
        actionParamList.add(UPDATE_STRING);

        final List<String> folderParamList = new ArrayList<String>();
        folderParamList.add(getFolderId(paramsMandatory));

        final List<String> taskIdParamList = new ArrayList<String>();
        taskIdParamList.add(paramsMandatory.get(TasksProperty.taskId.name()).get(0));

        final List<String> participantIdParamList = new ArrayList<String>();
        taskIdParamList.add(paramsMandatory.get(TasksProperty.participantId.name()).get(0));

        // Add timestamp
        final List<String> timeStampParamList = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        timeStampParamList.add(updateTime.toString());

        final List<String> sessionIdParamList = new ArrayList<String>();
        sessionIdParamList.add(paramsMandatory.get(TasksProperty.sessionId.name()).get(0));

        final List<String> cookieParamList = new ArrayList<String>();
        cookieParamList.add(paramsMandatory.get(TasksProperty.cookieString.name()).get(0));

        final List<String> utcParamList = new ArrayList<String>();
        utcParamList.add("UTC");

        paramsOptional.put(ACTION_STRING, actionParamList);
        paramsOptional.put(OXTasksProperty.folder.name(), folderParamList);
        paramsOptional.put(OXTasksProperty.id.name(), taskIdParamList);
        paramsOptional.put(OXTasksProperty.userId.name(), participantIdParamList);
        paramsOptional.put(OXContactsProperty.timestamp.name(), timeStampParamList);
        paramsOptional.put(OXTasksProperty.session.name(), sessionIdParamList);
        paramsOptional.put(TasksProperty.cookieString.name(), cookieParamList);
        paramsOptional.put(OXTasksProperty.timezone.name(), utcParamList);
        
        addHeaderUserId(requestState, paramsOptional);

        try {
            Map<String, String> attributes = ((OXTasksBackendState) requestState
                    .getBackendState().get(TasksDBTypes.ox.name())).attributes;

            String jsonString = JsonToTasksMapper.mapFromTaskParticipant(
                    attributes, getFolderId(paramsMandatory));

            final ClientResponse resp = put(subUrl, paramsOptional, jsonString);
            Response.validateResponse(resp);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public void updateTasksProgress(MxOSRequestState requestState)
            throws TasksException {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateTasksRecurrence(MxOSRequestState requestState)
    throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> paramsMandatory = requestState.getInputParams();
        Map<String, List<String>> paramsOptional = new HashMap<String, List<String>>();

        final List<String> actionParamList = new ArrayList<String>();
        actionParamList.add(UPDATE_STRING);

        final List<String> folderParamList = new ArrayList<String>();
        folderParamList.add(getFolderId(paramsMandatory));

        final List<String> taskIdParamList = new ArrayList<String>();
        taskIdParamList.add(paramsMandatory.get(TasksProperty.taskId.name()).get(0));

        // Add timestamp
        final List<String> timeStampParamList = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        timeStampParamList.add(updateTime.toString());

        final List<String> sessionIdParamList = new ArrayList<String>();
        sessionIdParamList.add(paramsMandatory.get(TasksProperty.sessionId.name()).get(0));

        final List<String> cookieParamList = new ArrayList<String>();
        cookieParamList.add(paramsMandatory.get(TasksProperty.cookieString.name()).get(0));

        final List<String> utcParamList = new ArrayList<String>();
        utcParamList.add("UTC");

        paramsOptional.put(ACTION_STRING, actionParamList);
        paramsOptional.put(OXTasksProperty.folder.name(), folderParamList);
        paramsOptional.put(OXTasksProperty.id.name(), taskIdParamList);
        paramsOptional.put(OXContactsProperty.timestamp.name(), timeStampParamList);
        paramsOptional.put(OXTasksProperty.session.name(), sessionIdParamList);
        paramsOptional.put(TasksProperty.cookieString.name(), cookieParamList);
        paramsOptional.put(OXTasksProperty.timezone.name(), utcParamList);
        
        addHeaderUserId(requestState, paramsOptional);

        try {
            Map<String, String> attributes = ((OXTasksBackendState) requestState
                    .getBackendState().get(TasksDBTypes.ox.name())).attributes;

            String jsonString = JsonToTasksMapper.mapFromTaskRecurrence(
                    attributes, getFolderId(paramsMandatory));

            final ClientResponse resp = put(subUrl, paramsOptional, jsonString);
            Response.validateResponse(resp);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public void validateUser(MxOSRequestState mxosRequestState)
            throws TasksException {
        try {
            validateUserOX(mxosRequestState);
        } catch (ComponentException e) {
            throw new TasksException(e);
        }
    }
    
    public void createTaskParticipant(MxOSRequestState requestState)
    throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> paramsMandatory = requestState.getInputParams();
        Map<String, List<String>> paramsOptional = new HashMap<String, List<String>>();

        final List<String> actionParamList = new ArrayList<String>();
        actionParamList.add(UPDATE_STRING);

        final List<String> folderParamList = new ArrayList<String>();
        folderParamList.add(getFolderId(paramsMandatory));

        final List<String> taskIdParamList = new ArrayList<String>();
        taskIdParamList.add(paramsMandatory.get(TasksProperty.taskId.name()).get(0));

        // Add timestamp
        final List<String> timeStampParamList = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        timeStampParamList.add(updateTime.toString());

        final List<String> sessionIdParamList = new ArrayList<String>();
        sessionIdParamList.add(paramsMandatory.get(TasksProperty.sessionId.name()).get(0));

        final List<String> cookieParamList = new ArrayList<String>();
        cookieParamList.add(paramsMandatory.get(TasksProperty.cookieString.name()).get(0));

        final List<String> utcParamList = new ArrayList<String>();
        utcParamList.add("UTC");

        paramsOptional.put(ACTION_STRING, actionParamList);
        paramsOptional.put(OXTasksProperty.folder.name(), folderParamList);
        paramsOptional.put(OXTasksProperty.id.name(), taskIdParamList);
        paramsOptional.put(OXContactsProperty.timestamp.name(), timeStampParamList);
        paramsOptional.put(OXTasksProperty.session.name(), sessionIdParamList);
        paramsOptional.put(TasksProperty.cookieString.name(), cookieParamList);
        paramsOptional.put(OXTasksProperty.timezone.name(), utcParamList);
        
        addHeaderUserId(requestState, paramsOptional);

        try {
            Map<String, String> attributes = ((OXTasksBackendState) requestState
                    .getBackendState().get(TasksDBTypes.ox.name())).attributes;

            String jsonString = JsonToTasksMapper.mapFromTaskParticipant(
                    attributes, getFolderId(paramsMandatory));

            final ClientResponse resp = put(subUrl, paramsOptional, jsonString);
            Response.validateResponse(resp);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }
    
    @Override
    public List<Participant> readAllTasksParticipants(MxOSRequestState requestState)
    throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> paramsMandatory = requestState.getInputParams();
        Map<String, List<String>> paramsOptional = new HashMap<String, List<String>>();

        final List<String> actionParamList = new ArrayList<String>();
        actionParamList.add(GET_STRING);

        final List<String> folderParamList = new ArrayList<String>();
        folderParamList.add(getFolderId(paramsMandatory));

        final List<String> taskIdParamList = new ArrayList<String>();
        taskIdParamList.add(paramsMandatory.get(TasksProperty.taskId.name()).get(0));

        final List<String> participantBaseColumns = new ArrayList<String>();
        participantBaseColumns.add(TASKS_BASE_COLUMNS);

        // Add timestamp
        final List<String> timeStampParamList = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        timeStampParamList.add(updateTime.toString());

        final List<String> sessionIdParamList = new ArrayList<String>();
        sessionIdParamList.add(paramsMandatory.get(TasksProperty.sessionId.name()).get(0));

        final List<String> cookieParamList = new ArrayList<String>();
        cookieParamList.add(paramsMandatory.get(TasksProperty.cookieString.name()).get(0));

        final List<String> utcParamList = new ArrayList<String>();
        utcParamList.add("UTC");

        paramsOptional.put(ACTION_STRING, actionParamList);
        paramsOptional.put(OXTasksProperty.folder.name(), folderParamList);
        paramsOptional.put(OXTasksProperty.id.name(), taskIdParamList);
        paramsOptional.put(OXContactsProperty.timestamp.name(), timeStampParamList);
        paramsOptional.put(OXTasksProperty.session.name(), sessionIdParamList);
        paramsOptional.put(TasksProperty.cookieString.name(), cookieParamList);
        paramsOptional.put(OXTasksProperty.timezone.name(), utcParamList);
        
        addHeaderUserId(requestState, paramsOptional);

        try {
            final ClientResponse r = get(subUrl, paramsOptional);
            return new ParticipantResponse().getAllParticipants(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }
    
    @Override
    public String createTaskFolder(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = FOLDER_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(NEW_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(TasksProperty.cookieString.name()).get(
                0));

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(ExternalProperty.session.name(), paramList2);
        paramsNew.put(ExternalProperty.cookieString.name(), paramList3);
        final List<String> paramList4 = new ArrayList<String>();
        if (params.get(TasksProperty.module.name()) != null) {
            if(params.get(TasksProperty.module.name()).get(0).equalsIgnoreCase("task"))
                paramList4.add(TASKS_STRING);
            else if (params.get(TasksProperty.module.name()).get(0).equalsIgnoreCase("addressBook"))
                paramList4.add("contacts");
            
        } 
        
        paramsNew.put(TasksProperty.module.name(), paramList4);
        
        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add(getFolderId(params));

        paramsNew.put(OXTasksProperty.folder_id.name(), paramList7);
        final List<String> paramList8 = new ArrayList<String>();
        Boolean autorename = true;
        paramList8.add(autorename.toString());
        paramsNew.put(TasksProperty.autorename.name(), paramList8);
        final List<String> paramList9 = new ArrayList<String>();
        paramList9.add(TREE_STRING);
        paramsNew.put(TasksProperty.tree.name(), paramList9);
        
        addHeaderUserId(mxosRequestState, paramsNew);
        
        try {

            Map<String, String> attributes = ((OXTasksBackendState) mxosRequestState
                    .getBackendState().get(TasksDBTypes.ox.name())).attributes;

            String jsonString = JsonToTasksMapper.mapFromTaskFolder(
                    attributes, getFolderId(params));
            final ClientResponse r = put(subUrl, paramsNew, jsonString);
            Folder folder = new TaskFolderResponse().getTaskFolder(r);
            return folder.getFolderId();
            
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public Folder readTasksFolder(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = FOLDER_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(GET_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(getFolderId(params));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(TasksProperty.folderId.name()).get(0));

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.folder.name(), paramList2);
        paramsNew.put(OXTasksProperty.id.name(), paramList3);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            return new TaskFolderResponse().getTaskFolder(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public void deleteTaskFolder(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = FOLDER_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(DELETE_STRING);
        // Add timestamp
        final List<String> paramList2 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList2.add(updateTime.toString());

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.timestamp.name(), paramList2);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add(params.get(TasksProperty.folderId.name()).get(0));
        paramsNew.put(OXTasksProperty.folder_id.name(), paramList7);
        
        addHeaderUserId(mxosRequestState, paramsNew);
        
        try {
            final List<String> foldersIds = new ArrayList<String>();
            foldersIds.add(params.get(TasksProperty.folderId.name()).get(0));
           
            String jsonString = JsonToTasksMapper.mapFromFolderIds(
                    foldersIds);

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }

        
    }
    
    @Override
    public void deleteAllTaskFolder(MxOSRequestState mxosRequestState, final List<String> folderIds)
            throws TasksException {
        final String subUrl = FOLDER_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(DELETE_STRING);
        // Add timestamp
        final List<String> paramList2 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList2.add(updateTime.toString());

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.timestamp.name(), paramList2);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        
        addHeaderUserId(mxosRequestState, paramsNew);
             
        try {
            String jsonString = JsonToTasksMapper.mapFromFolderIds(
                    folderIds);
            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
            // Cleanup the Default Folder
            List<String> res = readAllTaskIds(mxosRequestState);
            if (res != null && res.size() > 0)
            {
                if(params.get(TasksProperty.module.name()).get(0).equalsIgnoreCase("task"))
                deleteTasks(mxosRequestState, res);
            }
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }

        
    }
    
    @Override
    public void deleteTaskParticipant(MxOSRequestState mxosRequestState)
    throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> paramsMandatory = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);
        // Add timestamp
        final List<String> paramList2 = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        paramList2.add(updateTime.toString());
        
        final List<String> taskIdParamList = new ArrayList<String>();
        taskIdParamList.add(paramsMandatory.get(TasksProperty.taskId.name()).get(0));
        
        final List<String> folderParamList = new ArrayList<String>();
        folderParamList.add(getFolderId(paramsMandatory));

        final List<String> paramListSessionId = new ArrayList<String>();
        paramListSessionId.add(paramsMandatory.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(paramsMandatory.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.folder.name(), folderParamList);
        paramsNew.put(OXTasksProperty.id.name(), taskIdParamList);
        paramsNew.put(OXTasksProperty.timestamp.name(), paramList2);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        paramsNew.put(OXTasksProperty.session.name(), paramListSessionId);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        
        addHeaderUserId(mxosRequestState, paramsNew);
        
        try {
            List<Participant> memberList = readAllTasksParticipants(mxosRequestState);

            String participantEmail = mxosRequestState.getInputParams()
            .get(TasksProperty.participantId.name()).get(0);
            boolean memberExist = false;
            Iterator<Participant> it = memberList.iterator();
            
            while(it.hasNext()){
                Participant participant = it.next();
                if (participant.getParticipantEmail().equalsIgnoreCase(participantEmail)) {
                    it.remove();
                    memberExist = true;
                }
            }
            
            if (!memberExist) {
                throw new TasksException(
                        TasksError.TSK_PARTICIPANT_MEMBER_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        TasksError.TSK_PARTICIPANT_MEMBER_NOT_FOUND.name(), "");
            }

            List<Map> list = new ArrayList<Map>();
            Map<String, Object> map = new HashMap<String, Object>();

            for (Participant participant : memberList) {
                Map<String, String> map1 = new HashMap<String, String>();
                if(null != participant.getParticipantEmail()){
                    map1.put(OXTasksProperty.mail.name(),participant.getParticipantEmail());
                    map1.put(OXTasksProperty.id.name(),participant.getParticipantEmail());
                }
                if(null != participant.getParticipantType()){
                    map1.put(OXTasksProperty.type.name(),String.valueOf(participant.getParticipantType().ordinal()));
                }
                if(null != participant.getParticipantName()){
                    map1.put(OXTasksProperty.name.name(),participant.getParticipantName());
                }
                list.add(map1);
            }

            map.put("participants",list);
            map.put(OXTasksProperty.folder_id.name(), getFolderId(paramsMandatory));
                
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writeValueAsString(map);

            ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }

    @Override
    public void updateTasksDetails(MxOSRequestState requestState)
            throws TasksException {
        final String subUrl = TASKS_STRING;

        Map<String, List<String>> paramsMandatory = requestState.getInputParams();
        Map<String, List<String>> paramsOptional = new HashMap<String, List<String>>();

        final List<String> actionParamList = new ArrayList<String>();
        actionParamList.add(UPDATE_STRING);

        final List<String> folderParamList = new ArrayList<String>();
        folderParamList.add(getFolderId(paramsMandatory));

        final List<String> taskIdParamList = new ArrayList<String>();
        taskIdParamList.add(paramsMandatory.get(TasksProperty.taskId.name()).get(0));

        // Add timestamp
        final List<String> timeStampParamList = new ArrayList<String>();
        Long updateTime = System.nanoTime();
        timeStampParamList.add(updateTime.toString());

        final List<String> sessionIdParamList = new ArrayList<String>();
        sessionIdParamList.add(paramsMandatory.get(TasksProperty.sessionId.name()).get(0));

        final List<String> cookieParamList = new ArrayList<String>();
        cookieParamList.add(paramsMandatory.get(TasksProperty.cookieString.name()).get(0));

        final List<String> utcParamList = new ArrayList<String>();
        utcParamList.add("UTC");

        paramsOptional.put(ACTION_STRING, actionParamList);
        paramsOptional.put(OXTasksProperty.folder.name(), folderParamList);
        paramsOptional.put(OXTasksProperty.id.name(), taskIdParamList);
        paramsOptional.put(OXContactsProperty.timestamp.name(), timeStampParamList);
        paramsOptional.put(OXTasksProperty.session.name(), sessionIdParamList);
        paramsOptional.put(TasksProperty.cookieString.name(), cookieParamList);
        paramsOptional.put(OXTasksProperty.timezone.name(), utcParamList);
        
        addHeaderUserId(requestState, paramsOptional);

        try {
            Map<String, String> attributes = ((OXTasksBackendState) requestState
                    .getBackendState().get(TasksDBTypes.ox.name())).attributes;

            String jsonString = JsonToTasksMapper.mapFromTaskDetails(
                    attributes, getFolderId(paramsMandatory));

            final ClientResponse resp = put(subUrl, paramsOptional, jsonString);
            Response.validateResponse(resp);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
    }
    
    @Override
    public List<Folder> listTasksFolder(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = FOLDER_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add("allVisible");

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(FOLDERS_COLUMNS);

        final List<String> paramList4 = new ArrayList<String>();
        paramList4.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList5 = new ArrayList<String>();
        paramList5.add(params.get(TasksProperty.cookieString.name()).get(0));

        final List<String> paramList6 = new ArrayList<String>();
        paramList6.add("UTC");

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(OXTasksProperty.columns.name(), paramList2);
        paramsNew.put(OXTasksProperty.session.name(), paramList4);
        paramsNew.put(TasksProperty.cookieString.name(), paramList5);
        paramsNew.put(OXTasksProperty.timezone.name(), paramList6);
        final List<String> paramList7 = new ArrayList<String>();
        if (params.get(TasksProperty.module.name()).get(0)
                .equalsIgnoreCase("task"))
            paramList7.add(TASKS_STRING);
        else if (params.get(TasksProperty.module.name()).get(0)
                .equalsIgnoreCase("addressBook"))
            paramList7.add("contacts");
        paramsNew.put("content_type", paramList7);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {
            final ClientResponse r = get(subUrl, paramsNew);
            
            return new TaskFolderResponse().getAllFolders(r);
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }  
    }

    @Override
    public void updateTasksFolder(MxOSRequestState mxosRequestState)
            throws TasksException {
        final String subUrl = FOLDER_STRING;

        Map<String, List<String>> params = mxosRequestState.getInputParams();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        final List<String> paramList1 = new ArrayList<String>();
        paramList1.add(UPDATE_STRING);

        final List<String> paramList2 = new ArrayList<String>();
        paramList2.add(params.get(TasksProperty.sessionId.name()).get(0));

        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(params.get(TasksProperty.cookieString.name()).get(
                0));

        paramsNew.put(ACTION_STRING, paramList1);
        paramsNew.put(ExternalProperty.session.name(), paramList2);
        paramsNew.put(ExternalProperty.cookieString.name(), paramList3);
        final List<String> paramList4 = new ArrayList<String>();
        if (params.get(TasksProperty.module.name()) != null) {
            paramList4.add(params.get(TasksProperty.module.name()).get(0));
        } else {
            paramList4.add(TASKS_STRING);
        }
        paramsNew.put(TasksProperty.module.name(), paramList4);
        
        final List<String> paramList7 = new ArrayList<String>();
        paramList7.add(getFolderId(params));

        paramsNew.put(OXTasksProperty.id.name(), paramList7);
        final List<String> paramList8 = new ArrayList<String>();
        paramList8.add(TREE_STRING);
        paramsNew.put(TasksProperty.tree.name(), paramList8);
        
        addHeaderUserId(mxosRequestState, paramsNew);
        
        try {

            Map<String, String> attributes = ((OXTasksBackendState) mxosRequestState
                    .getBackendState().get(TasksDBTypes.ox.name())).attributes;

            String jsonString = JsonToTasksMapper.mapFromTaskFolder(
                    attributes, getFolderId(params));
            final ClientResponse resp = put(subUrl, paramsNew, jsonString);
            Response.validateResponse(resp);
            
        } catch (final TasksException e) {
            throw e;
        } catch (final Exception e) {
            throw new TasksException(e);
        }
        
    }
}
