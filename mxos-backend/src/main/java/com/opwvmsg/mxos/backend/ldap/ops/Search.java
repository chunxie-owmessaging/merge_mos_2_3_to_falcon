/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.ldap.ops;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import com.opwvmsg.mxos.backend.ldap.LDAPMapper;

/**
 * Class to execute LDAP search operation
 * 
 * @author mxos-dev
 * 
 */
public class Search extends Operation {
    // for 3 different parameter search methods
    public static final int SEARCH_CONDITION_1 = 1;
    public static final int SEARCH_CONDITION_2 = 2;
    public static final int SEARCH_CONDITION_3 = 3;

    // search condition
    private String name;
    private Attributes matchingAttributes;
    private String[] attributesToReturn;
    private String filter;
    private SearchControls cons;
    private String filterExpr;
    private Object[] filterArgs;

    // search result
    private NamingEnumeration<SearchResult> results;

    // serch type
    private int searchType;

    public Search(String name, Attributes matchingAttributes,
            String[] attributesToReturn) {
        this.name = name;
        this.matchingAttributes = matchingAttributes;
        this.attributesToReturn = attributesToReturn;
        this.searchType = SEARCH_CONDITION_1;
    }

    public Search(String name, String filter, SearchControls cons) {
        this.name = name;
        this.filter = filter;
        this.cons = cons;
        this.searchType = SEARCH_CONDITION_2;
    }

    public Search(String name, String filterExpr, Object[] filterArgs,
            SearchControls cons) {
        this.name = name;
        this.filterExpr = filterExpr;
        this.filterArgs = filterArgs;
        this.cons = cons;
        this.searchType = SEARCH_CONDITION_3;
    }

    protected void invoke() throws NamingException {
        switch (searchType) {
        case 1:
            this.results = ldapContext.search(name, matchingAttributes,
                    attributesToReturn);
            break;
        case 2:
            this.results = ldapContext.search(name, filter, cons);
            break;
        case 3:
            this.results = ldapContext.search(name, filterExpr, filterArgs,
                    cons);
            break;
        default:
            throw new RuntimeException("Unsupport search type: " + searchType);
        }
    }

    public NamingEnumeration<SearchResult> getResults() {
        return results;
    }

    public <T> List<T> getResults(LDAPMapper<T> mapper) throws NamingException {
        List<T> pojos = new ArrayList<T>();
        if (results != null) {
            while (results.hasMore()) {
                SearchResult result = results.next();
                Attributes attrs = result.getAttributes();
                T pojo = mapper.mapping(attrs);
                pojos.add(pojo);
            }
        }
        return pojos;
    }

}
