/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave/mxos/service/MySQLMetaConnectionPool.java#2 $
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicInteger;

import javax.naming.directory.DirContext;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Implementation of LDAP CRUD connection pool.
 * 
 * @author mxos-dev
 */
public class LdapSearchConnectionPool implements ICRUDPool<IMailboxCRUD> {

    protected static Logger logger = Logger
            .getLogger(LdapSearchConnectionPool.class);
    
    protected GenericObjectPool<LdapMailboxCRUD> objPool;
    protected static Hashtable<String, String> env;
    protected int maxSize = 10;
    protected static int activeHostIndex = 0;
    private LdapSearchCRUDFactory ldapSearchCRUDFactory;
    protected static int numHosts = 0;
    protected static String[] hosts;
    protected static String[] urls;
    protected final AtomicInteger counter = new AtomicInteger(0);
    protected final int ldapMinOperations;
    protected final boolean switchLdapFeature;
    protected final AtomicInteger failbackCounter = new AtomicInteger(0);
    protected final int failbackMinOperations;
    protected final boolean failback;

    /**
     * Constructor.
     * 
     * @throws Exception Exception.
     */
    public LdapSearchConnectionPool() throws MxOSException {
        env = new Hashtable<String, String>();
        env = getConfigParams();
        this.maxSize = MxOSConfig.getLdapPoolMaxSize();
        loadPool(env, maxSize);
        initializeJMXStats(maxSize);
        ldapMinOperations = Integer.parseInt(System.getProperty(
                SystemProperty.switchLdapHostAfterMinOperations.name(), "0"));
        if (ldapMinOperations > 0) {
            switchLdapFeature = true;
        } else {
            switchLdapFeature = false;
        }
        failbackMinOperations = Integer.parseInt(System.getProperty(
                SystemProperty.ldapFailbackMinOperations.name(), "0"));
        if (failbackMinOperations > 0) {
            failback = true;
        } else {
            failback = false;
        }
        logger.info("Loaded LdapConnectionPool...");
    }
    
    
    /**
     * Method to load Pool.
     */
    protected void loadPool(Hashtable<String, String> env, int maxSize) {
        if (logger.isDebugEnabled()) {
            logger.debug("creating search connection pool having maxsize :"
                    + maxSize);
        }
        ldapSearchCRUDFactory = new LdapSearchCRUDFactory();
        objPool = new GenericObjectPool<LdapMailboxCRUD>(ldapSearchCRUDFactory,
                maxSize);
        objPool.setMaxIdle(-1);
        objPool.setTestOnBorrow(true);
        logger.debug("LdapSearchConnectionPool:loadPool Exited");
    }


    @Override
    public IMailboxCRUD borrowObject() throws MxOSException {
        IMailboxCRUD obj = null;
        logger.debug(new StringBuilder("Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
        if (objPool.getNumActive() >= maxSize - 2) {
            logger.warn("LDAP Search active connections reaching max...:"
                    + objPool.getNumActive());
        }
        try {
            if (switchLdapFeature && counter.incrementAndGet() >= 
                    (ldapMinOperations + RandomUtils.nextInt(ldapMinOperations))) {
                logger.info("Switching to next available LDAP host");
                getActiveLdapHost();
                counter.set(0);
            }
            if (!switchLdapFeature && failback && !env.get(DirContext.PROVIDER_URL).equals(urls[0]) &&
                    failbackCounter.incrementAndGet() >= failbackMinOperations) {
                logger.info("Failback to original LDAP host");
                if (urls.length > 0) env.put(DirContext.PROVIDER_URL, urls[0]);
                if (hosts.length > 0) env.put(MxOSConfig.PROVIDER_HOST,
                        hosts[0].split(MxOSConstants.COLON)[0]);
                activeHostIndex = 1;
                failbackCounter.set(0);
            }
            obj = objPool.borrowObject();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("# Error while borrowing object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }

        if (obj == null) {
            logger.error("Borrowed object is null.");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name());
        }
        incrementJMXStats();
        return obj;
    }


    protected void initializeJMXStats(long count) {
        ConnectionStats.LDAPSEARCH.setCount(count);
        if (logger.isDebugEnabled()) {
            logger.debug("JMX stats initialized. Count: " + count);
        }
    }

    protected void incrementJMXStats() {
        if (logger.isDebugEnabled()) {
            logger.debug("Incrementing ACTIVE_LDAPSEARCH stats");
        }
        ConnectionStats.ACTIVE_LDAPSEARCH.increment();
    }

    protected void decrementJMXStats() {
        if (logger.isDebugEnabled()) {
            logger.debug("Decrementing ACTIVE_LDAPSEARCH stats");
        }
        ConnectionStats.ACTIVE_LDAPSEARCH.decrement();
    }


    @Override
    public void returnObject(IMailboxCRUD provisionCRUD) throws MxOSException {
        try {
            LdapMailboxCRUD obj = (LdapMailboxCRUD) provisionCRUD; 
            obj.releaseResources();
            objPool.returnObject(obj);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while return object object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        logger.debug(new StringBuilder("Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
    }

    @Override
    public void resetPool() throws MxOSException {
        if (null != objPool) {
            objPool.clear();
            logger.info("# Clearing the pools..");
        }
    }
    
    /**
     * loadLdapHosts
     */
    protected void loadLdapHosts() {
        String ldap = "ldap://";
        hosts = MxOSConfig.getLdapSearchServers();
        // Consider Master LDAP server to use for Search APIs
        // if there is no
        if (hosts == null || hosts.length == 0) {
            logger.warn("Default LDAP Server hosts not found, loading Master"
                    + " LDAP for SearchConnectionPool...");
            hosts = MxOSConfig.getLdapServers();
        }

        if (numHosts == 0) {
            numHosts = hosts.length;
        }

        urls = new String[hosts.length];
        for (int i = 0; i < hosts.length; i++) {
            if (!hosts[i].startsWith(ldap)) {
                urls[i] = ldap + hosts[i];
            } else {
                urls[i] = hosts[i];
            }
        }
    }

    /**
     * Reads the Configuration Parameters and store in the env.
     * 
     * @return
     */
    protected Hashtable<String, String> getConfigParams() {

        String defaultHost = MxOSConfig.getDefaultHost();
        String defaultDirServer = MxOSConfig.getDefaultDirectoryServer();

        String userDn = MxOSConfig.getString(defaultHost, defaultDirServer,
                MxOSConfig.Keys.DEFAULT_BIND_DN,
                MxOSConfig.getDefault(MxOSConfig.Keys.DEFAULT_BIND_DN));

        String password = MxOSConfig.getString(defaultHost, defaultDirServer,
                MxOSConfig.Keys.DEFAULT_BIND_PASSWORD,
                MxOSConfig.getDefault(MxOSConfig.Keys.DEFAULT_BIND_PASSWORD));

        loadLdapHosts();
        getActiveLdapHost();

        env.put(DirContext.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(DirContext.SECURITY_AUTHENTICATION, "simple");
        env.put(DirContext.SECURITY_PRINCIPAL, userDn);
        env.put(DirContext.SECURITY_CREDENTIALS, password);
        System.setProperty("com.sun.jndi.ldap.connect.pool", "false");
        if (logger.isDebugEnabled()) {
            logger.debug("ldap Connect timeout:"+MxOSConfig.getLdapConnectTimeout());
        }
        env.put("com.sun.jndi.ldap.read.timeout",String.valueOf(MxOSConfig.getLdapReadTimeout()));
        env.put("com.sun.jndi.ldap.connect.timeout",String.valueOf(MxOSConfig.getLdapConnectTimeout()));
        return env;
    }

    /**
     * Gets the LDAP URL of the active Host
     */
    protected static void getActiveLdapHost() {
        if(logger.isDebugEnabled()) {
            logger.debug("LdapConnectionPool:getLdapUrl");
            logger.debug("activeHostIndex value is : " + activeHostIndex);
            logger.debug("Ldap url is : " + urls[activeHostIndex]);
        }
        env.put(DirContext.PROVIDER_URL, urls[activeHostIndex]);
        env.put(MxOSConfig.PROVIDER_HOST,
                hosts[activeHostIndex].split(MxOSConstants.COLON)[0]);
        activeHostIndex++;
        if (activeHostIndex >= hosts.length) {
            activeHostIndex = 0 ;
        }       
    }
    
    /**
     * @return the numHosts
     */
    public static int getNumHosts() {
        return numHosts;
    }

    /**
     * @return the env
     */
    public static Hashtable<String, String> getEnv() {
        return env;
    }

}
