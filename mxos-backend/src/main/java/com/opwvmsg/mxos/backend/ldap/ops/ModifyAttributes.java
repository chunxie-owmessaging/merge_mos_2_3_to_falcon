/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.ldap.ops;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.ModificationItem;

/**
 * Class to execute LDAP modify operation
 * 
 * @author mxos-dev
 * 
 */
public class ModifyAttributes extends Operation {

    // for 3 different parameter modify methods
    public static final int MODIFY_CONDITION_1 = 1;
    public static final int MODIFY_CONDITION_2 = 2;

    // modify condition
    private String name;
    private int mod_op;
    private Attributes attrs;
    private ModificationItem[] mods;

    // modify type
    private int modifyType;

    public ModifyAttributes(String name, int mod_op, Attributes attrs) {
        this.name = name;
        this.mod_op = mod_op;
        this.attrs = attrs;
        this.modifyType = MODIFY_CONDITION_1;
    }

    public ModifyAttributes(String name, ModificationItem[] mods) {
        this.name = name;
        this.mods = mods;
        this.modifyType = MODIFY_CONDITION_2;
    }

    protected void invoke() throws NamingException {
        switch (modifyType) {
        case 1:
            ldapContext.modifyAttributes(name, mod_op, attrs);
            break;
        case 2:
            ldapContext.modifyAttributes(name, mods);
            break;
        default:
            throw new RuntimeException("Unsupport modify type: " + modifyType);
        }

    }

}
