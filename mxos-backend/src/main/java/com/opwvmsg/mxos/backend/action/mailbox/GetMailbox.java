/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;

/**
 * Action class to get Mailbox object.
 * 
 * @author mxos-dev
 * 
 */
public class GetMailbox implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetMailbox.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetMailbox action start."));
        }

        final long t0 = Stats.startTimer();
        StatStatus statStatus = StatStatus.fail;

        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        try {
            // preprocess
            //
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxSearchCRUD();

            final long t1 = Stats.startTimer();
            StatStatus borrowStatus = StatStatus.fail;
            try {
                mailboxCRUD = mailboxCRUDPool.borrowObject();
                borrowStatus = StatStatus.pass;
            } finally {
                Stats.stopTimer(ServiceEnum.LdapSearchObjAcquireTime,
                        Operation.GET, t1, borrowStatus);
            }

            // execution
            //
            final String uid = requestState.getInputParams()
                    .get(MailboxProperty.uid.name()).get(0);

            final Mailbox mailbox = mailboxCRUD.readMailbox(requestState, uid);
            
            // put the result 
            //
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.mailbox, mailbox);

            statStatus = StatStatus.pass;

        } finally {
            Stats.stopTimer(ServiceEnum.ActionGetMailboxTime, Operation.GET,
                    t0, statStatus);

            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                mailboxCRUDPool.returnObject(mailboxCRUD);
            }
        }

    }
}
