/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.webmailfeatures.addressbook;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set WebMailFeatures.addressbook::SetMaxContactsPerPage.
 *
 * @author mxos-dev
 */
public class SetMaxContactsPerPage implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetMaxContactsPerPage.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetMaxContactsPerPage action start."));
        }
        try {
            String maxContactsPP = requestState.getInputParams()
                    .get(MailboxProperty.maxContactsPerPage.name()).get(0);
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.maxContactsPerPage, maxContactsPP);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set maxContactsPerGroup.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_MAX_CONTACTS_PER_PAGE.name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetMaxContactsPerPage action end."));
        }
    }
}
