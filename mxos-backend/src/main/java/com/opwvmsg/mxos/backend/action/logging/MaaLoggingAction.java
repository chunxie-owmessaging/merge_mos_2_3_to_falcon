/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.logging;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Abstract Class to make MAA call.
 * 
 * @author mxos-dev
 * 
 */
public abstract class MaaLoggingAction implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(MaaLoggingAction.class);
    public static final String ZERO = "0";
    public static final String UTF_ENCODING = "UTF-8";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String ELEMENT_RESULT = "RESULT";
    public static final String ATTR_CODE = "code";

    public static final String ARG1 = "arg1";
    public static final String ARG2 = "arg2";
    public static final String ARG3 = "arg3";
    public static final String ARG4 = "arg4";
    public static final String ARG5 = "arg5";
    public static final String ARG6 = "arg6";
    public static final String ARG7 = "arg7";
    public static final String ARG8 = "arg8";
    public static final String ARG9 = "arg9";
    public static final String ARG10 = "arg10";
    public static final String ARG11 = "arg11";
    public static final String ARG12 = "arg12";
    public static final String ARG13 = "arg13";
    public static final String ARG14 = "arg14";

    public static final String ARG_COS_ID = "cosid";
    public static final String ARG_STATUS = "status";
    public static final String ARG_MSISDN = "msisdn";
    public static final String ARG_ALIAS_USER = "aliasuser";
    public static final String ARG_ALIAS_DOMAIN = "aliasdomain";
    public static final String ARG_EXPIRE_DAYS_PREF = "expiredayspref";
    public static final String ARG_APP_LOGIN = "applogin";
    public static final String ARG_APP_PASSWORD = "apppassword";

    public static final String CREATE_ACCOUNT_LOGGING_SUB_URL = "newAccount2.cgi";
    public static final String DELETE_ACCOUNT_LOGGING_SUB_URL = "delAccount1.cgi";
    public static final String ADD_ALIAS_LOGGING_SUB_URL = "changeAccountAddAlias1.cgi";
    public static final String UPDATE_ALIAS_LOGGING_SUB_URL = "changeAccountUpdateAlias1.cgi";
    public static final String DELETE_ALIAS_LOGGING_SUB_URL = "changeAccountDelAlias1.cgi";
    public static final String UPDATE_PASSWORD_LOGGING_SUB_URL = "changeAccountPassword1.cgi";
    public static final String UPDATE_OPTIONS_PREF_LOGGING_SUB_URL = "changeAccountOptionsPref1.cgi";
    public static final String UPDATE_STATUS_LOGGING_SUB_URL = "changeAccountStatus1.cgi";
    public static final String UPDATE_FORWARDING_LOGGING_SUB_URL = "changeAccountForward2.cgi";
    public static final String OUT_OF_OFFICE_LOGGING_SUB_URL = "OutOfOfficeSetReply.cgi";

    /**
     * Action to make call to MAA.
     * 
     * @param model model.
     * @throws Exception exception
     */
    public abstract void run(final MxOSRequestState requestState)
            throws MxOSException;

    /**
     * Method to make Http Call to MAA.
     * 
     * @param subURL subURL
     * @param params map of params.
     * @return LoggingResponseBean LoggingResponseBean
     */
    public LoggingResponseBean callMaa(String subURL, Map<String, String> params)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Start : callMaa");
            logger.debug("SubURL : " + subURL);
        }
        ICRUDPool<HttpClient> pool = null;
        HttpClient client = null;
        HttpPost httpPost = null;
        String maaUrl = MxOSConfig.getMailApiAdaptorUrl();
        LoggingResponseBean maaResponseBean = null;
        try {
            pool = MxOSApp.getInstance().getMAAConnectionPool();
            if (pool == null) {
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                        "MAA is currently disabled.");
            }
            client = pool.borrowObject();
            httpPost = new HttpPost(maaUrl + "/" + subURL);
            httpPost.addHeader(CONTENT_TYPE,
                    "application/x-www-form-urlencoded; charset="
                            + UTF_ENCODING);
            String appLogin = MxOSConfig.getMailApiAdaptorLogin();
            String appPassword = MxOSConfig.getMailApiAdaptorPassword();
            if (logger.isDebugEnabled()) {
                logger.debug("applogin = " + appLogin);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("URI to be called :  " + httpPost.getURI());
            }
            List<BasicNameValuePair> nvps = new ArrayList<BasicNameValuePair>();
            nvps.add(new BasicNameValuePair(ARG_APP_LOGIN, appLogin));
            nvps.add(new BasicNameValuePair(ARG_APP_PASSWORD, appPassword));
            for (Entry<String, String> entry : params.entrySet()) {
                if (logger.isDebugEnabled()) {
                    logger.debug(entry.getKey() + " = " + entry.getValue());
                }
                nvps.add(new BasicNameValuePair(entry.getKey(), entry
                        .getValue()));
            }
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
            HttpResponse response = client.execute(httpPost);
            int returnCode = response.getStatusLine().getStatusCode();
            if (logger.isDebugEnabled()) {
                logger.debug("return Code: " + returnCode);
            }

            BufferedReader bufferRead = null;
            if (returnCode == HttpStatus.SC_OK) {
                bufferRead = new BufferedReader(new InputStreamReader(response
                        .getEntity().getContent()));
                String xmlLine = null;
                StringBuffer sb = new StringBuffer();
                while (((xmlLine = bufferRead.readLine()) != null)) {
                    sb.append(xmlLine);
                }
                logger.info("MAA Response = " + sb);
                maaResponseBean = parseResponse(sb);
                if (maaResponseBean != null) {
                    String rc = maaResponseBean.getCode();
                    if (rc != null && rc.equals(ZERO)) {
                        logger.info("MAA Result : SUCCESS (" + rc + ")");
                    } else {
                        logger.info("MAA Result : FAIL (" + rc + ")");
                    }
                }
            }
        } catch (MxOSException e) {
            logger.error(e);
            ConnectionErrorStats.MAA.increment();
            throw e;
        } catch (Exception e) {
            logger.error(e);
            ConnectionErrorStats.MAA.increment();
        } finally {
            if (httpPost != null) {
                httpPost.releaseConnection();
            }
            if (pool != null) {
                try {
                    pool.returnObject(client);
                } catch (MxOSException e) {
                    logger.warn("Error in finally clause while returing "
                            + "Provisioning CRUD pool.", e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("End : callMaa");
        }
        return maaResponseBean;
    }

    /**
     * Method to parse the MAA response.
     * 
     * @param xmlContent xmlContent
     * @return LoggingResponseBean LoggingResponseBean
     */
    public LoggingResponseBean parseResponse(StringBuffer xmlContent) {
        LoggingResponseBean maaResponseBean = null;
        if (xmlContent == null || xmlContent.equals("")) {
            return maaResponseBean;
        }
        maaResponseBean = new LoggingResponseBean();
        maaResponseBean.setFullMessage(xmlContent);
        String content = xmlContent.toString();
        try {
            InputStream is = new ByteArrayInputStream(
                    content.getBytes(UTF_ENCODING));

            XMLInputFactory xmlif = XMLInputFactory.newInstance();
            XMLStreamReader xmlr = xmlif.createXMLStreamReader(is);
            if (logger.isDebugEnabled()) {
                logger.debug("Got the xml stream reader : " + xmlr);
            }
            int eventType = 0;

            while (xmlr.hasNext()) {
                eventType = xmlr.next();
                switch (eventType) {
                case XMLEvent.END_DOCUMENT:
                    break;
                case XMLEvent.START_ELEMENT:
                    String name = xmlr.getLocalName();
                    logger.debug("Element name : " + name);
                    if (ELEMENT_RESULT.equals(name) && maaResponseBean != null) {
                        String attrCodeValue = xmlr.getAttributeValue(null,
                                ATTR_CODE);
                        logger.debug("Attribute code value is : "
                                + attrCodeValue);
                        maaResponseBean.setCode(attrCodeValue);
                        String elementValue = xmlr.getElementText();
                        logger.debug("Element value : " + elementValue);
                        maaResponseBean.setMessage(elementValue);
                    }
                    break;
                default:
                    break;
                }
            }
        } catch (UnsupportedEncodingException e) {
            logger.error(e);
        } catch (Exception e) {
            logger.error(e);
        }
        return maaResponseBean;
    }

    /**
     * Analyze the maa response and log the exception in case of error.
     * 
     * @param maaResponse maaResponse
     * @throws MxOSException mxos exception
     */
    public void analyzeLoggingResponse(LoggingResponseBean maaResponse)
            throws MxOSException {
        if (maaResponse != null) {
            String resultCode = maaResponse.getCode();
            logger.info("Result code " + resultCode);
            if (resultCode != null) {
                if (resultCode.equals(ZERO)) {
                    logger.info("Result code recived is zero: success");
                } else {
                    Exception e = new Exception(maaResponse.getFullMessage()
                            .toString());
                    logger.error(
                            "The MaaResponse code is : "
                                    + maaResponse.getCode(), e);
                    throw new MxOSException(
                            MailboxError.MBX_MAA_RESPONSE_ERROR.name(), e);
                }
            }
        } else {
            logger.error("Error while parsing MAA Response (empty)");
            throw new MxOSException(MailboxError.MBX_MAA_RESPONSE_ERROR.name());
        }
    }
}
