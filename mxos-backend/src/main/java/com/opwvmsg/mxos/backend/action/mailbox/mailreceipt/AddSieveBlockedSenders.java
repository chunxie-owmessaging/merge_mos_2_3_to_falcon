/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to add sieve filters blocked senders.
 * 
 * @author mxos-dev
 */
public class AddSieveBlockedSenders implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(AddAllowedSendersList.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "AddSieveBlockedSenders action start."));
        }
        try {
            List<String> newSieveFilterBlockedSender = new ArrayList<String>();
            if (null != requestState.getInputParams().get(
                    MailboxProperty.blockedSender.name())) {
                newSieveFilterBlockedSender = requestState.getInputParams()
                        .get(MailboxProperty.blockedSender.name());
            } else if (null != requestState.getInputParams().get(
                    MailboxProperty.sieveFilterBlockedSender.name())) {
                newSieveFilterBlockedSender = requestState.getInputParams()
                        .get(MailboxProperty.sieveFilterBlockedSender.name());
            }

            List<String> blockedSendersList = (List<String>) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.sieveFiltersBlockedSendersList);

            if (null == blockedSendersList) {
                blockedSendersList = new ArrayList<String>();
            }
            if ((blockedSendersList.size() + newSieveFilterBlockedSender.size())
                    > MxOSConfig.getMaxSieveFiltersBlockedSenders()) {
                throw new InvalidRequestException(
                        MailboxError.MBX_SIEVE_BLOCKED_SENDERS_REACHED_MAX_LIMIT
                                .name());
            }
            for (String sieveFilterBlockedSender : newSieveFilterBlockedSender) {
                if (!blockedSendersList.contains(sieveFilterBlockedSender
                        .toLowerCase())) {
                    blockedSendersList.add(sieveFilterBlockedSender
                            .toLowerCase());
                }
            }
            final String[] allowedSendersListToArray = blockedSendersList
                    .toArray(new String[blockedSendersList.size()]);

            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.blockedSenders,
                            allowedSendersListToArray);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while add sieve blocked senders.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SIEVE_BLOCKED_SENDERS_CREATE
                            .name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "AddSieveBlockedSenders action end."));
        }
    }
}
