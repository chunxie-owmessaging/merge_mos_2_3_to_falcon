/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.cos;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Backend Service to COS main object to perform Create and Delete of COS.
 * 
 * @author mxos-dev
 */
public class BackendCosService implements ICosService {

    private static Logger logger = Logger.getLogger(BackendCosService.class);

    /**
     * Default Constructor.
     */
    public BackendCosService() {
        logger.info("BackendCosService created...");
    }

    @Override
    public void create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        /*
         * TODO: Cleanup GC issue because of String concat.
         */
        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.CosService, Operation.PUT);

            ActionUtils.setBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.CosService, Operation.PUT, t0,
                    StatStatus.pass);
            return;
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.CosService, Operation.PUT, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        /*
         * TODO: Cleanup GC issue because of String concat.
         */
        long t0 = Stats.startTimer();
        try {

            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.CosService, Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.CosService, Operation.DELETE, t0,
                    StatStatus.pass);
            return;
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.CosService, Operation.DELETE, t0,
                    StatStatus.fail);
            throw e;
        }
    }
}
