/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.tasks;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.common.IAppsFolderService;
import com.opwvmsg.mxos.task.pojos.Folder;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Tasks folder service exposed to client which is responsible for doing basic
 * task related activities e.g create folder, delete folder, etc.
 * 
 * @author mxos-dev
 */
public class BackendAppsFolderService implements IAppsFolderService {

    private static Logger logger = Logger.getLogger(BackendAppsFolderService.class);

    
    /**
     * Default Constructor.
     */
    public BackendAppsFolderService() {
        logger.info("BackendAppsFolderService created...");
    }

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.AppsFolderService, Operation.PUT);

            ActionUtils.setBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.AppsFolderService, Operation.PUT, t0,
                    StatStatus.pass);
            long fId = Long.parseLong(mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.folderId));
            return fId;
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.AppsFolderService, Operation.PUT, t0,
                    StatStatus.fail);
            throw e;
        }
    }

   
    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.AppsFolderService,
                    Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.AppsFolderService, Operation.DELETE,
                    t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.AppsFolderService, Operation.DELETE,
                    t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.AppsFolderService,
                    Operation.DELETEALL);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.AppsFolderService,
                    Operation.DELETEALL, t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.AppsFolderService,
                    Operation.DELETEALL, t0, StatStatus.fail);
            throw e;
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Folder> list(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.AppsFolderService, Operation.LIST);
            ActionUtils.setTasksBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.AppsFolderService, Operation.LIST, t0,
                    StatStatus.pass);

            return ((List<Folder>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.allFolders));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.AppsFolderService, Operation.LIST, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public Folder read(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.AppsFolderService,
                    Operation.GET);
            ActionUtils.setTasksBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.AppsFolderService, Operation.GET,
                    t0, StatStatus.pass);

            return ((Folder) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.taskFolder));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.AppsFolderService, Operation.GET,
                    t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.AppsFolderService,
                    Operation.POST);
            ActionUtils.setTasksBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.AppsFolderService,
                    Operation.POST, t0, StatStatus.pass);

        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.AppsFolderService,
                    Operation.POST, t0, StatStatus.fail);
            throw e;
        }
        
    }


}
