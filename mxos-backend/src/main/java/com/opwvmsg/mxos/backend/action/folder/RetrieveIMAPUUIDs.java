/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.folder;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.CRUDProtocol;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;

/**
 * Action class to get MailboxInfo and FolderInfo object.
 * 
 * @author mxos-dev
 */
public class RetrieveIMAPUUIDs implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(RetrieveIMAPUUIDs.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException  {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("RetrieveIMAPUUIDs action start."));
        }

        CRUDProtocol protocol = CRUDUtils.getCRUDProtocol(requestState);
        RmeDataModel rmeDataModel = (protocol == CRUDProtocol.httprme) ? RmeDataModel.Leopard : RmeDataModel.CL;
        if (rmeDataModel == RmeDataModel.Leopard) {
            // IMAPListUIDs is required for all the list operations of stateless RME
            ICRUDPool<IMetaCRUD> metaCRUDPool = null;
            IMetaCRUD metaCRUD = null;
            try {
                metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
                metaCRUD = metaCRUDPool.borrowObject();
                metaCRUD.listMessageUUIDs(requestState);
            } catch (final MxOSException e) {
                if (metaCRUD != null) {
                    metaCRUD.rollback();
                }
                throw e;
            }catch (final IntermailException e) {
                if (metaCRUD != null) {
                    metaCRUD.rollback();
                }
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            } finally {
                if (metaCRUDPool != null && metaCRUD != null) {
                    try {
                        metaCRUDPool.returnObject(metaCRUD);
                    } catch (final MxOSException e) {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                    }
                }
            }
        } else {
            // IMAPListUIDs is NOT required for stateful RME
            logger.info("Invalid MSS Rme Version found, considering " +
                    "as Stateless MSS.");
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("RetrieveIMAPUUIDs action end."));
        }
    }
}
