/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set add signature in reply type.
 * 
 * @author mxos-dev
 */
public class SetAddSignatureInReplyType implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetAddSignatureInReplyType.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetAddSignatureInReplyType action start."));
        }
        String addSignatureInReplyType = requestState.getInputParams()
                .get(MailboxProperty.addSignatureInReplyType.name()).get(0);

        try {
            if (addSignatureInReplyType != null
                    && !addSignatureInReplyType.equals("")) {

                Signature newSignature = (Signature) requestState
                        .getDbPojoMap()
                        .getPropertyAsObject(MxOSPOJOs.signature);
                if (newSignature != null) {

                    newSignature
                            .setAddSignatureInReplyType(MxosEnums.SignatureInReplyType
                                    .fromValue(addSignatureInReplyType));

                    requestState.getDbPojoMap().setProperty(
                            MxOSPOJOs.signature, newSignature);
                } else {

                    addSignatureInReplyType = Integer
                            .toString(MxosEnums.SignatureInReplyType.fromValue(
                                    addSignatureInReplyType).ordinal());

                    MxOSApp.getInstance()
                            .getMailboxHelper()
                            .setAttribute(requestState,
                                    MailboxProperty.addSignatureInReplyType,
                                    addSignatureInReplyType);
                }

            }

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set add signature in reply type.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_ADD_SIGNATURE_IN_REPLY_TYPE
                            .name(),
                    e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetAddSignatureInReplyType action end."));
        }
    }
}
