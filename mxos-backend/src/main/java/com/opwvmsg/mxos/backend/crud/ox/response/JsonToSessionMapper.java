/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ox.response;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.MultivaluedMap;

import org.codehaus.jackson.JsonNode;


import com.opwvmsg.mxos.addressbook.pojos.Cookies;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.sun.jersey.api.client.ClientResponse;

public class JsonToSessionMapper {

    public static ExternalSession mapToSession(ClientResponse resp, String sessionId) {
        Cookies cookies = new Cookies();
        Set<String> cookieSet = new HashSet<String>();

        ExternalSession session = mapToSessionFields(resp, sessionId, cookies,
                cookieSet);
        return session;
    }

    private static ExternalSession mapToSessionFields(ClientResponse resp,
            String sessionId, Cookies cookies, Set<String> cookieSet) {
        MultivaluedMap<String, String> headers = resp.getHeaders();
        List<String> cookiesIncoming = headers.get("Set-Cookie");

        int fromIndex = -1, toIndex = -1;
        String cookie1 = "", cookie2 = "", cookie3 = "";
        if (cookiesIncoming != null) {
            for (String cookieStr : cookiesIncoming) {
                if (cookieStr != null) {
                    fromIndex = cookieStr.indexOf("JSESSIONID");
                    if (fromIndex != -1) {
                        toIndex = cookieStr.indexOf(';', fromIndex);
                        if (toIndex != -1) {
                            cookie1 = cookieStr.substring(fromIndex, toIndex);
                        }
                    }
                    fromIndex = cookieStr
                            .indexOf("open-xchange-public-session");
                    if (fromIndex != -1) {
                        toIndex = cookieStr.indexOf(';', fromIndex);
                        if (toIndex != -1) {
                            cookie2 = cookieStr.substring(fromIndex, toIndex);
                        }
                    }
                    fromIndex = cookieStr.indexOf("open-xchange-secret-");
                    if (fromIndex != -1) {
                        toIndex = cookieStr.indexOf(';', fromIndex);
                        if (toIndex != -1) {
                            cookie3 = cookieStr.substring(fromIndex, toIndex);
                        }
                    }
                }
            }
        }

        cookieSet.add(cookie1);
        cookieSet.add(cookie2);
        cookieSet.add(cookie3);
        cookies.setCookies(cookieSet);

        ExternalSession session = new ExternalSession();
        session.setSessionId(sessionId);
        session.setCookieString(new StringBuilder().append(cookie1).append(";")
                .append(cookie2).append(";").append(cookie3).toString());
        return session;
    }

    public static String mapToUserId(JsonNode root) {
        return root.path(OXContactsProperty.login_info.name()).asText();
    }
}
