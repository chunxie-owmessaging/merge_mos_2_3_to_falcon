/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.message;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Folder service exposed to client which is responsible for doing basic
 * folder related activities e.g create folder , delete folder, etc. directly
 * in the database.
 * 
 * @author mxos-dev
 */
public class BackendFolderService implements
        com.opwvmsg.mxos.interfaces.service.message.IFolderService {

    private static Logger logger = Logger.getLogger(BackendFolderService.class);

    /**
     * Constructor.
     * 
     * @param metaObjectPool Object pool to talk to metadata.
     * @param blobCRUD Object pool to talk to blobdata.
     * @param serviceProperties Service properties like soft deleted, etc.
     */
    public BackendFolderService() {
        logger.info("BackendFolderService created...");
    }

    @Override
    public String create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.FolderService, Operation.PUT);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.FolderService, Operation.PUT, t0,
                    StatStatus.pass);
            return (String) mxosRequestState.getDbPojoMap().getProperty(
                    FolderProperty.folderId);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.FolderService, Operation.PUT, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public Folder read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.FolderService, Operation.GET);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.FolderService, Operation.GET, t0,
                    StatStatus.pass);
            return (Folder) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.folder);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.FolderService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Folder> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        /*
         * TODO: Cleanup GC issue because of String concat.
         */

        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.FolderService, Operation.LIST);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.FolderService, Operation.LIST, t0,
                    StatStatus.pass);
            return (List<Folder>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.folders);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.FolderService, Operation.LIST, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.FolderService, Operation.POST);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.FolderService, Operation.POST, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.FolderService, Operation.POST, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.FolderService, Operation.DELETE);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.FolderService, Operation.DELETE, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.FolderService, Operation.DELETE, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.FolderService, Operation.DELETEALL);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.FolderService, Operation.DELETEALL, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.FolderService, Operation.DELETEALL, t0,
                    StatStatus.fail);
            throw e;
        }
        
    }

}
