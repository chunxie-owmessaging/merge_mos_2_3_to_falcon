package com.opwvmsg.mxos.backend.crud.ox.tasks;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.crud.ITasksHelper;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.TasksDBTypes;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.exception.InvalidRequestException;

public class OXTasksHelper implements ITasksHelper {

    private Map<String, String> getAttributes(MxOSRequestState requestState) {
        IBackendState backendState = requestState.getBackendState().get(
                TasksDBTypes.ox.name());
        if (backendState == null) {
            backendState = new OXTasksBackendState();
            requestState.getBackendState().put(TasksDBTypes.ox.name(),
                    backendState);
        }
        return ((OXTasksBackendState) backendState).attributes;
    }

    private List<Map<String, String>> getAttributesList(
            MxOSRequestState requestState, int size) {
        IBackendState backendState = requestState.getBackendState().get(
                TasksDBTypes.ox.name());
        if (backendState == null) {
            backendState = new OXTasksBackendState();
            requestState.getBackendState().put(TasksDBTypes.ox.name(),
                    backendState);
        }
        OXTasksBackendState state = ((OXTasksBackendState) backendState);
        if (state.attributesList.size() == 0) {
            state.initialize(size);
        }
        return state.attributesList;
    }

    @Override
    public void setAttribute(final MxOSRequestState mxosRequestState,
            final TasksProperty key, final Object value)
            throws InvalidRequestException {
        if (OXTasksUtil.JSONToOX_Tasks.get(key) != null) {
            getAttributes(mxosRequestState).put(
                    OXTasksUtil.JSONToOX_Tasks.get(key).name(),
                    value.toString());
        } else if (OXTasksUtil.JSONToOX_TasksFolders.get(key) != null) {
            getAttributes(mxosRequestState).put(
                    OXTasksUtil.JSONToOX_TasksFolders.get(key).name(),
                    value.toString());
        }
    }

    @Override
    public void setAttributes(MxOSRequestState mxosRequestState,
            TasksProperty key, Object[] values) throws InvalidRequestException {
        for (int i = 0; i < values.length; i++) {
            if (OXTasksUtil.JSONToOX_Tasks.get(key) != null) {
                Map<String, String> map = getAttributesList(mxosRequestState,
                        values.length).get(i);
                map.put(OXTasksUtil.JSONToOX_Tasks.get(key).name(),
                        (String) values[i]);
            } else if (OXTasksUtil.JSONToOX_TasksFolders.get(key) != null) {
                Map<String, String> map = getAttributesList(mxosRequestState,
                        values.length).get(i);
                map.put(OXTasksUtil.JSONToOX_TasksFolders.get(key).name(),
                        (String) values[i]);
            }
        }
    }
}
