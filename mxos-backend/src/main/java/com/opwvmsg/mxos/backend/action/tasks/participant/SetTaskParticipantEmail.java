package com.opwvmsg.mxos.backend.action.tasks.participant;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.exception.MxOSException;

public class SetTaskParticipantEmail implements MxOSBaseAction{
	private static Logger logger = Logger.getLogger(SetTaskParticipantEmail.class);
	@Override
	public void run(MxOSRequestState requestState) throws MxOSException {
		// TODO Auto-generated method stub
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action start."));
		}

		int participantEmailCount = requestState.getInputParams()
		.get(TasksProperty.participantEmail.name()).size();

		StringBuilder participantEmails = new StringBuilder();
		for(int i=0;i<participantEmailCount; i++){
			participantEmails.append(requestState.getInputParams()
					.get(TasksProperty.participantEmail.name()).get(i));
			participantEmails.append(",");
		}

		MxOSApp.getInstance()
		.getTasksHelper()
		.setAttribute(requestState, TasksProperty.participantEmail,
				participantEmails.toString());

		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action end."));
		}
	}
}
