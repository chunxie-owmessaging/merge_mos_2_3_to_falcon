package com.opwvmsg.mxos.backend.action.saml;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.SamlError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.saml.service.SAMLService;
import com.opwvmsg.mxos.saml.service.SAMLConfig;
import com.opwvmsg.mxos.saml.service.SamlProvidersEnum;

public class CreateSamlAuthenticationRequest implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(CreateSamlAuthenticationRequest.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("CreateSamlAuthenticationRequest action start.");
        }
        try {
            String acsURL = requestState.getInputParams()
                    .get(MxOSConstants.ACS_URL).get(0);
            SAMLService samlService = SAMLConfig
                    .getInstance(SamlProvidersEnum.VM.name());
            String samlAuthenticationReq = samlService
                    .createSAMLAuthenticationRequest(acsURL);
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.samlRequest,
                    samlAuthenticationReq);
        } catch (MxOSException ex) {
            logger.error("Unable to build SAML authentication request - problems with OpenSAML");
            throw ex;
        } catch (Exception ex) {
            logger.error("Unable to build SAML authentication request due to: '"
                    + ex);
            throw new ApplicationException(
                    SamlError.SAML_UNABLE_TO_CREATE_AUTHENTICATION_REQUEST
                            .name(),
                    ex);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("CreateSamlAuthenticationRequest action end.");
        }
    }
}
