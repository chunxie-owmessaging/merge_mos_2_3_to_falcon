package com.opwvmsg.mxos.backend.crud.exception;

public class TasksException extends ComponentException {

    private static final long serialVersionUID = 8634793376715923619L;

    public TasksException(Exception e) {
        super(e);
    }

    public TasksException(String message) {
        super(message);
    }

    public TasksException(String message, Exception e) {
        super(message, e);
    }

    public TasksException(String message, String category, String code,
            String errorId) {
        super(message);
    }

    public TasksException(String message, String category, String code,
            String errorId, Exception e) {
        super(message, e);
    }
}
