/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import java.util.List;
import org.apache.log4j.Logger;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to get the signature by signatureId.
 * 
 * @author mxos-dev
 */
public class GetMailSendSignature implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetMailSendSignature.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetMailSendSignature action start."));
        }
        try {
            String signatureId = requestState.getInputParams()
                    .get(MailboxProperty.signatureId.name()).get(0);

            if (signatureId == null)
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_SIGNATURE_ID.name());

            MailSend mailSend = (MailSend) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mailSend);
            List<Signature> signatureList = mailSend.getSignatures();

            boolean isSignaturePresent = false;
            if (signatureList != null) {
                for (Signature signature : signatureList) {
                    if (signature.getSignatureId() == Integer
                            .parseInt(signatureId)) {
                        isSignaturePresent = true;
                        requestState.getDbPojoMap().setProperty(
                                MxOSPOJOs.signature, signature);
                        break;
                    }
                }
            }

            if (isSignaturePresent == false) {
                throw new InvalidRequestException(
                        MailboxError.MBX_MAIL_SEND_SIGNATURE_NOT_FOUND.name());
            }

        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while getting signature.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_GET_MAIL_SEND_SIGNATURE.name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetMailSendSignature action end."));
        }
    }
}
