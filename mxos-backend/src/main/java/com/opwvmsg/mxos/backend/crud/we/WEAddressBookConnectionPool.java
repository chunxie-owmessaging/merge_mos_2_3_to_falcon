/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.we;

import java.util.HashMap;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.utils.paf.config.Config;

/**
 * This class provides connection pool for Webedge access.
 * 
 * @author
 */
public class WEAddressBookConnectionPool implements ICRUDPool<IAddressBookCRUD> {
    private static class WEConnectionPoolHolder {
        public static WEAddressBookConnectionPool instance = new WEAddressBookConnectionPool();
    }

    private static Logger logger = Logger
            .getLogger(WEAddressBookConnectionPool.class);

    /**
     * Method to get Instance of RestConnectionPool object.
     * 
     * @return RestConnectionPool object
     * @throws Exception Exception
     */
    public static WEAddressBookConnectionPool getInstance() {
        return WEConnectionPoolHolder.instance;
    }

    private GenericObjectPool<WEAddressBookCRUD> objPool;

    /**
     * Constructor.
     * 
     * @throws Exception Exception.
     */
    public WEAddressBookConnectionPool() {
        createMxosObjectPool();
    }

    /**
     * Method to borrow connection.
     * 
     * @return connection connection
     * @throws Exception in case no connection is available.
     */
    @Override
    public WEAddressBookCRUD borrowObject() throws MxOSException {
        WEAddressBookCRUD obj = null;
        logger.debug(new StringBuilder("Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
        try {
            obj = objPool.borrowObject();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while borrowing object.", e);
            throw new ApplicationException(
                    ErrorCode.ABS_CONNECTION_ERROR.name(), e);
        }

        if (obj == null) {
            logger.error("Borrowed object is null.");
            throw new ApplicationException(
                    ErrorCode.ABS_CONNECTION_ERROR.name());
        }
        incrementJMXStats();
        return obj;
    }

    /**
     * Access to PAF RME calls.
     * 
     */
    private void createMxosObjectPool() {
        HashMap<String, Object> configdbMap = new HashMap<String, Object>(
                MxOSConfig.getDefaultValues());

        configdbMap.put("confServHost", MxOSConfig.getConfigHost());
        configdbMap.put("confServPort", MxOSConfig.getConfigPort());
        configdbMap.put("defaultApp", MxOSConfig.getDefaultApp());
        configdbMap.put("defaultHost", MxOSConfig.getDefaultHost());
        Config.setConfig("intermail", configdbMap);
        
        int mxosMaxConnections;
        if (System.getProperties().containsKey(
                SystemProperty.mxosMaxConnections.name())) {
            mxosMaxConnections = Integer.parseInt(System
                    .getProperty(SystemProperty.mxosMaxConnections.name()));
        } else {
            // Default max connections
            mxosMaxConnections = 10;
        }
        System.out.println("MxOS Max Connections = " + mxosMaxConnections);
        
        objPool = new GenericObjectPool<WEAddressBookCRUD>(
                new WEAddressBookFactory(), mxosMaxConnections);
        objPool.setMaxIdle(-1);
        System.out.println("WEMxosObjectPool Created with default values...");
        initializeJMXStats(mxosMaxConnections);
    }

    /**
     * Method to return the connection back.
     * 
     * @param restCRUD restCRUD
     */

    @Override
    public void returnObject(IAddressBookCRUD addressBookCRUD)
            throws MxOSException {
        try {
            objPool.returnObject((WEAddressBookCRUD) addressBookCRUD);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while returning object.", e);
            throw new ApplicationException(
                    ErrorCode.ABS_CONNECTION_ERROR.name(), e);
        }
        logger.debug(new StringBuilder("Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.WEADDRESSBOOK.setCount(count);
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_WEADDRESSBOOK.increment();
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_WEADDRESSBOOK.decrement();
    }

    @Override
    public void resetPool() throws MxOSException {
        // TODO Auto-generated method stub

    }
}
