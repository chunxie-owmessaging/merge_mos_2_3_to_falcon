/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;

/**
 * Action class to update email alias.
 * 
 * @author mxos-dev
 */
public class UpdateAlias implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(UpdateAlias.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateAlias action start."));
        }
        try {
            final String oldEmailAlias = requestState.getInputParams()
                    .get(MailboxProperty.oldEmailAlias.name()).get(0);
            final String newEmailAlias = requestState.getInputParams()
                    .get(MailboxProperty.newEmailAlias.name()).get(0);
            final String[] temp = newEmailAlias.split(MxOSConstants.AT_THE_RATE);
            if (temp.length != 2) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_NEW_ALIAS.name());
            }
    
            final List<String> allowedDomains;
    
            ICRUDPool<IMailboxCRUD> provCRUDPool = null;
            IMailboxCRUD mailboxCRUD = null;
    
            final Base base;
            try {
                provCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
                mailboxCRUD = provCRUDPool.borrowObject();
                base = ActionUtils.readMailboxProvisioning(requestState,
                        mailboxCRUD);
            } catch (final MxOSException e) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_NEW_ALIAS.name(), e);
            } catch (final Exception e) {
                logger.error("Error while update alias.", e);
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_NEW_ALIAS.name());
            } finally {
                if (provCRUDPool != null && mailboxCRUD != null) {
                    try {
                        provCRUDPool.returnObject(mailboxCRUD);
                    } catch (final MxOSException e) {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name());
                    }
                }
            }
    
            if (base == null) {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
    
            if (base.getAllowedDomains() != null) {
                allowedDomains = base.getAllowedDomains();
            } else {
                allowedDomains = new ArrayList<String>();
            }
            if (allowedDomains != null && !allowedDomains.contains(temp[1].toLowerCase())) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_NEW_ALIAS.name());
            }
            final List<String> emailAliasesList;
            if (base.getEmailAliases() != null) {
                emailAliasesList = base.getEmailAliases();
            } else {
                emailAliasesList = new ArrayList<String>();
            }
    
            if (emailAliasesList == null || emailAliasesList.size() == 0) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_ALIAS.name());
            }
            if (emailAliasesList.contains(newEmailAlias.toLowerCase())) {
                // Even though newAllowedDomain already exist..we are updating
                // the value with newAllowedDomain to avoid the error thrown 
                // in UpdateMailbox, if we return from here.
            }
            if (emailAliasesList.remove(oldEmailAlias.toLowerCase())) {
                emailAliasesList.add(newEmailAlias.toLowerCase());
                final String[] emailAliasesArray = emailAliasesList
                        .toArray(new String[emailAliasesList.size()]);
                MxOSApp.getInstance().getMailboxHelper()
                        .setAttribute(requestState, MailboxProperty.emailAlias,
                                emailAliasesArray);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_ALIAS.name());
            }
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while update alias.", e);
            throw new ApplicationException(
                    MailboxError.MBX_ALIAS_UNABLE_TO_POST.name(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateAlias action end."));
        }
    }
}
