/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.logging;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set password via MAA.
 *
 * @author Aricent
 *
 */

public class PasswordMaaLoggingAction extends MaaLoggingAction {
    private static Logger logger = Logger
            .getLogger(PasswordMaaLoggingAction.class);

    /**
     * Action to make call to MAA.
     *
     * @param model
     *            model.
     * @throws Exception
     *             exception
     */
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Start : PasswordMaaLoggingAction");
        }
        try {
            String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            /*
             * because MAA require the primary email address when user want to recovery the password,
             * we need to query dir server and read the email attribute.
             * so we read the Base object which have the email attribute.
             */
             
            Base base = ActionUtils.getUserInfoFromLDAP(email);
            
            String userName = ActionUtils.getUsernameFromEmail(base.getEmail());
            String domain = ActionUtils.getDomainFromEmail(base.getEmail());
            Map<String, String> params = new HashMap<String, String>();
            if (logger.isDebugEnabled()) {
                logger.debug("userName :" + userName);
                logger.debug("domain :" + domain);
            }
            params.put(ARG1, userName);
            params.put(ARG2, domain);
            if (requestState.getInputParams().get(
                    MailboxProperty.password.name()) != null) {
                params.put(ARG3,
                        requestState.getInputParams()
                                .get(MailboxProperty.password.name()).get(0));
            }

            LoggingResponseBean maaResponse = callMaa(
                    UPDATE_PASSWORD_LOGGING_SUB_URL, params);
            // Analyze logging response
            analyzeLoggingResponse(maaResponse);
        } catch(MxOSException me) {
            logger.error(me);
            throw me;
        } catch (Exception e) {
            logger.error(e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("End : PasswordMaaLoggingAction");
        }
        return;
    }
}
