/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/data/DomainTypeEnum.java#1 $
 */
package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.opwvmsg.mxos.data.enums.MxosEnums.DomainType;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.exception.InvalidRequestException;

/**
 * 
 * @author Aricent
 * 
 */
public class LDAPDomainType {
    private static final Map<String, DomainType> DOMAIN_TYPE_MAP =
        new HashMap<String, DomainType>();
    static {
        DOMAIN_TYPE_MAP.put("L", DomainType.LOCAL);
        DOMAIN_TYPE_MAP.put("N", DomainType.NONAUTH);
        DOMAIN_TYPE_MAP.put("R", DomainType.REWRITE);
    }
    /**
     * Method to get domain type of given ldap value.
     * @param ldapValue ldapValue
     * @return Domain.Type type of domain
     * @throws InvalidRequestException if not exists
     */
    public static DomainType fromLdap(final String ldapValue)
            throws InvalidRequestException {
        if (DOMAIN_TYPE_MAP.containsKey(ldapValue)) {
            return DOMAIN_TYPE_MAP.get(ldapValue);
        } else {
            throw new InvalidRequestException(
                    DomainError.DMN_INVALID_TYPE.name(),
                    "type = " + ldapValue);
        }
    }

    /**
     * Method to get ldap value of given domain type.
     * @param type Domain.Type
     * @return String ldap value
     * @throws InvalidRequestException if not exists
     */
    public static String toLdap(final DomainType type)
            throws InvalidRequestException {
        for (Entry<String, DomainType> entry : DOMAIN_TYPE_MAP.entrySet()) {
            if (type.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        throw new InvalidRequestException(
                DomainError.DMN_INVALID_TYPE.name(), "type = " + type);
    }

}
