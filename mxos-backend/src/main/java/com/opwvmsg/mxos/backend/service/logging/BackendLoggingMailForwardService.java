/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.logging;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailForwardService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Backend Logging Mail Forwarding service exposed to client which is
 * responsible for doing basic mailbox related activities e.g create mailbox,
 * delete mailbox, etc. directly in the database.
 *
 * @author mxos-dev
 */
public class BackendLoggingMailForwardService implements IMailForwardService {

    private static Logger logger = Logger
            .getLogger(BackendLoggingMailForwardService.class);

    /**
     * Default Constructor.
     */
    public BackendLoggingMailForwardService() {
        logger.info("BackendLoggingMailForwardService created...");
    }

    @Override
    public void create(final Map<String, List<String>> inputParams)
        throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.LoggingMailForwardService,
                    Operation.PUT);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.LoggingMailForwardService,
                    Operation.PUT, t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.LoggingMailForwardService,
                    Operation.PUT, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public List<String> read(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        
    }
}
