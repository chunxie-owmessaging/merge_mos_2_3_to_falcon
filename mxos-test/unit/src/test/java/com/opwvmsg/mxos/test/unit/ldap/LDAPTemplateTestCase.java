/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.test.unit.ldap;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchResult;

import org.junit.Before;
import org.junit.Test;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.ldap.LDAPLink;
import com.opwvmsg.mxos.backend.ldap.LDAPLinkManager;
import com.opwvmsg.mxos.backend.ldap.LDAPPolicy;
import com.opwvmsg.mxos.backend.ldap.LDAPTemplate;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Mailbox;

/**
 * class
 * 
 * @author mxos-dev
 * 
 */
public class LDAPTemplateTestCase {

    public static final String ldapRetryOnExceptions = "CommunicationException,ServiceUnavailableException,InsufficientResourcesException,InterruptedNamingException,LinkException,NoInitialContextException,PartialResultException";
    public static final String[] mailboxObjectClasses = new String[] { "top",
            "person", "mailuser", "mailuserprefs", "maillocaluserprefs",
            "msguser", "msguserprefs" };
    public static final String email = "ldap_1@openwave.com";
    public static final String domain = "dc=openwave,dc=com";
    public static final String name = "mail=" + email + "," + domain;

    private LDAPTemplate template = null;
    private boolean cacheServer = false;

    @Before
    public void setup() {
        LDAPLink masterLink = new LDAPLink(
                "ldap://bjfusion1.openwave.com:5008", "cn=root", "secret");
        LDAPLink cacheLink = new LDAPLink("ldap://bjfusion1.openwave.com:389",
                "cn=root", "secret");
        LDAPLinkManager.instance().addLDAPLink(true, cacheLink);
        LDAPLinkManager.instance().addLDAPLink(false, masterLink);

        LDAPPolicy policy = new LDAPPolicy()
                .setRetryPolicy(
                        Arrays.asList(ldapRetryOnExceptions.split(",")), 3,
                        1000).setFailover(true).setFailbackPolicy(100);

        try {
            template = new LDAPTemplate(policy, cacheServer);
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.opwvmsg.mxos.backend.ldap.LDAPTemplate#bind(java.lang.String, java.lang.Object, javax.naming.directory.Attributes)}
     * .
     */
    @Test
    public void testBind() {
        try {
            BasicAttribute objcs = new BasicAttribute("objectclass");
            for (String ojc : mailboxObjectClasses) {
                objcs.add(ojc);
            }
            
            Attributes attrs = new BasicAttributes();
            attrs.put(objcs);
            attrs.put("maillogin", "ldap_1");
            attrs.put("adminpolicydn", "cn=default,cn=admin root");
            attrs.put("cn", email);
            attrs.put("mail", email);
            attrs.put("sn", email);
            attrs.put("mailboxId", ActionUtils.generateMailboxId(email));
            attrs.put("mailpassword", "p");
            attrs.put("mailpasswordtype", "C");
            attrs.put("mailboxstatus", "A");
            attrs.put("mailautosavedraftmessageinterval", "100");
            attrs.put("mailmessagestore", "bjfusion1");
            attrs.put("mailstatuschangedate", "20141027103055Z");
            attrs.put("mailautoreplyhost", "bjfusion1");
            
            template.bind(name, null, attrs);
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
        
    }

    /**
     * Test method for
     * {@link com.opwvmsg.mxos.backend.ldap.LDAPTemplate#modifyAttributes(java.lang.String, int, javax.naming.directory.Attributes)}
     * .
     */
    @Test
    public void testModifyAttributesStringIntAttributes() {
        try {
            Attributes attrs = new BasicAttributes();
            attrs.put("maillogin", "ldap_1_x");
            template.modifyAttributes(name, DirContext.REPLACE_ATTRIBUTE, attrs);
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.opwvmsg.mxos.backend.ldap.LDAPTemplate#modifyAttributes(java.lang.String, javax.naming.directory.ModificationItem[])}
     * .
     */
    @Test
    public void testModifyAttributesStringModificationItemArray() {
        try {
            Attribute attr = new BasicAttribute("maillogin", "ldap_1_p");
            ModificationItem item = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, attr);
            template.modifyAttributes(name, new ModificationItem[] {item});
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.opwvmsg.mxos.backend.ldap.LDAPTemplate#search(java.lang.String, javax.naming.directory.Attributes, java.lang.String[])}
     * .
     */
    @Test
    public void testSearchStringAttributesStringArray() {
        try {
            Attributes attrs = new BasicAttributes();
            attrs.put("mail", email);
            NamingEnumeration<SearchResult> results = template.search(domain, attrs, null);
            int size = 0;
            while (results.hasMore()) {
                results.next();
                size++;
            }
            assertEquals(1, size);
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.opwvmsg.mxos.backend.ldap.LDAPTemplate#search(java.lang.String, javax.naming.directory.Attributes, java.lang.String[], com.opwvmsg.mxos.backend.ldap.LDAPMapper)}
     * .
     */
    @Test
    public void testSearchStringAttributesStringArrayLDAPMapperOfT() {
        try {
            Attributes attrs = new BasicAttributes();
            attrs.put("mail", email);
            List<Mailbox> results = template.search(domain, attrs, null, new MailboxMapper());
            assertEquals(1, results.size());
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.opwvmsg.mxos.backend.ldap.LDAPTemplate#search(java.lang.String, java.lang.String, javax.naming.directory.SearchControls)}
     * .
     */
    @Test
    public void testSearchStringStringSearchControls() {
        try {
            NamingEnumeration<SearchResult> results = template.search(domain, "mail=" + email, null);
            int size = 0;
            while (results.hasMore()) {
                results.next();
                size++;
            }
            assertEquals(1, size);
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.opwvmsg.mxos.backend.ldap.LDAPTemplate#search(java.lang.String, java.lang.String, javax.naming.directory.SearchControls, com.opwvmsg.mxos.backend.ldap.LDAPMapper)}
     * .
     */
    @Test
    public void testSearchStringStringSearchControlsLDAPMapperOfT() {
        try {
            List<Mailbox> results = template.search(domain, "mail=" + email, null, new MailboxMapper());
            assertEquals(1, results.size());
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.opwvmsg.mxos.backend.ldap.LDAPTemplate#search(java.lang.String, java.lang.String, java.lang.Object[], javax.naming.directory.SearchControls)}
     * .
     */
    @Test
    public void testSearchStringStringObjectArraySearchControls() {
        try {
            NamingEnumeration<SearchResult> results = template.search(domain, "mail={0}", new Object[] {email}, null);
            int size = 0;
            while (results.hasMore()) {
                results.next();
                size++;
            }
            assertEquals(1, size);
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.opwvmsg.mxos.backend.ldap.LDAPTemplate#search(java.lang.String, java.lang.String, java.lang.Object[], javax.naming.directory.SearchControls, com.opwvmsg.mxos.backend.ldap.LDAPMapper)}
     * .
     */
    @Test
    public void testSearchStringStringObjectArraySearchControlsLDAPMapperOfT() {
        try {
            List<Mailbox> results = template.search(domain, "mail={0}", new Object[] {email}, null, new MailboxMapper());
            assertEquals(1, results.size());
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.opwvmsg.mxos.backend.ldap.LDAPTemplate#getAttributes(java.lang.String, java.lang.String[])}
     * .
     */
    @Test
    public void testGetAttributesStringStringArray() {
        try {
            Attributes attrs = template.getAttributes(name, null);
            assertNotNull(attrs);
            Attribute attr = attrs.get("mail");
            assertNotNull(attr);
            assertEquals(email, attr.get().toString());
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.opwvmsg.mxos.backend.ldap.LDAPTemplate#getAttributes(java.lang.String, java.lang.String[], com.opwvmsg.mxos.backend.ldap.LDAPMapper)}
     * .
     */
    @Test
    public void testGetAttributesStringStringArrayLDAPMapperOfT() {
        try {
            Mailbox mailbox = template.getAttributes(name, null, new MailboxMapper());
            assertNotNull(mailbox);
            Base base = mailbox.getBase();
            assertNotNull(base);
            assertEquals(email, base.getEmail());
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }
    
    /**
     * Test method for
     * {@link com.opwvmsg.mxos.backend.ldap.LDAPTemplate#unbind(java.lang.String)}
     * .
     */
    @Test
    public void testUnbind() {
        try {
            template.unbind(name);
        } catch (NamingException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

}
