package com.opwvmsg.mxos.test.unit.logging;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to COS 
@SuiteClasses({
    LoggingMailboxTest.class
})
public class LoggingMailboxSuiteIntegration {
}
