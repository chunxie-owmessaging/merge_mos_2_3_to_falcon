package com.opwvmsg.mxos.test.unit.process;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.MsisdnStatus;
import com.opwvmsg.mxos.data.enums.MxosEnums.SmsServicesMsisdnStatusType;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.SmsNotifications;
import com.opwvmsg.mxos.data.pojos.SmsOnline;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsNotificationsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsOnlineService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsServicesService;
import com.opwvmsg.mxos.interfaces.service.process.IMsisdnDeactivationService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class DeactivateMSISDNServiceTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "PassW0rD";
    private static final String MSISDN = "+12345678901";
    private static final String OTHER_MSISDN = "+12345654321";
    private static IMsisdnDeactivationService service;
    private static IMailboxBaseService base;
    private static ICredentialService credentials;
    private static IMailReceiptService mr;
    private static ISmsOnlineService smsOnline;
    private static ISmsNotificationsService smsNotif;
    private static ISmsServicesService smsService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("DeactivateMSISDNServiceTest.setUpBeforeClass...");
        service = (IMsisdnDeactivationService) ContextUtils.loadContext()
                .getService(
                        ServiceEnum.MsisdnDeactivationService.name());
        base = (IMailboxBaseService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailboxBaseService.name());
        credentials = (ICredentialService) ContextUtils.loadContext()
                .getService(ServiceEnum.CredentialService.name());
        mr = (IMailReceiptService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailReceiptService.name());
        smsOnline = (ISmsOnlineService) ContextUtils.loadContext()
                .getService(ServiceEnum.SmsOnlineService.name());
        smsNotif = (ISmsNotificationsService) ContextUtils.loadContext()
                .getService(ServiceEnum.SmsNotificationsService.name());
        smsService = (ISmsServicesService) ContextUtils.loadContext()
                .getService(ServiceEnum.SmsServicesService.name());
        String mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != null);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("DeactivateMSISDNServiceTest.setUp...");
        assertNotNull("MailBoxBaseService object is null.", base);
        assertNotNull("CredentialsService object is null.", credentials);
        assertNotNull("SMSOnlineService object is null.", smsOnline);
        assertNotNull("SMSNotificationService object is null.", smsNotif);
        assertNotNull("SMSServicesService object is null.", smsService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("DeactivateMSISDNServiceTest.tearDown...");
        params.clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("DeactivateMSISDNServiceTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        base = null;
        credentials = null;
        smsOnline = null;
        smsNotif = null;
        smsService = null;
    }

    private static void deactivateMSISDN(Map<String, List<String>> updateParams) {
        deactivateMSISDN(updateParams, null);
    }

    private static void deactivateMSISDN(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            service.process(updateParams);
            if (null != expectedError) {
                fail("1. This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("2. This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        }
    }

    private static void setupBase(String msisdn, String status) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.status.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.status.name()).add(status);
        inputParams.put(MailboxProperty.msisdn.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.msisdn.name()).add(msisdn);
        inputParams.put(MailboxProperty.msisdnStatus.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.msisdnStatus.name()).add(
                MsisdnStatus.ACTIVATED.name());
        try {
            base.update(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static Base getBase() {
        Base baseObject = null;
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            baseObject = base.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("Base object is null.", baseObject);
        return baseObject;
    }

    private static void setupCredentials(String msisdn, String pwdPref,
            String pwdRecoveryEmail) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.passwordRecoveryPreference.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordRecoveryPreference.name()).add(
                pwdPref);
        if (null != pwdRecoveryEmail) {
            inputParams.put(MailboxProperty.passwordRecoveryEmail.name(),
                    new ArrayList<String>());
            inputParams.get(MailboxProperty.passwordRecoveryEmail.name()).add(
                    pwdRecoveryEmail);
            inputParams.put(MailboxProperty.passwordRecoveryEmailStatus.name(),
                    new ArrayList<String>());
            inputParams.get(MailboxProperty.passwordRecoveryEmailStatus.name())
                    .add(MxosEnums.MsisdnStatus.ACTIVATED.name());
        }
        inputParams.put(MailboxProperty.passwordRecoveryMsisdn.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordRecoveryMsisdn.name()).add(
                msisdn);
        inputParams.put(MailboxProperty.passwordRecoveryMsisdnStatus.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordRecoveryMsisdnStatus.name())
                .add(MsisdnStatus.ACTIVATED.name());
        try {
            credentials.update(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static Credentials getCredentials() {
        Credentials creds = null;
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            creds = credentials.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("Credentials object is null.", creds);
        return creds;
    }

    private static void setupSMSFeatures(String msisdn) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);

        inputParams.put(MailboxProperty.smsOnlineEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsOnlineEnabled.name()).add(
                BooleanType.YES.name());
        try {
            smsOnline.update(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        inputParams.remove(MailboxProperty.smsOnlineEnabled.name());

        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.copyOnForward.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.copyOnForward.name()).add("yes");
        try {
            mr.update(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        inputParams.remove(MailboxProperty.copyOnForward.name());

        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.smsBasicNotificationsEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsBasicNotificationsEnabled.name())
                .add(BooleanType.YES.name());
        inputParams.put(MailboxProperty.smsAdvancedNotificationsEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsAdvancedNotificationsEnabled.name())
                .add(BooleanType.YES.name());
        try {
            smsNotif.update(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        inputParams.remove(MailboxProperty.smsBasicNotificationsEnabled.name());
        inputParams.remove(MailboxProperty.smsAdvancedNotificationsEnabled
                .name());

        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.smsServicesMsisdn.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsServicesMsisdn.name()).add(msisdn);
        inputParams.put(MailboxProperty.smsServicesMsisdnStatus.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsServicesMsisdnStatus.name()).add(
                MsisdnStatus.ACTIVATED.name());
        try {
            smsService.update(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        inputParams.remove(MailboxProperty.smsServicesMsisdn.name());
        inputParams.remove(MailboxProperty.smsServicesMsisdnStatus.name());
    }

    private static SmsOnline getSmsOnline() {
        SmsOnline smsOnlineObject = null;
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            smsOnlineObject = smsOnline.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("SmsOnline object is null.", smsOnlineObject);
        return smsOnlineObject;
    }

    private static SmsNotifications getSmsNotifications() {
        SmsNotifications smsNotifObject = null;
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            smsNotifObject = smsNotif.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("SmsNotifications object is null.", smsNotifObject);
        return smsNotifObject;
    }

    private static SmsServices getSmsServices() {
        SmsServices smsServiceObject = null;
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            smsServiceObject = smsService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("SmsServices object is null.", smsServiceObject);
        return smsServiceObject;
    }

    @Test
    public void testDeactivateMSISDNWithoutMSISDN() throws Exception {
        System.out
                .println("DeactivateMSISDNServiceTest.testDeactivateMSISDNWithoutMSISDN...");
        deactivateMSISDN(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testDeactivateMSISDNWithNullMSISDN() throws Exception {
        System.out
                .println("DeactivateMSISDNServiceTest.testDeactivateMSISDNWithNullMSISDN...");
        String key = MailboxProperty.msisdn.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(null);
        deactivateMSISDN(params, ErrorCode.MXS_INPUT_ERROR.name());
        params.remove(key);
    }

    @Test
    public void testDeactivateMSISDNWithEmptyMSISDN() throws Exception {
        System.out
                .println("DeactivateMSISDNServiceTest.testDeactivateMSISDNWithEmptyMSISDN...");
        String key = MailboxProperty.msisdn.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("");
        deactivateMSISDN(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.remove(key);
    }

    @Test
    public void testDeactivateMSISDNWithJunkMSISDN() throws Exception {
        System.out
                .println("DeactivateMSISDNServiceTest.testDeactivateMSISDNWithJunkMSISDN...");
        String key = MailboxProperty.msisdn.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("SomethingJunk");
        deactivateMSISDN(params, "300");
        params.remove(key);
    }

    @Test
    public void testDeactivateMSISDNWithSplCharsInMSISDN() throws Exception {
        System.out
                .println("DeactivateMSISDNServiceTest.testDeactivateMSISDNWithSplCharsInMSISDN...");
        String key = MailboxProperty.msisdn.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("!@#$%^&*()_+-=");
        deactivateMSISDN(params, "300");
        params.remove(key);
    }

    @Test
    public void testDeactivateMSISDNWithInvalidMSISDN() throws Exception {
        System.out
                .println("DeactivateMSISDNServiceTest.testDeactivateMSISDNWithInvalidMSISDN...");
        String key = MailboxProperty.msisdn.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("+9198800123450");
        deactivateMSISDN(params, "300");
        params.remove(key);
    }

    @Test
    public void testDeactivateMSISDNWithNonExistingMSISDN() throws Exception {
        System.out
                .println("DeactivateMSISDNServiceTest.testDeactivateMSISDNWithNonExistingMSISDN...");
        String key = MailboxProperty.msisdn.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("+91999999999");
        deactivateMSISDN(params, MailboxError.MBX_UNABLE_TO_SEARCH.name());
        params.remove(key);
    }

    @Test
    public void testDeactivateMSISDNForProximusAccountSuccess() throws Exception {
        System.out
                .println("DeactivateMSISDNServiceTest.testDeactivateMSISDNForProximusAccountSuccess...");
        setupBase(MSISDN, MxosEnums.Status.OPEN.name());
        String key = MailboxProperty.msisdn.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(MSISDN);
        deactivateMSISDN(params);
        Base base = getBase();
        MsisdnStatus status = base.getMsisdnStatus();
        assertNotNull("MsisdnStatus is null.", status);
        assertTrue("MsisdnStatus has a wrong value.",
                status.equals(MsisdnStatus.DEACTIVATED));
        Object date = base.getLastMsisdnStatusChangeDate();
        assertNotNull("LastMsisdnStatusChangeDate is null.", date);
        setupBase(OTHER_MSISDN, MxosEnums.Status.OPEN.name());
        params.remove(key);
    }

    @Test
    public void testDeactivateMSISDNForPwdRecoveryPrefAll() throws Exception {
        System.out
                .println("DeactivateMSISDNServiceTest.testDeactivateMSISDNForPwdRecoveryPrefAll...");
        setupCredentials(MSISDN,
                MxosEnums.PasswordRecoveryPreference.ALL.name(),
                "test@openwave.com");
        String key = MailboxProperty.msisdn.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(MSISDN);
        deactivateMSISDN(params);
        Credentials creds = getCredentials();
        MxosEnums.PasswordRecoveryPreference pref = creds
                .getPasswordRecoveryPreference();
        assertNotNull("PasswordRecoveryPreference is null.", pref);
        assertTrue("PasswordRecoveryPreference has a wrong value.",
                pref.equals(MxosEnums.PasswordRecoveryPreference.EMAIL));
        MsisdnStatus status = creds.getPasswordRecoveryMsisdnStatus();
        assertNotNull("MsisdnStatus is null.", status);
        assertTrue("MsisdnStatus has a wrong value.",
                status.equals(MsisdnStatus.DEACTIVATED));
        Object date = creds.getLastPasswordRecoveryMsisdnStatusChangeDate();
        assertNotNull("LastPasswordRecoveryMsisdnStatusChangeDate is null.",
                date);
        setupCredentials(OTHER_MSISDN,
                MxosEnums.PasswordRecoveryPreference.ALL.name(),
                "test@openwave.com");
        params.remove(key);
    }

    @Test
    public void testDeactivateMSISDNForPwdRecoveryPrefSMS() throws Exception {
        System.out
                .println("DeactivateMSISDNServiceTest.testDeactivateMSISDNForPwdRecoveryPrefSMS...");
        setupCredentials(MSISDN,
                MxosEnums.PasswordRecoveryPreference.SMS.name(), null);
        String key = MailboxProperty.msisdn.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(MSISDN);
        deactivateMSISDN(params);
        Credentials creds = getCredentials();
        MxosEnums.PasswordRecoveryPreference pref = creds
                .getPasswordRecoveryPreference();
        assertNotNull("PasswordRecoveryPreference is null.", pref);
        assertTrue("PasswordRecoveryPreference has a wrong value.",
                pref.equals(MxosEnums.PasswordRecoveryPreference.NONE));
        MsisdnStatus status = creds.getPasswordRecoveryMsisdnStatus();
        assertNotNull("MsisdnStatus is null.", status);
        assertTrue("MsisdnStatus has a wrong value.",
                status.equals(MsisdnStatus.DEACTIVATED));
        Object date = creds.getLastPasswordRecoveryMsisdnStatusChangeDate();
        assertNotNull("LastPasswordRecoveryMsisdnStatusChangeDate is null.",
                date);
        setupCredentials(OTHER_MSISDN,
                MxosEnums.PasswordRecoveryPreference.ALL.name(), null);
        params.remove(key);
    }

    @Test
    public void testDeactivateMSISDNForSMSFeatures() throws Exception {
        System.out
                .println("DeactivateMSISDNServiceTest.testDeactivateMSISDNForSMSFeatures...");
        setupSMSFeatures(MSISDN);
        String key = MailboxProperty.msisdn.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(MSISDN);
        deactivateMSISDN(params);
        SmsOnline smsOnline = getSmsOnline();
        MxosEnums.BooleanType bool = smsOnline.getSmsOnlineEnabled();
        assertNotNull("SmsOnlineEnabled is null.", bool);
        assertTrue("SmsOnlineEnabled has a wrong value.",
                bool.equals(MxosEnums.BooleanType.NO));

        SmsNotifications smsNotif = getSmsNotifications();
        bool = smsNotif.getSmsBasicNotificationsEnabled();
        assertNotNull("SmsBasicNotificationsEnabled is null.", bool);
        assertTrue("SmsBasicNotificationsEnabled has a wrong value.",
                bool.equals(MxosEnums.BooleanType.NO));
        bool = smsNotif.getSmsAdvancedNotificationsEnabled();
        assertNotNull("SmsAdvancedNotificationsEnabled is null.", bool);
        assertTrue("SmsAdvancedNotificationsEnabled has a wrong value.",
                bool.equals(MxosEnums.BooleanType.NO));

        SmsServices smsServices = getSmsServices();
        SmsServicesMsisdnStatusType status = smsServices
                .getSmsServicesMsisdnStatus();
        assertNotNull("MsisdnStatus is null.", status);
        assertTrue("MsisdnStatus has a wrong value.",
                status.equals(SmsServicesMsisdnStatusType.DEACTIVATED));
        Object date = smsServices.getLastSmsServicesMsisdnStatusChangeDate();
        assertNotNull("LastSmsServicesMsisdnStatusChangeDate is null.", date);
        setupSMSFeatures(OTHER_MSISDN);
        params.remove(key);
    }
}
