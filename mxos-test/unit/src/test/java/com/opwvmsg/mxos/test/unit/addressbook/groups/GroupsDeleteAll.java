package com.opwvmsg.mxos.test.unit.addressbook.groups;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsBaseService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GroupsDeleteAll {

    private static final String TEST_NAME = "GroupsDeleteAll";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static List<Long> GROUPID_LIST;

    private static IGroupsService groupsService;
    private static IExternalLoginService externalLoginService;
    private static IGroupsBaseService groupsBaseService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();
    private static ExternalSession session = null;

    /**
     * 
     * @throws Exception
     */

    private static void groupsCreateMultiple() throws Exception {

        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }

        FileInputStream inputStream = new FileInputStream(home
                + "/config/UnitTest/groups.txt");
        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, MxOSConstants.UTF8);
        params.put(AddressBookProperty.groupsList.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.groupsList.name())
                .add(writer.toString());

        GROUPID_LIST = groupsService.createMultiple(params);
        assertNotNull("GroupId Is null", GROUPID_LIST);

    }

    private static List<GroupBase> getParamsList(
            Map<String, List<String>> params) {
        return getParamsList(params, null);
    }

    private static List<GroupBase> getParamsList(
            Map<String, List<String>> params, AddressBookException expectedError) {
        List<GroupBase> groupBaseList = null;
        try {
            groupBaseList = groupsBaseService.list(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            }
        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
        }
        return groupBaseList;
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        groupsService = (IGroupsService) ContextUtils.loadContext().getService(
                ServiceEnum.GroupsService.name());
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        groupsBaseService = (IGroupsBaseService) ContextUtils.loadContext()
                .getService(ServiceEnum.GroupsBaseService.name());
        login(USERID, PASSWORD);
        

    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        groupsService = null;
        externalLoginService = null;
    }

    // delete post
    @Test
    public void testGroupsDeleteAll() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testGroupsDeleteAll");

        groupsCreateMultiple();
        params.clear();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        groupsService.deleteAll(params);

        List<GroupBase> baseList = getParamsList(params);
        assertNull("GroupBase Is not null", baseList);
    }
}
