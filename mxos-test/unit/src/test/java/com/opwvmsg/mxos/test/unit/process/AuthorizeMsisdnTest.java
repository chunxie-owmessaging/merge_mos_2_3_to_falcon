package com.opwvmsg.mxos.test.unit.process;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.IAuthorizeMSISDNService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class AuthorizeMsisdnTest {

    private static final String SMSTYPE_BASIC_NOTIFICATION = "basicnotification";
    private static final String SMSTYPE_ADVANCED_NOTIFICATION = "advancednotification";
    private static final String SMSTYPE_PASSWORD_RECOVERY_SMS = "passwordrecoverysms";
    private static final String SMSTYPE_PASSWORD_RECOVERY_MSISDN = "passwordrecoverymsisdn";
    private static final String SMSTYPE_SMS_SERVICE_MSISDN = "smsservicesmsisdn";
    
    private static final String AUTHORIZE_MSISDN = "+19886576790";

    private static IAuthorizeMSISDNService iAuthorizeMSISDNService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws MxOSException
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("AuthorizeMsisdnTest.setUpBeforeClass...");
        if (System.getProperty("MXOS_HOME") == null ||
                System.getProperty("MXOS_HOME").equals("")) {
            System.setProperty("MXOS_HOME", "D:\\LeapFrog\\workspace\\mxos");
        }
        iAuthorizeMSISDNService = (IAuthorizeMSISDNService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.AuthorizeMsisdnService.name());
        assertNotNull("MailboxService object is null.", iAuthorizeMSISDNService);
    }

    /**
     * @throws java.lang.MxOSException
     */
    @AfterClass
    public static void tearDownAfterClass() throws MxOSException {
        System.out.println("AuthorizeMsisdnTest.tearDownAfterClass...");
        params.clear();
        params = null;
        iAuthorizeMSISDNService = null;
    }

    void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }
//
//    @Test
//    public void testAuthorizeMsisdn_Success() {
//        System.out.println("AuthorizeMsisdnTest.testAuthorizeMsisdn_Success...");
//        try {
//            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
//            addParams(inputParams, SmsProperty.smsType.name(),
//                    SMSTYPE_SMSONLINE);
//            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
//                    AUTHORIZE_MSISDN);
//            iAuthorizeMSISDNService.process(inputParams);
//
//        } catch (MxOSException e) {
//            
//            assertFalse("MxOSException Happened", true);
//        }
//    }
//
//    @Test
//    public void testAuthorizeMsisdn_MsisdnNotPresent() {
//        System.out.println("AuthorizeMsisdnTest.testAuthorizeMsisdn_MsisdnNotPresent...");
//        try {
//            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
//            addParams(inputParams, SmsProperty.smsType.name(),
//                    SMSTYPE_SMSONLINE);
//            iAuthorizeMSISDNService.process(inputParams);
//
//        } catch (MxOSException e) {
//            
//            assertEquals("Some unexpected error code.",
//                    CustomError.AUTHORIZE_MSISDN_MISSING_PARAMS.name(), e.getCode());
//        }
//    }
//
//    @Test
//    public void testAuthorizeMsisdn_SmstypeNotPresent() {
//        System.out.println("AuthorizeMsisdnTest.testAuthorizeMsisdn_SmstypeNotPresent...");
//        try {
//            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
//            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
//                    AUTHORIZE_MSISDN);
//            iAuthorizeMSISDNService.process(inputParams);
//
//        } catch (MxOSException e) {
//            
//            assertEquals("Some unexpected error code.",
//                    CustomError.AUTHORIZE_MSISDN_MISSING_PARAMS.name(), e.getCode());
//        }
//    }
//
//    @Test
//    public void testAuthorizeMsisdn_SmsTypeInvalidValue() {
//        System.out.println("AuthorizeMsisdnTest.testAuthorizeMsisdn_SmsTypeInvalidValue...");
//        try {
//            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
//            addParams(inputParams, SmsProperty.smsType.name(), "sdfadsf");
//            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
//                    AUTHORIZE_MSISDN);
//            iAuthorizeMSISDNService.process(inputParams);
//
//        } catch (MxOSException e) {
//            
//            assertEquals("Some unexpected error code.",
//                    CustomError.SMS_INVALID_SMS_TYPE.name(), e.getCode());
//        }
//    }
//
//    @Test
//    public void testAuthorizeMsisdn_SmsTypeNull() {
//        System.out.println("AuthorizeMsisdnTest.testAuthorizeMsisdn_SmsTypeNull...");
//        try {
//            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
//            addParams(inputParams, SmsProperty.smsType.name(), "");
//            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
//                    AUTHORIZE_MSISDN);
//            iAuthorizeMSISDNService.process(inputParams);
//
//        } catch (MxOSException e) {
//            
//            assertEquals("Some unexpected error code.",
//                    CustomError.SMS_INVALID_SMS_TYPE.name(), e.getCode());
//        }
//    }
//
//    @Test
//    public void testAuthorizeMsisdn_msisdnInvalidValue() {
//        System.out.println("AuthorizeMsisdnTest.testAuthorizeMsisdn_msisdnInvalidValue...");
//        try {
//            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
//            addParams(inputParams, SmsProperty.smsType.name(),
//                    SMSTYPE_SMSONLINE);
//            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
//                    "sdafsdf");
//            iAuthorizeMSISDNService.process(inputParams);
//
//        } catch (MxOSException e) {
//            
//            assertEquals("Some unexpected error code.",
//                    CustomError.AUTHORIZE_MSISDN_INVALID_MSISDN.name(), e.getCode());
//        }
//    }
//
//    @Test
//    public void testAuthorizeMsisdn_MsisdnInvalidLength() {
//        System.out.println("AuthorizeMsisdnTest.testAuthorizeMsisdn_MsisdnInvalidLength...");
//        try {
//            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
//            addParams(inputParams, SmsProperty.smsType.name(),
//                    SMSTYPE_SMSONLINE);
//            addParams(inputParams, SmsProperty.authorizeMsisdn.name(), "18");
//            iAuthorizeMSISDNService.process(inputParams);
//
//        } catch (MxOSException e) {
//            
//            assertEquals("Some unexpected error code.",
//                    CustomError.AUTHORIZE_MSISDN_INVALID_MSISDN.name(),
//                    e.getCode());
//        }
//    }
    
    @Test
    public void testAuthorizeMsisdn_basicNotificationSuccess() {
        System.out.println("AuthorizeMsisdnTest.basicNotificationSuccess...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_BASIC_NOTIFICATION);
            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
                    AUTHORIZE_MSISDN);
            iAuthorizeMSISDNService.process(inputParams);

        } catch (MxOSException e) {
            
            assertFalse("MxOSException Happened", true);
        }
    }
    
    
    @Test
    public void testAuthorizeMsisdn_advancedNotificationSuccess() {
        System.out.println("AuthorizeMsisdnTest.advancedNotificationSuccess...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCED_NOTIFICATION);
            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
                    AUTHORIZE_MSISDN);
            iAuthorizeMSISDNService.process(inputParams);

        } catch (MxOSException e) {
            
            assertFalse("MxOSException Happened", true);
        }
    }
    
    
    @Test
    public void testAuthorizeMsisdn_passwordrecoverysmsSuccess() {
        System.out.println("AuthorizeMsisdnTest.passwordrecoverysmsSuccess...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_PASSWORD_RECOVERY_SMS);
            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
                    AUTHORIZE_MSISDN);
            iAuthorizeMSISDNService.process(inputParams);

        } catch (MxOSException e) {
            
            assertFalse("MxOSException Happened", true);
        }
    }

    @Test
    public void testAuthorizeMsisdn_passwordrecoverymsisdnSuccess() {
        System.out.println("AuthorizeMsisdnTest.passwordrecoverymsisdnSuccess...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_PASSWORD_RECOVERY_MSISDN);
            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
                    AUTHORIZE_MSISDN);
            iAuthorizeMSISDNService.process(inputParams);

        } catch (MxOSException e) {
            
            assertFalse("MxOSException Happened", true);
        }
    }
    
    

    @Test
    public void testAuthorizeMsisdn_psmsservicesmsisdnSuccess() {
        System.out.println("AuthorizeMsisdnTest.psmsservicesmsisdnSuccess...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMS_SERVICE_MSISDN);
            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
                    AUTHORIZE_MSISDN);
            iAuthorizeMSISDNService.process(inputParams);

        } catch (MxOSException e) {
            
            assertFalse("MxOSException Happened", true);
        }
    }
    
    
}
