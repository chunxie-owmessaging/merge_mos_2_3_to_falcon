package com.opwvmsg.mxos.test.unit.paf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.backend.crud.mss.MssSlMetaCRUD;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateFolder;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.DeleteMailbox;

public class CreateFolderRmeTest {

    private static final String TEST_NAME = "CreateFolderRmeTest";
    private static final String ARROW_SEP = " --> ";

    public static String CONFIG_HOST = null;
    public static String CONFIG_PORT = null;
    public static String MSS_HOST = null;
    public static final String DEFAULT_APP = "mss";
    public static final String DEFAULT_HOST = "*";
    public static String MSS_RME_VERSION = "175";
    // public static final String MAILBOX_ID = "1366645701422";
    public static final String MAILBOX_ID = "7126905549041528032";
    private static final String PARENT_FOLDER_UUID_STRING = "00000000-0000-1000-0000-000000000000";
    private static final long UID_VALIDITY = 123;
    private static final String FOLDER_NAME = "NEW_FOLDER_1";

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        try {
            String mxosPropertyFile = System.getProperty("MXOS_HOME");
            String mxosconfigfile = mxosPropertyFile
                    + "/config/mxos.properties";
            Properties propfile = new Properties();
            try {
                propfile.load(new FileInputStream(mxosconfigfile));
            } catch (FileNotFoundException e) {
                System.err.println("Unable to find test properties file "
                        + propfile);
            } catch (IOException e) {
                System.err.println("Unable to open test properties file "
                        + propfile);
            }
            CONFIG_HOST = propfile.getProperty("configHost");
            CONFIG_PORT = propfile.getProperty("configPort");
            MSS_RME_VERSION = String.valueOf(MxOSConstants.RME_MX9_VER);
            MSS_HOST = CONFIG_HOST.substring(0, CONFIG_HOST.indexOf("."));
            System.out.println("CONFIG_HOST = " + CONFIG_HOST
                    + " CONFIG_PORT = " + CONFIG_PORT + " MSS_HOST = "
                    + MSS_HOST + " MSS_RME_VERSION = " + MSS_RME_VERSION);
            Config.setConfig("intermail", getConfigdbMap());
            Config config = Config.getInstance();
            createMailBox();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

    }

    /**
     * @throws java.lang.Exception
     */

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteMailBox();
        getConfigdbMap().clear();
    }

    private static Map<String, Object> getConfigdbMap() {
        HashMap<String, Object> configdbMap = new HashMap<String, Object>();
        System.setProperty("mssRmeVersion", MSS_RME_VERSION);
        configdbMap.put("confServHost", CONFIG_HOST);
        configdbMap.put("confServPort", CONFIG_PORT);
        configdbMap.put("defaultApp", DEFAULT_APP);
        configdbMap.put("defaultHost", DEFAULT_HOST);
        return configdbMap;
    }

    @Test
    public void testCreateFolderSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP + "testCreateFolderSuccess");
        try {
            UUID parentfolderUUID = UUID.fromString(PARENT_FOLDER_UUID_STRING);
            CreateFolder createFolder = new CreateFolder(MSS_HOST, MAILBOX_ID,
                    null, parentfolderUUID, FOLDER_NAME, UID_VALIDITY);
            createFolder.execute();
            assertEquals("Unxpected Errors: ", 0,
                    createFolder.getNumLogEvents());
            String newFolderUUID = createFolder.getFolderUUID().toString();
            System.out.println("Folder UUID : " + newFolderUUID);

            UUID folderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST, MAILBOX_ID,
                    null, FOLDER_NAME);
            assertNotNull("Folder Id is not null", folderUUID);
            assertEquals("Unxpected Folder UUID: ", newFolderUUID,
                    folderUUID.toString());
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testCreateFolderParentNotPresent() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateFolderParentNotPresent");
        try {
            UUID parentfolderUUID = null;
            CreateFolder createFolder = new CreateFolder(MSS_HOST, MAILBOX_ID,
                    null, parentfolderUUID, FOLDER_NAME, UID_VALIDITY);
            createFolder.execute();
            fail("This should not have been come!!!");
        } catch (IntermailException e) {
            assertNotNull("Error is not null", e.getFormattedString());
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void testCreateFolderNofolderName() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateFolderNofolderName");
        try {
            UUID parentfolderUUID = UUID.fromString(PARENT_FOLDER_UUID_STRING);
            CreateFolder createFolder = new CreateFolder(MSS_HOST, MAILBOX_ID,
                    null, parentfolderUUID, null, UID_VALIDITY);
            createFolder.execute();
            fail("This should not have been come!!!");
        } catch (IntermailException e) {
            assertNotNull("Error is not null", e.getFormattedString());
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void testCreateFolderInvalidMailboxId() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCreateFolderInvalidMailboxId");
        try {
            UUID parentfolderUUID = UUID.fromString(PARENT_FOLDER_UUID_STRING);
            CreateFolder createFolder = new CreateFolder(MSS_HOST, null, null,
                    parentfolderUUID, FOLDER_NAME, UID_VALIDITY);
            createFolder.execute();
            fail("This should not have been come!!!");
        } catch (IntermailException e) {
            assertNotNull("Error is not null", e.getFormattedString());
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    private static void createMailBox() {
        try {
            CreateMailbox createMailbox = new CreateMailbox(MSS_HOST,
                    MAILBOX_ID, null, null, 0, 0);
            createMailbox.execute();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception while creating mailbox.", true);
        }
    }

    private static void deleteMailBox() {
        try {
            DeleteMailbox deleteMAilbox = new DeleteMailbox(MSS_HOST,
                    MAILBOX_ID, null, false, false, false, RmeDataModel.Leopard);
            deleteMAilbox.execute();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception while deleting mailbox.", true);
        }
    }
}
