package com.opwvmsg.mxos.test.unit.cos.internalinfo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Test Suite for test cases related to InternalInfo and
 * its micro api MessageEventRecords for Cos service.
 * 
 * @author mxos-dev
 *
 */

@RunWith(Suite.class)
@SuiteClasses({
    GetCosInternalInfoTest.class,
    UpdateCosInternalInfoTest.class,
    GetCosMessageEventRecordsTest.class,
    UpdateCosMessageEventRecordsTest.class
})
public class CosInternalInfoSuite {
}
