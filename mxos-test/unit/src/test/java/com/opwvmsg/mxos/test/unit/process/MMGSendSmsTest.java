package com.opwvmsg.mxos.test.unit.process;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ISendSMSService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MMGSendSmsTest {

    private static final String SMSTYPE_BASICNOTIFICATION = "basicnotification";

    private static final String FROMADDRESS_VALUE = "+19886576790";

    private static final String TOADDRESS_VALUE = "+19886576790";
    private static final String TOADDRESS_VALUE_PERM_SNMPERROR = "+19916923750";

    private static final String MESSAGE_VALUE = "Hi Message";

    private static ISendSMSService sendSmsService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("MMGSendSmsTest.setUpBeforeClass...");
        if (System.getProperty("MXOS_HOME") == null
                || System.getProperty("MXOS_HOME").equals("")) {
            System.setProperty("MXOS_HOME", "D:\\LeapFrog\\workspace\\mxos");
        }
        sendSmsService = (ISendSMSService) ContextUtils.loadContext()
                .getService(ServiceEnum.SendSmsService.name());
        assertNotNull("MailboxService object is null.", sendSmsService);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("MMGSendSmsTest.tearDownAfterClass...");
        params.clear();
        params = null;
        sendSmsService = null;
    }

    void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void MMGSendSmsTest_fromAddressNotPresent() {
        System.out.println("MMGSendSmsTest.fromAddressNotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_BASICNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TOADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void MMGSendSmsTest_toAddressNotPresent() {
        System.out.println("MMGSendSmsTest.toAddressNotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_BASICNOTIFICATION);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROMADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void MMGSendSmsTest_messageNotPresent() {
        System.out.println("MMGSendSmsTest.messageNotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_BASICNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TOADDRESS_VALUE);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROMADDRESS_VALUE);
            sendSmsService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void MMGSendSmsTest_fromAddressNotInCorrectFormat() {
        System.out.println("MMGSendSmsTest.fromAddressNotInCorrectFormat...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_BASICNOTIFICATION);
            addParams(inputParams, SmsProperty.fromAddress.name(), "Openwave");
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TOADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_INVALID_FROM_ADDRESS.name(),
                    e.getCode());
        }
    }

    @Test
    public void MMGSendSmsTest_toAddressNotInCorrectFormat() {
        System.out.println("MMGSendSmsTest.toAddressNotInCorrectFormat...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_BASICNOTIFICATION);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROMADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(), "Openwave");
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_INVALID_TO_ADDRESS.name(),
                    e.getCode());
        }
    }

//    @Test
//    public void MMGSendSmsTest_PermanentSNMPError() {
//        System.out.println("MMGSendSmsTest_PermanentSNMPError...");
//        try {
//            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
//            addParams(inputParams, SmsProperty.smsType.name(),
//                    SMSTYPE_BASICNOTIFICATION);
//            addParams(inputParams, SmsProperty.fromAddress.name(),
//                    FROMADDRESS_VALUE);
//            addParams(inputParams, SmsProperty.toAddress.name(),
//                    TOADDRESS_VALUE_PERM_SNMPERROR);
//            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
//            sendSmsService.process(inputParams);
//        } catch (MxOSException e) {
//            assertEquals("Some unexpected error code.",
//                    CustomError.SEND_SMS_PERM_SNMP_ERROR.name(), e.getCode());
//            e.printStackTrace();
//        }
//    }


//    @Test
//    public void MMGSendSmsTest_Success() {
//        System.out.println("MMGSendSmsTest.Success...");
//        try {
//            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
//            addParams(inputParams, SmsProperty.smsType.name(),
//                    SMSTYPE_BASICNOTIFICATION);
//            addParams(inputParams, SmsProperty.fromAddress.name(),
//                    FROMADDRESS_VALUE);
//            addParams(inputParams, SmsProperty.toAddress.name(),
//                    "1001234435");
//            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
//            sendSmsService.process(inputParams);
//        } catch (MxOSException e) {
//            e.printStackTrace();
//            assertFalse("Exception Happened", true);
//        }
//    }
    
}
