package com.opwvmsg.mxos.test.unit.addressbook.contacts.workinfo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to WorkInfo
@SuiteClasses({ 
    ContactsWorkInfoGET.class,
    ContactsWorkInfoPOST.class
    })
public class ContactsWorkInfoTestSuite {

}
