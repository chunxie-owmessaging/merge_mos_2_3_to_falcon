package com.opwvmsg.mxos.test.unit.message;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MessageInFolderDELETE {

    private static final String TEST_NAME = "MessageInFolderDELETE";
    private static final String ARROW_SEP = " --> ";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "message.rest111@openwave.com";
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String PASSWORD = "test5";
    private static final String COS_ID_KEY = MailboxProperty.cosId.name();
    private static final String COS_VALUE = "default";

    private static final String RECEIVED_FROM_KEY = MxOSPOJOs.receivedFrom
            .name();
    private static final String RECEIVED_FROM_VALUE = "message.rest333@openwave.com";
    private static final String FROMADDRESS = "message.rest222@openwave.com";
    private static final String MESSAGE_KEY = SmsProperty.message.name();
    private static final String MESSAGE_VALUE = "Hi, Sending test mails";
    private static final String FOLDER_KEY = FolderProperty.folderName.name();
    private static final String FOLDER_INBOX = "INBOX";
    private static final String MESSAGE_ID_KEY = MessageProperty.messageId
            .name();
    private static final String IS_ADMIN_KEY = MessageProperty.isAdmin.name();
    private static final String IS_ADMIN_VALUE = "true";

    private static IMxOSContext context;
    private static IMessageService messageService;
    private static IMetadataService metadataService;
    private static IMailboxService mailboxService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        createMailBox(EMAIL, PASSWORD);
        createMailBox(FROMADDRESS, PASSWORD);
        messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
        metadataService = (IMetadataService) context
                .getService(ServiceEnum.MetadataService.name());
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("messageService object is null.", messageService);
        assertNotNull("MailBoxService object is null.", mailboxService);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteMailBox(EMAIL);
        deleteMailBox(FROMADDRESS);
        params.clear();
        params = null;
        messageService = null;
        mailboxService = null;
    }

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        inputParams.put(PASSWORD_KEY, new ArrayList<String>());
        inputParams.get(PASSWORD_KEY).add(password);
        inputParams.put(COS_ID_KEY, new ArrayList<String>());
        inputParams.get(COS_ID_KEY).add(COS_VALUE);
         
        String mailBoxId = null;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Unable to create Mailbox...", mailBoxId != null);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void createMessage() {
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, RECEIVED_FROM_KEY, RECEIVED_FROM_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messageService.create(inputParams);
        } catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testDeleteMessageInFolderNoEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteMessageInFolderNoEmail");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messageService.delete(inputParams);
        } catch (MxOSException e) {
            String expectedError = ErrorCode.GEN_BAD_REQUEST.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }

    @Test
    public void testDeleteMessageInFolderNoFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteMessageInFolderNoFolder");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            messageService.delete(inputParams);
        } catch (MxOSException e) {
            String expectedError = ErrorCode.GEN_BAD_REQUEST.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testDeleteMessageInFolderNonExistingFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteMessageInFolderNonExistingFolder");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, "zzzz");
            messageService.delete(inputParams);
        } catch (MxOSException e) {
            String expectedError = ErrorCode.GEN_BAD_REQUEST.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }

    @Test
    public void testDeleteAllMessageInFolderSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteAllMessageInFolderSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            createMessage();
            createMessage();
            createMessage();

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messageService.deleteAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            assertTrue("Messages not deleted", messages.size() == 0);
        } catch (Exception e) {
            assertFalse("Exception1 Occured...", true);
            e.printStackTrace();
        }
    }

    @Test
    public void testDeleteAllMessageInFolderWithIsAdminSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteAllMessageInFolderWithIsAdminSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            createMessage();
            createMessage();
            createMessage();

            // inputParams.clear();
            // addParams(inputParams, EMAIL_KEY, EMAIL);
            // InternalInfo info = infoService.read(inputParams);
            // String lastAccessTime1 = info.getLastAccessDate().toString();

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messageService.deleteAll(inputParams);
            // inputParams.clear();
            // addParams(inputParams, EMAIL_KEY, EMAIL);
            // InternalInfo info2 = infoService.read(inputParams);
            // String lastAccessTime2 = info2.getLastAccessDate().toString();
            // if (lastAccessTime1.equals(lastAccessTime2)) {
            // fail();
            // }
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            assertTrue("Messages not deleted", messages.size() == 0);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }

    @Test
    public void testDeleteMessageInFolderWithMessageIdSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteMessageInFolderWithMessageIdSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            createMessage();
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            String messageId = null;
            for (String msg : messages.keySet()) {
                messageId = msg;
            }

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, MESSAGE_ID_KEY, messageId);
            addParams(inputParams, IS_ADMIN_KEY, IS_ADMIN_VALUE);
            messageService.delete(inputParams);
            System.out.println("delete successfull");

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messages = metadataService.list(inputParams);
            for (String msg : messages.keySet()) {
                String msgIdAfterMove = msg;
                if (msgIdAfterMove != null && msgIdAfterMove.equals(messageId)) {
                    fail();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }

    @Test
    public void testDeleteMessageInFolderWithMessageIdWithIsAdminSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteMessageInFolderWithMessageIdWithIsAdminSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            createMessage();
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            String messageId = null;
            for (String msg : messages.keySet()) {
                messageId = msg;
            }
            // inputParams.clear();
            // addParams(inputParams, EMAIL_KEY, EMAIL);
            // InternalInfo info = infoService.read(inputParams);
            // String lastAccessTime1 = info.getLastAccessDate().toString();

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, MESSAGE_ID_KEY, messageId);
            addParams(inputParams, IS_ADMIN_KEY, IS_ADMIN_VALUE);
            messageService.delete(inputParams);
            System.out.println("delete successfull");
            // inputParams.clear();
            // addParams(inputParams, EMAIL_KEY, EMAIL);
            // InternalInfo info2 = infoService.read(inputParams);
            // String lastAccessTime2 = info2.getLastAccessDate().toString();
            // if (lastAccessTime1.equals(lastAccessTime2)) {
            // fail();
            // }
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messages = metadataService.list(inputParams);
            for (String msg : messages.keySet()) {
                String msgIdAfterMove = msg;
                if (msgIdAfterMove != null && msgIdAfterMove.equals(messageId)) {
                    fail();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }

    @Test
    public void testDeleteAllMessageInMailboxSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteAllMessageInFolderSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            createMessage();
            createMessage();
            createMessage();

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, "INBOX");
            messageService.deleteAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            assertTrue("Messages not deleted", messages.size() == 0);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }

    @Test
    public void testDeleteMultipleMessagesInFolderWithMessageIdSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteMultipleMessagesInFolderWithMessageIdSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messageService.deleteAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            createMessage();
            createMessage();
            createMessage();
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            inputParams.put(MESSAGE_ID_KEY, new ArrayList<String>());
            for (String msg : messages.keySet()) {
                inputParams.get(MESSAGE_ID_KEY).add(msg);
            }
            messageService.delete(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);

            Map<String, Metadata> messagesInFromFolder = metadataService
                    .list(inputParams);
            if (messagesInFromFolder.keySet().size() > 0) {
                fail();
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testDeleteMultipleMessages_Success() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testDeleteMultipleMessages_Success");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            createMessage();
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            String messageId = null;
            for (String msg : messages.keySet()) {
                messageId = msg;
            }

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, MESSAGE_ID_KEY, messageId);
            addParams(inputParams, IS_ADMIN_KEY, IS_ADMIN_VALUE);
            messageService.deleteMulti(inputParams);
            System.out.println("delete successfull");

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messages = metadataService.list(inputParams);
            for (String msg : messages.keySet()) {
                String msgIdAfterMove = msg;
                if (msgIdAfterMove != null && msgIdAfterMove.equals(messageId)) {
                    fail();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception1 Occured...", true);
        }
    }
}
