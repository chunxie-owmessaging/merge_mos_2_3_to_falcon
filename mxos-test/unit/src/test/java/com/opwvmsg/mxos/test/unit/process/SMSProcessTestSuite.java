package com.opwvmsg.mxos.test.unit.process;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    VampSendSmsTest.class,
    VampAndSdupTest.class,
    FBISendSmsTest.class,
    AuthorizeMsisdnTest.class,
    MaaLogToExternalEntityTest.class,
    SendMailTest.class,
    MMGSendSmsTest.class
})
public class SMSProcessTestSuite {
}
