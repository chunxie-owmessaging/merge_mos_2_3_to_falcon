package com.opwvmsg.mxos.test.unit.paf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.backend.crud.mss.MssSlMetaCRUD;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.mail.FolderInfo;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateFolder;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.DeleteFolder;
import com.opwvmsg.utils.paf.intermail.mail.ops.DeleteMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.RenameFolder;

public class RenameFolderRmeTest {

    private static final String TEST_NAME = "RenameFolderRmeTest";
    private static final String ARROW_SEP = " --> ";

    public static String CONFIG_HOST = null;
    public static String CONFIG_PORT = null;
    public static String MSS_HOST = null;
    public static final String DEFAULT_APP = "mss";
    public static final String DEFAULT_HOST = "*";
    public static String MSS_RME_VERSION = "175";
    public static String MAILBOX_ID = "7126905549041528032";
    // public static String MAILBOX_ID = "1367454367424";
    private static final String PARENT_FOLDER_UUID_STRING = "00000000-0000-1000-0000-000000000000";
    private static final int UID_VALIDITY = 123;
    private static final String FOLDER_NAME = "FOLDER_1";
    private static final String NEW_FOLDER_NAME = "FOLDER_2";
    public static final String FORWARD_SLASH = "/";
    public static UUID folderUUID = null;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        try {
            String mxosPropertyFile = System.getProperty("MXOS_HOME");
            String mxosconfigfile = mxosPropertyFile
                    + "/config/mxos.properties";
            Properties propfile = new Properties();
            try {
                propfile.load(new FileInputStream(mxosconfigfile));
            } catch (FileNotFoundException e) {
                System.err.println("Unable to find mxos properties file "
                        + propfile);
            } catch (IOException e) {
                System.err.println("Unable to open mxos properties file "
                        + propfile);
            }
            CONFIG_HOST = propfile.getProperty("configHost");
            CONFIG_PORT = propfile.getProperty("configPort");
            MSS_RME_VERSION = String.valueOf(MxOSConstants.RME_MX9_VER);
            MSS_HOST = CONFIG_HOST.substring(0, CONFIG_HOST.indexOf("."));
            System.out.println("CONFIG_HOST = " + CONFIG_HOST
                    + " CONFIG_PORT = " + CONFIG_PORT + " MSS_HOST = "
                    + MSS_HOST + " MSS_RME_VERSION = " + MSS_RME_VERSION);
            Config.setConfig("intermail", getConfigdbMap());
            Config config = Config.getInstance();
            createMailBox();
            folderUUID = createFolder();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {

    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteFolder();
        deleteMailBox();
        getConfigdbMap().clear();
    }

    private static Map<String, Object> getConfigdbMap() {
        HashMap<String, Object> configdbMap = new HashMap<String, Object>();
        System.setProperty("mssRmeVersion", MSS_RME_VERSION);
        configdbMap.put("confServHost", CONFIG_HOST);
        configdbMap.put("confServPort", CONFIG_PORT);
        configdbMap.put("defaultApp", DEFAULT_APP);
        configdbMap.put("defaultHost", DEFAULT_HOST);
        return configdbMap;
    }

    @Test
    public void testRenameFolderSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP + "testRenameFolderSuccess");
        try {
            UUID parentfolderUUID = UUID.fromString(PARENT_FOLDER_UUID_STRING);
            RenameFolder renameFolder = new RenameFolder(MSS_HOST, MAILBOX_ID,
                    null, folderUUID, NEW_FOLDER_NAME, parentfolderUUID, false,
                    UID_VALIDITY);
            renameFolder.execute();
            assertEquals("Unxpected Errors: ", 0,
                    renameFolder.getNumLogEvents());
            FolderInfo folderInfo = readFolderInfo(NEW_FOLDER_NAME);
            System.out
                    .println("After rename operation : folderInfo.getFolderName() = "
                            + folderInfo.getFolderName());
            assertEquals("Unxpected folder: ", NEW_FOLDER_NAME,
                    folderInfo.getFolderName());
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testRenameFolderWithParentFolderUUIDNull() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRenameFolderWithParentFolderUUIDNull");
        try {
            UUID parentfolderUUID = null;
            RenameFolder renameFolder = new RenameFolder(MSS_HOST, MAILBOX_ID,
                    null, folderUUID, NEW_FOLDER_NAME, parentfolderUUID, false,
                    UID_VALIDITY);
            renameFolder.execute();
            fail("This should not have been come!!!");
        } catch (IntermailException e) {
            assertNotNull("Error is not null", e);
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    @Test
    public void testRenameFolderWithRenameFolderUUIDNull() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRenameFolderWithRenameFolderUUIDNull");
        try {
            UUID folderUUID = null;
            UUID parentfolderUUID = UUID.fromString(PARENT_FOLDER_UUID_STRING);
            RenameFolder renameFolder = new RenameFolder(MSS_HOST, MAILBOX_ID,
                    null, folderUUID, NEW_FOLDER_NAME, parentfolderUUID, false,
                    UID_VALIDITY);
            renameFolder.execute();
            fail("This should not have been come!!!");
        } catch (IntermailException e) {
            assertNotNull("Error is not null", e.getFormattedString());
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }

    private static UUID createFolder() {
        try {
            UUID parentfolderUUID = UUID.fromString(PARENT_FOLDER_UUID_STRING);
            CreateFolder createFolder = new CreateFolder(MSS_HOST, MAILBOX_ID,
                    null, parentfolderUUID, FOLDER_NAME, UID_VALIDITY);
            createFolder.execute();
            return createFolder.getFolderUUID();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
        return null;
    }

    private static UUID deleteFolder() {
        try {
            UUID parentfolderUUID = UUID.fromString(PARENT_FOLDER_UUID_STRING);
            UUID renamedFolderUUID = MssSlMetaCRUD.getFolderUUID(MSS_HOST,
                    MAILBOX_ID, null, NEW_FOLDER_NAME);
            DeleteFolder deleteFolder = new DeleteFolder(MSS_HOST, MAILBOX_ID,
                    null, parentfolderUUID, renamedFolderUUID, false);
            deleteFolder.execute();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
        return null;
    }

    private static void createMailBox() {
        try {
            CreateMailbox createMailbox = new CreateMailbox(MSS_HOST,
                    MAILBOX_ID, null, null, 0, 0);
            createMailbox.execute();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception while creating mailbox.", true);
        }
    }

    private static void deleteMailBox() {
        try {
            DeleteMailbox deleteMAilbox = new DeleteMailbox(MSS_HOST,
                    MAILBOX_ID, null, false, false, false, RmeDataModel.Leopard);
            deleteMAilbox.execute();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception while deleting mailbox.", true);
        }
    }

    private FolderInfo readFolderInfo(String folderName) {
        FolderInfo folderInfo = null;
        try {
            FolderInfo[] folderInfoArray = MssSlMetaCRUD.getFolderInfo(
                    MSS_HOST, MAILBOX_ID, null);
            if (folderInfoArray != null) {
                for (FolderInfo f : folderInfoArray) {
                    if (f.getPathName().equals(FORWARD_SLASH + folderName)) {
                        folderInfo = f;
                        return f;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception while reading folder data.", true);
        }
        return folderInfo;
    }
}
