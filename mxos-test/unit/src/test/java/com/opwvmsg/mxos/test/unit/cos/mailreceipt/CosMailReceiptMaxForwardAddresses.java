package com.opwvmsg.mxos.test.unit.cos.mailreceipt;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */

public class CosMailReceiptMaxForwardAddresses {
    private static ICosMailReceiptService cosmr = null;
    private static ICosService cosService = null;
    private static Map <String, List<String> >inputParams =
        new HashMap<String, List<String>>();
    private static String test = "test";
    private static String cosIdKey = "cosId";
    
    @BeforeClass
    public static void BeforeClassSetUp() throws Exception {
        System.out.println("BeforeClassSetUp:cos starting......");
        inputParams.clear();
        inputParams.put(cosIdKey, new ArrayList<String>());
        inputParams.put("loadDefaultCos", new ArrayList<String>());
        inputParams.get(cosIdKey).add(test);
        inputParams.get("loadDefaultCos").add("yes");
        try {
            cosmr = (ICosMailReceiptService) ContextUtils.loadContext()
                    .getService(ServiceEnum.CosMailReceiptService.name());
            cosService = (ICosService) ContextUtils.loadContext()
                    .getService(ServiceEnum.CosService.name());
            cosService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
        }
        assertNotNull(cosmr);
    }
    
    
    @AfterClass
    public static void TearDown() throws Exception {
        System.out.println("TearDown:cos starting......");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            cosService.delete(inputParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testMaxForwardAddressesGet() {
        System.out.println("testMaxForwardAddressesGet:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            MailReceipt mr = cosmr.read(inputParams);
            assertNotNull(mr);
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMaxForwardAddressesPostNullValue() {
        System.out.println("testMaxForwardAddressesPostNullValue ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put(MailboxProperty.maxNumForwardingAddresses .name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses .name())
                .add(null);
        System.out.println("Updating cos mail receipt with <null>");
        try {
            cosmr.update(inputParams);
            fail();
            System.out
                    .println("Max forwarding addresses value accepted on updating cos mail receipt ");
        } catch(MxOSException me) {
            if (me.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
                assertNotNull("Invalid value for maxforwardingaddresses",me);
            }
            else {
                fail() ;
            }
            me.printStackTrace();
            
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMaxForwardAddressesPostEmptyValue() {
        System.out.println("testMaxForwardAddressesPostEmptyValue ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put(MailboxProperty.maxNumForwardingAddresses .name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses .name())
                .add("");
        System.out.println("Updating cos mail receipt with <empty value>");
        try {
            cosmr.update(inputParams);
            fail();
            System.out
                    .println("Max forwarding addresses value accepted on updating cos mail receipt ");
        } catch(MxOSException me) {
            if (me.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
                assertNotNull("Invalid value for maxforwardingaddresses",me);
            } else {
                fail() ;
            }
            me.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMaxForwardAddressesPostAlphaNumericValue() {
        System.out.println("testMaxForwardAddressesPostAlphaNumericValue ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put(MailboxProperty.maxNumForwardingAddresses .name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses .name())
                .add("12a");
        System.out
                .println("Updating cos mail receipt with AlphaNumericValue 12a");
        try {
            cosmr.update(inputParams);
            fail();
            System.out
                    .println("Max forwarding addresses value accepted on updating cos mail receipt ");
        } catch(MxOSException me) {
            if (me.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
                assertNotNull("Invalid value for maxforwardingaddresses",me);
            } else {
                fail() ;
            }
            me.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMaxForwardAddressesPostSpecialCharsValue() {
        System.out.println("testMaxForwardAddressesPostSpecialCharsValue ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put(MailboxProperty.maxNumForwardingAddresses .name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses .name())
                .add("2.%^(");
        System.out
        .println("Updating cos mail receipt with SpecialCharsValue %^(");
        try {
            cosmr.update(inputParams);
            fail();
            System.out
                    .println("Max forwarding addresses value accepted on updating cos mail receipt ");
        } catch(MxOSException me) {
            if (me.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
                assertNotNull("Invalid value for maxforwardingaddresses",me);
            } else {
                fail() ;
            }
            me.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMaxForwardAddressesPostMinLimitValue() {
        System.out.println("testSenderBlockingGet:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put(MailboxProperty.maxNumForwardingAddresses .name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses .name())
                .add("-1");
        System.out
        .println("Updating cos mail receipt with value < MinLimitValue(0) = -1");
        try {
            cosmr.update(inputParams);
            fail();
            System.out
                    .println("Max forwarding addresses value accepted on updating cos mail receipt ");
        } catch(MxOSException me) {
            if (me.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
                assertNotNull("Invalid value for maxforwardingaddresses",me);
            } else {
                fail() ;
            }
            me.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMaxForwardAddressesPostMaxLimitValue() {
        System.out.println("testSenderBlockingGet:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put(MailboxProperty.maxNumForwardingAddresses .name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses .name())
                .add("2147483648");
        System.out
                .println("Updating cos mail receipt with value > MaxLimitValue(2147483647) = 2147483648");
        try {
            cosmr.update(inputParams);
            fail();
            System.out
                    .println("Max forwarding addresses value accepted on updating cos mail receipt ");
        } catch(MxOSException me) {
            if (me.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
                assertNotNull("Invalid value for maxforwardingaddresses",me);
            } else {
                fail() ;
            }
            me.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMaxForwardAddressesPostCorrectValue() {
        System.out.println("testSenderBlockingGet:cos ....");
        inputParams.clear();
        Integer val = 15;
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put(MailboxProperty.maxNumForwardingAddresses .name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses .name())
                .add(val.toString());
        System.out
                .println("Updating cos mail receipt with CorrectValue(Range is 0-2147483647) =  15");
        try {
            cosmr.update(inputParams);
            inputParams.clear();
            inputParams.put("cosId", new ArrayList<String>());
            inputParams.get("cosId").add(test);
            MailReceipt mr = cosmr.read(inputParams);
            assertNotNull(mr);
            Integer value = mr.getMaxNumForwardingAddresses();
            assertNotNull("Updated with correct value",value);
            assertEquals("Has a wrong value.", value, val);
            System.out
            .println("Max forwarding addresses value after updating cos mail receipt "
                    + mr.toString());
        } catch(MxOSException me) {
            if (me.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
            } 
            me.printStackTrace();
            fail() ;
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}
