package com.opwvmsg.mxos.test.unit.credentials;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.opwvmsg.mxos.data.pojos.Credentials;

@RunWith(Suite.class)
//Include all the test case classes related to MailAccess 
@SuiteClasses({
    CredentialsTest.class
})
public class CredentialsSuite {
}
