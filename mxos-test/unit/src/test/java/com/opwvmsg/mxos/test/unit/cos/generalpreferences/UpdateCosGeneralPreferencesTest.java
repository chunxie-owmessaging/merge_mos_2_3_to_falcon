package com.opwvmsg.mxos.test.unit.cos.generalpreferences;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.PreferredUserExperience;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosGeneralPreferencesService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateCosGeneralPreferencesTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "default_cos_1234";
    private static ICosGeneralPreferencesService cosGeneralPreferencesService;
    private static IMxOSContext context;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out
                .println("UpdateCosGeneralPreferencesTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        cosGeneralPreferencesService = (ICosGeneralPreferencesService) context
                .getService(ServiceEnum.CosGeneralPreferencesService.name());
        boolean flag = CosHelper.createCos(COS_ID);
        assertTrue("Cos was not created.", flag);
        getParams.put(COSID_KEY, new ArrayList<String>());
        getParams.get(COSID_KEY).add(COS_ID);
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        assertNotNull("cosGeneralPreferencesService object is null.",
                cosGeneralPreferencesService);

    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out
                .println("UpdateCosGeneralPreferencesTest...tearDownAfterClass");
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        CosHelper.deleteCos(COS_ID);
        cosGeneralPreferencesService = null;
    }

    private static void UpdateCosGeneralPreferencesParams(
            Map<String, List<String>> updateParams) {
        updateCosGeneralPreferencesParams(updateParams, null);
    }

    private static void updateCosGeneralPreferencesParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            cosGeneralPreferencesService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!");
            }

        } catch (MxOSException me) {
            if (null == expectedError) {
                fail("This should not have come!");
            } else {
                assertNotNull("MxOSException is not null.", me);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, me.getCode());
            }
        } finally {

            if (updateParams.isEmpty()|| null == updateParams.get(COSID_KEY)
                    || updateParams.get(COSID_KEY).isEmpty()) {
                updateParams.put(COSID_KEY, new ArrayList<String>());
                updateParams.get(COSID_KEY).add(COS_ID);
            }
        }
    }

    private static GeneralPreferences GetCosGeneralPreferencesParams() {
        GeneralPreferences generalPreferences = null;
        try {
            generalPreferences = cosGeneralPreferencesService.read(getParams);
        } catch (MxOSException me) {
            me.printStackTrace();
            fail();

        } finally {
            if (getParams.isEmpty() || null == getParams.get(COSID_KEY)
                    || getParams.get(COSID_KEY).isEmpty()) {
                getParams.put(COSID_KEY, new ArrayList<String>());
                getParams.get(COSID_KEY).add(COS_ID);
            }
        }
        assertNotNull("GeneralPreferences object is null.", generalPreferences);
        return generalPreferences;
    }

    @Test
    public void testGetCosGeneralPreferencesWithCosId() {
        GeneralPreferences result = null;
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            inputParams.clear();
            inputParams.put(COSID_KEY, new ArrayList<String>());
            inputParams.get(COSID_KEY).add(COS_ID);

            result = cosGeneralPreferencesService.read(inputParams);
            assertNotNull(result);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull(result);
        System.out.println("GET API: " + result);

    }

    @Test
    public void testUpdateCosGeneralPreferencesWithoutCosId() throws Exception {
        updateParams.clear();
        System.out
                .println("UpdateCosGeneralPreferencesTest.testUpdateCosGeneralPreferencesWithoutCosId...");
        updateCosGeneralPreferencesParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateCosGeneralPreferencesWithEmptyCosId() throws Exception {
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add("");
        System.out
                .println("UpdateCosGeneralPreferencesTest.testUpdateCosGeneralPreferencesWithEmptyCosId...");
        updateCosGeneralPreferencesParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateCosGeneralPreferencesWithNullCosId() throws Exception {
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(null);
        System.out
                .println("UpdateCosGeneralPreferencesTest.testUpdateCosGeneralPreferencesWithNullCosId...");
        updateCosGeneralPreferencesParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testUpdateCosGeneralPreferencesWithoutAnyParam() throws Exception {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testUpdateCosGeneralPreferencesWithoutAnyParam...");
        updateCosGeneralPreferencesParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testRecycleBinEnabledEmptyParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testRecycleBinEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.recycleBinEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_RECYCLEBIN_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testRecycleBinEnabledNullParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testRecycleBinEnabledNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.recycleBinEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_RECYCLEBIN_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testRecycleBinEnabledInvalidParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testRecycleBinEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.recycleBinEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("junk");
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_RECYCLEBIN_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testRecycleBinEnabledSuccess() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testRecycleBinEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.recycleBinEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        UpdateCosGeneralPreferencesParams(updateParams);
        BooleanType rbe = GetCosGeneralPreferencesParams()
                .getRecycleBinEnabled();
        assertNotNull("recycleBinEnabled object is null.", rbe);
        assertTrue("recycleBinEnabled has a wrong value.",
                rbe.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testParentalControlEnabledEmptyParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testParentalControlEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.parentalControlEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_PARENTALCONTROL.name());
        updateParams.remove(key);
    }

    @Test
    public void testParentalControlEnabledNullParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testParentalControlEnabledNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.parentalControlEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_PARENTALCONTROL.name());
        updateParams.remove(key);
    }

    @Test
    public void testParentalControlEnabledInvalidParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testParentalControlEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.parentalControlEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("junk");
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_PARENTALCONTROL.name());
        updateParams.remove(key);
    }

    @Test
    public void testParentalControlEnabledSuccess() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testParentalControlEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.parentalControlEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        UpdateCosGeneralPreferencesParams(updateParams);
        BooleanType pce = GetCosGeneralPreferencesParams()
                .getParentalControlEnabled();
        assertNotNull("parentalControlEnabled object is null.", pce);
        assertTrue("parentalControlEnabled has a wrong value.",
                pce.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testWebmailMTAEmptyParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testWebmailMTAEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.webmailMTA.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_WEBMAILMTA.name());
        updateParams.remove(key);
    }

    @Test
    public void testWebmailMTANullParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testWebmailMTANullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.webmailMTA.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_WEBMAILMTA.name());
        updateParams.remove(key);
    }

    @Test
    public void testWebmailMTASuccess() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testWebmailMTASuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.webmailMTA.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("localhost");
        UpdateCosGeneralPreferencesParams(updateParams);
        String wa = GetCosGeneralPreferencesParams().getWebmailMTA();
        assertNotNull("WebmailMTA object is null.", wa);
        assertTrue("WebmailMTA has a wrong value.", wa.equals("localhost"));
        updateParams.remove(key);
    }

    @Test
    public void testTimezoneEmptyParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testTimezoneEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.timezone.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_TIMEZONE.name());
        updateParams.remove(key);
    }

    @Test
    public void testTimezoneInvalidParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testTimezoneInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.timezone.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("junk");
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_TIMEZONE.name());
        updateParams.remove(key);
    }

    @Test
    public void testTimezoneSuccess() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testWebmailMTASuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.timezone.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("PST8PDT");
        UpdateCosGeneralPreferencesParams(updateParams);
        String timeZone = GetCosGeneralPreferencesParams().getTimezone();
        assertNotNull("timeZone object is null.", timeZone);
        assertTrue("WebmailMTA has a wrong value.", timeZone.equals("PST8PDT"));
        updateParams.remove(key);
    }

    @Test
    public void testPreferredThemeEmptyParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testPreferredThemeEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.preferredTheme.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_PREFERRED_THEME.name());
        updateParams.remove(key);
    }

    @Test
    public void testPreferredThemeNullParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testPreferredThemeNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.preferredTheme.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_PREFERRED_THEME.name());
        updateParams.remove(key);
    }

    @Test
    public void testPreferredThemeSuccess() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testPreferredThemeSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.preferredTheme.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("green2");
        UpdateCosGeneralPreferencesParams(updateParams);
        String pt = GetCosGeneralPreferencesParams().getPreferredTheme();
        assertNotNull("preferredTheme object is null.", pt);
        assertTrue("preferredTheme has a wrong value.", pt.equals("green2"));
        updateParams.remove(key);
    }

    @Test
    public void testPreferredUserExperienceEmptyParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.PreferredUserExperience...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.preferredUserExperience.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_PREFERRED_USER_EXPERIENCE.name());
        updateParams.remove(key);
    }

    @Test
    public void testPreferredUserExperienceNullParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.PreferredUserExperience...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.preferredUserExperience.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_PREFERRED_USER_EXPERIENCE.name());
        updateParams.remove(key);
    }

    @Test
    public void testPreferredUserExperienceInvalidParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.PreferredUserExperience...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.preferredUserExperience.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("junk");
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_PREFERRED_USER_EXPERIENCE.name());
        updateParams.remove(key);
    }

    @Test
    public void testPreferredUserExperienceSuccess() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.PreferredUserExperience...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.preferredUserExperience.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(PreferredUserExperience.RICH.name());
        UpdateCosGeneralPreferencesParams(updateParams);
        PreferredUserExperience pue = GetCosGeneralPreferencesParams()
                .getPreferredUserExperience();
        assertNotNull("PreferredUserExperience object is null.", pue);
        assertTrue("PreferredUserExperience has a wrong value.",
                pue.equals(PreferredUserExperience.RICH));
        updateParams.remove(key);
    }

    @Test
    public void testCharsetEmptyParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testCharsetEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.charset.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_CHARSET.name());
        updateParams.remove(key);
    }

    @Test
    public void testCharsetNullParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testCharsetNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.charset.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_CHARSET.name());
        updateParams.remove(key);
    }

    @Test
    public void testCharsetInvalidParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testCharsetInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.charset.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("junk");
        updateCosGeneralPreferencesParams(updateParams,
                MailboxError.MBX_INVALID_CHARSET.name());
        updateParams.remove(key);
    }

    @Test
    public void testCharsetSuccess() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testCharsetSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.charset.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("ISO-8859-1");
        UpdateCosGeneralPreferencesParams(updateParams);
        String ct = GetCosGeneralPreferencesParams().getCharset();
        assertNotNull("Charset object is null.", ct);
        assertTrue("Charset has a wrong value.", ct.equals("ISO-8859-1"));
        updateParams.remove(key);
    }

    @Test
    public void testGeneralPreferencesWithoutAnyParam() {
        System.out
                .println("UpdateCosGeneralPreferencesTest.testGeneralPreferencesWithoutAnyParam...");
        updateCosGeneralPreferencesParams(updateParams,
                ErrorCode.GEN_BAD_REQUEST.name());

    }

}
