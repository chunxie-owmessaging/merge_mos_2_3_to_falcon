package com.opwvmsg.mxos.test.unit.tasks.Folder;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to MailAccess
@SuiteClasses({ AppFolderOperation.class })
public class AppsFolderSuite{
}
