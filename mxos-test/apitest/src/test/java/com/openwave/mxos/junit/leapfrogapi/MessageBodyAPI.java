package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
//import com.opwvmsg.mxos.data.pojos.Message;
//import com.opwvmsg.mxos.data.pojos.MessageBody;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IBodyService;
//import com.opwvmsg.mxos.interfaces.service.meta.IMessageBodyService;
import com.opwvmsg.mxos.message.pojos.Body;


public class MessageBodyAPI {

	static Logger alltestlog = Logger.getLogger(MessageBodyAPI.class);

	 public static int execute(final IMxOSContext context, ServiceEnum service,
	            Operation operation, String inputstring, String testCaseName,
	            String expectedOutput, String expResult) {

	        int testStatus = 0;
	        MxosRequestMap map;
	        map = createInputParams.create(inputstring, operation);

	        int checkExpOutput = 1;
	        if (null == expectedOutput || expectedOutput == "") {
	            checkExpOutput = 0;
	        }
	        System.out.println("checkExpOutput =" + checkExpOutput);
	        System.out.println("expHttpOut =" + expResult);

	        switch (service) {
	        case BodyService: {
	            switch (operation) {
            case GET: {
	                CommonUtil.printstartAPI("GET Message body");
	                testStatus = readMessageBody (context, service, map, expResult,
	                        expectedOutput, checkExpOutput);
	                return testStatus;
	            }
	            }
	        }
	        }
	     return 1;
    }
	
    public static int readMessageBody(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        Body mBody = null;
        int result;
        String method = "read MessageBody";

        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());

        try {
            CommonUtil.printOnBoth(method);
            final IBodyService messageBodyService = (IBodyService) context
                    .getService(service.name());
            mBody = messageBodyService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("...MessageBody  == " + mBody);
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mBody != null) {
                result = CommonUtil.comparePayload(mBody.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

}