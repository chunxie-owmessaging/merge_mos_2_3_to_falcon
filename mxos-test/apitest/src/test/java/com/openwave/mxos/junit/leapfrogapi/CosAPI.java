package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;

public class CosAPI {

    static Logger alltestlog = Logger.getLogger("alltestlogs");

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (operation) {
        case PUT: {
            CommonUtil.printstartAPI("CREATE COS SERVICE");
            testStatus = create(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case DELETE: {
            CommonUtil.printstartAPI("DELETE COS SERVICE");
            testStatus = delete(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        }
        return 1;
    }

    public static int create(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Create CosService";
        try {
            ICosService cosService = (ICosService) context.getService(service
                    .name());
            CommonUtil.printOnBoth(method);
            cosService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int delete(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Delete CosService";
        try {
            ICosService cosService = (ICosService) context.getService(service
                    .name());
            CommonUtil.printOnBoth(method);
            cosService.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

}
