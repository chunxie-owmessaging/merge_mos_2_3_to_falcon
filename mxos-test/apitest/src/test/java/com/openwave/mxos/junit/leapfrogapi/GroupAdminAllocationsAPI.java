package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.GroupAdminAllocations;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailAccessService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IEmailAliasService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IGroupAdminAllocationsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessAllowedIpService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessService;

public class GroupAdminAllocationsAPI {

	static Logger alltestlog = Logger.getLogger(MailReceiptAPI.class);

	public static int execute(final IMxOSContext context, ServiceEnum service,
			Operation operation, String inputstring, String testCaseName,
			String expectedOutput, String expResult) {

		int testStatus = 0;
		MxosRequestMap map;
		map = createInputParams.create(inputstring, operation);

		int checkExpOutput = 1;
		if (null == expectedOutput || expectedOutput == "") {
			checkExpOutput = 0;
		}
		System.out.println("checkExpOutput =" + checkExpOutput);
		System.out.println("expHttpOut =" + expResult);
		switch (service) {
		case GroupAdminAllocationsService: {
			switch (operation) {
			case GET: {
				CommonUtil.printstartAPI("GET Group Admin Allocation");
				testStatus = read(context, service, map, expResult,
						expectedOutput, checkExpOutput);
				return testStatus;
			}
			case POST: {
				CommonUtil.printstartAPI("UPDATE Group Admin Allocation");
				testStatus = update(context, service, map, expResult,
						expectedOutput, checkExpOutput);
				return testStatus;
			}
			}
		}
		}
		return 1;
	}

	public static int read(final IMxOSContext context, ServiceEnum service,
			MxosRequestMap map, String expErrorCode, String expectedOutput,
			int checkExpOutput) {
		String errorCode = LfConstants.HTTPSUCCESS;
		int result;
		String method = "read Group Admin Allocation";
		GroupAdminAllocations groupAdminAllocations = null;
		try {
			CommonUtil.printOnBoth(method);
			IGroupAdminAllocationsService groupAdminAllocationsObj = (IGroupAdminAllocationsService) context
					.getService(service.name());
			groupAdminAllocations = groupAdminAllocationsObj.read(map
					.getMultivaluedMap());
			alltestlog.info("groupAdminAllocations = " + groupAdminAllocations);
			CommonUtil.printOnBoth("groupAdminAllocations = "
					+ groupAdminAllocations);
			CommonUtil.printOnBoth(method + " ...done");
		} catch (MxOSException e) {
			errorCode = CommonUtil.getPrintErrorCode(method, e);
		}
		result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
		if (result == 1) {
			if (checkExpOutput == 1 && groupAdminAllocations != null) {
				result = CommonUtil.comparePayload(
						groupAdminAllocations.toString(), expectedOutput);
			}
		}
		return result;
	}

	public static int update(final IMxOSContext context, ServiceEnum service,
			MxosRequestMap map, String expErrorCode, String expectedOutput,
			int checkExpOutput) {
		String errorCode = LfConstants.HTTPSUCCESS;
		int result;
		String method = "update Group Admin Allocation";
		try {
			CommonUtil.printOnBoth(method);
			IGroupAdminAllocationsService groupAdminAllocationsObj = (IGroupAdminAllocationsService) context
					.getService(service.name());
			groupAdminAllocationsObj.update(map.getMultivaluedMap());
			CommonUtil.printOnBoth(method + " ..done");
		} catch (MxOSException e) {
			errorCode = CommonUtil.getPrintErrorCode(method, e);
		}
		result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
		return result;
	}

}
