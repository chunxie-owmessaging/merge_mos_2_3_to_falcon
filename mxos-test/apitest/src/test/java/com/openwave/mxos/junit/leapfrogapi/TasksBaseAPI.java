package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksBaseService;
import com.opwvmsg.mxos.task.pojos.TaskBase;

public class TasksBaseAPI {
	
    static Logger alltestlog = Logger.getLogger(TasksBaseAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
            case TasksBaseService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("Read Tasks Base Service");
                        testStatus = read(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("Update Tasks Base Service");
                        testStatus = update(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case LIST: {
                        CommonUtil.printstartAPI("List Tasks Base Service");
                        testStatus = list(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
          }
        return 1;
    }
    
    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        TaskBase taskBase = null;
        int result;
        String method = "Read Task Base";
        try {
            CommonUtil.printOnBoth(method);
            ITasksBaseService taskBaseService = (ITasksBaseService) context
                    .getService(service.name());
            taskBase = taskBaseService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("Task Base is = " + taskBase);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && taskBase != null) {
                result = CommonUtil.comparePayload(taskBase.toString(),
                        expectedOutput);
            }
        }
        return result;
    }
   
    public static int update(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update Task Base";

        try {
            CommonUtil.printOnBoth(method);
            final ITasksBaseService taskBaseService = (ITasksBaseService) context
                    .getService(service.name());
            taskBaseService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    public static int list(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        List<TaskBase> taskBaseList = null;

        String method = "List Task Base";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        try {
            CommonUtil.printOnBoth(method);
            final ITasksBaseService taskBaseService = (ITasksBaseService) context
                    .getService(service.name());
            taskBaseList = taskBaseService.list(multiMap);
            CommonUtil.printOnBoth("...task list == " + taskBaseList);
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && taskBaseList != null) {
                result = CommonUtil.comparePayload(taskBaseList.toString(),
                        expectedOutput);
            }
        }
        return result;    	
    }

}
