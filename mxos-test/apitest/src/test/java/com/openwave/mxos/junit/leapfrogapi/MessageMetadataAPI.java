package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.message.pojos.Metadata;

public class MessageMetadataAPI {


    static Logger alltestlog = Logger.getLogger(MessageMetadataAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult,
            Map<String, String> mapParams) {

        int testStatus = 0;
        MxosRequestMap map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (operation) {

        case GET: {
            CommonUtil.printstartAPI("get Message MetaData");
            testStatus = read(context, service, map, expResult, expectedOutput,
                    checkExpOutput);
            return testStatus;
        }
        case POST: {
            CommonUtil.printstartAPI("Update message MetaData");
            testStatus = update(context, service, map, expResult,
                    expectedOutput, checkExpOutput, mapParams);
            return testStatus;
        }
        case LIST: {
            CommonUtil.printstartAPI("List message MetaData");
            testStatus = list(context, service, map, expResult, expectedOutput,
                    checkExpOutput);
            return testStatus;
        }
        }
        return 1;
    }

    private static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput, Map<String, String> mapParams) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update message MetaData";
        String msgId = null;
        
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());
    
        try {
        	IMetadataService metadataService = (IMetadataService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            metadataService.update(multiMap);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    private static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        Metadata msg1 = null;
        String method = "Read message MetaData";
        
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());
        try {
        	IMetadataService metadataService = (IMetadataService) context
            .getService(service.name());
            CommonUtil.printOnBoth(method);
            msg1 = metadataService.read(multiMap);
            CommonUtil.printOnBoth(method + " ..done");
            CommonUtil.printOnBoth("msg metadata = " + msg1 );
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        CommonUtil.printOnBoth("checkExpOutput" + checkExpOutput);
        CommonUtil.printOnBoth("result" + result);

        if (result == 1) {
            if (checkExpOutput == 1 && msg1 != null) {
                CommonUtil.printOnBoth("********inside 1 *********");

                result = CommonUtil.comparePayload(msg1.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    private static int list(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        Map<String, Metadata> msg1 = null;
        String method = "List message metadata";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        CommonUtil.replcaePlaceholder(multiMap,
                MessageProperty.messageId.name());
        try {
        	IMetadataService metadataService = (IMetadataService) context
            .getService(service.name());
            CommonUtil.printOnBoth(method);
            msg1 =  metadataService.list(multiMap);
            CommonUtil.printOnBoth(method + " ..done");
            CommonUtil.printOnBoth("msg metadata list = " + msg1.toString() );
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            
        	if (checkExpOutput == 1 && msg1 != null) {
            
              MxosRequestMap mapexpout = createInputParams.expout(expectedOutput);
              Map<String, List<String>> multiMap1 = mapexpout.getMultivaluedMap();
              CommonUtil.replaceExpectOutputMsgId(multiMap1,
                      MessageProperty.messageId.name());
             List<String> msgList =  multiMap1.get(MessageProperty.messageId.name());
             String msgId = null;

             for (int i = 0; i < msgList.size(); i ++) {
                  msgId = msgList.get(i);
                  CommonUtil.printOnBoth("msgId = " + i + "  "  + msgId);
             }   
             result = CommonUtil.comparePayload(msg1.toString(),
                        expectedOutput);
            }
        	
        } else {
            return 0;
        }
        return result;
    }

}