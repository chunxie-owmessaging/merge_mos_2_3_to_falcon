package com.openwave.mxos.junit.helper;

import java.util.Map;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.mxUtil;
import com.openwave.mxos.junit.leapfrogapi.OXLoginAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressForDeliveryServiceAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookContactsBaseAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookContactsNameAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookContactsPersonalInfoAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookContactsPersonalInfoAddressAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookContactsPersonalInfoCommunicationAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookContactsPersonalInfoEventsAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookContactsWorkInfoAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookContactsWorkInfoAddressAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookContactsWorkInfoCommuAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookGroupAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookGroupBaseAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookMemberAPI;
import com.openwave.mxos.junit.leapfrogapi.AddressbookContactsAPI;
import com.openwave.mxos.junit.leapfrogapi.TasksAPI;
import com.openwave.mxos.junit.leapfrogapi.TasksBaseAPI;
import com.openwave.mxos.junit.leapfrogapi.TasksDetailsAPI;
import com.openwave.mxos.junit.leapfrogapi.TasksParticipantAPI;
import com.openwave.mxos.junit.leapfrogapi.TasksRecurrenceAPI;
import com.openwave.mxos.junit.leapfrogapi.AppsFolderAPI;

import com.openwave.mxos.junit.leapfrogapi.AdminApprovedSendersListServiceAPI;
import com.openwave.mxos.junit.leapfrogapi.AdminBlockedSendersListServiceAPI;
import com.openwave.mxos.junit.leapfrogapi.AllowedDomainAPI;
import com.openwave.mxos.junit.leapfrogapi.AllowedSendersListServiceAPI;
import com.openwave.mxos.junit.leapfrogapi.BlockedSendersListServiceAPI;
import com.openwave.mxos.junit.leapfrogapi.CaptchaAPI;
import com.openwave.mxos.junit.leapfrogapi.CosAPI;
import com.openwave.mxos.junit.leapfrogapi.CosBaseAPI;
import com.openwave.mxos.junit.leapfrogapi.CredentialServiceAPI;
import com.openwave.mxos.junit.leapfrogapi.DomainAPI;
import com.openwave.mxos.junit.leapfrogapi.EmailAliasAPI;
import com.openwave.mxos.junit.leapfrogapi.ExternalAccountAPI;
import com.openwave.mxos.junit.leapfrogapi.FionaMsisdnApi;
import com.openwave.mxos.junit.leapfrogapi.FolderAPI;
import com.openwave.mxos.junit.leapfrogapi.GeneralPreferenceAPI;
import com.openwave.mxos.junit.leapfrogapi.GroupAdminAllocationsAPI;
import com.openwave.mxos.junit.leapfrogapi.InternalInfoAPI;
import com.openwave.mxos.junit.leapfrogapi.MailAccessAPI;
import com.openwave.mxos.junit.leapfrogapi.MailForwardServiceAPI;
import com.openwave.mxos.junit.leapfrogapi.MailReceiptAPI;
import com.openwave.mxos.junit.leapfrogapi.MailStoreAPI;
import com.openwave.mxos.junit.leapfrogapi.MailboxAPI;
import com.openwave.mxos.junit.leapfrogapi.MessageAPI;
import com.openwave.mxos.junit.leapfrogapi.MessageBodyAPI;
import com.openwave.mxos.junit.leapfrogapi.MessageHeaderAPI;
import com.openwave.mxos.junit.leapfrogapi.MetadataAPI;
import com.openwave.mxos.junit.leapfrogapi.SendMailAPI;
import com.openwave.mxos.junit.leapfrogapi.SendMailAuthAPI;
import com.openwave.mxos.junit.leapfrogapi.SieveBlockedSendersServiceAPI;
import com.openwave.mxos.junit.leapfrogapi.SendSmsAPI;
import com.openwave.mxos.junit.leapfrogapi.SocialNetworksAPI;

import com.openwave.mxos.junit.leapfrogapi.smsAPI;
import com.openwave.mxos.junit.leapfrogapi.webmailAPI;
import com.openwave.mxos.junit.mainscript.Leapfrogtest.LeapFrogTest;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;

public class ApiFactory {

	static Logger alltestlog = Logger.getLogger(ApiFactory.class);

	public static int execute(final IMxOSContext context, ServiceEnum service,
			Operation operation, String inputParams, String testCaseName,
			String expectedOutput, String expResult,
			Map<String, String> mapParams) {

		switch (service) {
		case HeaderService: {
			int testStatus = MessageHeaderAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case BodyService: {
			int testStatus = MessageBodyAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case SendSmsService: {
			int testStatus = SendSmsAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		case SendMailService: {
			int testStatus = SendMailAuthAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case CosService: {
			int testStatus = CosAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		case CosBaseService: {
			int testStatus = CosBaseAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		case CosSocialNetworksService:
		case SocialNetworksService:
		case SocialNetworkSiteService: {
			int testStatus = SocialNetworksAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case MailboxBaseService:
		case MailboxService: {
			int testStatus = MailboxAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		case AllowedDomainService: {
			int testStatus = AllowedDomainAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case MailAliasService: {
			int testStatus = EmailAliasAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		case CosGeneralPreferencesService:
		case GeneralPreferenceService: {
			int testStatus = GeneralPreferenceAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case CredentialService:
		case AuthenticateService:
		case CosCredentialsService: {
			int testStatus = CredentialServiceAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case CosMailStoreService:
		case MailStoreService:
		case ExternalStoreService:
		case CosExternalStoreService: {
			int testStatus = MailStoreAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		case AddressForLocalDeliveryService: {
			int testStatus = AddressForDeliveryServiceAPI.execute(context,
					service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case MailForwardService: {
			int testStatus = MailForwardServiceAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case MailReceiptService:
		case MailReceiptFiltersService:
		case MailReceiptSieveFiltersService:
		case SenderBlockingService:
		case MailReceiptBMIFiltersService:
		case MailReceiptCommtouchFiltersService:
		case MailReceiptCloudmarkFiltersService:
		case MailReceiptMcAfeeFiltersService:
			// case MailForwardService:
		case CosMailReceiptService:
		case CosMailReceiptSieveFiltersService:
			// case CosMailReceiptBMIFiltersService:
		case CosSenderBlockingService: {
			int testStatus = MailReceiptAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case AllowedSendersListService: {
			int testStatus = AllowedSendersListServiceAPI.execute(context,
					service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case BlockedSendersListService: {
			int testStatus = BlockedSendersListServiceAPI.execute(context,
					service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case SieveBlockedSendersService: {
			int testStatus = SieveBlockedSendersServiceAPI.execute(context,
					service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case AdminApprovedSendersListService: {
			int testStatus = AdminApprovedSendersListServiceAPI.execute(
					context, service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case AdminBlockedSendersListService: {
			int testStatus = AdminBlockedSendersListServiceAPI.execute(context,
					service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case CosMailAccessService:
		case MailAccessService:
		case MailAccessAllowedIpService: {
			int testStatus = MailAccessAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		case MsisdnDeactivationService:
		case MsisdnReactivationService:
		case MsisdnSwapService: {
			int testStatus = FionaMsisdnApi.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case SmsServicesService:
		case SmsOnlineService:
		case SmsNotificationsService:
		case CosSmsServicesService:
		case CosSmsOnlineService:
			// case CosSmsNotificationsService:
		{
			int testStatus = smsAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		case CosWebMailFeaturesService:
		case WebMailFeaturesService: {
			int testStatus = webmailAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		case CosInternalInfoService:
		case CosMessageEventRecordsService:
		case InternalInfoService:
		case MessageEventRecordsService: {
			int testStatus = InternalInfoAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case CosExternalAccountService:
		case ExternalAccountService:
		case ExternalMailAccountService: {
			int testStatus = ExternalAccountAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case DomainService: {
			int testStatus = DomainAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		case CosMailSendService:
			// case CosMailSendFiltersService:
			// case CosMailSendBMIFiltersService:
			// case CosMailSendCommtouchFiltersService:
			// case CosMailSendMcAfeeFiltersService:
		case MailSendService:
		case MailSendSignatureService:
		case MailSendFiltersService:
		case MailSendBMIFiltersService:
		case MailSendCommtouchFiltersService:
		case MailSendMcAfeeFiltersService:
		case MailSendDeliveryMessagesPendingService: {
			int testStatus = SendMailAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}

		case FolderService: {
			int testStatus = FolderAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		
		case ExternalLoginService: {
			int testStatus = OXLoginAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}		
		
		case ContactsBaseService: {
			int testStatus = AddressbookContactsBaseAPI.execute(context,
					service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case ContactsNameService: {
			int testStatus = AddressbookContactsNameAPI.execute(context,
					service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case ContactsWorkInfoService: {
			int testStatus = AddressbookContactsWorkInfoAPI.execute(context,
					service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case ContactsWorkInfoAddressService: {
			int testStatus = AddressbookContactsWorkInfoAddressAPI.execute(
					context, service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case ContactsWorkInfoCommunicationService: {
			int testStatus = AddressbookContactsWorkInfoCommuAPI.execute(
					context, service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}

		case ContactsService: {
			int testStatus = AddressbookContactsAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case ContactsPersonalInfoService: {
			int testStatus = AddressbookContactsPersonalInfoAPI.execute(
					context, service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case ContactsPersonalInfoAddressService: {
			int testStatus = AddressbookContactsPersonalInfoAddressAPI.execute(
					context, service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case ContactsPersonalInfoCommunicationService: {
			int testStatus = AddressbookContactsPersonalInfoCommunicationAPI
					.execute(context, service, operation, inputParams,
							testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		case ContactsPersonalInfoEventsService: {
			int testStatus = AddressbookContactsPersonalInfoEventsAPI.execute(
					context, service, operation, inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		case MessageService: {
			int testStatus = MessageAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult,
					mapParams);
			return testStatus;
		}
		case CaptchaService: {
			int testStatus = CaptchaAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}
		case GroupsService: {
			int testStatus = AddressbookGroupAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case GroupsBaseService: {
			int testStatus = AddressbookGroupBaseAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case GroupsMemberService: {
			int testStatus = AddressbookMemberAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case AddressBookService: {
			int testStatus = AddressbookAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case TasksService: {
			int testStatus = TasksAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case TasksBaseService: {
			int testStatus = TasksBaseAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		
		case TasksParticipantService: {
			int testStatus = TasksParticipantAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		
		case TasksRecurrenceService: {
			int testStatus = TasksRecurrenceAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		
		case TasksDetailsService: {
			int testStatus = TasksDetailsAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		
		case AppsFolderService: {
			int testStatus = AppsFolderAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		
		case GroupAdminAllocationsService: {
			int testStatus = GroupAdminAllocationsAPI.execute(context, service,
					operation, inputParams, testCaseName, expectedOutput,
					expResult);
			return testStatus;
		}
		case MetadataService: {
			int testStatus = MetadataAPI.execute(context, service, operation,
					inputParams, testCaseName, expectedOutput, expResult);
			return testStatus;
		}

		default: {
			int testStatus = mxUtil.execute(LeapFrogTest.confHost,
					LeapFrogTest.confUser, LeapFrogTest.confPass,
					operation.name(), inputParams, testCaseName,
					expectedOutput, expResult);
			return testStatus;
		}
		}
	}
}
