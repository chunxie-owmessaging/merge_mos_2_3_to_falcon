package com.openwave.mxos.junit.helper;

public interface LfConstants {
	char SYMBOL_PERCENTAGE = '%';

	String USERNAME = "userName";
    String DOMAIN = "domain";
    String DOMAINTYPE = "type"; 
    String MAILREWRITEDOMAIN = "alternateDomain"; 
    String MAILRELAYHOST = "relayHost"; 
    String DEFAULTMAILBOX = "defaultMailbox";
    
    String MAILBOXID = "mailboxId";
    String STATUS = "status";

    String EMAIL = "email";
    
    String ALLOWEDDOMAIN = "allowedDomains";
    String OLDALLOWEDDOMAIN = "oldallowedDomains";
    String NEWALLOWEDDOMAIN = "newallowedDomains";
  
    String EMAILALIAS = "emailAlias";
    String NEWEMAILALIAS = "newEmailAlias";
    String OLDEMAILALIAS = "oldEmailAlias";
    String DELEMAILALIAS = "deleteEmailAlias";

    String MAXNUMALIASES = "maxNumAliases";
    String COSID = "cosId";
   
    String PASSSTORETYPE = "passwordStoreType";
    String PASSWORD = "password";

    String LOCALE = "locale";
    String TIMEZONE = "timezone";
        
    String FWDENABLE = "forwardingEnabled";
    String FWDADDRESS = "forwardingAddress";
      
    String AUTOREPLYMODE = "autoReplyMode";
    String AUTOREPLYMSG = "autoReplyMessage";
    String MAXRECMSGSIZEKB = "maxReceiveMessageSizeKB";
   	String MAXSTOARGESIZEKB = "maxStorageSizeKB";
    String MSGSTOREHOST = "messageStoreHosts";

    String NEWFWDADDRESS = "newForwardingAddress";
    String OLDFWDADDRESS = "oldForwardingAddress";
    String DELFWDADDRESS = "deleteForwardingAddress";
    String MAILAUTOREPLYHOST = "MailAutoReplyHost";
    String CUSTUMFIELD = "CustomFields";
        
    String HTTPSUCCESS = "200";

    String STARTSERVER = "startserver";
    String RESTARTSERVER = "restartserver";
    String STOPSERVER = "stopserver";
    String KILLSERVER = "killserver";

    String SETCONFIGKEY = "setconfigkey";
    String GETCONFIGKEY = "getconfigkey";
    String DELETECONFIGKEY = "deleteconfigkey";

    String TESTEND = "test_end";
    String TESTENDSKIP = "test_end_skip";
    String STEPSKIP = "step_skip";

}
