package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksDetailsService;
import com.opwvmsg.mxos.task.pojos.Details;

public class TasksDetailsAPI {
	
    static Logger alltestlog = Logger.getLogger(TasksDetailsAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
            case TasksDetailsService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("Read Tasks Details");
                        testStatus = read(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("Update Tasks Details");
                        testStatus = update(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }

                    
                }
            }
          }
        return 0;
    }
    
    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        Details details = null;
        int result;
        String method = "Read Task Details Service";
        try {
            CommonUtil.printOnBoth(method);
            ITasksDetailsService taskDetailsService = (ITasksDetailsService) context
                    .getService(service.name());
            details = taskDetailsService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("Task Details is = " + details);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && details != null) {
                result = CommonUtil.comparePayload(details.toString(),
                        expectedOutput);
            }
        }
        return result;
    }
   
    public static int update(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update task Details Service";

        try {
            CommonUtil.printOnBoth(method);
            final ITasksDetailsService taskDetailsService = (ITasksDetailsService) context
                    .getService(service.name());
            taskDetailsService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    

}
