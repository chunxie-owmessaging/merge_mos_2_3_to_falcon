package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosInternalInfoService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMessageEventRecordsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IInternalInfoService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMessageEventRecordsService;

public class InternalInfoAPI {

    static Logger alltestlog = Logger.getLogger(InternalInfoAPI.class);
    static String service = "internalInfo";

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
            case InternalInfoService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET INTERNAL INFO");
                        testStatus = read(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE INTERNAL INFO");
                        testStatus = update(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case MessageEventRecordsService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET MESSAGE EVENT RECORDS");
                        testStatus = readMers(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE MESSAGE EVENT RECORDS");
                        testStatus = updateMers(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case CosInternalInfoService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET COS INTERNAL INFO");
                        testStatus = readCosInternalInfo(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE COS INTERNAL INFO");
                        testStatus = updateCosInternalInfo(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case CosMessageEventRecordsService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET COS MESSAGE EVENT RECORDS");
                        testStatus = readCosMers(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE COS MESSAGE EVENT RECORDS");
                        testStatus = updateCosMers(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
        }
        return 1;
    }

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        System.out.println("33333333333333333333333333333333333333333");
        String errorCode = LfConstants.HTTPSUCCESS;
        InternalInfo internalInfo = null;
        int result;
        try {
            final IInternalInfoService internalInfoService =
                (IInternalInfoService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("read " + service + "service");
            internalInfo = internalInfoService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("internalInfo = " + internalInfo);
            CommonUtil.printOnBoth("read " + service + "service ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("read " + service
                    + "service", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && internalInfo != null) {
                result = CommonUtil.comparePayload(internalInfo.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            final IInternalInfoService internalInfoService =
                (IInternalInfoService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("update " + service + "service");
            internalInfoService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth("update " + service + "service ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("update " + service
                    + "service", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readMers(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        MessageEventRecords mers = null;
        int result;
        try {
            final IMessageEventRecordsService mersService =
                (IMessageEventRecordsService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("read " + service + "service");
            mers = mersService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("MessageEventRecords = " + mers);
            CommonUtil.printOnBoth("read " + service + "service ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("read " + service
                    + "service", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mers != null) {
                result = CommonUtil.comparePayload(mers.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateMers(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            final IMessageEventRecordsService mersService =
                (IMessageEventRecordsService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("update " + service + "service");
            mersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth("update " + service + "service ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("update " + service
                    + "service", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCosInternalInfo(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        System.out.println("33333333333333333333333333333333333333333");
        String errorCode = LfConstants.HTTPSUCCESS;
        InternalInfo internalInfo = null;
        int result;
        try {
            final ICosInternalInfoService cosInternalInfoService =
                (ICosInternalInfoService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("read " + service + "service");
            internalInfo = cosInternalInfoService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("cosinternalInfo = " + internalInfo);
            CommonUtil.printOnBoth("read " + service + "service ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("read " + service
                    + "service", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && internalInfo != null) {
                result = CommonUtil.comparePayload(internalInfo.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateCosInternalInfo(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            final ICosInternalInfoService cosInternalInfoService =
                (ICosInternalInfoService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("update " + service + "service");
            cosInternalInfoService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth("update " + service + "service ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("update " + service
                    + "service", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCosMers(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        MessageEventRecords mers = null;
        int result;
        try {
            final ICosMessageEventRecordsService mersService =
                (ICosMessageEventRecordsService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("read " + service + "service");
            mers = mersService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("MessageEventRecords = " + mers);
            CommonUtil.printOnBoth("read " + service + "service ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("read " + service
                    + "service", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mers != null) {
                result = CommonUtil.comparePayload(mers.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateCosMers(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            final ICosMessageEventRecordsService mersService =
                (ICosMessageEventRecordsService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("update " + service + "service");
            mersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth("update " + service + "service ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("update " + service
                    + "service", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
}
