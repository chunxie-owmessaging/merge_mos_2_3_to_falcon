package com.openwave.mxos.junit.leapfrogapi;


import org.apache.log4j.Logger;
import java.util.List;
import java.util.Map;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.common.IAppsFolderService;
import com.opwvmsg.mxos.task.pojos.Folder;
import com.openwave.mxos.junit.mainscript.Leapfrogtest.LeapFrogTest;


public class AppsFolderAPI {
	
    static Logger alltestlog = Logger.getLogger(AppsFolderAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
            case AppsFolderService: {
                switch (operation) {
                    case PUT: {
                        CommonUtil.printstartAPI("Create AppsFolderService");
                        testStatus = createAppsFolderService(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case GET: {
                        CommonUtil.printstartAPI("Read AppsFolderService");
                        testStatus = readAppsFolderService(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("Update AppsFolderService");
                        testStatus = updateAppsFolderService(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case DELETE: {
                        CommonUtil.printstartAPI("Delete AppsFolderService");
                        testStatus = deleteAppsFolderService(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case DELETEALL: {
                        CommonUtil.printstartAPI("Deleta All AppsFolderService");
                        testStatus = deleteAllAppsFolderService(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case LIST: {
                        CommonUtil.printstartAPI("List AppsFolderService");
                        testStatus = listAppsFolderService(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }

                    
                }
            }
          }
        return 1;
    }
    
    public static int readAppsFolderService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        Folder data = null;
        int result;
        String method = "Read AppsFolderService";
        try {
            CommonUtil.printOnBoth(method);
            IAppsFolderService servcieObject = (IAppsFolderService) context
                    .getService(service.name());
            data = servcieObject.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("AppsFolderService Data is = " + data);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && data != null) {
                result = CommonUtil.comparePayload(data.toString(),
                        expectedOutput);
            }
        }
        return result;
    }
    
    public static int createAppsFolderService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "CREATE AppsFolderService";
        try {
            CommonUtil.printOnBoth(method);
            IAppsFolderService servcieObject = (IAppsFolderService) context
                    .getService(service.name());
            long dataID = servcieObject.create(map.getMultivaluedMap());
            LeapFrogTest.appsFolderIdArray.add(dataID);
            CommonUtil.printOnBoth("AppsFolderService Data id = " + dataID);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
   
    public static int updateAppsFolderService(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update AppsFolderService";

        try {
            CommonUtil.printOnBoth(method);
            final IAppsFolderService servcieObject = (IAppsFolderService) context
                    .getService(service.name());
            servcieObject.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    public static int deleteAppsFolderService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
    	 String errorCode = LfConstants.HTTPSUCCESS;
         int result;
         String method = "DELETE AppsFolderService";
         try {
             CommonUtil.printOnBoth(method);
             IAppsFolderService servcieObject = (IAppsFolderService) context
                     .getService(service.name());
             servcieObject.delete(map.getMultivaluedMap());
             CommonUtil.printOnBoth(method + " ..done");
         } catch (MxOSException e) {
             errorCode = CommonUtil.getPrintErrorCode(method, e);
         }
         result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
         return result;
    	
    }
    
    
     public static int deleteAllAppsFolderService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
    	 String errorCode = LfConstants.HTTPSUCCESS;
         int result;
         String method = "DELETE All AppsFolderService";
         try {
             CommonUtil.printOnBoth(method);
             IAppsFolderService servcieObject = (IAppsFolderService) context
                     .getService(service.name());
             servcieObject.deleteAll(map.getMultivaluedMap());
             CommonUtil.printOnBoth(method + " ..done");
         } catch (MxOSException e) {
             errorCode = CommonUtil.getPrintErrorCode(method, e);
         }
         result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
         return result;
    	
    }
    public static int listAppsFolderService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        List<Folder> dataList = null;

        String method = "LIST AppsFolderService";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        try {
            CommonUtil.printOnBoth(method);
            final IAppsFolderService servcieObject = (IAppsFolderService) context
                    .getService(service.name());
            dataList = servcieObject.list(multiMap);
            CommonUtil.printOnBoth("...AppsFolderService data list == " + dataList);
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && dataList != null) {
                result = CommonUtil.comparePayload(dataList.toString(),
                        expectedOutput);
            }
        }
        return result;    	
    }
    

}

