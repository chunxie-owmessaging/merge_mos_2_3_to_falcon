/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/loadgenerator/src/main/java/com/openwave/db/ThreadBatchLauncher.java#1 $
 */

/**
 * Thread batch launcher used by Load Generator
 *
 * @author mxos-dev
 */

package com.opwv.leapfrog.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ThreadPoolExecutor;

import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.MxOSContextFactory;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 * Thread batch launcher used by Load Generator.
 * 
 * @author mxos-dev
 */

public class ThreadBatchLauncher {

    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private static Logger logger = Logger.getLogger(ThreadBatchLauncher.class);

    /**
     * Constructor Function.
     * 
     * @param properties TestProperties used by LoadGenerator
     * @throws Exception if Any
     */
    public ThreadBatchLauncher(TestProperties properties) throws Exception {
        this.properties = properties;

        String contextId = "LOAD_MXOS_2.0";
        if (properties.getRunToolAs().equals(LoadGeneratorConstants.BACKEND)) {
            Properties pContext = new Properties();
            pContext.setProperty(ContextProperty.MXOS_CONTEXT_TYPE,
                    ContextEnum.BACKEND.name());
            mxoscontext = MxOSContextFactory.getInstance().getContext(
                    contextId, pContext);
        } else if (properties.getRunToolAs()
                .equals(LoadGeneratorConstants.REST)) {
            Properties pContext = new Properties();
            pContext.setProperty(ContextProperty.MXOS_CONTEXT_TYPE,
                    ContextEnum.REST.name());
            pContext.setProperty(LoadGeneratorConstants.MXOSBASEURL,
                    properties.getMxosBaseUrl());
            pContext.setProperty(LoadGeneratorConstants.MXOSCONNECTIONS,
                    Integer.toString(properties.getNumendThreads() + 10));
            pContext.setProperty(LoadGeneratorConstants.BGCMXOSBASEURL,
                    properties.getBgcMxosBaseUrl());
            pContext.setProperty(LoadGeneratorConstants.BGCMXOSCONNECTIONS,
                    Integer.toString(properties.getNumendThreads() + 10));
            pContext.setProperty(LoadGeneratorConstants.CUSTOM,
                    properties.isCustomEnabled());
            mxoscontext = MxOSContextFactory.getInstance().getContext(
                    contextId, pContext);
        }
    }

    /**
     * Function for starting the executor threads. The threads then will be
     * added to the executor threadpool. If the provisioning mode is enabled
     * then this function will first launch provisioning threads.
     * 
     * @param threadPool Executor thread pool for all the threads..
     * @throws InvalidDataException
     * @throws ClassNotFoundException
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */

    public void startExecutors(ThreadPoolExecutor threadPool)
            throws InvalidDataException, ClassNotFoundException,
            NoSuchMethodException, SecurityException, InstantiationException,
            IllegalAccessException, IllegalArgumentException,
            InvocationTargetException {

        String className = DefaultParams
                .getClass(properties.getServiceToTest());
        Class<?> serviceClass = Class.forName(className);
        Constructor<?> classConstrutor = serviceClass
                .getDeclaredConstructor(new Class[] { Integer.class,
                        TestProperties.class, IMxOSContext.class });
        int numThreads = 0;
        int threadsRemaining = properties.getNumendThreads()
                - threadPool.getLargestPoolSize();
        numThreads = threadsRemaining >= properties.getNumstartThreads() ? properties
                .getNumstartThreads() : threadsRemaining;
        logger.info("Creating " + numThreads + " threads in the executor pool.");
        for (int i = 0; i < numThreads; i++) {
            threadPool.execute((Runnable) classConstrutor
                    .newInstance(new Object[] { new Integer(i), properties,
                            mxoscontext }));
        }
    }
}
