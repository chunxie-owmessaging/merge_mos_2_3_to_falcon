/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/loadgenerator/src/main/java/com/openwave/db/HelperFunctions.java#1 $
 */

/**
 * Helper functions used by Load Generator
 *
 * @author mxos-dev
 */

package com.opwv.leapfrog.test;

import java.util.Date;
import java.util.List;
import java.util.Random;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Helper functions used by Load Generator.
 * 
 * @author mxos-dev
 */
public final class HelperFunctions {

    /**
     * Default constructor.
     */
    private HelperFunctions() {
    }

    /**
     * Function for generation Message UUID.
     * 
     * @return msguuid Message UID as string
     */

    public static String getTimeUUID() {
        java.util.UUID msguuid = java.util.UUID
                .fromString(new com.eaio.uuid.UUID().toString());
        return msguuid.toString();
    }

    /**
     * Function for Generating random email id.
     * 
     * @param properties TestProperties used by Load generator.
     * @return emailId email id
     */
    public static String getRandomUser(TestProperties properties) {
        long maxNum = properties.getMboxIdStart() + properties.getNumMboxes();
        long minNum = properties.getMboxIdStart();
        long mboxId = minNum + (long) (Math.random() * ((maxNum - minNum) + 1));
        final String emailId = String.format(properties.getUsernamePrefix(),
                mboxId) + properties.getDefaultDomain();
        return emailId;
    }

    /**
     * Function for Generating random Folder Name.
     * 
     * @return folderName Random folder name
     */
    public static String generateRandomFolderName() {
        String folderName = LoadGeneratorConstants.FOLDERNAMEPREFIX.concat(Long
                .toString(System.currentTimeMillis()));
        return folderName;
    }

    /**
     * Function for generating current Time in milli seconds.
     * 
     * @return curMilsecs current milli seconds
     */
    public static long getepochMilSecs() {

        Long curMilsecs = System.currentTimeMillis();
        return curMilsecs;
    }

    /**
     * Function for getting Inbox folder POJO from a list of folder POJOs.
     * 
     * @param folders Map of FolderId and folder object.
     * @return inboxFolder Inbox Folder POJO on success or null in case of
     *         failure.
     */
    public static Folder getInboxId(List<Folder> folders) {
        Folder inboxFolder = null;
        if (folders != null) {
            for (int i = 0; i < folders.size(); i++) {
                Folder folder = folders.get(i);
                if (folder.getFolderName().equalsIgnoreCase(
                        LoadGeneratorConstants.INBOX)) {
                    inboxFolder = folder;
                    break;
                }
            }
        }
        return inboxFolder;
    }

    /**
     * Function for getting any folder POJO from a list of folder POJOs.
     * 
     * @param folders Map of FolderId and folder object.
     * @return anyFolder Any folder POJO on success or null in case of failure.
     */
    public static Folder getAnyFolderId(List<Folder> folders) {
        Folder anyFolder = null;
        if (folders.size() > 0) {
            Random generator = new Random();
            anyFolder = folders.get(generator.nextInt(folders.size()));
        }
        return anyFolder;
    }

    /**
     * Function for generation current Time in nano seconds.
     * 
     * @return nanosec current nano seconds
     */

    public static long getNanoTime() {

        Long nanosec = System.nanoTime();
        return nanosec;
    }

    /**
     * Function for converting string to integers.
     * 
     * @param propertyValue Integer number as string.
     * @return intValue Converted Integer value.
     * @throws Exception if any.
     */
    public static int validateInteger(String propertyValue) throws Exception {
        int intValue = Integer.parseInt(propertyValue);
        return intValue;
    }

    /**
     * Function for converting string to long.
     * 
     * @param propertyValue Long number as string.
     * @return longValue Converted long value.
     * @throws Exception if any.
     */
    public static long validateLong(String propertyValue) throws Exception {
        long longValue = Long.parseLong(propertyValue);
        return longValue;
    }

    /**
     * Function for converting string to boolean.
     * 
     * @param propertyValue Boolean as string.
     * @return booleanValue Converted boolean value.
     * @throws Exception if any.
     */
    public static boolean validateBoolean(String propertyValue)
            throws Exception {
        boolean booleanValue = false;
        if (propertyValue.equalsIgnoreCase(LoadGeneratorConstants.FALSE)
                || propertyValue.equalsIgnoreCase(LoadGeneratorConstants.TRUE)) {
            booleanValue = Boolean.parseBoolean(propertyValue);
        } else {
            throw new Exception("Invalid value for boolean variable");
        }
        return booleanValue;
    }

    /**
     * Function for converting string to integers.
     * 
     * @return Date as string.
     */
    public static String generateDate() {
        return new Date().toString();
    }

    public static String getPhoneNumber() {
        final String phoneNumber = "+"
                + Long.toString(System.currentTimeMillis()).substring(1, 12);
        return phoneNumber;
    }

    public static String getSmsType() {
        int pick = new Random().nextInt(LoadGeneratorConstants.SMSTYPES.length);
        return LoadGeneratorConstants.SMSTYPES[pick];
    }

    public static String getBmiSpamAction() {
        int pick = new Random()
                .nextInt(LoadGeneratorConstants.BMIFILTERACTIONS.length);
        return LoadGeneratorConstants.BMIFILTERACTIONS[pick];
    }

    public static String getCommtouchAction() {
        int pick = new Random()
                .nextInt(LoadGeneratorConstants.COMMTOUCHACTIONS.length);
        return LoadGeneratorConstants.COMMTOUCHACTIONS[pick];
    }

    public static String getMcafeeAction() {
        int pick = new Random()
                .nextInt(LoadGeneratorConstants.MCAFEEACTIONS.length);
        return LoadGeneratorConstants.MCAFEEACTIONS[pick];
    }

    public static String getCloumarkAction() {
        int pick = new Random()
                .nextInt(LoadGeneratorConstants.CLOUDMARKACTIONS.length);
        return LoadGeneratorConstants.CLOUDMARKACTIONS[pick];
    }

    public static String getAutoReplyMode() {
        int pick = new Random()
                .nextInt(LoadGeneratorConstants.AUTOREPLYMODE.length);
        return LoadGeneratorConstants.AUTOREPLYMODE[pick];
    }

    public static String getBlockSendersAction() {
        int pick = new Random()
                .nextInt(LoadGeneratorConstants.BLOCKSENDERSACTION.length);
        return LoadGeneratorConstants.BLOCKSENDERSACTION[pick];
    }

    public static String getRandomString() {
        final String randomString = LoadGeneratorConstants.RANDOMSTRING
                + Long.toString(System.currentTimeMillis());
        return randomString;
    }

    public static String generateRandomBoolean() {
        int pick = new Random().nextInt(LoadGeneratorConstants.BOOLEAN.length);
        return LoadGeneratorConstants.BOOLEAN[pick];
    }

    public static String getConfirmation() {
        int pick = new Random()
                .nextInt(LoadGeneratorConstants.CONFIRMATION.length);
        return LoadGeneratorConstants.CONFIRMATION[pick];
    }

    public static String getAccessType() {
        int pick = new Random()
                .nextInt(LoadGeneratorConstants.ACCESSTYPE.length);
        return LoadGeneratorConstants.ACCESSTYPE[pick];
    }

    public static String generateRandomIpAddress() {
        Random rTmp = new Random();
        final String randomIp = rTmp.nextInt(256) + "." + rTmp.nextInt(256)
                + "." + rTmp.nextInt(256) + "." + rTmp.nextInt(256);
        return randomIp;
    }

    public static String getSocialNetworkSite() {
        int pick = new Random()
                .nextInt(LoadGeneratorConstants.SOCIALNETWORKSITES.length);
        return LoadGeneratorConstants.SOCIALNETWORKSITES[pick];
    }

    public static String getUserExperience() {
        int pick = new Random()
                .nextInt(LoadGeneratorConstants.USEREXPERIANCE.length);
        return LoadGeneratorConstants.USEREXPERIANCE[pick];
    }

    public static String generateRandomStatusForMailbox() {
        int pick = new Random()
                .nextInt(LoadGeneratorConstants.MAILBOXSTATUS.length);
        return LoadGeneratorConstants.MAILBOXSTATUS[pick];
    }
/*
    public static Session getSessionId(MxosRequestMap mxosRequestMap,
            IMxOSContext mxoscontext) throws MxOSException {
        Session session = null;
        MxosRequestMap newmxosRequestMap = mxosRequestMap;
        newmxosRequestMap.add(MailboxProperty.password.name(),
                LoadGeneratorConstants.DEFAULTPASSWORD);
        long time = Stats.startTimer();
        try {
            ILoginUserService testService = (ILoginUserService) mxoscontext
                    .getService(ServiceEnum.LoginUserService.name());
            session = testService.loginUser(newmxosRequestMap
                    .getMultivaluedMap());
            Stats.stopTimer(ServiceEnum.LoginUserService, Operation.POST, time,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.LoginUserService, Operation.POST, time,
                    StatStatus.fail);
            throw e;
        }
        return session;
    }
*/
    public static ExternalSession getSessionId(String userId, String oxUrl,
            IMxOSContext mxoscontext) throws MxOSException {
        ExternalSession session = null;
        MxosRequestMap newmxosRequestMap = new MxosRequestMap();
        newmxosRequestMap.add(AddressBookProperty.userId.name(), userId);
        newmxosRequestMap.add(AddressBookProperty.oxHttpURL.name(), oxUrl);
        newmxosRequestMap.add(MailboxProperty.password.name(),
                LoadGeneratorConstants.DEFAULTPASSWORD);
        long time = Stats.startTimer();
        try {
            IExternalLoginService testService = (IExternalLoginService) mxoscontext
                    .getService(ServiceEnum.ExternalLoginService.name());
            session = testService.login(newmxosRequestMap
                    .getMultivaluedMap());
            Stats.stopTimer(ServiceEnum.ExternalLoginService, Operation.POST, time,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ExternalLoginService, Operation.POST, time,
                    StatStatus.fail);
            throw e;
        }
        return session;
    }

    public static String generateMaritalStatusForContact() {
        int pick = new Random()
                .nextInt(LoadGeneratorConstants.MARITALSTATUSVALUES.length);
        return LoadGeneratorConstants.MARITALSTATUSVALUES[pick];
    }
}
