package com.opwv.leapfrog.services;

import java.util.List;

import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAddressForDeliveryService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on AddressForDeliveryService.
 * 
 * @author mxos-dev
 */
public class AddressForDeliveryServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(AddressForDeliveryServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public AddressForDeliveryServiceTester(Integer tId,
            TestProperties properties, IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for
     * AddressForDeliveryService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }

            if (properties.isCreateEnabled()) {
                createAddressForLocalDelivery();
            }

            if (properties.isReadEnabled()) {
                readAddressForLocalDelivery();
            }

            if (properties.isUpdateEnabled()) {
                updateAddressForLocalDelivery();
            }

            if (properties.isDeleteEnabled()) {
                deleteAddressForLocalDelivery();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    private String generateNewAddressForLocalDelivery() {
        final String localAddress = properties.getUsernamePrefix()
                + Long.toString(System.currentTimeMillis())
                + properties.getDefaultDomain();
        return localAddress;
    }

    /**
     * Method for create operation for AddressForDeliveryService.
     */
    private void createAddressForLocalDelivery() {

        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.AddressForLocalDeliveryService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        final String localAddress = generateNewAddressForLocalDelivery();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.addressForLocalDelivery.name(),
                localAddress);
        time = Stats.startTimer();
        try {
            IAddressForDeliveryService testService = (IAddressForDeliveryService) mxoscontext
                    .getService(serviceName.name());
            testService.create(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);

            logger.info("Successfully created address for local delivery for user "
                    + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for read operation for AddressForDeliveryService.
     */
    private List<String> readAddressForLocalDelivery() {

        List<String> localDeliveryAddresses = null;
        currentOpr = Operation.GET;
        serviceName = ServiceEnum.AddressForLocalDeliveryService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            IAddressForDeliveryService testService = (IAddressForDeliveryService) mxoscontext
                    .getService(serviceName.name());
            localDeliveryAddresses = testService.read(mxosRequestMap
                    .getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            if (properties.isDebug()) {
                if (localDeliveryAddresses != null
                        && localDeliveryAddresses.size() > 0) {
                    logger.debug("For user " + emailId
                            + " local delivery address are "
                            + localDeliveryAddresses.toString());
                } else {
                    logger.debug("For user " + emailId
                            + " local delivery address are not set.");
                }
            }
            logger.info("Successfully read local delivery address for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return localDeliveryAddresses;
    }

    /**
     * Method for update operation for AddressForDeliveryService.
     */

    private void updateAddressForLocalDelivery() {
        currentOpr = Operation.POST;
        serviceName = ServiceEnum.AddressForLocalDeliveryService;
        List<String> localDeliveryAddresses = readAddressForLocalDelivery();
        if (localDeliveryAddresses != null && localDeliveryAddresses.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(
                    MailboxProperty.oldAddressForLocalDelivery.name(),
                    localDeliveryAddresses.get(0));
            mxosRequestMap.add(
                    MailboxProperty.newAddressForLocalDelivery.name(),
                    generateNewAddressForLocalDelivery());
            time = Stats.startTimer();
            try {
                IAddressForDeliveryService testService = (IAddressForDeliveryService) mxoscontext
                        .getService(serviceName.name());
                testService.update(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully updated address for local delivery for user "
                        + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("Address for local delivery is not set for user "
                    + emailId);
        }
    }

    /**
     * Method for delete operation for AddressForDeliveryService.
     */

    private void deleteAddressForLocalDelivery() {
        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.AddressForLocalDeliveryService;
        List<String> localDeliveryAddresses = readAddressForLocalDelivery();
        if (localDeliveryAddresses != null && localDeliveryAddresses.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(MailboxProperty.addressForLocalDelivery.name(),
                    localDeliveryAddresses.get(0));
            time = Stats.startTimer();
            try {
                IAddressForDeliveryService testService = (IAddressForDeliveryService) mxoscontext
                        .getService(serviceName.name());
                testService.delete(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully deleted address "
                        + localDeliveryAddresses.get(0)
                        + " from local delivery addresses for user " + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("Local delivery addresses are not set for user "
                    + emailId);
        }
    }
}
