package com.opwv.leapfrog.services;

import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.LoadGeneratorConstants;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * * Class for doing CRUD operations on MailboxService.
 * 
 * @author mxos-dev
 * */
public class MailboxServiceTester implements Runnable {

    private static Logger logger = Logger.getLogger(MailboxServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * * Constructor. * *
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * */

    public MailboxServiceTester(Integer tId, TestProperties properties,
            IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * * Thread run method for running CRUD operations for MailboxService.
     * */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }
            if (properties.isCreateEnabled()) {
                createMailbox();
            }

            if (properties.isDeleteEnabled()) {
                deleteMailbox();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * * Method for create operation for MailboxService.
     * */
    private void createMailbox() {

        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.MailboxService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.password.name(),
                LoadGeneratorConstants.DEFAULTPASSWORD);
        mxosRequestMap.add(MailboxProperty.maxNumAliases.name(), 10);
        mxosRequestMap.add(LoadGeneratorConstants.MAXMSGSIZE,
                LoadGeneratorConstants.UNLIMITEDQUOTA);
        mxosRequestMap.add(MailboxProperty.maxStorageSizeKB.name(),
                LoadGeneratorConstants.UNLIMITEDQUOTA);
        mxosRequestMap.add(MailboxProperty.maxReceiveMessageSizeKB.name(),
                LoadGeneratorConstants.UNLIMITEDQUOTA);
        mxosRequestMap.add(MailboxProperty.maxSendMessageSizeKB.name(),
                LoadGeneratorConstants.UNLIMITEDQUOTA);
        mxosRequestMap.add(MailboxProperty.locale.name(),
                LoadGeneratorConstants.DEFAULTLOCALE);
        mxosRequestMap.add(MailboxProperty.customFields.name(),
                LoadGeneratorConstants.DEFAULTCUSTOMFIELDS);
        mxosRequestMap.add(MailboxProperty.passwordStoreType.name(),
                LoadGeneratorConstants.DEFAULTPASSWORDTYPE);
        time = Stats.startTimer();
        try {
            IMailboxService mboxService = (IMailboxService) mxoscontext
                    .getService(serviceName.name());
            mboxService.create(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully created mailbox for user " + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * * Method for delete operation for MailboxService.
     * */
    private void deleteMailbox() {
        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.MailboxService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            IMailboxService mboxService = (IMailboxService) mxoscontext
                    .getService(serviceName.name());
            mboxService.delete(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully deleted mailbox for user " + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }
}
