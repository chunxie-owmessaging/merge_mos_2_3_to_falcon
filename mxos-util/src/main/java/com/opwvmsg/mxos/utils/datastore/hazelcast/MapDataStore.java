/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.utils.datastore.hazelcast;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import com.hazelcast.core.IMap;
import com.opwvmsg.mxos.utils.datastore.IDataStoreMap;

import static com.opwvmsg.mxos.utils.datastore.hazelcast.HazelcastProvider.getInstance;

/**
 * Provides Hazelcast {@link IMap} support for {@link IDataStoreMap} 
 * @author 
 *
 * @param <K>
 * @param <V>
 */
class MapDataStore<K, V> implements IDataStoreMap<V, K> {

    @Override
    public V put(String mapId, K key, V value) {
        final ConcurrentMap<K, V> map = getInstance().getMap(mapId);
        return map.put(key, value);
    }

    @Override
    public V put(String mapId, K key, V value, long ttl, TimeUnit timeunit) {
        final IMap<K, V> map = getInstance().getMap(mapId);
        return map.put(key, value, ttl, timeunit);
    }

    @Override
    public void putAll(String mapId, Map<? extends K, ? extends V> map) {
        final ConcurrentMap<K, V> mapc = getInstance().getMap(mapId);
        mapc.putAll(map);
    }

    @Override
    public void set(String mapId, K key, V value, long ttl, TimeUnit timeunit) {
        getInstance().getMap(mapId).set(key, value, ttl, timeunit);
    }

    @Override
    public V get(String mapId, Object key) {
        final ConcurrentMap<K, V> map = getInstance().getMap(mapId);
        return map.get(key);
    }

    @Override
    public Map<K, V> getAll(String mapId, Set<K> keys) {
        final IMap<K, V> map = getInstance().getMap(mapId);
        return map.getAll(keys);
    }

    @Override
    public V remove(String mapId, Object key) {
        final ConcurrentMap<K, V> map = getInstance().getMap(mapId);
        return map.remove(key);
    }

    @Override
    public boolean evict(String mapId, K key) {
        final IMap<K, V> map = getInstance().getMap(mapId);
        return map.evict(key);
    }

    @Override
    public void clear(String mapId) {
        final ConcurrentMap<K, V> map = getInstance().getMap(mapId);
        map.clear();
    }

    @Override
    public int size(String mapId){
        final ConcurrentMap<K, V> map = getInstance().getMap(mapId);
        return map.size();
    }
}
