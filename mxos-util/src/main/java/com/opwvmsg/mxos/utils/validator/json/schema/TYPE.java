/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.json.schema;

/**
 * TYPE enum class of json schema.
 *
 * @author mxos-dev
 *
 */
public enum TYPE {

    OBJECT("object"),

    ARRAY("array"),

    STRING("string"),

    EMAIL("email"),

    LONG("long"),

    INTEGER("integer"),

    DOUBLE("double"),

    FLOAT("float"),

    BOOLEAN("boolean"),

    DATE("date"),

    NULL("null"),

    ANY("any"),

    UNKNOWN("unknown"),

    UNION("union");

    private String type;
    /**
     * default constructor.
     * @param typeStr typeStr
     */
    private TYPE(String typeStr) {
        this.type = typeStr;
    }
    /**
     * toString.
     * @return string string
     */
    public String toString() {
        return this.type;
    }
}
