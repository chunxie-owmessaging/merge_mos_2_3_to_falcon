/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;

/**
 * MaximumValidator class for maximum validator.
 *
 * @author mxos-dev
 * 
 */
public class FormatValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = -6065577788738619222L;
    private static Logger LOG = Logger.getLogger(FormatValidator.class);
    public static final String PROPERTY = "format";
    protected String format;

    /**
     * default constructor.
     *
     * @param formatNode
     *            formatNode
     */
    public FormatValidator(JsonNode formatNode) {
        if ((formatNode != null) && (formatNode.isTextual())) {
            this.format = formatNode.getTextValue();
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();

        if (this.format == null) {
            return errors;
        }
        if (null != node && null != node.getTextValue()
                && node.getTextValue().length() > 0) {
            String dateInputValue = node.getTextValue();
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(this.format);
                sdf.setLenient(false);
                sdf.parse(dateInputValue);
                // new SimpleDateFormat(this.format).parse(dateInputValue);
            } catch (Exception e) {
                LOG.error("Exception while formating date: " + e);
                errors.add(at + ": " + node.getTextValue()
                        + " not a valid date format");
            }
        }
        return errors;
    }
}
