/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.json.schema.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchema;
import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchemaException;
import com.opwvmsg.mxos.utils.validator.list.AdditionalPropertiesValidator;
import com.opwvmsg.mxos.utils.validator.list.MaximumValidator;
import com.opwvmsg.mxos.utils.validator.list.MinimumValidator;

/**
 * JacksonSchema class for Json Jackson Schema validation.
 *
 * @author mxos-dev
 */
public class JacksonSchema implements JSONSchema, JSONValidator, Serializable {
    private static final long serialVersionUID = -3585793275135068320L;
    private static Logger LOG = Logger.getLogger(JacksonSchema.class);
    protected ObjectMapper mapper;
    protected List<JSONValidator> validators;
    protected boolean optional = false;

    /**
     * default constructor.
     *
     * @param mapper mapper
     * @param schemaNode schemaNode
     */
    public JacksonSchema(ObjectMapper mapper, JsonNode schemaNode) {
        this.mapper = mapper;
        this.mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS,
                true);
        this.validators = new ArrayList<JSONValidator>();
        read(schemaNode);
    }

    /**
     * Default constructor.
     *
     * @param schemaNode schemaNode
     */
    public JacksonSchema(JsonNode schemaNode) {
        this.validators = new ArrayList<JSONValidator>();
        read(schemaNode);
    }

    /**
     * read method.
     *
     * @param schemaNode schemaNode
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void read(JsonNode schemaNode) {
        Iterator<String> pnames = schemaNode.getFieldNames();
        while (pnames.hasNext()) {
            String pname = pnames.next();
            if (LOG.isInfoEnabled()) {
                LOG.info("Loading attribute : " + pname);
            }
            JsonNode n = schemaNode.get(pname);

            if (("optional".equals(pname)) && (n.isBoolean())
                    && (n.getBooleanValue())) {
                this.optional = true;
            } else if ("additionalProperties".equals(pname)) {
                this.validators.add(new AdditionalPropertiesValidator(
                        schemaNode.get("properties"), schemaNode
                                .get("additionalProperties")));
            } else if ("minimum".equals(pname)) {
                this.validators.add(new MinimumValidator(schemaNode
                        .get("minimum"), schemaNode.get("minimumCanEqual")));
            } else {
                if ("minimumCanEqual".equals(pname)) {
                    continue;
                }

                if ("maximum".equals(pname)) {
                    this.validators
                            .add(new MaximumValidator(
                                    schemaNode.get("maximum"), schemaNode
                                            .get("maximumCanEqual")));
                } else {
                    if ("maximumCanEqual".equals(pname)) {
                        continue;
                    }
                    // skip, if is a ref to other attribute
                    if ("$ref".equals(pname)) {
                        continue;
                    }

                    String className = Character.toUpperCase(pname.charAt(0))
                            + pname.substring(1) + "Validator";
                    if (n != null) {
                        try {
                            Class clazz = Class.forName("com.opwvmsg.mxos."
                                    + "utils.validator.list." + className);
                            Constructor c = clazz.getConstructor(
                                    new Class[] {JsonNode.class});
                            this.validators.add((JSONValidator) c
                                    .newInstance(new Object[] {n}));
                        } catch (Exception e) {
                            LOG.error("Could not load validator " + pname, e);
                        }
                    } else if ("type".equals(pname)) {
                        throw new JSONSchemaException(
                                "Invalid JSON Schema: \"type\" "
                                        + "property not found!");
                    }
                }
            }
        }
    }

    /**
     * method to validate.
     *
     * @param jsonNode jsonNode
     * @param at at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode jsonNode, String at) {
        return validate(jsonNode, null, at);
    }

    /**
     * method2 to validate.
     *
     * @param jsonNode jsonNode
     * @param parent patent
     * @param at at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode jsonNode, JsonNode parent, String at) {
        List<String> errors = new ArrayList<String>();
        for (JSONValidator v : this.validators) {
            errors.addAll(v.validate(jsonNode, parent, at));
        }
        return errors;
    }

    /**
     * Method to validate.
     *
     * @param json json
     * @return list list
     */
    @Override
    public List<String> validate(String json) {
        try {
            JsonNode jsonNode = this.mapper.readTree(json);
            return validate(jsonNode, "$");
        } catch (IOException ioe) {
            LOG.error("Failed to load json instance!", ioe);
            throw new JSONSchemaException(ioe);
        }
    }

    /**
     * method to validate.
     *
     * @param jsonStream jsonStream
     * @return list list
     */
    @Override
    public List<String> validate(InputStream jsonStream) {
        try {
            JsonNode jsonNode = this.mapper.readTree(jsonStream);
            return validate(jsonNode, "$");
        } catch (IOException ioe) {
            LOG.error("Failed to load json instance!", ioe);
            throw new JSONSchemaException(ioe);
        }
    }

    /**
     * method json validate.
     *
     * @param jsonReader jsonReader
     * @return list list
     */
    @Override
    public List<String> validate(Reader jsonReader) {
        try {
            JsonNode jsonNode = this.mapper.readTree(jsonReader);
            return validate(jsonNode, "$");
        } catch (IOException ioe) {
            LOG.error("Failed to load json instance!", ioe);
            throw new JSONSchemaException(ioe);
        }
    }

    /**
     * method json validate.
     *
     * @param jsonURL jsonURL
     * @return list list
     */
    @Override
    public List<String> validate(URL jsonURL) {
        try {
            JsonNode jsonNode = this.mapper.readTree(jsonURL.openStream());
            return validate(jsonNode, "$");
        } catch (IOException ioe) {
            LOG.error("Failed to load json instance!", ioe);
            throw new JSONSchemaException(ioe);
        }
    }

    /**
     * is optional.
     *
     * @return boolean boolean
     */
    public boolean isOptional() {
        return this.optional;
    }
}
