/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/config/POJOConfigProvider.java#1 $
 */

package com.opwvmsg.mxos.utils.config;

import java.util.Map;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.spi.ConfigProvider;

/**
 * POJO based config provider. Many PAF and existing WebEdge / Rich Mail
 * components have dependencies on config.db, which  makes difficult
 * to reuse those components if the application has a differnet configuration
 * mechanism. This config provider aims to be an adapter for config.db
 * independent applications to reuse config.db dependant components.
 *
 * @author Aricent (partially used from RM)
 */
public class POJOConfigProvider implements ConfigProvider {

    private static POJOConfig singleton;

    /**
     * Returns a new POJOConfig object using the given setup information.
     *
     * @param setup key-value pairs in config.db
     * @return a new POJOConfig object
     */
    @SuppressWarnings("all")
    public Config getConfig(Map setup) {
        if (singleton == null) {
            singleton = new POJOConfig(setup);
        }
        return singleton;
    }

    /**
     * Tests whether this provider is capable of providing the
     * Config API for the <i>pojo</i> provider type.
     *
     * @param type the desired type of provider
     * @return true if the named type is handled by this provider,
     * otherwise false
     */
    public boolean isType(String type) {
        return "pojo".equalsIgnoreCase(type);
    }

    /**
     * Sets the singleton to null.
     */
    static void reset() {
        singleton = null;
    }
}
