/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.utils.datastore.hazelcast;

import static com.opwvmsg.mxos.error.ErrorCode.GEN_INTERNAL_ERROR;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.datastore.IClusterIdGenerator;
import com.opwvmsg.mxos.utils.datastore.IDataStoreProvider;
import com.opwvmsg.mxos.utils.datastore.IDataStoreMap;
import com.opwvmsg.mxos.utils.datastore.IDataStoreMultiMap;

/**
 * Provides Hazelcast implementation for Interfaces under package
 * com.opwvmsg.mxos.utils.datastore
 * 
 * @author
 */
public final class HazelcastDataStoreProvider<K, V> implements IDataStoreProvider<K, V>{
    
    private final IDataStoreMap<V, K> mapDS = new MapDataStore<K, V>();
    private final IDataStoreMultiMap<K, V> multiMapDS = new MultiMapDataStore<K, V>();
    private final IClusterIdGenerator clusterIdGenerator = new ClusterIdGenerator();
    
    public HazelcastDataStoreProvider() throws MxOSException {
        /* checking if Hazelcast instance is running */
        if (!HazelcastProvider.instanceCreated()) {
            throw new MxOSException(GEN_INTERNAL_ERROR.name(),
                    "Hazelcast not started!");
        }
    }
    
    /**
     * Provides Hazelcast instance for {@link IDataStoreMap}
     */
    @Override
    public IDataStoreMap<V, K> getMapDataStore(){
        return mapDS;
    }
    
    /**
     * Provides Hazelcast instance for {@link IDataStoreMultiMap}
     */
    @Override
    public IDataStoreMultiMap<K, V> getMultiMapDataStore(){
        return multiMapDS;
    }
    
    /**
     * Provides Hazelcast instance for {@link IClusterIdGenerator}
     */
    @Override
    public IClusterIdGenerator getClusterIdGenerator(){
        return clusterIdGenerator;
    }
    
    /**
     * Shuts down all running Hazelcast instances on this JVM, including
     * the default one if it is running. 
     */
    @Override
    public void shutdownDataStore(){
        HazelcastProvider.shutdownAll();
    }
}
