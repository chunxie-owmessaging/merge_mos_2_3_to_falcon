/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser.NumberType;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;

/**
 * MinimumValidator class for minimum validator.
 *
 * @author Aricent
 * 
 */
public class MinimumValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = -3480112301401177525L;
    private static Logger LOG = Logger.getLogger(MinimumValidator.class);
    public static final String PROPERTY = "minimum";
    public static final String PROPERTY_CANEQUAL = "minimumCanEqual";
    protected Number minimum;
    protected NumberType numberType;
    protected boolean canEqual = true;

    /**
     * default constructor.
     *
     * @param minimumNode
     *            minimumNode
     * @param minimumCanEqualNode
     *            minimumCanEqualNode
     */
    public MinimumValidator(JsonNode minimumNode, JsonNode minimumCanEqualNode)
    {
        if ((minimumNode != null) && (minimumNode.isNumber())) {
            this.minimum = minimumNode.getNumberValue();
            this.numberType = minimumNode.getNumberType();
        }

        if ((minimumCanEqualNode != null) && (minimumCanEqualNode.isBoolean()))
        {
            this.canEqual = minimumCanEqualNode.getBooleanValue();
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();

        if (this.minimum == null) {
            return errors;
        }
        if (null != node && null != node.getTextValue()
                && node.getTextValue().length() > 0) {
            long value = 0;
            try {
                value = Long.parseLong(node.getTextValue());
            } catch (Exception e) {
                errors.add(at + ": Unable to parse");
                return errors;
            }
            boolean smallerThanMin = false;
            if (value < this.minimum.longValue()) {
                smallerThanMin = true;
            }

            /*
             * switch (numberType) { case LONG: if
             * (node.getDecimalValue().compareTo( (BigDecimal) this.minimum) >=
             * 0) { break; } smallerThanMin = true; break; case DOUBLE: if
             * (node.getBigIntegerValue().compareTo( (BigInteger) this.minimum)
             * >= 0) { break; } smallerThanMin = true; break; case INT: if
             * (node.getDoubleValue() >= this.minimum.doubleValue()) { break; }
             * smallerThanMin = true; break; case FLOAT: if
             * (node.getDoubleValue() >= this.minimum.doubleValue()) { break; }
             * smallerThanMin = true; break; case BIG_DECIMAL: if
             * (node.getIntValue() >= this.minimum.intValue()) { break; }
             * smallerThanMin = true; break; case BIG_INTEGER: if
             * (node.getLongValue() >= this.minimum.longValue()) { break; }
             * smallerThanMin = true; default: }
             */

            if ((smallerThanMin)
                    || ((!this.canEqual) && (value == this.minimum.longValue())))
            {
                errors.add(at + ": must have a minimum value of "
                        + this.minimum);
            }
        }
        return errors;
    }
}
