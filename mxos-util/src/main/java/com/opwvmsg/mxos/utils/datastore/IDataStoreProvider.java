/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.utils.datastore;

/**
 * Interface to be implemented by Data store provider class. e.g.
 * com.opwvmsg.mxos.utils.datastore.hazelcast.DataStoreProvider<K, V>
 * 
 * @author
 * 
 * @param <K>
 * @param <V>
 */
public interface IDataStoreProvider<K, V> {

    /**
     * Provides MapDataStore (Key-Value) pair
     * 
     * @return instance of IMapDataStore
     */
    IDataStoreMap<V, K> getMapDataStore();
    
    /**
     * Provides MapDataStore (Key-MultipleValues) pair.
     * 
     * @return instance of IMultiMapDataStore
     */
    IDataStoreMultiMap<K, V> getMultiMapDataStore();
    
    /**
     * Provides ClusterIdGenerator
     * 
     * @return instance of IClusterIdGenerator
     */
    IClusterIdGenerator getClusterIdGenerator();
    
    /**
     * Method to shutdown JVM instance of Data Store while application is
     * stopping.
     */
    void shutdownDataStore();
    
}
