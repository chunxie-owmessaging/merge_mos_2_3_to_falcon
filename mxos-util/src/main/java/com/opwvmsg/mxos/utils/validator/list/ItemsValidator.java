/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.JacksonSchema;

/**
 * ItemsValidator class for items validator.
 *
 * @author mxos-dev
 * 
 */
public class ItemsValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = -5023382376825229965L;
    private static Logger LOG = Logger.getLogger(ItemsValidator.class);
    public static final String PROPERTY = "items";
    protected JacksonSchema schema;
    protected List<JacksonSchema> tupleSchema;

    /**
     * default constructor.
     *
     * @param itemSchema
     *            itemSchema
     */
    public ItemsValidator(JsonNode itemSchema) {
        if (itemSchema.isObject()) {
            this.schema = new JacksonSchema(itemSchema);
        }

        if (itemSchema.isArray()) {
            this.tupleSchema = new ArrayList<JacksonSchema>();
            for (JsonNode s : itemSchema) {
                this.tupleSchema.add(new JacksonSchema(s));
            }
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();

        int i = 0;
        for (JsonNode n : node) {
            if (this.schema != null) {
                errors.addAll(this.schema.validate
                        (n, node, at + "[" + i + "]"));
            }

            if (this.tupleSchema != null) {
                if (i >= this.tupleSchema.size()) {
                    errors.add(at + "[" + i
                            + "]: no validator found at this index");
                } else {
                    errors.addAll((this.tupleSchema.get(i)).validate(n, node,
                            at + "[" + i + "]"));
                }
            }

            i++;
        }
        return errors;
    }
}
