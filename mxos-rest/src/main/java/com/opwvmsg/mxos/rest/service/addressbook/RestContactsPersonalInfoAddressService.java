/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.rest.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoAddressService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * Rest Service to Contacts Personal Info level operations like Read and Update.
 * 
 * @author mxos-dev
 */
public class RestContactsPersonalInfoAddressService extends AbstractRestService
        implements IContactsPersonalInfoAddressService {
    private static Logger logger = Logger
            .getLogger(RestContactsPersonalInfoAddressService.class);

    @Override
    public Address read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.PERSONAL_INFO_ADDRESS_SUB_URL, inputParams);

        Address address = (Address) callRest(subUrl, inputParams,
                Address.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return address;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.PERSONAL_INFO_ADDRESS_SUB_URL, inputParams);
        callRest(subUrl, inputParams, Operation.POST);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
