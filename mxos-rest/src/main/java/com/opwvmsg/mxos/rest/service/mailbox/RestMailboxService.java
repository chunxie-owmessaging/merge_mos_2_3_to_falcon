/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * Mailbox related activities e.g create, update, read, search and delete
 * Mailbox via REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestMailboxService extends AbstractRestService implements
        IMailboxService {
    private static Logger logger = Logger.getLogger(RestMailboxService.class);

    @Override
    public String create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILBOX_SUB_URL, pool.isCustom(), inputParams);

        final String mailboxId = (String) callRest(subUrl, inputParams, String.class,
                Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return mailboxId;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILBOX_SUB_URL, pool.isCustom(), inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public Mailbox read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        
        String subUrl = inputParams.containsKey(MailboxProperty.email.name()) ?
                RestConstants.getFinalSubUrl(RestConstants.MAILBOX_SUB_URL, inputParams) :
                RestConstants.getFinalSubUrl(RestConstants.MAILBOX_SUB_URL_UID, inputParams);

        final Mailbox mailbox = (Mailbox) callRest(subUrl, inputParams,
                Mailbox.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return mailbox;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Mailbox> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILBOXES_SUB_URL, inputParams);

        final List<Mailbox> mailAccessAllowedipList = (List<Mailbox>) callRest(
                subUrl, inputParams, new GenericType<List<Mailbox>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return mailAccessAllowedipList;
    }

    @Override
    public void deleteMSS(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILBOX_DELETE_IN_MSS_SUB_URL, pool.isCustom(),
                inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;

    }

}
