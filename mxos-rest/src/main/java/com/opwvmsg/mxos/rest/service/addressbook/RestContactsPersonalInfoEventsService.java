/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.rest.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Event;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoEventsService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Rest Service to Contacts Personal Info Events level operations like Create,
 * Read, Update, Delete.
 * 
 * @author mxos-dev
 */
public class RestContactsPersonalInfoEventsService extends AbstractRestService
        implements IContactsPersonalInfoEventsService {
    private static Logger logger = Logger
            .getLogger(RestContactsPersonalInfoEventsService.class);

    @Override
    public void create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.PERSONAL_INFO_EVENTS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.PERSONAL_INFO_EVENTS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.PERSONAL_INFO_ALL_EVENTS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public Event read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.PERSONAL_INFO_EVENTS_SUB_URL, inputParams);

        Event event = (Event) callRest(subUrl, inputParams, Event.class,
                Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return event;
    }

    @Override
    public List<Event> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.PERSONAL_INFO_ALL_EVENTS_SUB_URL, inputParams);

        final List<Event> eventsList = (List<Event>) callRest(subUrl,
                inputParams, new GenericType<List<Event>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return eventsList;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.PERSONAL_INFO_EVENTS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
