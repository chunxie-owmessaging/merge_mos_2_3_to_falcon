/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:$
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendSignatureService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * MailSend signature operations interface which will be exposed to the client.
 * This interface is responsible for doing Signature related operations (like
 * Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class RestMailSendSignatureService extends AbstractRestService implements
        IMailSendSignatureService {
    private static Logger logger = Logger
            .getLogger(RestMailSendSignatureService.class);

    @Override
    public Long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.MAILSEND_SIGNATURE_SUB_URL).toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        final long signature = (Long) callRest(subUrl, inputParams, Long.class,
                Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return signature;
    }

    @Override
    public List<Signature> read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILSEND_SIGNATURE_SUB_URL, inputParams);

        final List<Signature> signatureList = (List<Signature>) callRest(
                subUrl, inputParams, new GenericType<List<Signature>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return signatureList;
    }

    @Override
    public Signature readSignature(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.MAILSEND_SIGNATURE_SUB_URL).append(
                "/{signatureId}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        final Signature signature = (Signature) callRest(subUrl, inputParams,
                Signature.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return signature;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.MAILSEND_SIGNATURE_SUB_URL).append(
                "/{signatureId}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.MAILSEND_SIGNATURE_SUB_URL).append(
                "/{signatureId}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
