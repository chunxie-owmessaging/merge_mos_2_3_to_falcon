/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.rest.service;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSService;
import com.opwvmsg.mxos.interfaces.service.RetryStrategy;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.opwvmsg.mxos.interfaces.service.Operation;

/**
 * Abstract MxOS Service.
 * 
 * @author mxos-dev
 */
public abstract class AbstractRestService implements IMxOSService {
    private static Logger logger = Logger.getLogger(AbstractRestService.class);

    public RestConnectionPool pool;

    public void init(final RestConnectionPool pool) {
        this.pool = pool;
    }

    public <T> T setupRetryStrategy(T service, RetryStrategy retryStrategy)
            throws MxOSException {
        // Return dynamic proxy, so calls can be intercepted
        return (T) RestServiceProxy.newInstance(this, retryStrategy);
    }

    /**
     * Performs the Rest call and returns the output Object
     * 
     * @param url: REST URL to be triggered
     * @param inputParams: Map of input Parameters
     * @param returnObjType: Return class Object Type
     * @param operation: REST operation type
     * @return
     * @throws MxOSException
     */
    protected Object callRest(final String url,
            final Map<String, List<String>> inputParams, Class returnObjType,
            final Operation operation) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("callRest method start");
        }

        if ((url == null) || ("".equals(url))) {
            logger.error("URL cannot be null");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Invalid URL received");
        }

        if (returnObjType == null) {
            logger.error("Return Object Type cannot be null");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Return Object Type cannot be null");
        }

        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            if (rest == null) {
                logger.error("Failed to borrow the REST connection from the pool");
                throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(),
                        "Failed to borrow the REST connection from the pool");
            }
            switch (operation) {
            case GET:
                resp = rest.get(url, inputParams);
                break;
            case PUT:
                resp = rest.put(url, inputParams);
                break;
            case POST:
                resp = rest.post(url, inputParams);
                break;
            case DELETE:
                resp = rest.delete(url, inputParams);
                break;
            default:
                logger.error("Unsupported Operatopn Type Received");
                break;
            }
            if (logger.isDebugEnabled()) {
                logger.debug("callRest method end");
            }
            return resp.getEntity(returnObjType);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error("Unable to return the connection", e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    /**
     * Performs the Rest call
     * 
     * @param url: REST URL to be triggered
     * @param inputParams: Map of input Parameters
     * @param operation: REST operation type
     * @return
     * @throws MxOSException
     */
    protected void callRest(final String url,
            final Map<String, List<String>> inputParams,
            final Operation operation) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("callRest method start");
        }

        if ((url == null) || ("".equals(url))) {
            logger.error("URL cannot be null");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Invalid URL received");
        }

        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            if (rest == null) {
                logger.error("Failed to borrow the REST connection from the pool");
                throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(),
                        "Failed to borrow the REST connection from the pool");
            }
            switch (operation) {
            case GET:
                resp = rest.get(url, inputParams);
                break;
            case PUT:
                resp = rest.put(url, inputParams);
                break;
            case POST:
                resp = rest.post(url, inputParams);
                break;
            case DELETE:
                resp = rest.delete(url, inputParams);
                break;
            default:
                logger.error("Unsupported Operatopn Type Received");
                break;
            }
            if (logger.isDebugEnabled()) {
                logger.debug("callRest method end");
            }
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error("Unable to return the connection", e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    /**
     * Performs the Rest call and returns the output Object. This is used when
     * the return object type is List
     * 
     * @param url: REST URL to be triggered
     * @param inputParams: Map of input Parameters
     * @param returnObjType: Return class Object
     * @param operation: REST operation type
     * @return
     * @throws MxOSException
     */
    protected <T> Object callRest(final String url,
            final Map<String, List<String>> inputParams,
            GenericType<T> returnObjType, final Operation operation)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("callRest method start");
        }

        if ((url == null) || ("".equals(url))) {
            logger.error("URL cannot be null");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Invalid URL received");
        }

        if (returnObjType == null) {
            logger.error("Return Object Type cannot be null");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Return Object Type cannot be null");
        }

        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            if (rest == null) {
                logger.error("Failed to borrow the REST connection from the pool");
                throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(),
                        "Failed to borrow the REST connection from the pool");
            }
            switch (operation) {
            case GET:
                resp = rest.get(url, inputParams);
                break;
            case PUT:
                resp = rest.put(url, inputParams);
                break;
            case POST:
                resp = rest.post(url, inputParams);
                break;
            case DELETE:
                resp = rest.delete(url, inputParams);
                break;
            default:
                logger.error("Unsupported Operatopn Type Received");
                break;
            }
            if (logger.isDebugEnabled()) {
                logger.debug("callRest method end");
            }
            return resp.getEntity(returnObjType);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error("Unable to return the connection", e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }
    
    /**
     * Performs the Rest call
     * 
     * @param <T>
     * 
     * @param url: REST URL to be triggered
     * @param inputParams: Map of input Parameters
     * @param operation: REST operation type
     * @return
     * @throws MxOSException
     */
    protected <T> Object callPostMultiValue(final String url,
            final Map<String, List<String>> inputParams, final File file,
            GenericType<T> returnObjType) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("postMultipart method start");
        }

        if ((url == null) || ("".equals(url))) {
            logger.error("URL cannot be null");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Invalid URL received");
        }

        if (file == null) {
            logger.error("Empty File received");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Empty File received");
        }

        RestCRUD rest = null;
        ClientResponse resp = null;
        try {
            rest = pool.borrowObject();
            if (rest == null) {
                logger.error("Failed to borrow the REST connection from the pool");
                throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(),
                        "Failed to borrow the REST connection from the pool");
            }
            resp = rest.postMultipart(url, file, inputParams);
            if (logger.isDebugEnabled()) {
                logger.debug("postMultipart method end");
            }
            if (returnObjType != null)
                return resp.getEntity(returnObjType);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        } finally {
            if (resp != null) {
                try {
                    resp.close();
                } catch (final Exception e) {
                    logger.warn("WARN: Unabled to close mOS Response.");
                }
            }
            if (rest != null) {
                try {
                    pool.returnObject(rest);
                } catch (final Exception e) {
                    logger.error("Unable to return the connection", e);
                    throw new MxOSException(
                            ErrorCode.MXS_CONNECTION_ERROR.name(), e);
                }
            }
        }
        return null;
    }
    
}