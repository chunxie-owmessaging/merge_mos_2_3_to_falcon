/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.tasks;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksParticipantService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.opwvmsg.mxos.task.pojos.Participant;
import com.sun.jersey.api.client.GenericType;

/**
 * Rest Service to Tasks Participant Object level operations like Read, Update
 * and List.
 * 
 * @author mxos-dev
 */
public class RestTasksParticipantService extends AbstractRestService implements
        ITasksParticipantService {
    private static Logger logger = Logger
            .getLogger(RestTasksParticipantService.class);

    @Override
    public void create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_PARTICIPANTS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public Participant read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_PARTICIPANT_SUB_URL, inputParams);

        final Participant participant = (Participant) callRest(subUrl,
                inputParams, Participant.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return participant;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_PARTICIPANTS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Long.class, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_PARTICIPANT_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public List<Participant> list(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_PARTICIPANTS_LIST_SUB_URL, inputParams);

        final List<Participant> participantList = (List<Participant>) callRest(
                subUrl, inputParams, new GenericType<List<Participant>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return participantList;
    }
}
