/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:$
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessAllowedIpService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * MailAccess Allowed Ip operations interface which will be exposed to the
 * client. This interface is responsible for doing allowed Ip related operations
 * (like Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class RestMailAccessAllowedIpService extends AbstractRestService
        implements IMailAccessAllowedIpService {
    private static Logger logger = Logger
            .getLogger(RestMailAccessAllowedIpService.class);

    @Override
    public void create(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp;
        List<String> allowedIP = inputParams.get(MailboxProperty.allowedIP
                .name());
        if (allowedIP != null && allowedIP.size() > 1) {
            temp = new StringBuffer(
                    RestConstants.MAILACCESS_ALLOWED_IPS_SUB_URL).toString();
        } else {
            temp = new StringBuffer(
                    RestConstants.MAILACCESS_SINGLE_ALLOWED_IPS_SUB_URL)
                    .toString();
        }
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public List<String> read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILACCESS_ALLOWED_IPS_SUB_URL, inputParams);

        final List<String> mailAccessAllowedipList = (List<String>) callRest(
                subUrl, inputParams, new GenericType<List<String>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return mailAccessAllowedipList;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.MAILACCESS_ALLOWED_IPS_SUB_URL).append(
                "/{oldAllowedIP}/{newAllowedIP}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.MAILACCESS_ALLOWED_IPS_SUB_URL).append(
                "/{allowedIP}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

}
