/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.INumDelayedDeliveryMessagesPendingService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Delivery messages pending service exposed to client which is responsible for
 * doing basic delayed delivery messages pending service related activities
 * (like Create, Read, Update, Delete, etc.) via REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestMailSendNumDelayedDeliveryMessagesPendingService extends
        AbstractRestService implements
        INumDelayedDeliveryMessagesPendingService {
    private static Logger logger = Logger
            .getLogger(RestMailSendNumDelayedDeliveryMessagesPendingService.class);

    /**
     * Default Constructor.
     * 
     * @throws Exception on any error.
     */
    public RestMailSendNumDelayedDeliveryMessagesPendingService()
            throws MxOSException {
    }

    @Override
    public void create(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp;
        List<String> numDelayedDeliveryMessagesPending = inputParams
                .get(MailboxProperty.numDelayedDeliveryMessagesPending.name());
        if (numDelayedDeliveryMessagesPending != null
                && numDelayedDeliveryMessagesPending.size() > 1) {
            temp = new StringBuffer(
                    RestConstants.MAILSEND_DELIVERY_MESSAGES_PENDING_SUB_URL)
                    .toString();
        } else {
            temp = new StringBuffer(
                    RestConstants.SINGLE_MAILSEND_DELIVERY_MESSAGES_PENDING_SUB_URL)
                    .toString();
        }

        final String subUrl = RestConstants.getFinalSubUrl(temp,
                pool.isCustom(), inputParams);

        callRest(subUrl, inputParams, Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public List<String> read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILSEND_DELIVERY_MESSAGES_PENDING_SUB_URL,
                inputParams);

        final List<String> messagesList = (List<String>) callRest(subUrl,
                inputParams, new GenericType<List<String>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return messagesList;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.MAILSEND_DELIVERY_MESSAGES_PENDING_SUB_URL)
                .append("/{oldNumDelayedDeliveryMessagesPending}/{newNumDelayedDeliveryMessagesPending}")
                .toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp,
                pool.isCustom(), inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.MAILSEND_DELIVERY_MESSAGES_PENDING_SUB_URL)
                .append("/{numDelayedDeliveryMessagesPending}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp,
                pool.isCustom(), inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
