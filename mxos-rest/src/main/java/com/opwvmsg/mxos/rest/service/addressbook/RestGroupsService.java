/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.rest.service.addressbook;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Groups service exposed to client which is responsible for doing basic Groups
 * related activities e.g create, update, read, search and delete Group via REST
 * API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestGroupsService extends AbstractRestService implements
        IGroupsService {
    private static Logger logger = Logger.getLogger(RestGroupsService.class);

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_SUB_URL, pool.isCustom(), inputParams);

        long id = (Long) callRest(subUrl, inputParams, Long.class,
                Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return id;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_DELETE_SUB_URL, pool.isCustom(),
                inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_SUB_URL, pool.isCustom(), inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public List<Long> createMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_SUB_URL, pool.isCustom(), inputParams);
        try {
            File file = File.createTempFile(MxOSConstants.EXTENSION,
                    MxOSConstants.EXTENSION);
            byte[] dataBytes = inputParams
                    .get(AddressBookProperty.groupsList.name()).get(0)
                    .getBytes(MxOSConstants.UTF8);
            FileUtils.writeByteArrayToFile(file, dataBytes);
            inputParams.remove(AddressBookProperty.groupsList.name());
            List<Long> ids = (List<Long>) callPostMultiValue(subUrl,
                    inputParams, file, new GenericType<List<Long>>() {
                    });
            return ids;
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.ABS_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void moveMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants
                .getFinalSubUrl(RestConstants.GROUPS_MOVE_SUB_URL,
                        pool.isCustom(), inputParams);
        try {
            File file = File.createTempFile(MxOSConstants.EXTENSION,
                    MxOSConstants.EXTENSION);
            byte[] dataBytes = inputParams
                    .get(AddressBookProperty.groupsList.name()).get(0)
                    .getBytes(MxOSConstants.UTF8);
            FileUtils.writeByteArrayToFile(file, dataBytes);
            inputParams.remove(AddressBookProperty.groupsList.name());
            callPostMultiValue(subUrl, inputParams, file, null);
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.ABS_CONNECTION_ERROR.name(), e);
        }
    }
}
