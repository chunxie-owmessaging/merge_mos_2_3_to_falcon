/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Image;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsImageService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * Rest Service to Address Book Object level operations like Read, Update and
 * List.
 * 
 * @author mxos-dev
 */
public class RestContactsImageService extends AbstractRestService implements
        IContactsImageService {
    private static Logger logger = Logger
            .getLogger(RestContactsImageService.class);

    @Override
    public Image read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CONTACTS_IMAGE_SUB_URL, inputParams);

        Image image = (Image) callRest(subUrl, inputParams, Image.class,
                Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return image;
    }

    @Override
    public byte[] readImage(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CONTACTS_ACTUAL_IMAGE_SUB_URL, inputParams);

        byte[] image = (byte[]) callRest(subUrl, inputParams, byte[].class,
                Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return image;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = null;
        subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CONTACTS_IMAGE_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
