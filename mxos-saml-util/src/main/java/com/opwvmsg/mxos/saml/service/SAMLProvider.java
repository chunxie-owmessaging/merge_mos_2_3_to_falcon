/*
 * Copyright (c) 2013 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Messaging Inc. The software may be used and/or copied only
 * with the written permission of Openwave Messaging Inc. or in
 * accordance with the terms and conditions stipulated in the
 * agreement/contract under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.service;

/**
 * Interface to create SAML provider instance.
 *
 * @author mxos-dev
 * 
 */
public interface SAMLProvider {
    SAMLService createSamlService();
}

