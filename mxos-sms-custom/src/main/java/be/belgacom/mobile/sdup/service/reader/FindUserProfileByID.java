/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package be.belgacom.mobile.sdup.service.reader;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content 
 * contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clientApp" 
 *         type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subscriberID" 
 *         type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceId" 
 *         type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "clientApp", "subscriberID", "serviceId" })
@XmlRootElement(name = "findUserProfileByID")
public class FindUserProfileByID {

    @XmlElementRef(name = "clientApp", namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        type = JAXBElement.class)
    protected JAXBElement<String> clientApp;
    @XmlElementRef(name = "subscriberID", namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        type = JAXBElement.class)
    protected JAXBElement<String> subscriberID;
    @XmlElementRef(name = "serviceId", namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        type = JAXBElement.class)
    protected JAXBElement<String> serviceId;

    /**
     * Gets the value of the clientApp property.
     * 
     * @return possible object is {@link JAXBElement }
     * {@code <}{@link String } {@code >}
     * 
     */
    public JAXBElement<String> getClientApp() {
        return clientApp;
    }

    /**
     * Sets the value of the clientApp property.
     * 
     * @param value allowed object is {@link JAXBElement }
     * {@code <}{@link String } {@code >}
     * 
     */
    public void setClientApp(JAXBElement<String> value) {
        this.clientApp = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the subscriberID property.
     * 
     * @return possible object is {@link JAXBElement }
     * {@code <}{@link String } {@code >}
     * 
     */
    public JAXBElement<String> getSubscriberID() {
        return subscriberID;
    }

    /**
     * Sets the value of the subscriberID property.
     * 
     * @param value allowed object is {@link JAXBElement }
     * {@code <}{@link String } {@code >}
     * 
     */
    public void setSubscriberID(JAXBElement<String> value) {
        this.subscriberID = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the serviceId property.
     * 
     * @return possible object is {@link JAXBElement }
     * {@code <}{@link String } {@code >}
     * 
     */
    public JAXBElement<String> getServiceId() {
        return serviceId;
    }

    /**
     * Sets the value of the serviceId property.
     * 
     * @param value allowed object is {@link JAXBElement }
     * {@code <}{@link String } {@code >}
     * 
     */
    public void setServiceId(JAXBElement<String> value) {
        this.serviceId = ((JAXBElement<String>) value);
    }

}
