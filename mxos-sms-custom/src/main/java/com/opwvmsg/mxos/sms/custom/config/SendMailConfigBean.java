/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sms.custom.config;

import java.util.Properties;

import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * Config Bean class with the properties for Log to External Entity Service.
 * 
 * @author mxos-dev
 */
public class SendMailConfigBean {

    private String type;
    private String provider;
    private String host;
    private String port;
    private int maxConnections;
    private String debug;
    private String authEnabled;
    private String mechanisms;
    private String useRSet;
    private String connectionTimeout;
    private String timeout;
    private String sslEnabled;
    private String sslCheckServerIdentity;
    private String saslEnabled;
    private String saslMechanisms;
    private String saslRealm;
    private String quitWait;
    
    private Properties properties ;
    
    /**
     * @return the properties
     */
    public Properties getProperties() {
        return properties;
    }

    /**
     * Default Constructor.
     */
    public SendMailConfigBean() {
        type = null;
        provider = null;
        host = null;
        port = "25";
        maxConnections = 0;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @return the maxConnections
     */
    public int getMaxConnections() {
        return maxConnections;
    }

    /**
     * @param maxConnections the maxConnections to set
     */
    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    /**
     * @return the mechanisms
     */
    public String getMechanisms() {
        return mechanisms;
    }

    /**
     * @param mechanisms the mechanisms to set
     */
    public void setMechanisms(String mechanisms) {
        this.mechanisms = mechanisms;
    }

    /**
     * @return the saslMechanisms
     */
    public String getSaslMechanisms() {
        return saslMechanisms;
    }

    /**
     * @param saslMechanisms the saslMechanisms to set
     */
    public void setSaslMechanisms(String saslMechanisms) {
        this.saslMechanisms = saslMechanisms;
    }

    /**
     * @return the saslRealm
     */
    public String getSaslRealm() {
        return saslRealm;
    }

    /**
     * @param saslRealm the saslRealm to set
     */
    public void setSaslRealm(String saslRealm) {
        this.saslRealm = saslRealm;
    }

    /**
     * @param sslCipherSuites the sslCipherSuites to set
     */
    public void setProperties() {
        this.properties = new Properties();
        properties.put(SmsConstants.MAIL_SMTP_DEBUG, debug);
        properties.put(SmsConstants.MAIL_SMTP_CONNECTION_TIMEOUT,
                connectionTimeout);
        properties.put(SmsConstants.MAIL_SMTP_TIMEOUT, timeout);
        properties.put(SmsConstants.MAIL_SMTP_AUTH, authEnabled);
        properties.put(SmsConstants.MAIL_SMTP_AUTH_MECHANISMS, mechanisms);
        properties.put(SmsConstants.MAIL_SMTP_USE_RSET, useRSet);
        properties.put(SmsConstants.MAIL_SMTP_SSL_ENABLED, sslEnabled);
        properties.put(SmsConstants.MAIL_SMTP_SSL_CHECK_SERVER_IDENTITY,
                sslCheckServerIdentity);
        properties.put(SmsConstants.MAIL_SMTP_SASL_ENABLED, saslEnabled);
        properties.put(SmsConstants.MAIL_SMTP_SASL_MECHANISMS, saslMechanisms);
        properties.put(SmsConstants.MAIL_SMTP_SASL_REALM , saslRealm);
        properties.put(SmsConstants.MAIL_SMTP_QUITWAIT , quitWait);
    }

    /**
     * @return the debug
     */
    public String getDebug() {
        return debug;
    }

    /**
     * @param debug the debug to set
     */
    public void setDebug(String debug) {
        this.debug = debug;
    }

    /**
     * @return the authEnabled
     */
    public String getAuthEnabled() {
        return authEnabled;
    }

    /**
     * @param authEnabled the authEnabled to set
     */
    public void setAuthEnabled(String authEnabled) {
        this.authEnabled = authEnabled;
    }

    /**
     * @return the useRSet
     */
    public String getUseRSet() {
        return useRSet;
    }

    /**
     * @param useRSet the useRSet to set
     */
    public void setUseRSet(String useRSet) {
        this.useRSet = useRSet;
    }

    /**
     * @return the connectionTimeout
     */
    public String getConnectionTimeout() {
        return connectionTimeout;
    }

    /**
     * @param connectionTimeout the connectionTimeout to set
     */
    public void setConnectionTimeout(String connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    /**
     * @return the timeout
     */
    public String getTimeout() {
        return timeout;
    }

    /**
     * @param timeout the timeout to set
     */
    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    /**
     * @return the sslEnabled
     */
    public String getSslEnabled() {
        return sslEnabled;
    }

    /**
     * @param sslEnabled the sslEnabled to set
     */
    public void setSslEnabled(String sslEnabled) {
        this.sslEnabled = sslEnabled;
    }

    /**
     * @return the sslCheckServerIdentity
     */
    public String getSslCheckServerIdentity() {
        return sslCheckServerIdentity;
    }

    /**
     * @param sslCheckServerIdentity the sslCheckServerIdentity to set
     */
    public void setSslCheckServerIdentity(String sslCheckServerIdentity) {
        this.sslCheckServerIdentity = sslCheckServerIdentity;
    }

    /**
     * @return the saslEnabled
     */
    public String getSaslEnabled() {
        return saslEnabled;
    }

    /**
     * @param saslEnabled the saslEnabled to set
     */
    public void setSaslEnabled(String saslEnabled) {
        this.saslEnabled = saslEnabled;
    }

    /**
     * @return the quitWait
     */
    public String getQuitWait() {
        return quitWait;
    }

    /**
     * @param quitWait the quitWait to set
     */
    public void setQuitWait(String quitWait) {
        this.quitWait = quitWait;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    /*     (non-Javadoc)
     * @see java.lang.Object#toString()
     * */
     
    @Override
    public String toString() {
        return "SendMailConfigBean [type=" + type + ", provider=" + provider
                + ", host=" + host + ", port=" + port + ", maxConnections="
                + maxConnections + ", debug=" + debug + ", authEnabled="
                + authEnabled + ", mechanisms=" + mechanisms + ", useRSet="
                + useRSet + ", connectionTimeout=" + connectionTimeout
                + ", timeout=" + timeout + ", sslEnabled=" + sslEnabled
                + ", sslCheckServerIdentity=" + sslCheckServerIdentity
                + ", saslEnabled=" + saslEnabled + ", saslMechanisms="
                + saslMechanisms + ", saslRealm=" + saslRealm + ", quitWait="
                + quitWait + ", properties=" + properties + "]";
    }

}
