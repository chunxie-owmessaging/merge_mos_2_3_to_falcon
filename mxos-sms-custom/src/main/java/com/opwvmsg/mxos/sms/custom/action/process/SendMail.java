/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.action.process;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.sms.custom.gateway.ISendMailGateway;
import com.opwvmsg.mxos.sms.custom.gateway.SmsGatewayFactory;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;

/**
 * Action class for Send Mail Service.
 * 
 * @author mxos-dev
 */
public class SendMail implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SendMail.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SendMail action start."));
        }

        final String sendMailGateway = System
                .getProperty(SystemProperty.sendMailGateway.name());

        ((ISendMailGateway) SmsGatewayFactory.getInstance()
                .getSmsGatewayContext(SmsConstants.SENDMAILGATEWAYS,
                        sendMailGateway))
                .process(requestState.getInputParams());

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SendMail action end."));
        }
    }
}
