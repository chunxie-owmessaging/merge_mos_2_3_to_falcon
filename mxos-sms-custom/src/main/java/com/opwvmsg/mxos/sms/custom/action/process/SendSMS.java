/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.action.process;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.sms.custom.gateway.ISendSMSGateway;
import com.opwvmsg.mxos.sms.custom.gateway.SmsGatewayFactory;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;

/**
 * Action class for Send SMS.
 *
 * @author mxos-dev
 */
public class SendSMS implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SendSMS.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SendSMS action start."));
        }
        
        ((ISendSMSGateway) SmsGatewayFactory.getInstance()
                .getSmsGatewayContext(
                        SmsConstants.SENDSMSGATEWAYS,
                        requestState.getInputParams()
                                .get(SmsProperty.smsType.name()).get(0)))
                .process(requestState.getInputParams());

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SendSMS action end."));
        }
    }
}
