/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.gateway;

import java.util.List;
import java.util.Map;

import javax.mail.Address;
import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.SmsGatewayException;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.mxos.sms.custom.config.SendMailConfigBean;
import com.opwvmsg.mxos.sms.custom.smtp.SmtpConnection;
import com.opwvmsg.mxos.sms.custom.smtp.SmtpConnectionPool;
import com.opwvmsg.mxos.sms.custom.smtp.SmtpConnectionPoolFactory;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;
import com.sun.mail.smtp.SMTPAddressFailedException;
import com.sun.mail.smtp.SMTPSendFailedException;
import com.sun.mail.smtp.SMTPSenderFailedException;

/**
 * Send SMS Gateway implementation class that sends SMS using External VAMP.
 * Interface.
 * 
 * @author mxos-dev
 */
public class MTASendMailGateway implements ISendMailGateway {

    private static Logger logger = Logger.getLogger(MTASendMailGateway.class);
    private SendMailConfigBean configBean;

    /**
     * Constructor
     * 
     * @param configBean SendMailConfigBean
     */
    public MTASendMailGateway(final SendMailConfigBean configBean) {
        this.configBean = configBean;
    }

    @Override
    public void process(final Map<String, List<String>> inputParams)
            throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("MTASendMailGateway process start."));
        }

        String email = null;
        if (inputParams.containsKey(SmsProperty.toAddress.name())) {
            email = inputParams.get(SmsProperty.toAddress.name()).get(0);
        }

        SmtpConnectionPool smtpPool = null;
        SmtpConnection smtpConn = null;

        String user = null;
        String password = null;
        try {

            if (inputParams.containsKey(MailboxProperty.fromAddress.name())
                    && inputParams.containsKey(MailboxProperty.password.name())) {
                user = inputParams.get(MailboxProperty.fromAddress.name()).get(
                        0);
                password = inputParams.get(MailboxProperty.password.name())
                        .get(0);
            }
            if (inputParams.containsKey(MailboxProperty.smtpAuthUser.name())) {
                if (logger.isDebugEnabled())
                    logger.debug("Input param map contains"
                            + MailboxProperty.smtpAuthUser.name());
                user = inputParams.get(MailboxProperty.smtpAuthUser.name())
                        .get(0);
            }
            smtpPool = SmtpConnectionPoolFactory.getInstance().getSMTPPool(
                    SmsConstants.MTA_SMTP_POOL, configBean.getHost(),
                    configBean.getPort(), configBean.getMaxConnections(),
                    configBean.getProperties());
            smtpConn = smtpPool.borrowObject();
            smtpConn.connect(user, password);
            ConnectionStats.ACTIVE_EXTERNAL_MTA.increment();
        } catch (AuthenticationFailedException e) {
            logger.error("Authentication failed for email : " + user
                    + " during SMTP connect. Message : " + e);
            StringBuffer errorMsg = new StringBuffer(e.getMessage());
            while (null != e.getNextException()) {
                errorMsg.append(";");
                errorMsg.append(e.getNextException().getMessage());
                e = (AuthenticationFailedException) e.getNextException();
            }
            throw new SmsGatewayException(
                    CustomError.SEND_MAIL_AUTHENTICATION_ERROR.name(),
                    errorMsg.toString());
        } catch (MessagingException e) {
            logger.error("MTA Error(MessagingException) received for email : "
                    + user + " during SMTP connect. Message : " + e);
            StringBuffer errorMsg = new StringBuffer(e.getMessage());
            while (null != e.getNextException()) {
                if (e.getNextException() instanceof java.net.ConnectException) {
                    ConnectionErrorStats.MTA.increment();
                    throw new SmsGatewayException(
                            CustomError.SEND_MAIL_CONNECT_ERROR.name(),
                            errorMsg.toString());
                }
                errorMsg.append(";");
                errorMsg.append(e.getNextException().getMessage());
                e = (MessagingException) e.getNextException();
            }
            throw new SmsGatewayException(CustomError.SEND_MAIL_ERROR.name(),
                    errorMsg.toString());
        } catch (Exception e) {
            logger.error("Exception occured during SMTP connect. Message : "
                    + e);
            ConnectionErrorStats.MTA.increment();
            throw new SmsGatewayException(CustomError.SEND_MAIL_ERROR.name(),
                    e.getMessage());

        }

        if (logger.isDebugEnabled()) {
            logger.debug("Successfully borrowed a SMTP connection from "
                    + "Connection Pool. Sending the message for email : "
                    + email);
        }
        try {
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "Sending SMTP message with fromAddress: ")
                        .append(inputParams.get(
                                MailboxProperty.fromAddress.name()).get(0))
                        .append(", toAddress: ").append(email));
            }
            smtpConn.sendSMTPMessage(
                    inputParams.get(SmsProperty.fromAddress.name()).get(0),
                    email, inputParams.get(SmsProperty.message.name()).get(0));

        } catch (AuthenticationFailedException e) {
            logger.error("MTA Error(AuthenticationFailedException) occured.", e);
            StringBuffer errorMsg = new StringBuffer(e.getMessage());
            while (null != e.getNextException()) {
                errorMsg.append(";");
                errorMsg.append(e.getNextException().getMessage());
                e = (AuthenticationFailedException) e.getNextException();
            }
            throw new SmsGatewayException(
                    CustomError.SEND_MAIL_AUTHENTICATION_ERROR.name(),
                    errorMsg.toString());

        } catch (AddressException e) {
            logger.error("MTA Error(AddressException) occured.", e);
            throw new SmsGatewayException(CustomError.SEND_MAIL_ERROR.name(),
                    e.getMessage());
        } catch (MessagingException e) {
            logger.error("MTA Error(MessagingException) occured.", e);
            StringBuffer errorMsg = new StringBuffer(e.getMessage());
            while (null != e.getNextException()) {
                if (e.getNextException() instanceof java.net.ConnectException) {
                    throw new SmsGatewayException(
                            CustomError.SEND_MAIL_CONNECT_ERROR.name(),
                            errorMsg.toString());
                } else if (e.getNextException() instanceof SendFailedException) {
                    final MessagingException sendEx = (MessagingException) e
                            .getNextException();
                    if (sendEx instanceof SMTPAddressFailedException) {
                        final SMTPAddressFailedException sendAddrFailedException = (SMTPAddressFailedException) sendEx;
                        int rc = sendAddrFailedException.getReturnCode();
                        errorMsg.append(";").append(rc)
                                .append(" Invalid recipient: <");
                        InternetAddress addr = sendAddrFailedException
                                .getAddress();
                        errorMsg.append(addr.getAddress()).append(">");
                    } else if (sendEx instanceof SMTPSenderFailedException) {
                        final SMTPSenderFailedException senderFailedException = (SMTPSenderFailedException) sendEx;
                        int rc = senderFailedException.getReturnCode();
                        errorMsg.append(";").append(rc)
                                .append(" Invalid sender: <");
                        InternetAddress addr = senderFailedException
                                .getAddress();
                        errorMsg.append(addr.getAddress()).append(">");
                    } else if (sendEx instanceof SMTPSendFailedException) {
                        final SMTPSendFailedException sendFailedException = (SMTPSendFailedException) sendEx;
                        int rc = sendFailedException.getReturnCode();
                        errorMsg.append(";").append(rc)
                                .append(" Invalid SMTP command: <");
                        errorMsg.append(sendFailedException.getCommand())
                                .append(">");
                    } else {
                        logger.error("SendFailedException errorMsg: " + sendEx.getMessage());
                    }
                }
                e = (MessagingException) e.getNextException();
            }
            logger.error("SEND_MAIL_ERROR longMessage: " + errorMsg);
            throw new SmsGatewayException(CustomError.SEND_MAIL_ERROR.name(),
                    errorMsg.toString());
        } catch (Exception e) {
            logger.error("MTA Error(Exception) occured.", e);
            throw new SmsGatewayException(CustomError.SEND_MAIL_ERROR.name(),
                    e.getMessage());
        } finally {
            if (smtpPool != null && smtpConn != null) {
                try {
                    smtpPool.returnObject(smtpConn);

                    ConnectionStats.ACTIVE_EXTERNAL_MTA.decrement();
                } catch (final Exception e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }

        logger.info("Successfully sent the mail to MTA");

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("MTASendMailGateway process end."));
        }

    }
}
