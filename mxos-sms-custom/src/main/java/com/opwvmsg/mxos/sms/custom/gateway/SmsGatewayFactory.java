/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sms.custom.gateway;

import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.sms.custom.config.SmsGatewaysConfigHandler;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * Factory class to get SMS Gateway Context instance.
 * 
 * @author mxos-dev
 */
public class SmsGatewayFactory {

    private Map<String, Map<String, Object>> smsGateways = null;
    public static SmsGatewayFactory instance = null;

    private static Logger logger = Logger.getLogger(SmsGatewayFactory.class);

    /**
     * Method to get SmsGatewayFactory.
     * 
     * @return instance
     * @throws MxOSException
     */
    public static SmsGatewayFactory getInstance() throws MxOSException {

        if (instance == null) {
            instance = new SmsGatewayFactory();
        }

        return instance;
    }

    /**
     * Default private Constructor.
     * 
     * @throws MxOSException
     */
    private SmsGatewayFactory() throws MxOSException {

        smsGateways = new LinkedHashMap<String, Map<String, Object>>();

        SmsGatewaysConfigHandler.loadSmsGatewayConfigFromXML(
                SmsConstants.SMS_GATEWAY_CFG_FILENAME, smsGateways);

        logger.info("Parsed the gateway-config.xml File successfully");
        logger.info("Number of Send SMS Gateways Configured : "
                + smsGateways.get(SmsConstants.SENDSMSGATEWAYS).size());
        logger.info("Number of Auth Gateways Configured : "
                + smsGateways.get(SmsConstants.AUTHGATEWAYS).size());
        logger.info("Number of Log to External Entity Gateways Configured : "
                + smsGateways.get(SmsConstants.LOGTOEXTERNALENTITYGATEWAYS)
                        .size());
        logger.info("Number of Send Mail Gateways Configured : "
                + smsGateways.get(SmsConstants.SENDMAILGATEWAYS).size());
    }

    /**
     * Method to get SMS Implementation class instance.
     * 
     * @param gateway String
     * @param smsType String
     * @return Object
     * @throws MxOSException
     */
    public Object getSmsGatewayContext(final String gateway,
            final String smsType) throws MxOSException {

        if ((smsGateways != null) && (smsGateways.get(gateway) != null)) {
            return smsGateways.get(gateway).get(smsType);
        } else {
            return null;
        }

    }
}
