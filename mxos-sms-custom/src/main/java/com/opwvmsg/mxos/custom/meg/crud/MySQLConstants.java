/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave/mxos/service/MySqlConstants.java#5 $
 */

/**
 * Constants for MySql
 *
 * @author Shantanu
 *         
 */
package com.opwvmsg.mxos.custom.meg.crud;

/**
 * Mysql metadata constants.
 *
 * @author mxos-dev
 *         
 */
public final class MySQLConstants {

    public static final String USER = "megMySqlUser";
    public static final String PASSWORD = "megMySqlPassword";
    public static final String MYSQL_DB_NAME = "mysqlDBName";
    public static final String MYSQL_PERHOST_FAILURE_ATTEMPTS = "mysqlPerHostFailureAttempts";
    public static final String MYSQL_HOST_AND_PORT = "mysqlHostAndPort";
    public static final String MEG_RETRY_INTERVAL = "megRetryInterval";
    public static final String MEG_MAX_RETRIES = "megMaxRetries";

    /**
     * Utility class - Private constructor.
     */
    private MySQLConstants() {

    }
}
