/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:$
 */

package com.opwvmsg.mxos.snmp.monitor;

import javax.ws.rs.core.Response;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

/**
 * Process monitor implementation.
 * 
 * @author mxos-dev
 */
public class ProcessMonitorImpl implements ProcessMonitor {

    private static final String MONITOR_STRING = "/monitor";
    private String serverUrl;

    /**
     * Constructor with server url parameter.
     * 
     * @param serverUrl String
     */
    public ProcessMonitorImpl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    @Override
    public boolean checkController() {

        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(serverUrl);

        // Call monitoring controller service
        try {
            ClientResponse resp = service.path(MONITOR_STRING).get(
                    ClientResponse.class);
            if (resp.getStatus() == Response.Status.OK.getStatusCode()) {
                return true;
            }
        } catch (Exception e) {
            // TODO - Need to log this exception
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
