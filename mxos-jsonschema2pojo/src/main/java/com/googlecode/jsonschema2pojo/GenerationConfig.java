package com.googlecode.jsonschema2pojo;

import java.io.File;

/**
 * Defines the configuration options for Java type generation, including source
 * and target paths/packages and all behavioural options (e.g should builders be
 * generated, should primitives be used, etc).
 * <p>
 * Devs: add to this interface if you need to introduce a new config property.
 */
public interface GenerationConfig {

    /**
     * @return Whether to generate builder-style methods of the form
     *         <code>withXxx(value)</code> (that return <code>this</code>),
     *         alongside the standard, void-return setters.
     */
    boolean isGenerateBuilders();

    /**
     * @return whether to use primitives (<code>long</code>, <code>double</code>
     *         , <code>boolean</code>) instead of wrapper types where possible
     *         when generating bean properties (has the side-effect of making
     *         those properties non-null).
     */
    boolean isUsePrimitives();

    /**
     * @return The source file or directory from which JSON Schema will be read
     */
    File getSource();

    /**
     * @return The target directory into which generated types will be written
     *         (may or may not exist before types are written)
     */
    File getTargetDirectory();

    /**
     * @return The java package used for generated types.
     */
    String getTargetPackage();

}
