/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james.user;

import java.util.ArrayList;
import java.util.List;

import org.apache.james.mailbox.store.transaction.NonTransactionalMapper;
import org.apache.james.mailbox.store.user.SubscriptionMapper;
import org.apache.james.mailbox.store.user.model.Subscription;
import org.apache.james.mailbox.store.user.model.impl.SimpleSubscription;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Folder;
import com.opwvmsg.mxos.interfaces.service.meta.IFolderService;
import com.opwvmsg.mxos.james.pojos.MxosRequestMap;

/**
 * MxosSubscriptionMapper Mapper for Mxos Subscription Implementations.
 *
 * @author mxos-dev
 */
public class MxosSubscriptionMapper extends NonTransactionalMapper implements
        SubscriptionMapper {

    protected IFolderService folderService = null;

    /**
     * Defaut constructor.
     *
     * @param folderService folderService
     */
    public MxosSubscriptionMapper(final IFolderService folderService) {
        this.folderService = folderService;
    }

    @Override
    public synchronized void delete(final Subscription subscription) {
    }

    @Override
    public Subscription findMailboxSubscriptionForUser(final String user,
            final String mailbox) {
        SimpleSubscription simpleSubscription = null;
        List<Folder> list = null;
        if (folderService != null) {
            try {
                MxosRequestMap map = new MxosRequestMap();
                map.add(MailboxProperty.email, user);
                list = folderService.readAll(map.getMultivaluedMap());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (list != null) {
            for (Folder folder : list) {
                if (folder.getFolderName().equalsIgnoreCase(mailbox)) {
                    simpleSubscription = new SimpleSubscription(user, mailbox);
                }
            }
        }
        return simpleSubscription;
    }

    @Override
    public List<Subscription> findSubscriptionsForUser(String user) {
        final List<Subscription> results = new ArrayList<Subscription>();
        List<Folder> list = null;
        if (folderService != null) {
            try {
                MxosRequestMap map = new MxosRequestMap();
                map.add(MailboxProperty.email, user);
                list = folderService.readAll(map.getMultivaluedMap());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (list != null) {
            for (Folder folder : list) {
                Subscription sub = new SimpleSubscription(user,
                        folder.getFolderName());
                results.add(sub);
            }
        }
        return results;
    }

    @Override
    public synchronized void save(final Subscription subscription) {
    }

    @Override
    public void endRequest() {
    }

}
