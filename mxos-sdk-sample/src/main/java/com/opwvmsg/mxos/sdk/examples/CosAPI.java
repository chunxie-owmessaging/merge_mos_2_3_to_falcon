/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sdk.examples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosBaseService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
/**
 * Utility class for COS APIs.
 *
 * @author mxos-dev
 */
public class CosAPI {

    /**
     * Create Cos.
     * @param context mxosContext
     * @param cosId cosId
     * @throws MxOSException on any error
     */
    public static void create(final IMxOSContext context, final String cosId)
            throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.cosId.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.cosId.name()).add(cosId);
        ICosService service = (ICosService) context
                .getService(ServiceEnum.CosService.name());
        service.create(requestMap);
        System.out.println("COS Created...\n");
    }

    /**
     * Delete Cos.
     * @param context mxosContext
     * @param cosId cosId
     * @throws MxOSException on any error
     */
    public static void delete(final IMxOSContext context, final String cosId)
            throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.cosId.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.cosId.name()).add(cosId);
        ICosService service = (ICosService) context
                .getService(ServiceEnum.CosService.name());
        service.delete(requestMap);
        System.out.println("COS Deleted...\n");
    }

    /**
     * Read Cos Base.
     * @param context mxosContext
     * @param cosId cosId
     * @return Cos Base object
     * @throws MxOSException on any error
     */
    public static Base readBase(final IMxOSContext context, final String cosId)
            throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.cosId.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.cosId.name()).add(cosId);
        ICosBaseService service = (ICosBaseService) context
                .getService(ServiceEnum.CosBaseService.name());
        Base obj = service.read(requestMap);

        System.out.println("Cos Base Read...\n");
        return obj;
    }

    /**
     * Update Cos Base.
     * @param context mxosContext
     * @param cosId cosId
     * @param maxNumAliases maxNumAliases
     * @throws MxOSException on any error
     */
    public static void updateBase(final IMxOSContext context,
            final String cosId, final int maxNumAliases) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.cosId.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.cosId.name()).add(cosId);

        requestMap.put(MailboxProperty.maxNumAliases.name(),
                new ArrayList<String>());
        requestMap.get(MailboxProperty.maxNumAliases.name()).add(
                String.valueOf(maxNumAliases));

        ICosBaseService service = (ICosBaseService) context
                .getService(ServiceEnum.CosBaseService.name());
        service.update(requestMap);
        System.out.println("COS Updated...\n");
    }
}
