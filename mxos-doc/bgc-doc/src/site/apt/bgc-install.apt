 ------------------------------------------------------------------------------
 BGC MxOS Installation
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

BGC MxOS Installation

 This document outlines the installation instructions of Mxos for BGC. Mxos itself
 is a web application provided as a WAR file. Concrete products which use MxOS
 API as a component should have its own installation instructions including MxOS
 API. Therefore, this installation instructions are <<internal use only>> for
 those of you who want to setup Mxos API for your testing or development work.

* Requirements

 [[1]] JDK 1.7 or above installed and set JAVA_HOME to it.
 
 [[2]] Installation has to be done as "root" user only.
 
 [[3]] DS (LDAP) Connectivity is required.
 
 [[4]] MSS Connectivity is required.
 
 [[5]] LDAP Attributes required for MxOS

       Access the below link for the procedure to install LDAP Schema:
       
       {{{./bgc-ldap-schema-preparation.html}Ldap Schema Preparation}}  
 
* Installation and Configuration Steps

* Scratch Installation and Configuration Steps

 [[1]] Copy the below rpms to the machine where mxos is being installed:
 
 		mxos-thirdparty-rpm-<version>.rpm
		
 		mxos-rpm-2.0.<version>.rpm	
 		
 [[2]] Create user mxos and group mxos (Not required if already exists):
 
 		a)  Log in to the host as a root user. 
 		
 		b) Create mxos group:
 			groupadd -g 273 mxos
 			
 		c)	Create mxos user:
 			useradd -g mxos -d /var/mxos -s /bin/bash mxos -m 
 
 [[3]] Create home directory for mxos2.0.
 
 	mkdir /var/mxos2/
 
 [[4]] Install mxos-thirdparty-rpm-<version>.rpm, please use the below command 
 		
 		rpm -ivh mxos-thirdparty-rpm-<version>.rpm
 		
 [[5]] Check in the browser by typing http://<hostname>:<port>/
 	   Note: default port is 8081.
 
 	You will get message "MXOS thirdParty is successfully installed". 		
 
 [[6]] Install mxos-rpm-2.0.<version>.rpm, please use the below command
 
 		rpm -ivh mxos-rpm-2.0.<version>.rpm

 [[7]] Configure mxos.properties as per the target environment.
 
 [[8]] Execute the step given in SSL Configuration section. Skip this step if you already have server.key.
  	   
 [[9]] Start MxOS Service (as mxos user)
    	
    	su mxos
    
    	/var/mxos2/scripts/mxos.sh start | stop | status
        
 [[10]] On successfull installation you can see the below dirs under /var/mxos2/.
 
 		/var/mxos2/
 			|--> lib\
       		|--> server\
       		|        |--> ...\
       		|        |--> ...\
       		|--> config\
       		|		 |--> mxos\DbSchema\
       		|		 |--> mxos\DbSchema\*.xml files\
       		|        |--> mxos.properties\
       		|--> scripts\
       		|        |--> mxos.sh\
       		|--> logs
       		
     		
* Debugging and Logs

 [[1]] See /var/mxos2/server/logs for more information on server status.
 
 [[2]] See /var/mxos2/logs/mxos.log for  MxOS application logs.
  
 [[3]] See /var/mxos2/logs/mxos.stats for  MxOS application reports.
  
 [[4]] For deployment in production, please be sure to have all log levels set to ERROR, while testing this can be preferably changed to DEBUG level for better analysis.
 