 ------------------------------------------------------------------------------
 changeAccountStatus1
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Note: Use "custom" as "true" to access Logging APIs, while creating the IMxOSContext.

MAA Logging - changeAccountStatus1 API

 To Update Existing status attributes

* API Description

** Invoking using SDK

 *  void IMailboxBaseService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/logging/mailbox/v2/\{email\}/base

** Mandatory Parameters

 * email=<Subscriber's email address>
 
 * status=<Subscriber status - open / closing / locked / closed / pending / inactive >
 	
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body
 
 ** Error Codes

*** API specific errors

+--
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters."/>
    <error code="MBX_MAA_RESPONSE_ERROR" message="Error in MAA response." />
+--

*** Note:
    The error codes from MxOS1.0 are returned as part of the long message,
    for more information on error code mappings for this API refer MxOS1.0 documentation

*** Sample Response for logging API

+--
{
"code":"MBX_MAA_RESPONSE_ERROR",
"requestParams":"{<request params>}",
"operationType":"<Operation-Type>",
"shortMessage":"Error in MAA response.",
"longMessage":"
    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <API type="Mail" xmlns="http://www.belgacom.be/ordina/schema/MAA_Response">
        <INFO function="function" server="ldap.hosts" mode="test"/>
            <RESULT code="<error-code>">[Error from MxOS1.0]</RESULT>
    </API>"
}
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}
