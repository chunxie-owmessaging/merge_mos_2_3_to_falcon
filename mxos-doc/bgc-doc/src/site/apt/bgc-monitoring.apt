 ------------------------------------------------------------------------------
 BGC JMX Monitoring Manual
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

BGC JMX Monitoring Manual

 MxOS is using standard JMX MBeans for monitoring.

 1. Open JConsole on the same machine on which MXOS is running 
    and connect to JMX MBeans with the following URL -
  
 2. service:jmx:rmi:///jndi/rmi://localhost:9999/jmxrmi
 
 3. Under MBeans tab, following MxOS beans will be accessible -
    com.opwvmsg.mxos.monitoring
        BrokenConnections
        
    com.opwvmsg.mxos.pooling
        ConnectionPools
        
    com.opwvmsg.mxos.process
        NotificationBroadcaster
        
    com.opwvmsg.mxos.requests
        RequestProcessors
 
 4. Notification broadcaster MBean is a notification MBean and sends out start/stop notifications.
    All other MBeans attributes can be viewed through JConsole.
 
 5. List of all the attributes and MBeans for monitoring purposes is below -

*------------------------------+--------------------------------------+-----------------------------------------+ 
| MBean Name                   | JMX Attributes                       | Operations                              |
*------------------------------+--------------------------------------+-----------------------------------------+ 
| Java.lang, Threading         | ThreadCount                          |                                         |
|                              | (Number of total threads running     |                                         |
|                              | inside the Java virtual machine. This|                                         |
|                              | includes the number of threads from  |                                         |
|                              | the thread pool)                     |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | PeakThreadCount                      |                                         |
|                              | (The maximum number of threads       |                                         |
|                              | executed in the JVM)                 |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              |                                      |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
| Catalina, Thread Pool        | currentThreadCount                   |                                         |
|                              | (Current number of threads in the    |                                         |
|                              | tomcat thread pool)                  |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | maxThreadCount                       |                                         |
|                              | (Maximum number of threads in the    |                                         |
|                              | tomcat thread pool)                  |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              |                                      |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
| Java.lang, Memory            | FreePhysicalMemorySize               |                                         |
|                              | (The free physical memory (in        |                                         |
|                              | kilobytes) of the operation system)  |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | TotalPhysicalMemorySize              |                                         |
|                              | (The total physical memory (in       |                                         |
|                              | kilobytes) of the operation system)  |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | FreeSwapSpaceSize                    |                                         |
|                              | (The free swap space size (in        |                                         |
|                              | kilobytes) of the operating system)  |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | TotalSwapSpaceSize                   |                                         |
|                              | (The total swap space size (in       |                                         |
|                              | kilobytes) of the operating system)  |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              |                                      |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
| Java.lang, CPU               | ProcessCpuTime                       |                                         |
|                              | (CPU time used by the process on     |                                         |
|                              | running JVM in nanoseconds)          |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | ProcessCpuLoad                       |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | SystemCpuLoad                        |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | SystemLoadAverage                    |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              |                                      |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
| Java.lang, Operating System  | MaxFileDescriptorCount               |                                         |
|                              | (Maximum number of file handles that |                                         |
|                              | can be taken by the JVM)             |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | OpenFileDescriptorCount              |                                         |
|                              | (Number of all file handles taken by |                                         |
|                              | the Java virtual machine currently.  |                                         |
|                              | This includes all created sockets and|                                         |
|                              | virtual machine resources, too.      |                                         |
|                              | Example notification value:          |                                         |
|                              | (MaxFileDescriptorCount -            |                                         |
|                              | OpenFileDescriptorCount) \< 100.      |                                         |
|                              | Monitor to determine if the number of|                                         |
|                              | open files that can be opened by the |                                         |
|                              | vm is sufficient))                   |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              |                                      |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
| Java.lang, Memory Pool       | Eden Space                           |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | Survivor Space                       |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | Tenured Gen                          |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | Perm Gen                             |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | Memory                               |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              |                                      |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
| Java.lang, Memory            | HeapMemoryUsage                      |                                         |
|                              | (It is a runtime data area from which|                                         |
|                              | the JVM allocates memory for all     |                                         |
|                              | class instances and arrays. The      |                                         |
|                              | garbage collector reclaims heap      |                                         |
|                              | memory)                              |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NonHeapMemoryUsage                   |                                         |
|                              | (It includes a method area shared    |                                         |
|                              | among all threads and memory required|                                         |
|                              | for the internal processing or       |                                         |
|                              | optimization for the JVM. It is      |                                         |
|                              | logically part of the heap but,      |                                         |
|                              | depending on the implementation, a   |                                         |
|                              | Java VM may not garbage collect or   |                                         |
|                              | compact it)                          |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              |                                      |                                         |                          
*------------------------------+--------------------------------------+-----------------------------------------+ 
| MxOS RequestProcessor        | NumActiveRequests                    |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+  
|                              | NumTotalRequests                     |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+  
|                              |                                      |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
| MxOS LDAP ConnectionPools    | NumMaxLDAPConnections                |                                         |
|                              | (Max LDAP connections to the DirServ |                                         |
|                              | for read, update and delete queries) |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumActiveLDAPConnections             |                                         |
|                              | (Active LDAP connections to the      |                                         |
|                              | DirServ for read, update and delete  |                                         |
|                              | queries)                             |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumMaxLDAPSearchConnections          |                                         |
|                              | (Max LDAP connections to the         |                                         |
|                              | DirCacheServ for search queries)     |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumActiveLDAPSearchConnections       |                                         |
|                              | (Active LDAP connections to the      |                                         |
|                              | DirServ for read, update and delete  |                                         |
|                              | queries)                             |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              |                                      |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumMaxMMGConnections                 |                                         |
|                              | (Number of Max MMG connections for   |                                         |
|                              | Password Recovery SMS)               |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumActiveMMGConnections              |                                         |
|                              | (Number of Active MMG connections for|                                         |
|                              | Password Recovery SMS)               |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              |                                      |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumMaxExternalMTAConnections         |                                         |
|                              | (Number of Max SMTP connections to   |                                         |
|                              | the External MTA for sending mails)  |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumActiveExternalMTAConnections      |                                         |
|                              | (Number of Active SMTP connections to|                                         |
|                              | the External MTA for sending mails)  |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              |                                      |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumMaxMAAConnections                 |                                         |
|                              | (Number of Max connections to        |                                         |
|                              | the MAA for logging functionality)   |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumActiveExternalMTAConnections      |                                         |
|                              | (Number of Active MAA connections to |                                         |
|                              | the MAA for logging functionality)   |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              |                                      |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
| MxOS Connection Errors       | NumLDAPConnectionErrors              | resetNumLDAPConnectionErrors()          |
|                              | (LDP_CONNECTION_ERROR or             |                                         |
|                              | LDP_AUTHENTICATION_ERROR)            |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumLDAPSearchConnectionErrors        | resetNumLDAPSearchConnectionErrors()    |
|                              | (LDP_CONNECTION_ERROR or             |                                         |
|                              | LDP_AUTHENTICATION_ERROR)            |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumMSSConnectionErrors               | resetNumMSSConnectionErrors()           |
|                              | (MSS_CONNECTION_ERROR)               |                                         |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumMAAConnectionErrors               | resetNumMAAConnectionErrors()           |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumSDUPConnectionErrors              | resetNumSDUPConnectionErrors()          |                         
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumMMGConnectionErrors               | resetNumMMGConnectionErrors()           |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumVAMPConnectionErrors              | resetNumVAMPConnectionErrors()          |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumFBIConnectionErrors               | resetNumFBIConnectionErrors()           |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumMTAConnectionErrors               | resetNumMTAConnectionError()            |
*------------------------------+--------------------------------------+-----------------------------------------+ 
|                              | NumConfigDBConnectionErrors          | resetConfigDBConnectionErrors()         |
*------------------------------+--------------------------------------+-----------------------------------------+ 

 6. Notification MBean - Provides MBean notifications, whenever MxOS is started/stopped/restarted/not responding.

    For start, stop and kill notifications, pls start the Process Monitor module using following command - 

    $MXOS_HOME/scripts/monitor.sh start
    
    Process monitor uses the following service http://<host>:<port>/mxos/monitor to monitor MxOS service health