 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MessageMetaData service - list message UUIDs and UIDs of messages metadata in particular folder

* API Description

** Invoking using SDK - Returns Map\<String, MetaData\>

 * Map\<String, MetaData\> IMetadataService.listUIDs(
                final Map\<String, List\<String\>\> inputParams) throws MxOSException
 
** Invoking using REST URL - Returns Map of Metadata JSON Objects with with message id as key and uid as value

 * URL - GET http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{folderName\}/messages/metadata/uid/list

** Mandatory Parameters

 * email=<Subscriber's email id>
 
 * folderName=<Folder Name which contains messages to be retrieved>
 
** Optional Parameters
 
 * isAdmin=<Specifies whether the operation is performed by admin user or end user, allowed values are true/false.  �  Only for RME Version 163>
 
 * popDeletedFlag=<Pop deleted flag, allowed values are true/false - Only for RME version 175 and higher>
       false - List messages that are marked as pop deleted.
       true  - Do not list the messages that are marked as pop deleted.

 * mailboxId=<Mailbox Id of the user. It's a mandatory parameter if messageStoreHost is present. Allowed values: Long>

 * messageStoreHost=<Mailbox Message Store host of the user. It's a mandatory parameter if mailboxId is present>

 * realm=<Mailbox mail realm of the user. e.g. "dns://mmsc.east.jsmail.jp"> 

 If mailboxId and messageStoreHost is present, existence of email address will not be validated in LDAP.

** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
{
   	50770988-f306-11e2-bde6-086a36e277d8: { "uid": 1033 },
   	2179750e-f305-11e2-9401-ce0678153951: { "uid": 1030 },
   	59ebab54-f306-11e2-a5ee-d02a11ddf651: { "uid": 1028 },
   	5739fde8-f306-11e2-9155-5baeb2d90f6a: { "uid": 1027 },
   	520c4f1a-f306-11e2-a790-6331dd470085: { "uid": 1024 },
   	548f9a80-f306-11e2-b786-32f5081ce290: { "uid": 1022 },
	...
	...
}
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
    <error code="FLD_INVALID_FOLDERNAME" message="Invalid folderName value." />
    <error code="FLD_NOT_FOUND" message="Given folder NOT found." />    
    <error code="MSG_INVALID_IS_ADMIN" message="Invalid isAdmin value." />
    <error code="MSG_INVALID_POP_DELETE_FLAG" message="Invalid Pop Deleted Flag." />
    <error code="MBX_INVALID_MAILBOXID" message="Invalid mailboxid." />
    <error code="MSG_INVALID_MESSAGE_STORE_HOST" message="Invalid Message Store Host." />    
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />
    <error code="MBX_UNABLE_TO_MSSLINKINFO_GET" message="Unable to perform MssLinkInfo GET operation." />
    <error code="MSG_MISSING_MAILBOXID_OR_MESSAGESTOREHOST_PARAM" message="Unable to perform operation, messageStoreHost and mailboxId both are mandatory parameters if either is present." />
    <error code="MSG_UNABLE_TO_GET_MESSAGE_METADATA_UID_LIST" message="Unable to get the messages meta data uids list." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}
