 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Message service - copy multiple messages API
 
 Copies multiple messages from one folder and its sub folders to another folder.

* API Description

** Invoking using SDK - Returns void

 * IMessageService.copyMulti(final Map<String, List<String>> inputParams) throws MxOSException;

** Invoking using REST URL - Copy multiple messages from source folder to destination folder.

 * URL - PUT http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{folderName\}/messages/copy/\{toFolderName\}
 * Body - messageId={messageId1}&messageId={messageId2}

** Mandatory Parameters

 * email=<Subscriber's email id>
 
 * srcFolderName=<Folder Name which contains the messages to be copied>
 
 * toFolderName=<Folder Name to which the messages to be copied>
 
 * messageId=<One or more MessageIDs which need to be copied>

** Optional Parameters

 * isAdmin=<Specifies whether the operation is performed by admin user or end user, allowed values are true/false  � Only for RME version 163>

 * optionFolderIsHint=<Specifies that the message may not be in specified src folder, allowed values are true/false - Only for RME version 163>

 * optionMultipleOk=<Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false � Only for RME version 163>

 * optionSupressMers=<Specifies to suppress MERS events for this operation, allowed values are true/false � Only for RME version 163>
 
 * mailboxId=<Mailbox Id of the user. Allowed values: Long>
 
 * messageStoreHost=<Mailbox Message Store host of the user>
 
 * realm=<Mailbox mail realm of the user. e.g. "dns://mmsc.east.jsmail.jp">
 
 * disableNotificationFlag=<Disable notification flag, allowed values are true/false - Only for RME version 175 and higher>
    false - Send notification to the IMAP client
    true - Do not send notification to the IMAP client  
 
 Note: Copy multiple messages API does not support flags and uid as request parameters.
 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body.

** Error Codes

*** API specific errors

+--
 * Error code, if email is bad formatted or Invalid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." /> 
 * Error code, if any required parameter is missing.
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />
 * Error code, if folderName is Invalid.   
    <error code="FLD_INVALID_SRC_FOLDERNAME" message="Invalid source folderName value." />
 * Error code, if folderName is Invalid.   
    <error code="FLD_INVALID_DEST_FOLDERNAME" message="Invalid destination folderName value." />
 * Error code, if messageId is Invalid.
    <error code="MSG_INVALID_MESSAGE_ID" message="Invalid messageId value." />
 * Error code, if uid is already exists in given folder.
	<error code="MSG_UID_ALREADY_EXISTS" message="Given message UID already exists in given folder." />
 * Error code, if deleteNotificationFlag is bad formatted or Invalid.
    <error code="MSG_INVALID_DISABLE_NOTIFICATION_FLAG" message="Invalid Disable Notification Flag." />   
 * Error code, when unable to perform move operation.   
    <error code="MSG_UNABLE_TO_PERFORM_COPY" message="Unable to perform copy message operation." />
 * Error code, when source and destination folders are same.
    <error code="MSG_ERROR_COPY_SOURCE_DEST_CANNOT_BE_SAME" message="Unable to copy messages, source and destination folders cannot be same." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}