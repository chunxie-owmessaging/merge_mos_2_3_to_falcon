 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Mailbox APIs

 Mailbox operations interface which will be exposed to the client. This
 interface is responsible for doing mailbox related operations (like Create,
 Read, Update, Delete, etc.).
 
* Mailbox Macro APIs

 * {{{./mailbox/mailbox-create.html}IMailboxService.create(�)}}
 
    This operation is responsible for creating mailbox. It uses one or more
    actions to do this activity.
    
 * {{{./mailbox/mailbox-read.html}IMailboxService.read(�)}}
 
    This operation is responsible for reading mailbox configured in configuration file

 * {{{./mailbox/mailbox-delete.html}IMailboxService.delete(�)}}
 
    This operation is responsible for deleting mailbox. It uses one or more
    actions to do this activity.
    
 * {{{./mailbox/mailbox-deleteInMss.html}IMailboxService.deleteMss(�)}}
 
    This operation is responsible for deleting mailbox in mss. It uses one or more
    actions to do this activity.   
    
 * {{{./mailbox/mailboxes-read.html}IMailboxService.list(�)}}
 
    This operation is responsible reading multiple mailbox configured in configuration file
 
* Mailbox Micro APIs

** base APIs
 
  Mailbox operations interface which will be exposed to the client. This interface is responsible for doing mailbox related operations (like Create,
  Read, Update, Delete, etc.).
 
 * {{{./mailbox/base/base-read.html}IMailboxBaseService.read(�)}}
 
    This operation is responsible for reading mailbox. It uses one or more actions to do this activity.
 
 * {{{./mailbox/base/base-update.html}IMailboxBaseService.update(�)}}
 
    This operation is responsible for updating mailbox. It uses one or more actions to do this activity.
  
 * {{{./mailbox/base/base-search.html}IMailboxBaseService.search(�)}} 
 
    This operation is responsible for searching mailbox. It uses one or more actions to do this activity.

** base/allowedDomains APIs
 
  Allowed Domains operations interface which will be exposed to the client. This interface is responsible for doing Allowed domain related operations (like Create, Read, Update, Delete, etc.).
 
 * {{{./mailbox/base/allowedDomains/allowedDomains-create.html}IAllowedDomainService.create(�)}}
 
    This operation is responsible for creating allowed domain.
    
 * {{{./mailbox/base/allowedDomains/allowedDomains-read.html}IAllowedDomainService.read(�)}}
 
    This operation is responsible for reading allowed domain.
 
 * {{{./mailbox/base/allowedDomains/allowedDomains-update.html}IAllowedDomainService.update(�)}}
  
    This operation is responsible for updating allowed domain.
    
 * {{{./mailbox/base/allowedDomains/allowedDomains-delete.html}IAllowedDomainService.delete(�)}} 
 
    This operation is responsible for deleting allowed domain.
    
** base/emailAliases APIs

  Email alias operations interface which will be exposed to the client. This
  interface is responsible for doing email alias related operations (like
  Create, Read, Update, Delete, etc.).
 
 * {{{./mailbox/base/emailAliases/emailAliases-create.html}IEmailAliasService.create(�)}}
 
    This operation is responsible for creating email alias.
 
 * {{{./mailbox/base/emailAliases/emailAliases-read.html}IEmailAliasService.read(�)}}
 
    This operation is responsible for reading email alias.
    
 * {{{./mailbox/base/emailAliases/emailAliases-update.html}IEmailAliasService.update(�)}}
  
    This operation is responsible for updating email alias.    
  
 * {{{./mailbox/base/emailAliases/emailAliases-delete.html}IEmailAliasService.delete(�)}} 
 
    This operation is responsible for deleting email alias.
     
** webMailFeatures APIs

  WebMailFeatures interface which will be exposed to the client. This interface is responsible for doing web mail features related operations (like Read, Update, etc.).

 * {{{./mailbox/webMailFeatures/webMailFeatures-read.html}IWebMailFeaturesService.read(�)}}
 
    This operation is responsible for reading web mail features.
 
 * {{{./mailbox/webMailFeatures/webMailFeatures-update.html}IWebMailFeaturesService.update(�)}}
 
    This operation is responsible for updating web mail features.
    
** webMailFeatures/addressBookFeature APIs

  WebMailFeaturesAddressBook interface which will be exposed to the client. This interface is responsible for doing address book features related operations (like Read, Update, etc.).

 * {{{./mailbox/webMailFeatures/addressBook/addressBookFeatures-read.html}IWebMailFeaturesAddressBookService.read(�)}}
 
    This operation is responsible for reading address book features.
 
 * {{{./mailbox/webMailFeatures/addressBook/addressBookFeatures-update.html}IWebMailFeaturesAddressBookService.update(�)}}
 
    This operation is responsible for updating address book features.
     
** credentials APIs

  Credential operations interface which will be exposed to the client. This interface is responsible for doing allowed credential related operations (like Read, Update, etc.).

 * {{{./mailbox/credentials/credentials-read.html}ICredentialsService.read(�)}}

    This operation is responsible for reading credentials. It uses one or more actions to do this activity.
 
 * {{{./mailbox/credentials/credentials-update.html}ICredentialsService.update(�)}}
 
    This operation is responsible for updating credentials. It uses one or more actions to do this activity.

** authenticate APIs

 * {{{./mailbox/authenticate/authenticate.html}ICredentialsService.authenticate(�)}} - Will be deprecated shortly

    This operation is responsible for authenticating client.
    
** authenticateAndGetMailbox APIs

 * {{{./mailbox/authenticate/authenticateAndGetMailbox.html}ICredentialsService.authenticateAndGetMailbox(�)}} 

    This operation is responsible for authenticating client and returning the mailbox.
 
** generalPreferences APIs

  GeneralPreference interface which will be exposed to the client. This interface is responsible for doing general preference related operations (like Read, Update, etc.).

 * {{{./mailbox/generalPreferences/generalPreferences-read.html}IGeneralPreferencesService.read(�)}}
 
    This operation is responsible for reading general preferences.
    
 * {{{./mailbox/generalPreferences/generalPreferences-update.html}IGeneralPreferencesService.update(�)}} 

    This operation is responsible for updating general preferences.
    
** mailSend APIs

  Mail send operations interface which will be exposed to the client. This interface is responsible for doing mail send related operations (like Read,Update, etc.).

 * {{{./mailbox/mailSend/mailSend-read.html}IMailSendService.read(�)}}
 
    This operation is responsible for reading mail send.
 
 * {{{./mailbox/mailSend/mailSend-update.html}IMailSendService.update(�)}}
 
    This operation is responsible for updating mail send.

** mailSend/filters APIs

 * {{{./mailbox/mailSend/filters/filters-read.html}IMailSendFiltersService.read(�)}}
 
** mailSend/filters/bmiFilters APIs

  Mailbox mailSend filter operations interface which will be exposed to the client. This interface is responsible for doing mailSend filter related operations (like Create, Read, Update, Delete, etc.).

 * {{{./mailbox/mailSend/filters/bmiFilters/bmiFilters-read.html}IMailSendBMIFiltersService.read(�)}}
 
    This operation is responsible for reading mailSend filters.

 * {{{./mailbox/mailSend/filters/bmiFilters/bmiFilters-update.html}IMailSendBMIFiltersService.update(�)}}
 
    This operation is responsible for updating mailSend filters.
 
** mailSend/filters/commtouchFilters APIs

  Mailbox mailSend commtouch filter operations interface which will be exposed to the client. This interface is responsible for doing mailSend commtouch filter related operations (like Create, Read, Update, Delete, etc.).

 * {{{./mailbox/mailSend/filters/commtouchFilters/commtouchFilters-read.html}IMailSendCommtouchFiltersService.read(�)}}
 
    This operation is responsible for reading mailSend commtouch filters.

 * {{{./mailbox/mailSend/filters/commtouchFilters/commtouchFilters-update.html}IMailSendCommtouchFiltersService.update(�)}}
 
    This operation is responsible for updating mailSend commtouch filters.
 
** mailSend/filters/mcafeeFilters APIs

  Mailbox mailSend mcAfee filter operations interface which will be exposed to the client. This interface is responsible for doing mailSend mcAfee filter related operations (like Create, Read, Update, Delete, etc.).

 * {{{./mailbox/mailSend/filters/mcAfeeFilters/mcAfeeFilters-read.html}IMailSendMcAfeeFiltersService.read(�)}}

    This operation is responsible for reading mailSend mcAfee filters.

 * {{{./mailbox/mailSend/filters/mcAfeeFilters/mcAfeeFilters-update.html}IMailSendMcAfeeFiltersService.update(�)}}

    This operation is responsible for updating mailSend mcAfee filters.

** mailSend/numDelayedDeliveryMessagesPending APIs

  Delivery messages pending operations interface which will be exposed to the client.
  This interface is responsible for doing delayed delivery messages pending related operations
  (like Create, Read, Update, Delete, etc.).
 
 * {{{./mailbox/mailSend/numDelayedDeliveryMessagesPending/numDelayedDeliveryMessagesPending-create.html}INumDelayedDeliveryMessagesPendingService.create(�)}}
 
    This operation is responsible for creating delayed delivery messages pending.
 
 * {{{./mailbox/mailSend/numDelayedDeliveryMessagesPending/numDelayedDeliveryMessagesPending-read.html}INumDelayedDeliveryMessagesPendingService.read(�)}}
 
    This operation is responsible for reading delayed delivery messages pending.
    
 * {{{./mailbox/mailSend/numDelayedDeliveryMessagesPending/numDelayedDeliveryMessagesPending-update.html}INumDelayedDeliveryMessagesPendingService.update(�)}}
  
    This operation is responsible for updating delayed delivery messages pending.    
  
 * {{{./mailbox/mailSend/numDelayedDeliveryMessagesPending/numDelayedDeliveryMessagesPending-delete.html}INumDelayedDeliveryMessagesPendingService.delete(�)}} 
 
    This operation is responsible for deleting delayed delivery messages pending.

** mailSend/signatures APIs

  Signature operations interface which will be exposed to the client.
  This interface is responsible for doing signature related operations
  (like Create, Read, Update, Delete, etc.).
 
 * {{{./mailbox/mailSend/signature/mailSendSignature-create.html}IMailSendSignatureService.create(�)}}
 
    This operation is responsible for adding a signature.
 
 * {{{./mailbox/mailSend/signature/mailSendSignature-read.html}IMailSendSignatureService.read(�)}}
 
    This operation is responsible for reading the signature.
    
 * {{{./mailbox/mailSend/signature/mailSendSignature-update.html}IMailSendSignatureService.update(�)}}
  
    This operation is responsible for updating the signature.    
  
 * {{{./mailbox/mailSend/signature/mailSendSignature-delete.html}IMailSendSignatureService.delete(�)}} 
 
    This operation is responsible for deleting the signature.
     
** mailReceipt APIs
 
  Mail receipt operations interface which will be exposed to the client. This interface is responsible for doing mail receipt related operations (like Read, Update, etc.).
 
 * {{{./mailbox/mailReceipt/mailReceipt-read.html}IMailReceiptService.read(�)}}
 
    This operation is responsible for reading mail receipt.
 
 * {{{./mailbox/mailReceipt/mailReceipt-update.html}IMailReceiptService.update(�)}} 
 
    This operation is responsible for updating mail receipt.

** mailReceipt/forwardingAddress APIs
 
  Mail forward interface which will be exposed to the client. This interface is responsible for doing mail forward related operations (like Create, Read, Update, Delete, etc.).
 
 * {{{./mailbox/mailReceipt/forwardingAddress/forwarding-create.html}IMailForwardService.create(�)}}
 
    This operation is responsible for creating mail forward address.
 
 * {{{./mailbox/mailReceipt/forwardingAddress/forwarding-read.html}IMailForwardService.read(�)}}
 
    This operation is responsible for reading mail forward address.
 
 * {{{./mailbox/mailReceipt/forwardingAddress/forwarding-update.html}IMailForwardService.update(�)}}
 
    This operation is responsible for updating mail forward address.
  
 * {{{./mailbox/mailReceipt/forwardingAddress/forwarding-delete.html}IMailForwardService.delete(�)}}
 
    This operation is responsible for deleting mail forward address. 
 
** mailReceipt/addressesForLocalDelivery APIs
 
  Address For Delivery interface which will be exposed to the client. This interface is responsible for doing Address For Delivery related operations (like Create, Read, Update, Delete, etc.).
 
 * {{{./mailbox/mailReceipt/addressesForLocalDelivery/addressesForLocalDelivery-create.html}IAddressForDeliveryService.create(�)}}
   
    This operation is responsible for creating Address For Delivery.
   
 * {{{./mailbox/mailReceipt/addressesForLocalDelivery/addressesForLocalDelivery-read.html}IAddressForDeliveryService.read(�)}}
    
    This operation is responsible for reading Address For Delivery. 
    
 * {{{./mailbox/mailReceipt/addressesForLocalDelivery/addressesForLocalDelivery-update.html}IAddressForDeliveryService.update(�)}}
    
    This operation is responsible for updating Address For Delivery.
     
 * {{{./mailbox/mailReceipt/addressesForLocalDelivery/addressesForLocalDelivery-delete.html}IAddressForDeliveryService.delete(�)}} 
 
    This operation is responsible for deleting Address For Delivery.
 
** mailReceipt/adminControl APIs
 
  Admin Control operations interface which will be exposed to the client. This interface is responsible for doing Admin Control related operations (like Read, Update, etc.).
 
 * {{{./mailbox/mailReceipt/adminControl/adminControl-read.html}IAdminControlService.read(�)}}
 
    This operation is responsible for reading Admin Control.
 
 * {{{./mailbox/mailReceipt/adminControl/adminControl-update.html}IAdminControlService.update(�)}}
  
    This operation is responsible for updating Admin Control.

** mailReceipt/adminControl/adminApprovedSendersList APIs
 
  Admin Approved Senders List Service operations interface which will be exposed to the client. 
  This interface is responsible for Admin Approved Senders List related operations (like Create, 
  Read, Update, Delete, etc.). 
 
 * {{{./mailbox/mailReceipt/adminControl/adminApprovedSendersList/adminApprovedSendersList-create.html}
   IAdminApprovedSendersListService.create(�)}}
 
    This operation is responsible for creating Admin Approved Senders List.
 
 * {{{./mailbox/mailReceipt/adminControl/adminApprovedSendersList/adminApprovedSendersList-read.html}
   IAdminApprovedSendersListService.read(�)}}
 
    This operation is responsible for reading Admin Approved Senders List.
 
 * {{{./mailbox/mailReceipt/adminControl/adminApprovedSendersList/adminApprovedSendersList-update.html}
   IAdminApprovedSendersListService.update(�)}}
 
    This operation is responsible for updating Admin Approved Senders List.
 
 * {{{./mailbox/mailReceipt/adminControl/adminApprovedSendersList/adminApprovedSendersList-delete.html}
   IAdminApprovedSendersListService.delete(�)}}
  
    This operation is responsible for deleting Admin Approved Senders List.
  
** mailReceipt/adminControl/adminBlockedSendersList APIs
 
  Admin Blocked Senders List Service operations interface which will be exposed to the client. 
  This interface is responsible for Admin Blocked Senders List related operations (like Create, 
  Read, Update, Delete, etc.). 
 
 * {{{./mailbox/mailReceipt/adminControl/adminBlockedSendersList/adminBlockedSendersList-create.html}
   IAdminBlockedSendersListService.create(�)}}
 
    This operation is responsible for creating Admin Blocked Senders List.
 
 * {{{./mailbox/mailReceipt/adminControl/adminBlockedSendersList/adminBlockedSendersList-read.html}
   IAdminBlockedSendersListService.read(�)}}
 
    This operation is responsible for reading Admin Blocked Senders List.
 
 * {{{./mailbox/mailReceipt/adminControl/adminBlockedSendersList/adminBlockedSendersList-update.html}
   IAdminBlockedSendersListService.update(�)}}
 
    This operation is responsible for updating Admin Blocked Senders List.
 
 * {{{./mailbox/mailReceipt/adminControl/adminBlockedSendersList/adminBlockedSendersList-delete.html}
   IAdminBlockedSendersListService.delete(�)}}
  
    This operation is responsible for deleting Admin Blocked Senders List.

** mailReceipt/senderBlocking APIs
 
  Sender Blocking operations interface which will be exposed to the client. This interface is responsible for doing Sender Blocking related operations (like Read, Update, etc.).
 
 * {{{./mailbox/mailReceipt/senderBlocking/senderBlocking-read.html}ISenderBlockingService.read(�)}}
 
    This operation is responsible for reading Sender Blocking.
 
 * {{{./mailbox/mailReceipt/senderBlocking/senderBlocking-update.html}ISenderBlockingService.update(�)}}
  
    This operation is responsible for updating Sender Blocking.
  
** mailReceipt/senderBlocking/allowedSendersList APIs
 
  Allowed Senders Service operations interface which will be exposed to the client. This interface is responsible for Allowed Sender related operations (like Create, Read, Update, Delete, etc.). 
 
 * {{{./mailbox/mailReceipt/senderBlocking/allowedSendersList/allowedSendersList-create.html}IAllowedSendersListService.create(�)}}
 
    This operation is responsible for creating Allowed Sender.
 
 * {{{./mailbox/mailReceipt/senderBlocking/allowedSendersList/allowedSendersList-read.html}IAllowedSendersListService.read(�)}}
 
    This operation is responsible for reading Allowed Sender.
 
 * {{{./mailbox/mailReceipt/senderBlocking/allowedSendersList/allowedSendersList-update.html}IAllowedSendersListService.update(�)}}
 
    This operation is responsible for updating Allowed Sender.
 
 * {{{./mailbox/mailReceipt/senderBlocking/allowedSendersList/allowedSendersList-delete.html}IAllowedSendersListService.delete(�)}}
  
    This operation is responsible for deleting Allowed Sender.
  
** mailReceipt/senderBlocking/blockedSendersList APIs
 
  Blocked Senders interface which will be exposed to the client. This interface is responsible for doing Blocked Senders related operations (like Create, Read, Update, Delete, etc.). 
 
 * {{{./mailbox/mailReceipt/senderBlocking/blockedSendersList/blockedSendersList-create.html}IBlockedSendersListService.create(�)}}
 
    This operation is responsible for creating Blocked Senders.
 
 * {{{./mailbox/mailReceipt/senderBlocking/blockedSendersList/blockedSendersList-read.html}IBlockedSendersListService.read(�)}}

    This operation is responsible for reading Blocked Senders.

 * {{{./mailbox/mailReceipt/senderBlocking/blockedSendersList/blockedSendersList-update.html}IBlockedSendersListService.update(�)}}
 
    This operation is responsible for updating Blocked Senders.
 
 * {{{./mailbox/mailReceipt/senderBlocking/blockedSendersList/blockedSendersList-delete.html}IBlockedSendersListService.delete(�)}}

    This operation is responsible for deleting Blocked Senders.

** mailReceipt/filters APIs
 
  Mail Receipt filter operations interface which will be exposed to the client. This interface is responsible for doing mailReceipt filter related operations (like Create, Read, Update, Delete, etc.).
 
 * {{{./mailbox/mailReceipt/filters/filters-read.html}IMailReceiptFiltersService.read(�)}}
  
    This operation is responsible for reading mailReceipt filters.
  
** mailReceipt/filters/sieveFilters APIs

  Mailbox mailReceipt sieve filter operations interface which will be exposed to the client. This interface is responsible for doing mailReceipt sieve filter related operations (like Create, Read, Update, Delete, etc.).
 
 * {{{./mailbox/mailReceipt/filters/sieveFilters/sieveFilters-read.html}IMailReceiptSieveFiltersService.read(�)}}

    This operation is responsible for reading mailReceipt sieve filters.

 * {{{./mailbox/mailReceipt/filters/sieveFilters/sieveFilters-update.html}IMailReceiptSieveFiltersService.update(�)}}

    This operation is responsible for updating mailReceipt sieve filters.
  
** mailReceipt/filters/sieveFilters/blockedSenders APIs
 
  Blocked Senders interface which will be exposed to the client. This interface is responsible for doing Blocked Senders related operations (like Create, Read, Update, Delete, etc.).
 
 * {{{./mailbox/mailReceipt/filters/sieveFilters/sieveBlockedSenders/sieveBlockedSenders-create.html}ISieveBlockedSendersService.create(�)}}

    This operation is responsible for creating Blocked Senders.

 * {{{./mailbox/mailReceipt/filters/sieveFilters/sieveBlockedSenders/sieveBlockedSenders-read.html}ISieveBlockedSendersService.read(�)}}

    This operation is responsible for reading Blocked Senders.

 * {{{./mailbox/mailReceipt/filters/sieveFilters/sieveBlockedSenders/sieveBlockedSenders-update.html}ISieveBlockedSendersService.update(�)}}

    This operation is responsible for updating Blocked Senders.

 * {{{./mailbox/mailReceipt/filters/sieveFilters/sieveBlockedSenders/sieveBlockedSenders-delete.html}ISieveBlockedSendersService.delete(�)}}

    This operation is responsible for deleting Blocked Senders.

** mailReceipt/filters/bmiFilters APIs

  Mailbox MailReceipt BMI filter operations interface which will be exposed to the client. This interface is responsible for doing mailReceipt BMI filter related operations (like Read, Update etc.).
 
 * {{{./mailbox/mailReceipt/filters/bmiFilters/bmiFilters-read.html}IMailReceiptBMIFiltersService.read(�)}}

    This operation is responsible for reading mailReceipt BMI filters.

 * {{{./mailbox/mailReceipt/filters/bmiFilters/bmiFilters-update.html}IMailReceiptBMIFiltersService.update(�)}}

    This operation is responsible for updating mailReceipt BMI filters.

** mailReceipt/filters/commtouchFilters APIs

  Mailbox mailReceipt commtouch filter operations interface which will be exposed to the client. This interface is responsible for doing mailReceipt commtouch filter related operations (like Create, Read, Update, Delete, etc.).
 
 * {{{./mailbox/mailReceipt/filters/commtouchFilters/commtouchFilters-read.html}IMailReceiptCommtouchFiltersService.read(�)}}

    This operation is responsible for reading mailReceipt commtouch filters.

 * {{{./mailbox/mailReceipt/filters/commtouchFilters/commtouchFilters-update.html}IMailReceiptCommtouchFiltersService.update(�)}}

    This operation is responsible for updating mailReceipt commtouch filters.

** mailReceipt/filters/cloudmarkFilters APIs
 
  Mailbox mailReceipt cloudmark filter operations interface which will be exposed to the client. This interface is responsible for doing mailReceipt cloudmark filter related operations (like Read, Update etc.).
 
 * {{{./mailbox/mailReceipt/filters/cloudmarkFilters/cloudmarkFilters-read.html}IMailReceiptCloudmarkFiltersService.read(�)}}

    This operation is responsible for reading mailReceipt cloudmark filters.

 * {{{./mailbox/mailReceipt/filters/cloudmarkFilters/cloudmarkFilters-update.html}IMailReceiptCloudmarkFiltersService.update(�)}}

    This operation is responsible for updating mailReceipt cloudmark filters.

** mailReceipt/filters/mcafeeFilters APIs

  Mailbox mailReceipt macAfee filter operations interface which will be exposed to the client. This interface is responsible for doing mailReceipt macAfee filter related operations (like Create, Read, Update, Delete, etc.).
 
 * {{{./mailbox/mailReceipt/filters/mcAfeeFilters/mcafeeFilters-read.html}IMailReceiptMcAfeeFiltersService.read(�)}}

    This operation is responsible for reading mailReceipt macAfee filters.
    
 * {{{./mailbox/mailReceipt/filters/mcAfeeFilters/mcafeeFilters-update.html}IMailReceiptMcAfeeFiltersService.update(�)}}

    This operation is responsible for updating mailReceipt macAfee filters.

** mailAccess APIs
 
  Mail access operations interface which will be exposed to the client. This interface is responsible for doing mail access related operations (like Read, Update, etc.)
 
 * {{{./mailbox/mailAccess/mailAccess-read.html}IMailAccessService.read(�)}}

    This operation is responsible for reading mail access.

 * {{{./mailbox/mailAccess/mailAccess-update.html}IMailAccessService.update(�)}}

    This operation is responsible for updating mail access.
 
** mailAccess/allowedIPs APIs

  MailAccess Allowed Ip interface which will be exposed to the client. This interface is responsible for doing MailAccess Allowed Ip related operations (like Create, Read, Update, Delete, etc.).
 
 * {{{./mailbox/mailAccess/allowedIPs/allowedIPs-create.html}IMailAccessAllowedIpService.create(�)}}

    This operation is responsible for creating MailAccess Allowed Ip. 
 
 * {{{./mailbox/mailAccess/allowedIPs/allowedIPs-read.html}IMailAccessAllowedIpService.read(�)}}

    This operation is responsible for reading MailAccess Allowed Ip. 

 * {{{./mailbox/mailAccess/allowedIPs/allowedIPs-update.html}IMailAccessAllowedIpService.update(�)}}

    This operation is responsible for updating MailAccess Allowed Ip.
 
 * {{{./mailbox/mailAccess/allowedIPs/allowedIPs-delete.html}IMailAccessAllowedIpService.delete(�)}}

    This operation is responsible for deleting MailAccess Allowed Ip.

** mailStore APIs

  Mail store operations interface which will be exposed to the client. This interface is responsible for doing mail store related operations (like Read, Update, etc.).

 * {{{./mailbox/mailStore/mailStore-read.html}IMailStoreService.read(�)}}
 
    This operation is responsible for reading mail store. It uses one or more actions to do this activity.
    
 * {{{./mailbox/mailStore/mailStore-update.html}IMailStoreService.update(�)}}
 
    This operation is responsible for updating mail store. It uses one or more actions to do this activity.
 
** mailStore/groupAdminAllocations APIs

  Group Admin Allocations operations interface which will be exposed to the client. This interface is responsible for doing GroupAdminAllocations related operations (like Read, Update, etc.).

 * {{{./mailbox/mailStore/groupAdminAllocations/groupAdminAllocations-read.html}IGroupAdminAllocationsService.read(�)}}
 
    This operation is responsible for reading Group Admin Allocations.

 * {{{./mailbox/mailStore/groupAdminAllocations/groupAdminAllocations-update.html}IGroupAdminAllocationsService.update(�)}}
 
    This operation is responsible for updating Group Admin Allocations.

** mailStore/externalStore APIs

  External store operations interface which will be exposed to the client. This interface is responsible for doing External store related operations (like Read, Update, etc.).

 * {{{./mailbox/mailStore/externalStore/externalStore-read.html}IExternalStoreService.read(�)}}
 
    This operation is responsible for reading External mail store.

 * {{{./mailbox/mailStore/externalStore/externalStore-update.html}IExternalStoreService.update(�)}}
 
    This operation is responsible for updating External mail store.

** socialNetworks APIs

  SocialNetwork interface which will be exposed to the client. This interface is responsible for doing Social Network related operations (like Read, Update, etc.).

 * {{{./mailbox/socialNetworks/socialNetworks-read.html}ISocialNetworkService.read(�)}}
 
    This operation is responsible for reading Social Network.

 * {{{./mailbox/socialNetworks/socialNetworks-update.html}ISocialNetworkService.update(�)}}
 
    This operation is responsible for updating Social Network.

** socialNetworks/socialNetwrorkSites APIs

  Social Network Site interface which will be exposed to the client. This interface is responsible for doing Social Network Site related operations (like Create, Read, Update, Delete, etc.).

 * {{{./mailbox/socialNetworks/socialNetwrorkSites/socialNetworkSites-create.html}ISocialNetworkSiteService.create(�)}}
 
    This operation is responsible for creating Social Network Site.

 * {{{./mailbox/socialNetworks/socialNetwrorkSites/socialNetworkSites-read.html}ISocialNetworkSiteService.read(�)}}
 
    This operation is responsible for reading Social Network Site.

 * {{{./mailbox/socialNetworks/socialNetwrorkSites/socialNetworkSites-update.html}ISocialNetworkSiteService.update(�)}}
 
    This operation is responsible for updating Social Network Site.

 * {{{./mailbox/socialNetworks/socialNetwrorkSites/socialNetworkSites-delete.html}ISocialNetworkSiteService.delete(�)}}
 
    This operation is responsible for deleting Social Network Site.
 
** externalAccounts APIs

  Mailbox external account operations interface which will be exposed to the client. This interface is responsible for doing mailbox external account related operations (like Read, Update etc.).

 * {{{./mailbox/externalAccounts/externalAccounts-read.html}IExternalAccountsService.read(�)}}
 
    This operation is responsible for reading external account. 

 * {{{./mailbox/externalAccounts/externalAccounts-update.html}IExternalAccountsService.update(�)}}
 
    This operation is responsible for updating external account. 

** externalAccounts/mailAccounts APIs

  External MailAccount interface which will be exposed to the client. This interface is responsible for doing External MailAccount related operations (like Create, Read, Update, Delete, etc.).

 * {{{./mailbox/externalAccounts/mailAccounts/mailAccounts-create.html}IExternalMailAccountService.create(�)}}
 
    This operation is responsible for creating External MailAccount. 

 * {{{./mailbox/externalAccounts/mailAccounts/mailAccounts-read.html}IExternalMailAccountService.read(�)}}
 
    This operation is responsible for reading External MailAccount.

 * {{{./mailbox/externalAccounts/mailAccounts/mailAccounts-update.html}IExternalMailAccountService.update(�)}}
 
    This operation is responsible for updating External MailAccount.. 

 * {{{./mailbox/externalAccounts/mailAccounts/mailAccounts-delete.html}IExternalMailAccountService.delete(�)}}
 
    This operation is responsible for deleting External MailAccount.

** smsServices APIs

  Mailbox SMS Services operations interface which will be exposed to the client. This interface is responsible for doing mailbox SMS Service related operations (like Read, Update etc.).

 * {{{./mailbox/smsServices/smsServices-read.html}ISmsServicesService.read(�)}}
 
    This operation is responsible for reading SMS Services.
    
 * {{{./mailbox/smsServices/smsServices-update.html}ISmsServicesService.update(�)}}
 
    This operation is responsible for reading SMS Services.

** smsServices/smsOnline APIs

  Mailbox SMS Online Service operations interface which will be exposed to the client. This interface is responsible for doing mailbox SMS Online Service related operations (like Read, Update etc.).

 * {{{./mailbox/smsServices/smsOnline/smsOnline-read.html}ISmsOnlineService.read(�)}}
 
    This operation is responsible for reading SMS Online Service.

 * {{{./mailbox/smsServices/smsOnline/smsOnline-update.html}ISmsOnlineService.update(�)}}
 
    This operation is responsible for updating SMS Online Service.

** smsServices/smsNotifications APIs

  Mailbox SMS Notifications Service operations interface which will be exposed to the client. This interface is responsible for doing mailbox SMS Notification Service related operations (like Read, Update etc.).

 * {{{./mailbox/smsServices/smsNotifications/smsNotifications-read.html}ISmsNotificationService.read(�)}}

    This operation is responsible for reading SMS Notifications Service.

 * {{{./mailbox/smsServices/smsNotifications/smsNotifications-update.html}ISmsNotificationService.update(�)}}

    This operation is responsible for updating SMS Notifications Service.

** internalInfo APIs

  Mail internal info operations interface which will be exposed to the client. This interface is responsible for doing mail internal info related operations (like Read, Update, etc.).

 * {{{./mailbox/internalInfo/internalInfo-read.html}IInternalInfoService.read(�)}}

    This operation is responsible for reading mail internal info.

 * {{{./mailbox/internalInfo/internalInfo-update.html}IInternalInfoService.update(�)}}

    This operation is responsible for updating mail internal info.

** internalInfo/messageEventRecords APIs

  MessageEventRecords object of mail internal info operations interface which will be exposed to the client. This interface is responsible for doing MessageEventRecords related operations (like Read, Update, etc.).

 * {{{./mailbox/internalInfo/messageEventRecords/messageEventRecords-read.html}IMessageEventRecordsService.read(�)}}

    This operation is responsible for reading message event records.
    
 * {{{./mailbox/internalInfo/messageEventRecords/messageEventRecords-update.html}IMessageEventRecordsService.update(�)}}

    This operation is responsible for updating message event records.

** internalInfo/messageStoreHosts APIs

 * {{{./mailbox/internalInfo/messageStoreHosts/messageStoreHosts-create.html}ImessageStoreHost.create(�)}} TBD

 * {{{./mailbox/internalInfo/messageStoreHosts/messageStoreHosts-read.html}ImessageStoreHost.read(�)}} TBD

 * {{{./mailbox/internalInfo/messageStoreHosts/messageStoreHosts-update.html}ImessageStoreHost.update(�)}} TBD
 
 * {{{./mailbox/internalInfo/messageStoreHosts/messageStoreHosts-delete.html}ImessageStoreHost.delete(�)}} TBD
 
 * NOTE: {{{./mailbox/internalInfo/internalInfo-update.html}IInternalInfoService.update(�)}} can be used to update MessageStoreHosts (input param - messageStoreHost).
 
** mssLinkInfo APIs
 
  MssLinkInfo operations interface which will be exposed to the client. This interface is responsible for reading mssLinkInfo.
 
 * {{{./mailbox/msslinkinfo/msslinkinfo-read.html}IMssLinkInfoService.read(�)}}
 
    This operation is responsible for reading mssLinkInfo. It uses one action to do this activity.
