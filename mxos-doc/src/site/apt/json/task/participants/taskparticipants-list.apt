 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Task Participant - List API
 
 To Read a Task Participant object of a Task

* API Description

 Read Task Participant API will return entire task Participant data

** Invoking using SDK - Returns List of Participant objects

 *  List<Participant> ITasksParticipantService.list(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - GET http://mxosHost:mxosPort/mxos/task/v2/\{userId\}/tasks/\{taskId\}/participants/list

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>

 * taskId=<ID of the Task>
 
** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>  
 
** REST URL Response

 * Success Response - HTTP 200 with List of Participant objects as body

+--
[{		
	participantId: 123,
	participantType: "user",
	participantName: "Foo",
	participantEmail: "foo@bar.com",
	participantStatus: "accepted",
	participantMessage: "accepted", 
	participantHref: "somelink"
},
{		
	participantId: 1234,
	participantType: "user1",
	participantName: "Foo1",
	participantEmail: "foo1@bar.com",
	participantStatus: "accepted",
	participantMessage: "accepted", 
	participantHref: "somelink1"
}]
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="TSK_INVALID_USERNAME" message="Invalid username." />
    <error code="TSK_INVALID_SESSION" message="Invalid session." />
    <error code="TSK_INVALID_COOKIE" message="Invalid cookie." />
    <error code="TSK_INVALID_TASKID" message="Invalid Task Id." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 