 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

COS MailReceipt SieveFilters - update API
 
 To update COS MailReceipt SieveFilters attributes

* API Description

** Invoking using SDK

 *  void update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST https://mxosHost:mxosPort/mxos/cos/v2/\{cosId\}/mailReceipt/filters/sieveFilters

** Mandatory Parameters

 * cosId=<Distinguished name of the class of service>
 
** Optional Parameters

 * sieveFilteringEnabled=<Sieve filtering is enabled or disabled for the subscriber - yes|no>
 
 * blockedSenderAction=<Blocked sender action - bounce|reject|sideline|toss|none>
 
 * blockedSenderMessage=<Message associated with the denied sender action>
 
 * rejectBouncedMessage=<Reject bounced message enabled or disabled - yes|no>
  
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_INVALID_SIEVE_FILTERING_ENABLED" message="Invalid sieve filtering enabled." />
    <error code="MBX_UNABLE_TO_SET_SIEVE_FILTERING_ENABLED" message="Unable to set sieveFilteringEnabled." />
    <error code="MBX_INVALID_SIEVE_FILTERS_BLOCKED_SENDER_ACTION" message="Invalid sieve filters blocked sender action." />
    <error code="MBX_UNABLE_TO_SET_SIEVE_FILTERS_BLOCKED_SENDER_ACTION" message="Unable to set blockedSenderAction." />
    <error code="MBX_INVALID_SIEVE_FILTERS_BLOCKED_SENDER_MESSAGE" message="Invalid sieve filters blocked sender message." />
    <error code="MBX_UNABLE_TO_SET_SIEVE_FILTERS_BLOCKED_SENDER_MESSAGE" message="Unable to set blockedSenderMessage." />
    <error code="MBX_INVALID_SIEVE_FILTERS_REJECT_BOUNCED_MESSAGE" message="Invalid sieve filters reject bounced message." />
    <error code="MBX_UNABLE_TO_SET_SIEVE_FILTERS_REJECT_BOUNCED_MESSAGE" message="Unable to set rejectBouncedMessage." />
    
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 