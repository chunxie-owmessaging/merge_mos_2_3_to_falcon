 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Contacts - list API
 
 To list all Contacts

* API Description

 API to list Contact

** Invoking using REST Context in SDK - Return ContactId

 *  List<Contact> IContactsService.list(final Map<String, List<String>> inputParams) throws MxOSException;
  
** Invoking using REST URL

 To list contact 

 * URL - GET http://mxosHost:mxosPort/mxos/addressBook/v2/\{userId\}/contacts/list

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>
  
** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>  
 
** Optional Parameters

 * query=\<query for searching based on attribute criteria\>\
   Supported keys: schema fields\
   Example: query= firstName==John && lastName!= Does || displayName ~ John && displayName !~ Does && middleName ^= ab && birthDate>1980-06-10T00:00:00Z && anniversaryDate $= cd
   
 * sortKey=\<attribute to sort on\>\
   Supported keys: all the addressBook schema fields of contacts like displayName,firstName,customFields,companyName etc (Refer to the JSON response below)\
   Example: sortKey=lastName
   
 * sortOrder=\<order of sorting\>\
   Supported values: ascending or descending\
   Example: sortOrder=ascending

** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body.

+--
{
 - "contacts":{
   - "base": {
		"contactId": "1",
		"created": "2012-11-07T05:16:06Z",
		"updated": "2012-12-19T14:30:00Z",
		"folderId": "22",
		"isPrivate": "no",
		"colorLabel": 0,
		"categories": "",
		"notes": "38"
		"customFields": "pager=123456,yomiFirstName=yomiFN,yomiLastName=yomiLN,yomiCompany=yomiC,email3=email3@test.com,fileName=fileN,otherPhone=12345,uid=uid1"
		}
   - "name":{
		"displayName": "testName1",
		"firstName": "fn123459",
		"middleName": "10",
		"lastName": "a1",
		"prefix": "",
		"suffix": "11",
		"nickName": "23"
		}
   - "workInfo":{
		"companyName": "5",
		"department": "16",
		"title": "",
		"manager": "",
		"assistant": "64",
		"assistantPhone": "14",
		"employeeId": "",
		"webPage": "15",
		- "address": {
			"street": "Street1",
			"city": "City1",
			"stateOrProvince": "State1",
			"postalCode": "12",
			"country": "Countr1"
			},
		- "communication":{
			"phone1": "123",
			"phone2": "123",
			"mobile": "Y3",
			"fax": "123",
			"email": "Y5@gh.com",
			"imAddress": ""
			"voip": "sip://test.com"
			}
		}
   - "personalInfo":
  		{
 		"maritalStatus": "Married",
 		"spouseName": "XYZ",
 		"homeWebPage": "15",
 		"children": "child1, child2",
 		- "address": 
 			{
   			"street": "Street1",
   			"city": "City1",
   			"stateOrProvince": "State1",
   			"postalCode": "12",
   			"country": "Countr1"
 			},
 		- "communication": 
 			{
   			"phone1": "123",
   			"phone2": "123",
   			"voip": "28",
   			"mobile": "123",
   			"fax": "123",
   			"email": "29@29.com",
   			"imAddress": "dfghdklfg"
 			},
 		- "events": 
 			{
     		"type": "Birthday",
     		"date": "1980-06-11T00:00:00Z"
    		},
    		{
			"type": "Anniversary",
			"date": "1990-06-11T00:00:00Z"
		}
    - "image":
		{
		"imageUrl": http://localhots:8080/mxos/addressBook/v2/userId/contacts/1/actualImage 
		}
    }  
}

Note: customFields key's value will be in UTF-8 url encoded format. Example: "customFields": "telephone_pager=UTF-8 url encoded string,yomiFirstName=UTF-8 url encoded string"
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="ABS_INVALID_USERNAME" message="Invalid username." />
    <error code="ABS_INVALID_SESSION" message="Invalid session." />
    <error code="ABS_INVALID_COOKIE" message="Invalid cookie." />
	<error code="ABS_OX_ERROR" message="Open-Xchange error." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 