 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailReceipt forwardingAddress - update API
 
 To update MailReceipt forwardingAddress attributes

* API Description

** Invoking using SDK

 *  void update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST https://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/mailReceipt/forwardingAddress/\{oldForwardingAddress\}/\{newForwardingAddress\}

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

 * oldForwardingAddress=<Subscriber's old forwarding address to be replaced>

 * newForwardingAddress=<Subscriber's new forwarding address>
 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
    <error code="MBX_INVALID_OLD_FORWARDING_ADDRESS" message="Invalid old forwarding address." />
    <error code="MBX_INVALID_OLD_FORWARDING_ADDRESS_NOT_EXIST" message="Old forwarding address does not exist." />
    <error code="MBX_INVALID_NEW_FORWARDING_ADDRESS" message="Invalid new forwarding address." />
    <error code="MBX_UNABLE_TO_FORWARD_ADDRESS_GET" message="Unable to perform MailForward GET operation." />
    <error code="MBX_UNABLE_TO_GET_GROUP_MALIBOX_DN" message="Unable to read group mailbox dn." />
    <error code="MBX_FORWARD_ADDRESSES_AND_EMAIL_SAME" message="Forwarding address cannot be same as email." />
    <error code="MBX_UNABLE_TO_FORWARD_ADDRESS_POST" message="Unable to perform MailForward POST operation." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 