 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Mailbox External MailAccount - update API
 
 To Update MailAccount attributes

* API Description

** Invoking using SDK - Returns void

 *  void IExternalMailAccountService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\/externalAccounts/mailAccounts/\{accountId\}}

** Mandatory Parameters

 * email=<Subscriber's email address >
 
 * accountId=<AccountId of the mailAccount, allowed Value - String>

** Optional Parameters

 * 	accountName=< Specifies the text description of the account associated with the external server, allowed Value - String>
 * 	accountType=< Specifies the type of external mail server, such as POP, POP SSL, or IMAP, allowed Value - String>
 *  serverAddress=< Specifies the name of the external mail server that contains the account, allowed Value - String>
 *  serverPort=< Specifies the port number of the external mail server, allowed Value - Integer>
 *  timeoutSecs=< Specifies the subscriber-preferred amount of time, in seconds, that the application waits for a response from the external mail server, allowed Value - Integer>
 *  emailAddress=< Specifies the SMTP address of the external mail server  account, allowed Value - String>
 *	accountUserName=< Specifies the name of the account on the external mail server, allowed Value - String>
 *	accountPassword=< Specifies the password, in the format identified by the password encryption type, for the external mail server account, allowed Value - String>
 *	displayImage=< Specifies a number corresponding to the subscriber-preferred image that the interface displays to indicate that the message was fetched from a particular external account, allowed Value - String>
 *	fetchMailsEnabled=< Specifies, using a boolean value, whether mail in the remote account is downloaded (true) or viewed (false).(Rich Mail only), allowed Value - String>
 *	autoFetchMails=< Specifies the subscriber-preferred option as to whether (true) or not (false), allowed Value - String>
 *	fetchedMailsFolderName=< Specifies the subscriber-preferred folder in which messages from the external mail server account are placed after being downloaded, allowed Value - String>
 *	leaveEmailsOnServer=< Specifies the subscriber-preferred option of whether messages in the external mail server account are deleted (true) or saved (false) after they are retrieved., allowed Value - String>
 *	fromAddressInReply=< Specifies the subscriber-preferred email address placed in the From message header when replying to messages retrieved from the external mail, allowed Value - String>
 *	mailSendEnabled=< Specifies, using a boolean value, whether (true) or not (false) outgoing mail is enabled from this account.(Rich Mail only), allowed Value - String>
 *	mailSendSMTPAddress=< Specifies the hostname of the external SMTP server.(Rich Mail only), allowed Value - String>
 *	mailSendSMTPPort=< Specifies the port number of external SMTP server.(Rich Mail only), allowed Value - String>
 *	smtpSSLEnabled=< Specifies whether the external SMTP server uses the SSL protocol (SMTPS) or not (SMTP).(Rich Mail only), allowed Value - String>
  
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
 * Error code, if update operation is unsuccessful.
    <error code="MBX_UNABLE_TO_SET_MAILACCOUNTS" message="Unable to set MailAccounts" />
 * Error code, if email is invalid
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
 * Error code, if mailAccount parameter in request is Invalid.    
	<error code="MBX_INVALID_INPUT" message="Invalid input of MailAccount" />
 * Error code, if accountId provided in parameter is Invalid.   
	<error code="MBX_MAILACCOUNT_NOTFOUND" message="The given MailAccount does not exist" />
 
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 