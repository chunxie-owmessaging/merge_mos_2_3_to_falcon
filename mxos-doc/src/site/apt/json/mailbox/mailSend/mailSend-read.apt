 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailSend - read API
 
 To read MailSend attributes

* API Description

** Invoking using SDK

 *  void read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - GET https://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/mailSend

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>
 
** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
    {
        "fromAddress": "bar@foo.com",
        "futureDeliveryEnabled" : "yes",
        "maxFutureDeliveryDaysAllowed" : 0,
        "maxFutureDeliveryMessagesAllowed" : 0,
        "alternateFromAddess": "bar1@foo.com",
        "replyToAddress": "bar@foo.com",
        "useRichTextEditor": "html",
        "includeOriginalMailInReply": "yes",
        "originalMailSeperatorCharacter": ">",
        "autoSaveSentMessages": "yes",
        "addSignatureForNewMails": "yes",
        "addSignatureInReplyType": "beforeOriginalMessage",
        "signature": "Signature 1",
		signatures: 
			{
			signatureId: 23
			signatureName: "sn3"
			addSignatureInReplyType: "beforeOriginalMessage"
			signature: "TEST3"
		}        
		"autoSpellCheckEnabled": "yes",
        "confirmPromptOnDelete": "yes",
        "includeReturnReceiptReq": "yes",
        "maxSendMessageSizeKB": 2048,
        "maxAttachmentSizeKB": 2048,
        "maxAttachmentsInSession": 10,
        "maxAttachmentsToMessage": 5,
        "maxCharactersPerPage": 1024,
        "filters": {
            "bmiFilters": {
                "spamAction": "Add Header",
                "spamMessageIndicator": ""
            },
            "commtouchFilters": {
                "spamAction": "Tag",
                "virusAction": "Tag"
            },
            "mcAfeeFilters": {
                "virusAction": "clean"
            }
        }
    }
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_UNABLE_TO_MAILSEND_GET" message="Unable to perform MailSend GET operation." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 