 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailSend Signature - update API
 
 To update MailSend attributes

* API Description

** Invoking using SDK

 * void update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST https://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/mailSend/signatures/\{signatureId\}

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>
 
 * signatureId=<SignatureId of the signature, allowed Value - String>
 
** Optional Parameters

 * signatureName=< Specifies the name of the signature, allowed Value - String>

 * signature=< Specifies the textual content of the signature, allowed Value - String> 

 * isDefault=< Specifies whether signature is default or not, allowed Value - yes|no>

 * addSignatureInReplyType=< Specifies the subscriber-preferred option to place the personal signature, allowed Value - one of beforeOriginalMessage, afterOriginalMessage>
 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
 * <error code="MBX_INVALID_EMAIL" message="Invalid email format" />

 * <error code="MBX_SIGNATURE_ALREADY_EXISTS" message="Unable to set mail send signature" />

 * <error code="MBX_INVALID_SIGNATURE" message="Invalid signature" />

 * <error code="MBX_INVALID_ADD_SIGNATURE_IN_REPLY_TYPE" message="Invalid add signature in reply" />

 * <error code="MBX_INVALID_SIGNATURE_DEFAULT" message="Invalid signature isDefault flag" />

 * <error code="MBX_INVALID_SIGNATURE_NAME" message="Invalid signature name" />

 * <error code="MBX_INVALID_SIGNATURE_ID" message="Invalid signature Id" />

 * <error code="MBX_INVALID_MAILSEND_SIGNATURE_ID" message="Invalid Account Id of Signature" />

 * <error code="MBX_MAILSEND_SIGNATURE_NOT_FOUND" message="The given Signature does not exist" />

 * <error code="MBX_UNABLE_TO_UPDATE_MAILSEND_SIGNATURE" message="Unable to update Signature" />    
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 