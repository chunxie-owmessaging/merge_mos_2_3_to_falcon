 ------------------------------------------------------------------------------
 Openwave Mxos - Procedure to Modify the LDAP attribute and Schema
 ------------------------------------------------------------------------------

Openwave Messaging Mxos - Modify the LDAP attribute and Schema


 Some of the important notes, as below,

    1. Ignore if MxOS 2.0 LDAP Schema is already installed/upgraded.\
  	2. Add all the Filter related LDAP attributes to LDAP schema (not required if not installed)

        /var/mxos2/config/DbSchema/ldap/rm_ldifs \
        /var/mxos2/config/DbSchema/ldap/bmi_ldifs
		
    3. While executing the below mentioned commands if directory is throwing the error \
       'line 2 of entry: cn=schemape modify' then ldif files need to be converted as Unix files.
	 

Scratch Installation on top of SUR.

 * Copy all the files from below path to Directory Machine.
 
 /var/mxos2/config/DbSchema/ldap/forMx9.0

+--

 * Add additional new LDAP attributes to LDAP schema by following command:

    ldapmodify -D cn=root -w secret -h <hostname> -p <LDAP port> -f new_sprint6_att.ldif

  * Add additonal NetMail LDAP attributes to LDAP schema by following command:

    ldapmodify -D cn=root -w secret -h <hostname> -p <LDAP port> -f leapfrog_attrs_netmail.ldif    

 * Update the objectclass mailuserprefs with additional LDAP attributes. 
   Use leapfrog_withFilterAttrs_object_mailuserprefs.ldif if Filters are installed.

    ldapmodify -D cn=root -w secret -h <hostname> -p <LDAP port> -f leapfrog_withoutFilterAttrs_object_mailuserprefs.ldif
    
    Note: In case of below error during updating of objectclass mailuserprefs perform 
          the steps mentioned in troubleshoot section and run the above command again: 
          "modifying entry cn=schema: return code 16 ldap_modify: No such attribute"
    
 * Update the objectclass netmailuserprefs with additional netmail LDAP attributes

    ldapmodify -D cn=root -w secret -h <hostname> -p <LDAP port> -f opwvnetmail_scheme.ldif  
     
 * Update the objectclass mailuser with additional mailuser LDAP attributes

    ldapmodify -D cn=root -w secret -h <hostname> -p <LDAP port> -f leapfrog_object_mailuser.ldif 
    
* Update the attributetypes mailpasswordtype with additional bcrypt passwrodStoreType

    ldapmodify -D cn=root -w secret -h <hostname> -p <LDAP port> -f leapfrog_attris_mailpasswordtype.ldif 

    Note: In case of below error during updating of objectclass mailuser perform 
          the steps mentioned in troubleshoot section and run the above command again: 
          "modifying entry cn=schema: return code 16 ldap_modify: No such attribute"

+--
    
Troubleshooting step:
 
 * Search, LDAP objectclass "mailuserprefs" using following command:

   ldapsearch -D cn=root -w secret -h \<hostname\> -p \<LDAP port\> -b "cn=schema" -s base "objectclass=*" + | grep �i mailuserprefs

   In case of mismatch of the search result of objectclass, mailuserprefs with data in the line 4 of leapfrog_withoutFilterAttrs_object_mailuserprefs.ldif file:
   
   1. Replace the mailuserprefs objectclass in leapfrog_withoutFilterAttrs_object_mailuserprefs.ldif file (both delete and add part as described in example below) 
      with the mailuserprefs objectclass from LDAP search result \*.\
   2. Add new LDAP attributes mailwebmailchangepassword, mailMaxForwardingAddresses, mailMaxFailedCaptchaLoginAttempts 
      to mailuserprefs objectclass (only in add part as described in example below)\*\*.

       Example:\
        dn: cn=schema\
        changetype: modify\
        delete: objectclasses\
        \<mailuserprefs objectclass from LDAP search\>\
        -\
        add: objectclasses\
        \<mailuserprefs objectclass from LDAP search\> $ mailwebmailchangepassword $ mailMaxForwardingAddresses $ mailMaxFailedCaptchaLoginAttempts
        
 
 * LDAP Search the objectclass mailuser by following command:

   ldapsearch -D cn=root -w secret -h \<hostname\> -p \<LDAP port\> -b "cn=schema" -s base "objectclass=*" + | grep �i mailuser

   In case of mismatch of the search result of objectclass, mailuser with data in the line 4 of leapfrog_withoutFilterAttrs_object_mailuserprefs.ldif file:
   
   1. Replace the mailuser objectclass in leapfrog_object_mailuser.ldif file (both delete and add part as described in example below) 
      with the mailuser objectclass from LDAP search result \*.\
   2. Add new LDAP attribute mailFailedCaptchaLoginAttempts to mailuser objectclass (only in add part as described in example below).
   
       Example:\
        dn: cn=schema\
        changetype: modify\
        delete: objectclasses\
        \<mailuser objectclass from LDAP search\>\
        -\
        add: objectclasses\
        \<mailuser objectclass from LDAP search\> $ mailFailedCaptchaLoginAttempts 
        
        
 * \* Note: Make sure to use "objectclasses:" instead of "objectclasses=" in the ldif files.
 
 * \*\* Note: If required add filter attributes to mailuserprefs objectclass (only in add part).