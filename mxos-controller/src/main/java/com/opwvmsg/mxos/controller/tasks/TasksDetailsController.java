/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.tasks;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksDetailsService;
import com.opwvmsg.mxos.task.pojos.Details;

/**
 * ProgressController class that controls all the requests for tasks.
 * 
 * @author mxos-dev
 */
@Path("/task/v2/{userId}/tasks/{taskId}/details")
public class TasksDetailsController {
    private static Logger logger = Logger
            .getLogger(TasksDetailsController.class);

    /**
     * Retrieve tasks Progress.
     * 
     * @param userId String
     * @param taskId String
     * @param uriInfo UriInfo
     * 
     * @return Response - tasks Progress object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTasksProgress(
            @PathParam("userId") final String userId,
            @PathParam("taskId") final String taskId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Tasks ExternalParticipant Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(TasksProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    TasksProperty.taskId.name(), taskId);
            final Details recurrence = ((ITasksDetailsService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.TasksDetailsService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, recurrence);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update tasks progress.
     * 
     * @param userId String
     * @param taskId String
     * @param inputParams MultivaluedMap<String, String>
     * 
     * @return Response - Empty object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateTasksProgress(
            @PathParam("userId") final String userId,
            @PathParam("taskId") final String taskId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update tasks Progress Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(TasksProperty.userId.name(), userId);
            inputParams.add(TasksProperty.taskId.name(), taskId);
            ((ITasksDetailsService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.TasksDetailsService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
