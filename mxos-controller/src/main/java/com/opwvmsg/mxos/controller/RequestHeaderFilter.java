/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.controller;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxOSConstants;

/**
 * Filter to log X-Application header info.
 * @author Aricent
 *
 */
public class RequestHeaderFilter implements Filter {
    private static Logger logger = Logger
            .getLogger(RequestHeaderFilter.class);

    /**
     * This method is to set the request and responses to specified types.
     *
     * @param req
     *            - ServletRequest
     * @param res
     *            - ServletResponse
     * @param chain
     *            - FilterChain
     * @throws ServletException
     *             - in case any problem while getting PrintWriter
     * @throws IOException
     *             - in case any problem while getting PrintWriter
     *
     */
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException {
        try {
            HttpServletRequest request = (HttpServletRequest) req;
            HttpServletResponse response = (HttpServletResponse) res;
            logger.info(new StringBuilder("Request Received from: ")
                    .append(request.getRemoteHost()).append(", for: ")
                    .append(request.getMethod()).append(" ")
                    .append(request.getPathInfo()).toString());
            request.setCharacterEncoding(MxOSConstants.UTF8);
            String xApp = request.getHeader("X-Application");
            if (xApp != null && logger.isTraceEnabled()) {
                logger.trace("X-Application = " + xApp);
            }
            // pass the request/response on
            chain.doFilter(request, response);
            final Response.Status status = Response.Status
                    .fromStatusCode(response.getStatus());
            final StringBuilder sb = new StringBuilder("Sending Response: ");
            sb.append(response.getStatus());
            if (status != null) {
                sb.append("(").append(status).append(")");
            }
            logger.info(sb.toString());
        } catch (final Exception e) {
            if (!e.getClass().getSimpleName().equals("ClientAbortException")) {
                logger.error("Severe Error: ", e);
            }
        }
    }

    /**
     * This method is to initialize the filter.
     *
     * @param filterConfig
     *            - FilterConfig
     */
    public void init(FilterConfig filterConfig) {
    }

    /**
     * This method is to destroy the filter.
     *
     */
    public void destroy() {
    }
}
