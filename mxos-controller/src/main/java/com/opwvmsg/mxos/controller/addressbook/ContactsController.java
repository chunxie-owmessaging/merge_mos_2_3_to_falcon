/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.addressbook;

import java.io.InputStream;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.addressbook.pojos.Image;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsBaseService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsImageService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsService;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.FormDataParam;

/**
 * contactController class that controls all the requests for contact.
 * 
 * @author mxos-dev
 */
@Path("/addressBook/v2/{userId}/contacts")
public class ContactsController {
    private static Logger logger = Logger.getLogger(ContactsController.class);

    /**
     * Create Contacts.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - contactId object
     * @throws Exception Exception
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createContact(@PathParam("userId") final String userId,
            final MultivaluedMap<String, String> inputParams) throws Exception {

        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create Contacts Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(AddressBookProperty.userId.name(), userId);
            final long cId = ((IContactsService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.ContactsService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, cId);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create multiple Contacts.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - contactId object
     * @throws Exception Exception
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("multipart/form-data")
    public Response createMultipleContacts(
            @PathParam("userId") final String userId,
            @FormDataParam("file") InputStream inputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail,
            @FormDataParam("sessionId") final String sessionId,
            @FormDataParam("cookieString") final String cookieString)
            throws Exception {

        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create multiple Contacts Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            MultivaluedMap<String, String> inputParams = new MultivaluedMapImpl();
            inputParams.add(AddressBookProperty.userId.name(), userId);
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, MxOSConstants.UTF8);
            inputParams.add(AddressBookProperty.contactsList.name(),
                    writer.toString());
            if (sessionId != null && !sessionId.isEmpty()) {
                inputParams.add(AddressBookProperty.sessionId.name(),
                        URLDecoder.decode(sessionId, MxOSConstants.UTF8));
            }
            if (cookieString != null && !cookieString.isEmpty()) {
                inputParams.add(AddressBookProperty.cookieString.name(),
                        URLDecoder.decode(cookieString, MxOSConstants.UTF8));
            }
            final List<Long> cIds = ((IContactsService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.ContactsService.name()))
                    .createMultiple(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, cIds);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Move multiple Contacts.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - contactId object
     * @throws Exception Exception
     */
    @POST
    @Path("/move")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("multipart/form-data")
    public Response moveMultipleContacts(
            @PathParam("userId") final String userId,
            @FormDataParam("sessionId") String sessionId,
            @FormDataParam("cookieString") String cookieString,
            @FormDataParam("folderId") String folderId,
            @FormDataParam("moveToFolderId") String moveToFolderId,
            @FormDataParam("file") InputStream inputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail)
            throws Exception {

        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Move multiple Contacts Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            MultivaluedMap<String, String> inputParams = new MultivaluedMapImpl();
            inputParams.add(AddressBookProperty.userId.name(), userId);
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, MxOSConstants.UTF8);
            inputParams.add(AddressBookProperty.contactsList.name(),
                    writer.toString());
            if (sessionId != null && !sessionId.isEmpty()) {
                inputParams.add(AddressBookProperty.sessionId.name(),
                        URLDecoder.decode(sessionId, MxOSConstants.UTF8));
            }
            if (cookieString != null && !cookieString.isEmpty()) {
                inputParams.add(AddressBookProperty.cookieString.name(),
                        URLDecoder.decode(cookieString, MxOSConstants.UTF8));
            }
            if (folderId != null && !folderId.isEmpty()) {
                inputParams.add(AddressBookProperty.folderId.name(),
                        URLDecoder.decode(folderId, MxOSConstants.UTF8));
            }
            inputParams.add(AddressBookProperty.moveToFolderId.name(),
                    URLDecoder.decode(moveToFolderId, MxOSConstants.UTF8));

            ((IContactsService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.ContactsService.name()))
                    .moveMultiple(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Contacts.
     * 
     * @param userId String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Path("/{contactId}")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response delete(@PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete contact Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.contactId.name(), contactId);
            ((IContactsService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.ContactsService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Retrieve contact.
     * 
     * @param String
     * @param uriInfo UriInfo
     * @return Response - contact Base object
     */
    @GET
    @Path("/{contactId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContact(@PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Contacts Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.contactId.name(), contactId);
            final Contact contact = ((IContactsService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ContactsService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, contact);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve contact Base.
     * 
     * @param String
     * @param uriInfo UriInfo
     * @return Response - contact Base object
     */
    @GET
    @Path("/{contactId}/base")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContactBase(@PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Contacts Base Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.contactId.name(), contactId);
            final ContactBase contactBase = ((IContactsBaseService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ContactsBaseService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, contactBase);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve List of contact objects.
     * 
     * @param uriInfo UriInfo
     * @return Response - List of contact objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public Response listContacts(@PathParam("userId") final String userId,
            @QueryParam("sessionId") final String sessionId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info(new StringBuffer("List contact Request Received"));
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            final List<Contact> contactObjects = ((IContactsService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ContactsService.name()))
                    .list(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, contactObjects);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve List of contact Base objects.
     * 
     * @param uriInfo UriInfo
     * @return Response - List of contact Base objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/base/list")
    public Response listContactBase(@PathParam("userId") final String userId,
            @QueryParam("sessionId") final String sessionId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info(new StringBuffer("List contacts base Request Received"));
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            final List<ContactBase> contactBaseObjects = ((IContactsBaseService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ContactsBaseService.name()))
                    .list(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, contactBaseObjects);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update contact Base.
     * 
     * @param userId String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @POST
    @Path("/{contactId}/base")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateContactBase(@PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update contact Base Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(AddressBookProperty.userId.name(), userId);
            inputParams.add(AddressBookProperty.contactId.name(), contactId);
            ((IContactsBaseService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.ContactsBaseService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve contact Image.
     * 
     * @param String
     * @param uriInfo UriInfo
     * @return Response - contact Base object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{contactId}/image")
    public Response getContactImage(@PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get contacts image request received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.contactId.name(), contactId);
            final Image contactImage = ((IContactsImageService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ContactsImageService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, contactImage);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve contacts actualImage.
     * 
     * @param String
     * @param uriInfo UriInfo
     * @return Response - contact Base object
     */
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Path("/{contactId}/actualImage")
    public Response getContactActualImage(
            @PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get contacts image request received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.contactId.name(), contactId);
            final byte[] contactImage = ((IContactsImageService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ContactsImageService.name()))
                    .readImage(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(
                    MediaType.APPLICATION_OCTET_STREAM, Response.Status.OK,
                    contactImage);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update contacts image.
     * 
     * @param userId String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{contactId}/image")
    public Response updateContactsImage(
            @PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update contacts image Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {

            inputParams.add(AddressBookProperty.userId.name(), userId);
            inputParams.add(AddressBookProperty.contactId.name(), contactId);

            ((IContactsImageService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.ContactsImageService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Searches the contact Base objects.
     * 
     * @param uriInfo UriInfo
     * @return Response - List of contact Base objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/base/search")
    public Response searchContactBase(@PathParam("userId")
    final String userId, @Context
    final UriInfo uriInfo) {
        List<ContactBase> contactBaseObjects = new ArrayList<ContactBase>();
        if (logger.isInfoEnabled()) {
            logger.info(new StringBuffer(
                    "Search contacts base Request Received"));
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    userId);
            contactBaseObjects = ((IContactsBaseService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.ContactsBaseService.name()))
                    .search(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, contactBaseObjects);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
