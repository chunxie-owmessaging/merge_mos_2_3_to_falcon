/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.controller.cos;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosExternalAccountsService;

/**
 * CosExternalAccountsController class that controls all the requests for COS
 * ExternalAccounts objects.
 *
 * @author mxos-dev
 */
@Path("/cos/v2/{cosId}/externalAccounts")
public class CosExternalAccountsController {
    private static Logger logger = Logger
            .getLogger(CosExternalAccountsController.class);

    /**
     * Update Cos ExternalAccount.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, change password is success or failure
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateExternalAccount(
            @PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update Cos ExternalAccount Request Received for cosId(")
                    .append(cosId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ((ICosExternalAccountsService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosExternalAccountService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Cos ExternalAccount object.
     *
     * @param uriInfo UriInfo
     * @return Response - List of Cos Base objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getExternalAccounts(@PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Cos ExternalAccount Request Received for cosId(")
                    .append(cosId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final ExternalAccounts externalAccount =
                    ((ICosExternalAccountsService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.CosExternalAccountService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, externalAccount);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
