/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.MsgFlagChangeInfo;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;

/**
 * This operation will store message flags for given messages. It replies flag
 * update status as response.
 */
public class StoreMsgFlags extends AbstractMssOperation {

    // input
    private String name;
    private UUID folderUUID;
    private MsgFlagChangeInfo[] msgFlagChangeInfo;
    private int options;
    private int accessId;

    // constants
    public static final int SPECIAL_DELETED_FLAG = 0x800;
    public static final int DISABLE_NOTIFICATION = 0x1000;

    // output

    // internal
    private String url;
    private String hostHeader;

    /**
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param folderUUID Folder from which we want list of messages.
     * @param msgFlagChangeInfo List of msgFlagChangeInfo to be changed with
     *            msgUUID
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options - SpecialDeleteFlag 0x800 - Set this if you want to mark
     *            message as specially deleted otherwise it will be processed as
     *            not-specially deleted 
     *            DisableNotification 0x1000 - Set this if you do not want 
     *            notification to be sent otherwise notification will be sent
     * 
     * */
    public StoreMsgFlags(String host, String name, String realm,
            UUID folderUUID, MsgFlagChangeInfo[] msgFlagChangeInfo,
            int options, int accessId) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_SL_STOREMSGFLAGS);
        this.folderUUID = folderUUID;
        this.msgFlagChangeInfo = msgFlagChangeInfo;
        this.options = options;
        this.accessId = accessId;
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (folderUUID == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "folderUUID", "StoreMsgFlags" }));
        }
        if (msgFlagChangeInfo == null || msgFlagChangeInfo.length == 0) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "msgFlagChangeInfo", "StoreMsgFlags" }));
        } else {
            for (int i = 0; i < msgFlagChangeInfo.length; i++) {
                if (msgFlagChangeInfo[i] == null) {
                    throw new IntermailException(LogEvent.ERROR,
                            LogEvent.formatLogString("Ms.BadRequestArg",
                                    new String[] {
                                            "msgFlagChangeInfo[" + i + "]",
                                            "StoreMsgFlags" }));
                }
            }
        }
        if (rmeDataModel != RmeDataModel.Leopard) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_SL_STOREMSGFLAGS", host,
                            "dataModel=" + rmeDataModel }));
        } else {
            callRme(MSS_SL_STOREMSGFLAGS);
        }
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        // Legacy RME operation is not supported
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // Legacy RME operation is not supported
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_STOREMSGFLAGS));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeString(url);
        outStream.writeUUID(folderUUID);
        outStream.writeInt(msgFlagChangeInfo.length);
        if (msgFlagChangeInfo.length > 0) {
            for (int i = 0; i < msgFlagChangeInfo.length; i++) {
                msgFlagChangeInfo[i].write(outStream);
            }
        }
        outStream.writeInt(accessId);
        outStream.writeInt(options);
        outStream.flush();
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    @Override
    protected void receiveHttpData() throws IOException {
        // debugReceive();
        logEvents = inStream.readLogEvent();
    }

}
