/*
 * PatternMapper.java
 *
 * Copyright (c) 2002 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/PatternMapper.java#1 $
 */

package com.opwvmsg.utils.paf.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


/**
 * This class maintains a table of information which is indexed
 * by regular expressions.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public class PatternMapper {
    /**
     * An entry in the PatternMapper.  The entry is presumed to consist
     * of a name and the regular expression describing all values that
     * map to that name.
     */
    public static class Entry {
        /**
         * The name of this entry.  This is the value that
         * the pattern maps to.
         */
        private String name;
    
        /**
         * The text of the regular expression to be matched against.
         */
        private String expression;
    
        /**
         * The compiled regular expression.
         */
        private Pattern pattern;

        /**
         * Create an entry for the pattern map with the specified
         * name and regular expression.
         */
        public Entry(String name, String expression)
            throws PatternSyntaxException {
            this(name, expression, Pattern.COMMENTS);
        }

        public Entry(String name, String expression, int options)
            throws PatternSyntaxException {
            this.name = name;
            this.expression = expression;
            try {
                pattern = Pattern.compile(expression, options);
            } catch (PatternSyntaxException e) {
                throw e;
            }
        }

        /**
         * Get the name for this <code>PatternMapper.Entry</code>.
         *
         * @return a <code>String</code> name
         */
        public String getName() {
            return name;
        }

        /**
         * Get the expression for this <code>PatternMapper.Entry</code>.
         *
         * @return a <code>String</code> expression
         */
        public String getExpression() {
            return expression;
        }

        /**
         * Get the compiled pattern for this <code>PatternMapper.Entry</code>.
         *
         * @return a <code>Pattern</code>
         */
        public Pattern getPattern() {
            return pattern;
        }

        /**
         * Create a matcher for the given string.
         *
         * @param input The <code>String</code> for which to create a matcher.
         * @return matcher for input.
         */
        public Matcher getMatcher(String input) {
            return getPattern().matcher(input);
        }

        /**
         * Determines if a string contains this entry's pattern.
         *
         * @param input The <code>String</code> to test for a match.
         * @return True if the input contains a pattern match, false
         *    otherwise.
         */
        public boolean contains(String input) {
            return getMatcher(input).find();
        }

        public String toString() {
            StringBuffer buf = new StringBuffer(getName());
            buf.append(" <-- ");
            buf.append(getExpression());
            return buf.toString();
        }
    }

    /**
     * Successful matches are cached here for faster lookups.
     */
    private Map cache;

    /**
     * The list of patterns to match against.
     */
    private List patterns;

    /**
     * Original map text.
     */
    private String mapText;

    public PatternMapper() {
        cache = new HashMap();
        patterns = new ArrayList();
    }

    /**
     * Load the map from text containing individual entries 
     * separated by newlines.  Each entry is parsed into
     * an <code>Entry</code> instance by the method
     * <code>createEntryFromText</code>.  Malformed entries
     * (as defined by the implementation of 
     * <code>createEntryFromText</code>) are ignored.
     *
     * @param mapText a <code>String</code> containing the entire map data
     */
    synchronized public void load(String mapText) {
        clear();
        this.mapText = mapText;
        if (mapText != null && mapText.length() > 0) {
            StringTokenizer tokenizer = new StringTokenizer(mapText, "\n");
            while (tokenizer.hasMoreTokens()) {
                String entryText = tokenizer.nextToken();
                try {
                    add(entryText);
                } catch (ParseException e) {
                    // ignore a badly formatted entry
                }
            }
        }
    }

    /**
     * Add a single entry to the map. The entry is parsed into
     * an <code>Entry</code> instance by the method
     * <code>createEntryFromText</code>.
     *
     * @param entryText a <code>String</code> containing the entry data
     *
     * @throws ParseException if the text cannot be properly parsed
     */
    synchronized public void add(String entryText) throws ParseException {
        Entry entry = createEntryFromText(entryText);
        add(entry);
    }

    /**
     * Add a single <code>Entry</code> to the map.
     *
     * @param entry the <code>Entry</code> to add to the map
     */
    protected void add(Entry entry) {
        patterns.add(entry);
    }

    /**
     * Clear all patterns from the map.
     */
    synchronized public void clear() {
        cache.clear();
        patterns.clear();
    }

    /**
     * Create an instance of an <code>Entry</code> based on the given
     * line of text.  The default version of this method assumes the
     * text contains two elements, separated by space or tab characters.
     * The first element is the name to map to, and the second element
     * is the regular expression which maps to it.  Subclasses can override
     * to parse and return their own subclasses of 
     * <code>PatternMapper.Entry</code>.
     *
     * @param text the <code>String</code> containing the entry data
     *
     * @return a <code>PatternMapper.Entry</code>
     *
     * @throws ParseException if the text cannot be properly parsed
     */
    protected Entry createEntryFromText(String text) 
        throws ParseException {

        try {
            StringTokenizer tokenizer = new StringTokenizer(text, " \t");
            String name = tokenizer.nextToken();
            String expression = tokenizer.nextToken();
            return new Entry(name, expression);
        } catch (NoSuchElementException nse) {
            throw new ParseException(nse.getMessage(), 0);
        } catch (PatternSyntaxException mp) {
            throw new ParseException(mp.getMessage(), 0);
        }
    }

    /**
     * Get the <code>PatternMapper.Entry</code> which matches
     * the specified string value. 
     *
     * @param str the <code>String</code> value to look up in the map
     *
     * @return the <code>String</code> matching <code>Entry</code>
     *         or <code>null</code> if there is no match
     */
    synchronized public Entry get(String str) {
        Entry entry = (Entry)cache.get(str);
        if (entry == null) {
            Iterator iterator = patterns.iterator();
            while (iterator.hasNext()) {
                Entry item = (Entry)iterator.next();
                if (item.contains(str)) {
                    entry = item;
                    cache.put(str, entry);
                    break;
                }
            }
        }
        return entry;
    }

    /**
     * Determine if a specified string value matches any entries in the map.
     *
     * @param str the <code>String</code> value to look up in the map
     *
     * @return <code>true</code> if a matching <code>Entry</code>
     *         is found, else <code>false</code>
     */
    synchronized public boolean contains(String str) {
        if (cache.containsKey(str)) {
            return true;
        } else {
            Iterator iterator = patterns.iterator();
            while (iterator.hasNext()) {
                Entry item = (Entry)iterator.next();
                if (item.contains(str)) {
                    cache.put(str, item);
                    return true;
                }
            }
        }
        return false;
    }

    public String getMapText() {
        return mapText;
    }

}
