/*
 * Copyright (c) 2003-2004 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/MimeExtension.java#1 $
 */
package com.opwvmsg.utils.paf.util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.activation.FileTypeMap;
import javax.activation.MimetypesFileTypeMap;
import javax.mail.internet.ContentType;
import javax.mail.internet.ParseException;

/**
 * Maps mime type to extensions (both ways). <p>
 *
 * MIME types mappings are specified in three places, a supplied file,
 * a mime.types file in META-INF, and the default from activation.java. <p>
 *
 * See javax.activation.FileTypeMap Javadoc for more details. <p>
 *
 * The contents of the specified file (if not null) and the /META-INF/mime.types
 * file must be a list of the form: <p>
 *
 * audio/amr           amr <br />
 * audio/sp-midi       mid <br />
 * audio/midi          midi mid <br />
 * audio/mid           midi mid <br />
 * audio/x-mid         mid <br />
 * audio/imelody       imy <br />
 * audio/x-imy         imy <br />
 * image/vnd.wap.wbmp  wbmp <br />
 * ... <p>
 *
 * MIME types will map to file extensions directly, with the first one in
 * the entry being the preferred one. <p>
 *
 * When mapping file extensions to MIME types, for those that map to more than
 * one MIME type, the one highest in the list will be returned as the
 * preferred MIME type. The entries in the supplied file will take precedence
 * over the META-INF/mime.types, which in turn takes precedence over the
 * default mimetypes.default.<p>
 */
public class MimeExtension {
    private static final Logger log = Logger.getLogger(MimeExtension.class);
    private static HashMap typeToExtension = null;

    // No instances
    private MimeExtension() {
    }

    /**
     * Initializes the MIME type extensions using the MIME type mappings in
     * the specified file. The MIME type mappings specified in the file will
     * be prepended to any /META-INF/mime.types and the default MIME types
     * from activation.jar.<p>
     *
     * If the input file name is null, empty, or invalid the default file
     * mappings will be used.<p>
     *
     * This should be called only once.
     *
     * @param mimeTypes InputStream containing new MIME mappings
     * @throws IOException if file cannot be read
     */
    synchronized public static void initialize(InputStream mimeTypes)
            throws IOException {
        if (typeToExtension != null) {
            throw new IllegalStateException("Already initialized");
        }
        typeToExtension = new HashMap();

        // Create typeToExtension mappings in the order:
        // InputStream -> /META-INF/mime.types -> /META-INF/mimetypes.default
        // Parse additional MIME types
        String additionalMimeTypes = null;
        if (mimeTypes != null && mimeTypes.available() > 0) {
            additionalMimeTypes = parseMimeTypes(mimeTypes);
        }

        // Get the META-INF/mime.types if present.
        String metaInfMimeTypes = null;
        InputStream in = MimeExtension.class.
                getResourceAsStream("/META-INF/mime.types");
        if (in != null && in.available() > 0) {
            metaInfMimeTypes = parseMimeTypes(in);
        }
 
        // Get MIME types from activation.jar.
        in = MimeExtension.class.
                getResourceAsStream("/META-INF/mimetypes.default");
        if (in != null && in.available() > 0) {
            parseMimeTypes(in);
        } else {
            log.warn("Can't find mimetypes.default, skipping.");
        }

        // Set default FileTypeMap in the order:
        // /META-INF/mimetypes.default -> /META-INF/mime.types -> InputStream
        MimetypesFileTypeMap mapping = new MimetypesFileTypeMap(in);
        if (metaInfMimeTypes != null) {
            mapping.addMimeTypes(metaInfMimeTypes);
        }
        if (additionalMimeTypes != null) {
            mapping.addMimeTypes(additionalMimeTypes);
        }
        MimetypesFileTypeMap.setDefaultFileTypeMap(mapping);
    }

    /**
     * Returns a valid mime type for the given filename.
     * If the mime type can't be found, "application/octet-stream" is returned.
     *
     * @param filename filename to find mime type for
     * @return the MIME type for the file extension, or
     *         "application/octet-stream"
     * if no match found
     */
    public static String getMimeType(String filename) {
        if (typeToExtension == null) {
            throw new IllegalStateException("Not initialized");
        }
        if (filename == null || filename.trim().length() == 0) {
            return "application/octet-stream";
        }
        filename = filename.toLowerCase();
        FileTypeMap mp = FileTypeMap.getDefaultFileTypeMap();
        String type = mp.getContentType(filename).toLowerCase();

        // Ugly hack for both image/pjpeg and audio/mid
        // Most phones don't understand these so we never return them.
        if (type.equals("image/pjpeg")) {
            type = "image/jpeg";
        }
        if (type.equals("audio/mid")) {
            type = "audio/midi";
        }
        return type;
    }

    /**
     * Returns a file extension (including preceding '.') for the given
     * mimeType. If no match is found we return <code>null</code>.
     *
     * @param mimeType MIME type to find file extension for.
     * @return File extension for the given mime type or null if not found
     */
    public static String getExtension(String mimeType) {
        if (typeToExtension == null) {
            throw new IllegalStateException("Not initialized");
        }
        String extension = null;
        String[] extensions = getAllExtensions(mimeType);
        if (extensions.length > 0) {
            extension = extensions[0];
            if (".jpeg".equalsIgnoreCase(extension)) {
                extension = ".jpg";
            }
        }
        return extension;
    }

    /**
     * Returns all known extensions for a MIME type.
     *
     * @param mimeType MIME type to find file extension for.
     * @return Array of matching file extensions for the given mime type
     *         or an empty array if not found
     */
    public static String[] getAllExtensions(String mimeType) {
        if (typeToExtension == null) {
            throw new IllegalStateException("Not initialized");
        }
        List extensions = new ArrayList();
        try {
            ContentType ct = new ContentType(mimeType);
            for (Iterator it = typeToExtension.keySet().iterator();
                    it.hasNext();) {
                String extensionType = (String)it.next();
                if (ct.match(extensionType)) {
                    extensions.addAll((List)typeToExtension.get(extensionType));
                }
            }
        } catch (ParseException exc) {
            // Bad type, nothing will match it
        }
        return (String[])extensions.toArray(new String[extensions.size()]);
    }

    /**
     * Parses InputStream for MIME types and creates map of MIME type to
     * preferred extension.
     *
     * @param in InputStream for MIME types being parsed
     * @throws IOException on problems reading InputStream
     * @return the MIME type list as a string
     */
    private static String parseMimeTypes(InputStream in) throws IOException {
        // Store for MIME types text, for adding to FileTypeMap later.
        StringBuffer mimeTypes = new StringBuffer();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String line = null;
        while ((line = br.readLine()) != null) {
            if (!line.startsWith("#")) {
                mimeTypes.append(line).append("\n");
                String[] items = StringUtils.split(line.toLowerCase());
                if (items.length >= 2) {
                    String mime = items[0].trim();
                    List extensions = (List)typeToExtension.get(mime);
                    if (extensions == null) {
                        extensions = new ArrayList();
                        typeToExtension.put(mime, extensions);
                    }
                    for (int i = 1; i < items.length; i++) {
                        String extension = '.' + items[i].trim();
                        if (!extensions.contains(extension)) {
                            extensions.add(extension);
                        }
                    }
                }
            }
        }
        return mimeTypes.toString();
    }
}
