/*  
 *      Copyright 2005 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: 
 */
package com.opwvmsg.utils.paf.intermail.config;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.intermail.IntermailConfig;
import com.opwvmsg.utils.paf.intermail.config.ops.UpdateConfigConnect;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;

/**
 * This is a monitoring thread that checks the status of the currently running
 * ConfigChangeListener thread. If that thread exits for any reason (i.e.
 * confserv restart or network error causes socket to close), then this thread
 * will re-initialize a new config connection and start a new listener thread.
 */
public class ConfigChangeListenerAdmin extends Thread {

    private static final Logger logger = Logger.getLogger(ConfigChangeListenerAdmin.class);

    private IntermailConfig config = null;
    private String configServerHost = null;
    private int configServerPort = 0;
    private String defaultHost = null;
    private String defaultApp = null;
    private ConfigChangeListener configListener = null;
    private int retryInterval = 0;
    
    /**
     * Initialize the class with specific config connection parameters.
     * 
     * @param config the Config object that will accept updates
     * @param configServerHost the config server host
     * @param configServerPort the config server port
     * @param defaultHost our default hostname
     * @param defaultApp the default application name
     * @param initialThread the initial configListener thread - may be null.
     * @param retryInterval time in ms between detection of an error, and when
     *            we'll try to reconnect to the server
     */
    public ConfigChangeListenerAdmin(IntermailConfig config, String configServerHost,
            int configServerPort, String defaultHost, String defaultApp,
            ConfigChangeListener initialThread, int retryInterval) {
        super("ConfigChangeListenerAdmin");
        this.config = config;
        this.configServerHost = configServerHost;
        this.configServerPort = configServerPort;
        this.defaultHost = defaultHost;
        this.defaultApp = defaultApp;
        configListener = initialThread;
        this.retryInterval = retryInterval;
        
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        // start an infinite loop that tries to listen for
        // updates from the conf server
        while (!isInterrupted()) {
            try {
                try {
                    if (configListener == null) {
                        UpdateConfigConnect ucc = new UpdateConfigConnect(
                                config, configServerHost, configServerPort,
                                defaultHost, defaultApp, true);
                        ucc.execute();
                        configListener = ucc.getListenerThread();
                        logger.info("Connection to confserv restored.");                        
                    }
                    configListener.join();
                    configListener = null;
                    logger.error("Connection to confserv was lost.. retrying in 15 seconds");
                } catch (IntermailException ie) {
                    logger.log(ie.getLog4jLevel(), ie.getFormattedString());
                }
                Thread.sleep(retryInterval);
            } catch (InterruptedException ie) {
                // interrupting this thread will break out of the loop
                break;
            }
        }
        // just log this in case we exit and didn't expect to..
        logger.info ("Config Change Listener Admin Thread is exiting.");
    }

}
