/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.utils.paf.intermail.http;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;

/**
 * This managers manages connections to the various mss connection pools, and
 * implements Mss specific features like round robin port selection in the case
 * of a multiple process configuration.
 * 
 * @author mxos-dev
 */
public class HTTPConnectionManager {

    private static Logger logger = Logger
            .getLogger(HTTPConnectionManager.class);

    private static Map<String, HTTPSockPoolInstances> mssSocketPool = null;
    public static HTTPConnectionManager instance = null;
    private static Config config = null;

    private static int rmeConnectTimeout = 120000; // 2 mins
    private static int rmeReadTimeout = 120000; // 2 mins
    

    private static int mssPoolCapacity = 75;

    /**
     * Method to get instance of the HTTPConnectionManager.
     * 
     * @return instance
     * @throws MxOSException
     */
    public static HTTPConnectionManager getInstance() {
        if (instance == null) {
            instance = new HTTPConnectionManager();
        }
        return instance;
    }

    /**
     * Default private Constructor.
     * 
     * @throws MxOSException
     */
    private HTTPConnectionManager() {

        config = Config.getInstance();
        try {
            rmeReadTimeout = config.getInt("rmeLookupTimeout") * 1000;
        } catch (ConfigException ce) {
            rmeReadTimeout = config.getInt("netTimeout", 240) * 1000;
        }
        rmeConnectTimeout = config.getInt("rmeConnectTimeout", (rmeReadTimeout/1000)) * 1000;
        mssPoolCapacity = config.getInt("mssMaxConnectionPoolSize", 50);
        mssSocketPool = new HashMap(20);
    }

    /**
     * Given a hostname, return a HTTPSockPoolInstances Object.
     * 
     * @param hostName
     *            the MSS host
     * @return HTTPSockPoolInstances representing all instances on that host
     */
    public static HTTPSockPoolInstances getMssInstances(final String hostName) {
        HTTPSockPoolInstances instances = (HTTPSockPoolInstances) mssSocketPool
                .get(hostName);
        if (instances == null) {
            instances = initMssHost(hostName);
        }
        return instances;
    }

    /**
     * Set up the appropriate socket pools for the specified MSS host as it is
     * encountered for the first time
     * 
     * @param hostName
     *            the MSS host
     * @return HTTPSockPoolInstances representing all instances on that host
     */
    private static HTTPSockPoolInstances initMssHost(final String hostName) {
        Config config = Config.getInstance();
        int mssBase = config.getInt(hostName, "mss", "mssBasePort");
        int mssNum = config.getInt(hostName, "mss", "mssNumPorts", 1);

        HTTPConnectionPool[] pools = new HTTPConnectionPool[mssNum];
        for (int i = 0; i < mssNum; i++) {
            pools[i] = new HTTPConnectionPool(mssPoolCapacity, hostName,
                    mssBase + i, rmeConnectTimeout, rmeReadTimeout);
        }
        HTTPSockPoolInstances inst = new HTTPSockPoolInstances(mssBase, mssNum,
                pools);

        mssSocketPool.put(hostName, inst);
        return inst;
    }

    /**
     * Given an mss hostname (and optionally a port number) get an initialized
     * connection to the mss
     * 
     * @param hostName
     *            The MSS hostname
     * @param port
     *            the MSS port number, or 0 to use the next available port.
     * @param usePool
     *            true if connections may come from the pool, false if a new
     *            connection is required.
     * @return an initialized RmeSocket
     * @throws IntermailException
     *             on any RME or socket error
     * @throws MxOSException
     */
    public HTTPConnectionPool getHttpConnectionPool(final String hostName,
            final int port, final boolean usePool) throws IntermailException {

        if (hostName == null || !config.isAConfiguredHost(hostName)) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Conf.NotAConfiguredHost",
                            new String[] { hostName }));
        }

        HTTPConnectionPool mssPool = null;
        HTTPSockPoolInstances instances = null;
        if (port != 0) {
            mssPool = getSpecificPool(hostName, port);
        } else {
            instances = getMssInstances(hostName);
            mssPool = instances.getNextPool();
        }
        return mssPool;
    }

    /**
     * Given a hostname and a port, get a specific socket pool.
     * 
     * @param hostName
     *            the MSS hostname
     * @param port
     *            the mss port
     * @return SocketPool for that particular connection
     */
    private synchronized static HTTPConnectionPool getSpecificPool(
            final String hostName, final int port) {
        HTTPSockPoolInstances instances = (HTTPSockPoolInstances) mssSocketPool
                .get(hostName);
        if (instances == null) {
            instances = initMssHost(hostName);
        }
        return instances.getSpecificPool(port);
    }

    /**
     * Puts a HTTPConnection back into the pool.
     * 
     * @param httpConnection
     * @throws IntermailException
     */
    public static void returnHttpConnection(final HTTPConnection httpConnection)
            throws IntermailException {
        HTTPConnectionPool mssPool = getSpecificPool(httpConnection.getHost(),
                httpConnection.getPort());
        mssPool.returnObject(httpConnection);
    }

    /**
     * Puts a HTTPConnection back into the pool.
     * 
     * @param sock
     *            the socket
     * @throws IntermailException
     * @throws MxOSException
     */
    public static void printPoolUsageInfo() throws IntermailException {

        if (mssSocketPool == null) {
            logger.info("mssSocketPool is not initiazed");
            return;
        }
        logger.debug("Number of MSS Socket Pools : " + mssSocketPool.size());
        Iterator entries = mssSocketPool.entrySet().iterator();
        while (entries.hasNext()) {
            logger.debug("==============================================================");
            Entry thisEntry = (Entry) entries.next();
            HTTPSockPoolInstances mssPool = (HTTPSockPoolInstances) thisEntry
                    .getValue();
            logger.debug("MSS Host : " + (String) thisEntry.getKey());
            logger.debug("Base Port : " + mssPool.getBasePort());
            logger.debug("Number Of Ports: " + mssPool.getNumPorts());
            logger.debug("Current Port Offset: " + mssPool.getCurPortOffset());
            logger.debug("");
            HTTPConnectionPool[] httpConnectionPool = mssPool.getHttpPools();
            for (int i = 0; i < httpConnectionPool.length; i++) {
                logger.debug("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
                logger.debug("Hostname: " + httpConnectionPool[i].getHostName());
                logger.debug("Port : " + httpConnectionPool[i].getPort());
                logger.debug("Number of Active Connections : "
                        + httpConnectionPool[i].getObjPool().getNumActive());
                logger.debug("Number of Idle Connections : "
                        + httpConnectionPool[i].getObjPool().getNumIdle());
            }
        }
        logger.debug("==============================================================");
    }
}
