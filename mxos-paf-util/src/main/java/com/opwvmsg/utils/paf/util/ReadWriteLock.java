/*
 *      ReadWriteLock.java
 *      
 *      Copyright 2000 Openwave Systems Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      Originally written by Doug Lea and released into the public domain.
 *      This may be used for any purposes whatsoever without acknowledgment.
 *      Thanks for the assistance and support of Sun Microsystems Labs,
 *      and everyone contributing, testing, and using this code.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/ReadWriteLock.java#1 $
 *      
 */

package com.opwvmsg.utils.paf.util;

/**
 * ReadWriteLocks maintain a pair of associated locks.
 * The readLock may be held simultanously by multiple
 * reader threads, so long as there are no writers. The writeLock
 * is exclusive. ReadWriteLocks are generally preferable to
 * plain Sync locks or synchronized methods in cases where:
 * <ul>
 *   <li> The methods in a class can be cleanly separated into
 *        those that only access (read) data vs those that 
 *        modify (write).
 *   <li> Target applications generally have more readers than writers.
 *   <li> The methods are relatively time-consuming (as a rough
 *        rule of thumb, exceed more than a hundred instructions), so it
 *        pays to introduce a bit more overhead associated with
 *        ReadWrite locks compared to simple synchronized methods etc
 *        in order to allow concurrency among reader threads.
 *        
 * </ul>
 * Different implementation classes differ in policies surrounding
 * which threads to prefer when there is
 * contention. By far, the most commonly useful policy is 
 * WriterPreferenceReadWriteLock. Other implementations
 * are targeted for less common, niche applications.
 *<p>
 * Standard usage:
 * <pre>
 * class X {
 *   ReadWriteLock rw;
 *   // ...
 *
 *   public void read() throws InterruptedException { 
 *     rw.readLock().acquire();
 *     try {
 *       // ... do the read
 *     }
 *     finally {
 *       rw.readlock().release()
 *     }
 *   }
 *
 *
 *   public void write() throws InterruptedException { 
 *     rw.writeLock().acquire();
 *     try {
 *       // ... do the write
 *     }
 *     finally {
 *       rw.writelock().release()
 *     }
 *   }
 * }
 * </pre>
 *
 * @see Sync
 *
 * @author Doug Lea
 * @version $Revision: #1 $ 
 */
public interface ReadWriteLock {
    /**
     * Get the readLock.
     *
     * @return the readLock
     *
     **/
    Sync readLock();

    /** 
     * Get the writeLock.
     *
     * @return the writeLock
     *
     */
    Sync writeLock();
}
