package com.opwvmsg.utils.paf.intermail.mail;

/**
 * Enum of the Attribute Ids of the Messages.
 * 
 * @author mOS-dev
 */
public enum AttributeId {
    attr_messageUUID(0),
    attr_bounce(1),
    attr_deliveredNDR(2),
    attr_private(3),
    attr_hasAttachment(4),
    attr_richMailFlag(5),
    attr_arrivalTime(6),
    attr_expirationTime(7),
    attr_firstSeenTime(8),
    attr_firstAccessedTime(9),
    attr_lastAccessedTime(10),
    attr_size(11),
    attr_uid(12),
    attr_msgFlags(13),
    attr_keywords(14),
    attr_type(15),
    attr_priority(16),
    attr_multipleMsgs(17),
    attr_subject(18),
    attr_from(19),
    attr_to(20),
    attr_cc(21),
    attr_date(22),
    attr_references(23),
    attr_sender(24),
    attr_bcc(25),
    attr_replyTo(26),
    attr_inReplyTo(27),
    attr_blobMessageId(28),
    attr_specialDeleted(29),
    attr_folderUUID(30),
    attr_conversation(31);
    // When you add an attribute, always place items in ascending order with
    // values. Otherwise, the method valueOf() does not work properly.

    // These two static members are used for resolving an AttributeId by a short
    // value. The resolveTable is a possibly-sparse array of AttributeId
    // objects. Indexes are their values.  AttributeId's are resolvable by
    // value using this table.
    private static AttributeId[] resolveTable = null;
    private static int maximumIndex;
    
    static {
        AttributeId[] ids = AttributeId.values();
        maximumIndex = ids[ids.length - 1].getValue();
        resolveTable = new AttributeId[maximumIndex + 1];
        for (AttributeId id : ids) {
            resolveTable[id.getValue()] = id;
        }
    }

    private short type;

    /**
     * Constructor of the enum entry.
     * @param value value of the attribute ID.
     */
    private AttributeId(int value) {
        this.type = (short) value;
    }

    /**
     * @return Value of the entry.
     */
    public short getValue() {
        return this.type;
    }

    /**
     * Resolves an AttributeId entry by value.
     * 
     * @param key Value of the entry to find.
     * @return AttributeId entry that has the given value. The method returns
     *         null if the corresponding entry was not found.
     */
    public static AttributeId valueOf(final short key) {
        if (key < 0 || key > maximumIndex) {
            return null;
        }
        
        return resolveTable[key];
    }

    /**
     * @return Number of entries in this enum. 
     */
    public static int getNumAttributes() {
        return maximumIndex + 1;
    }
}