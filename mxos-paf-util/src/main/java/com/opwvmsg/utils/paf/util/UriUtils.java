/*
 * Copyright (c) 2003 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/UriUtils.java#1 $
 */
package com.opwvmsg.utils.paf.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.opwvmsg.utils.paf.jsp.tagext.TagUtilities;

/**
 * Rewrites the URI with additional query parameters to create unique URIs for
 * particular variants and cache-invalidated URIs.
 *
 * @author Paul Linden
 * @version $Revision: #1 $
 */
public class UriUtils {

    private static final String DOTDOT = "\\.\\.";

    /**
     * Adds query parameter to URI to enable (a) cache invalidation if the
     * cacheInvalidate tag has been called on the specified URI (b) generation
     * of unique URIs in multi-variant applications.
     *
     * @param uri URI to be rewritten
     * @param page the current PageContext
     * @return the modified URI
     */
    static public String rewrite(String uri, PageContext page) {
        if (uri == null) {
            return uri;
        }

        HttpServletRequest request = (HttpServletRequest) page.getRequest();
        UriStore cache = UriStore.getInstance(request.getSession(), false);
        String locale =
            (String) request.getAttribute("com.openwave.paf.rswitch.locale");
        // flush all '..' and leading '/'
        locale = locale.replaceAll(DOTDOT, "");
        int index;
        while ((index = locale.indexOf('/')) == 0) {
            locale = locale.substring(index+1);
        }

        String variant =
            (String) request.getAttribute("com.openwave.paf.rswitch.variant");
        // flush all '..' and leading '/'
        variant = variant.replaceAll(DOTDOT, "");
        while ((index = variant.indexOf('/')) == 0) {
            variant = variant.substring(index+1);
        }

        if (cache != null || locale != null || variant != null) {
            String path = uri;
            String query = "";
            String anchor = "";
            int question = uri.lastIndexOf('?');
            int pound = uri.lastIndexOf('#');

            if (question >= 0) {
                path = uri.substring(0, question);
                if (pound >= 0) {
                    query = uri.substring(question, pound);
                } else {
                    query = uri.substring(question);
                }
            }
            if (pound >= 0) {
                anchor = uri.substring(pound);
                if (question < 0) {
                    path = uri.substring(0, pound);
                }
            }

            // Get maximum length new query can be.
            // 14 is twice the length of &amp; plus the length of "l=" and "v="
            int length = query.length() + 14;

            if (locale != null) {
                length += locale.length();
            }
            if (variant != null) {
                length += variant.length();
            }

            String amp = null;
            if (page.findAttribute("org.apache.struts.globals.XHTML") != null ||
                page.findAttribute(TagUtilities.KEY_WML_ESCAPES) != null ||
                page.findAttribute(TagUtilities.KEY_HDML_ESCAPES) != null) {
                amp = "&amp;";
            } else {
                amp = "&";
            }

            StringBuffer newQuery = new StringBuffer(length);
            newQuery.append(query);
            if (locale != null) {
                if (newQuery.length() > 0) {
                    newQuery.append(amp);
                    newQuery.append("l=");
                } else {
                    newQuery.append("?l=");
                }
                newQuery.append(locale);
            }
            if (variant != null) {
                if (newQuery.length() > 0) {
                    newQuery.append(amp);
                    newQuery.append("v=");
                } else {
                    newQuery.append("?v=");
                }
                newQuery.append(variant);
            }
            if (cache != null) {
                Object cacheValue = cache.get(path);

                if (cacheValue != null) {
                    if (newQuery.length() > 0) {
                        newQuery.append(amp);
                        newQuery.append("xx=");
                    } else {
                        newQuery.append("?xx=");
                    }
                    newQuery.append(cacheValue);
                }
            }
            String result = path + newQuery + anchor;
            return result;
        }
        return uri;
    }

    /**
     * HTML entities to be decoded
     */
    static private final Pattern[] HTMLEncodedPatterns = { 
        Pattern.compile("&amp;"), 
        Pattern.compile("&lt;"), 
        Pattern.compile("&gt;"), 
        Pattern.compile("&quot;"), 
        Pattern.compile("&#39;") 
    };
    static private final String[] HTMLDecodedChars = { "&", "<", ">", "\'", "`" };
        
    /**
     * Decodes URIs turning HTML escape sequences back to their normal 8-bit chars
     *
     * @param uri URI to be decoded
     * @return the modified URI
     */
    static public String decodeUri(String uri) {
        StringBuffer newUri = new StringBuffer(uri);
        for (int i=0; i<HTMLEncodedPatterns.length; i++) {
            StringBuffer sb = new StringBuffer();
            Matcher matcher = HTMLEncodedPatterns[i].matcher(newUri);
            while (matcher.find()) {
                matcher.appendReplacement(sb, HTMLDecodedChars[i]);
            }
            matcher.appendTail(sb);
            newUri = sb;
        }
        return newUri.toString();
    }
}
