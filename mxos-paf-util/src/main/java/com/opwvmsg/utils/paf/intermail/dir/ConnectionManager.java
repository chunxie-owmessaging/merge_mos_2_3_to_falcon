/*
 * Copyright 2005 Openwave Systems, Inc. All Rights Reserved.
 * 
 * The copyright to the computer software herein is the property of
 * Openwave Systems, Inc. The software may be used and/or copied only with the
 * written permission of Openwave Systems, Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id:
 * 
 */
package com.opwvmsg.utils.paf.intermail.dir;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.intermail.io.RmeSocket;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.rme.SocketPool;
import com.opwvmsg.utils.paf.util.LazyClock;
import com.opwvmsg.utils.paf.util.Utilities;

/**
 * This class manages the connection parameters to the dir or dircache, and
 * controls the current pool of connections to that server. It is also
 * responsible for failover logic
 */
public class ConnectionManager {

    // config items
    private static int rmeTimeout = 120000; //2 mins
    private static String[] dirCacheHosts;
    private static int[] dirCachePorts;
    private static int numDirHosts;
    private static int dirPoolCapacity = 50;
    private static int dirMinPool = 0;
    private static int dirMaxIdle = 600;

    // cycle this to failover
    private static int dirHostNum = 0;

    private static LazyClock failBackClock = null;
    private static long lastCheckTime = 0;
    private static long failBackCheckTime = 300000; // 5 minutes
    
    private static boolean initialized = false;

    private static SocketPool dirSocketPool = null;

    private static final Logger logger = Logger.getLogger(ConnectionManager.class);

    /* Initialization */
    static {
        initRme();
    }
    
    /**
     * Get a connection to the dircache server.
     * 
     * @param usePool if true, will use a pool connection if possible, if false
     *            a new connection is created
     * @return a valid, initialized connection to the dircache
     * @throws IntermailException on any error
     */
    protected static RmeSocket getRmeSock(boolean usePool)
            throws IntermailException {

        RmeSocket sock = null;

        if (numDirHosts == 1) {
            // ignore failover logic
            if (usePool) {
                sock = (RmeSocket) dirSocketPool.getItem();
            }
            if (sock == null)
                sock = dirSocketPool.createSocket();
            return sock;
        } else {
            if (dirHostNum != 0) {
                // if we are not using the primary dircache,
                // periodically check to see if it is back up
                if (failBackClock != null && 
                        (failBackClock.currentTimeMillis()-lastCheckTime > failBackCheckTime)) {
                    failBack();
                }
                        
            }
            // get local copy to avoid synchronize
            // it could be out of date, but failover code
            // handles that
            SocketPool sockPool = dirSocketPool;
            String startHost = sockPool.getHost();
            int startPort = sockPool.getPort();
            while (sock == null) {
                try {
                    if (usePool) {
                        sock = (RmeSocket) sockPool.getItem();
                    }
                    if (sock == null)
                        sock = sockPool.createSocket();
                } catch (IntermailException ie) {
                    // this should cause a new pool to be created.
                    // however, we pass in host, in case
                    // our local sockPool is stale and someone
                    // already failed it over

                    // since this is unknown host, should we
                    // remove it from this list? I don't right now
                    // just failover
                    sockPool = failOver(sockPool.getHost(), sockPool.getPort());
                    if (sockPool.getHost().equals(startHost)
                            && sockPool.getPort() == startPort) {
                        throw new IntermailException(
                                LogEvent.URGENT,
                                LogEvent.formatLogString(
                                    "Nio.ConnServerFail",
                                    new String[] {
                                            "imdircacheserv",
                                            startHost,
                                            Integer.toString(dirCachePorts[dirHostNum]),
                                            "No more servers available for failover." }));
                    } else {
                        logger.log(
                            Level.ERROR,
                            LogEvent.formatLogString(
                                "Nio.ConnServerFail",
                                new String[] {
                                        "imdircacheserv",
                                        sockPool.getHost(),
                                        Integer.toString(dirCachePorts[dirHostNum]),
                                        "Failing over to next server." }));
                    }
                }
            }
        }

        return sock;
    }

    /**
     * Reset the current socket pool, and initialize a new pool that refers to
     * the next server in line.
     * 
     * @param hostName the current directory host
     * @param portNum the currnet directory port
     * @return a new SocketPool that refers to the next server
     */
    private synchronized static SocketPool failOver(String hostName, int portNum) {
        if (dirSocketPool.getHost().equals(hostName)
                && dirSocketPool.getPort() == portNum) {
            
            //initialize the failBackClock
            if (failBackClock == null) {
                failBackClock = LazyClock.getInstance();
            }
            dirHostNum = (dirHostNum + 1) % numDirHosts;
            SocketPool oldPool = dirSocketPool;
            dirSocketPool = new SocketPool(dirPoolCapacity, dirMinPool,
                    dirMaxIdle, dirCacheHosts[dirHostNum],
                    dirCachePorts[dirHostNum], rmeTimeout, "dircacheserv");
            oldPool.clear();
        }
        return dirSocketPool;
    }

    /**
     * Switch connections back to the primary dir cache server
     * 
     * @return a new socket pool pointing to the primary server
     */
    private synchronized static SocketPool failBack() {
        if (dirHostNum == 0) {
            // already using primary server
            return dirSocketPool;
        }
        dirHostNum = 0;
        SocketPool oldPool = dirSocketPool;
        dirSocketPool = new SocketPool(dirPoolCapacity, dirMinPool,
                dirMaxIdle, dirCacheHosts[dirHostNum],
                dirCachePorts[dirHostNum], rmeTimeout, "dircacheserv");
        oldPool.clear();       
        return dirSocketPool;
    }
            
    /**
     * Returns the connection to the pool.
     * 
     * @param sock the open RME connection
     */
    protected synchronized static void putRmeSock(RmeSocket sock) {
        dirSocketPool.putItem(sock);
    }

    /**
     * Initialize the RME connection profile and connection pools.
     * 
     * @param dirPoolCap the maximum capacity of the directory connection pool
     * @param dirMinPool the minimum number of connections this pool should
     *            serve
     * @param dirMaxIdle the max idle time for connections in the pool
     * @param dirHosts the dir hosts
     * @param dirPorts the dir ports corresponding to the above hosts
     * @param timeout the RME timeout
     */
    private static void initRme(int dirPoolCap, int dirMinPool, int dirMaxIdle,
                               String[] dirHosts, int[] dirPorts, int timeout) {
        dirPoolCapacity = dirPoolCap;
        rmeTimeout = timeout;

        // init dir
        if (dirHosts != null && dirHosts.length > 0 && dirPorts != null
                && dirPorts.length > 0) {
            dirCacheHosts = dirHosts;
            dirCachePorts = dirPorts;
        } else {
            dirCacheHosts = new String[] { "" };
            dirCachePorts = new int[] { 0 };
        }

        numDirHosts = dirCacheHosts.length;

        // set the pool to the first dircache
        dirSocketPool = new SocketPool(dirPoolCapacity, dirMinPool, dirMaxIdle,
                dirCacheHosts[0], dirCachePorts[0], rmeTimeout, "dircacheserv");

        initialized = true;

    }

    /**
     * Initialize the RME connection information, and pool sizes. Configuration
     * items are read from config.db
     */
    private static void initRme() {
        if (!initialized) {
            Config config = Config.getInstance();

            String dirRmeHostsCfg = Utilities.arrayToDsv(config.getArray("dirRmeHosts"),':');
            int timeout;
            try {
                timeout = config.getInt("rmeLookupTimeout") * 1000;
            } catch (ConfigException ce) {
                timeout = config.getInt("netTimeout", 240) * 1000;
            }
            dirPoolCapacity = config.getInt("dirMaxConnectionPoolSize", 50);
            dirMinPool = config.getInt("dirMinConnectionPoolSize", 0);
            dirMaxIdle = config.getInt("dirMaxConnectionPoolIdleTimeSec", 600);
            failBackCheckTime = config.getInt("dirFailBackCheckTime",300)*1000L;
            
            
            String[] dirHosts = Utilities.dsvToArray(dirRmeHostsCfg, 
                new char[] {':','\t',' ',','}, false);
            
            int[] dirPorts = new int[] { 0 };

            if (dirHosts.length > 0) {
                dirPorts = new int[dirHosts.length];
                for (int i = 0; i < dirHosts.length; i++) {
                    dirPorts[i] = config.getInt(dirHosts[i], "imdircacheserv",
                        "dirCachePort");
                }
            }
            initRme(dirPoolCapacity, dirMinPool, dirMaxIdle, dirHosts,
                dirPorts, timeout);
        }
    }

}

