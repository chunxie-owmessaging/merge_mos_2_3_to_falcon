/*
 * Copyright (c) 2003 Openwave Systems Inc.  All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.  The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/password/NoSuchProviderException.java#1 $
 */

package com.opwvmsg.utils.paf.util.password;

/**
 * This exception occurs if the requested provider is not found.
 */
public class NoSuchProviderException extends PasswordException {

    public NoSuchProviderException() {
        super();
    }

    public NoSuchProviderException(String s) {
        super(s);
    }

    public NoSuchProviderException(Throwable e) {
        super(e);
    }

    public NoSuchProviderException(String s, Throwable e) {
        super(s, e);
    }
}
