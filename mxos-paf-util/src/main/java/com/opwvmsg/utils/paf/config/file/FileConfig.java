/*
 * Copyright (c) 2002-2005 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/config/file/FileConfig.java#1 $
 *
 */

package com.opwvmsg.utils.paf.config.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.util.Utilities;

/**
 * This class provides access to configuration information stored in a
 * properties file or an instance of the <code>Properties</code> class.
 * <p/>
 * Configuration data is normally looked up using a combination of host,
 * application, and key. For this provider, however, the host and application
 * are not used. Put another way, a host of "*" and an app of "common" are
 * always assumed. In the properties file, only the key is provided.
 * <p/>
 * Multi-value entries are represented using the newline character as
 * a delimiter.  Embedded newlines are supported using the backslash
 * character as an escape.  For example, a value of
 * <I>hello,\\\nworld!</I> is a scalar value while
 * <I>hello,\nworld!</I> is a multi-value where the first value is
 * <I>hello,</I> and the second value is <I>world!</I>.
 *
 * @author Conrad Damon
 * @author Mark Abbott
 * @author Forrest Girouard
 * @version $Revision: #1 $
 */
class FileConfig extends Config {
    private static final Logger logger = 
        Logger.getLogger("com.openwave.paf.config.file.FileConfig");

    private Properties properties = null;

    /**
     * Creates an instance using the setup information provided.
     * <p/>
     * The setup map contains one of two possible keys: <I>path</I>
     * or <I>props</I>.  The value in the specified map of the
     * <I>path</I> key contains the path to a properties file while
     * the <I>props</I> key contains a reference to a
     * <code>Properties</code> instance.
     * <p/>
     * The FileConfig object holds information read in from the given
     * properties.
     *
     * @param setup a map with one of two possible elements, elther
     *        <I>path</I> or <I>props</I>
     * @throws ConfigException if an IOException occurs loading the
     *        file or if not one and only one key is defined in the map
     */
    protected FileConfig(Map setup) throws ConfigException {

        if (logger.isDebugEnabled()) {
            logger.debug("enter: " + setup);
        }
        try {
            Iterator keys = setup.keySet().iterator();
            while (keys.hasNext()) {
                String key = (String)keys.next();
                if (properties != null) {
                    throw new ConfigException(key + ": ambiguous");
                } else if (key.equals("path")) {
                    loadProperties((String)setup.get(key));
                } else if (key.equals("props")) {
                    properties = Utilities.getProperties(
                            (Properties)setup.get(key));
                } else {
                    throw new ConfigException(key + ": unrecognized");
                }
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("exit: " + setup);
            }
        }
    }

    /**
     * Loads config values out of a properties file. The file will be
     * read into a static Properties object the first time through only.
     *
     * @throws ConfigException if an IOException occurs reading the file
     */
    private void loadProperties(String fileName) throws ConfigException {
        if (logger.isDebugEnabled()) {
            logger.debug("enter: " + fileName);
        }
        try {
            if (properties == null) {
                properties = new Properties();
                FileInputStream input = null;
                if (fileName == null) {
                    throw new ConfigException("Path to config file is empty");
                }
                File file = new File(fileName);
                try {
                    input = new FileInputStream(file);
                    properties.load(input);
                } catch (IOException e) {
                    throw new ConfigException("Open failed on " + fileName, e);
                } finally {
                    try {
                        if (input != null) {
                            input.close();
                        }
                    } catch (IOException ignored) {
                    }
                }
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("exit: " + fileName);
            }
        }
    }

    /**
     * Gets the configuration value with the given host, app, and key, as a
     * String from the properties file. Leading and trailing white space is
     * trimmed. If the value is empty, the empty string (rather than null) is
     * returned.
     * <p/>
     * Note: host and app are ignored.
     * Note: For multi-valued entries only the first value is returned.
     *
     * @param host the host associated with this configuration item
     * @param app the application associated with this configuration item
     * @param key the name of this configuration item
     * @return the value of the configuration item with this key
     * @throws ConfigException if the key does not exist
     */
    public String get(String host, String app, String key)
            throws ConfigException {
        String value = getString(host, app, key);
        if (value.indexOf('\n') != -1) {
            String[] values = Utilities.dsvToArray(value, '\n', '\\');
            if (values.length > 0) {
                value = values[0];
            }
        }
        return value;
    }

    /**
     * Gets the configuration value with the given host, app, and
     * key, as a String array from the properties file.  Leading and
     * trailing white space is trimmed.
     * <p/>
     * The value in the properties file is treated as a DSV
     * (delimiter separated value) where the newline character is the
     * delimiter and the backslash character is the escape.
     * <p/>
     * If the value is empty, a zero-length array (rather than null)
     * is returned.
     * <p/>
     * Note: host and app are ignored.
     *
     * @param host the host associated with this configuration item
     * @param app the application associated with this configuration item
     * @param key the name of this configuration item
     * @return the value of the configuration item with this key
     * @throws ConfigException if the key does not exist
     */
    public String[] getArray(String host, String app, String key)
            throws ConfigException {
        String value = getString(host, app, key);
        if (value.indexOf('\n') != -1) {
            return Utilities.dsvToArray(value, '\n', '\\');
        } else {
            return new String[] { value };
        }
    }

    /**
     * Gets the configuration value with the given host, app, and key, as a
     * boolean from the properties file. Follows the Java convention whereby the
     * string "true" evaluates to true, and anything else is false.
     *
     * @param host the host associated with this configuration item
     * @param app the application associated with this configuration item
     * @param key the name of this configuration item
     * @return true or false, false if the value is empty
     * @throws ConfigException if the key does not exist
     */
    public boolean getBoolean(String host, String app, String key)
            throws ConfigException {

        return Boolean.valueOf(get(host, app, key)).booleanValue();
    }

    /**
     * Gets the configuration value with the given host, app, and key, as an
     * integer from the properties file.
     *
     * @param host the host associated with this configuration item
     * @param app the application associated with this configuration item
     * @param key the name of this configuration item
     * @return the integer value of the configuration item with this key
     * @throws ConfigException if the key does not exist, or if the value is not
     * parsable as an int
     */
    public int getInt(String host, String app, String key)
            throws ConfigException {

        try {
            return Integer.parseInt(get(host, app, key));
        } catch (NumberFormatException e) {
            throw new ConfigException("integer conversion error", e);
        }
    }

    /**
     * Gets the configuration value with the given host, app, and key, as a
     * float from the properties file.
     *
     * @param host the host associated with this configuration item
     * @param app the application associated with this configuration item
     * @param key the name of this configuration item
     * @return the float value of the configuration item with this key
     * @throws ConfigException if the key does not exist, or if the value is not
     * parsable as a float
     */
    public float getFloat(String host, String app, String key)
            throws ConfigException {

        try {
            return Float.parseFloat(get(host, app, key));
        } catch (NumberFormatException e) {
            throw new ConfigException("floating point conversion error", e);
        }
    }

    /**
     * Returns true if the given host, app, and key combination exists in the
     * properties file.
     *
     * @param host the host associated with this configuration item
     * @param app the application associated with this configuration item
     * @param key the name of this configuration item
     * @return true if the given key exists
     */
    public boolean contains(String host, String app, String key) {
        return (properties.containsKey(key));
    }

    /**
     * Remove a cached value from the config cache, so it will be retrieved from
     * the config.db next time the key is requested
     * <p/>
     * Since this implementation has no cache, this does nothing.
     *
     * @param host the host part of the cache key
     * @param app the app part of the cache key
     * @param key the key's name
     */
    public void removeCachedValue(String host, String app, String key) {
    }

    /**
     * Gets the configuration value with the given host, app, and key, as a
     * String from the properties file. Leading and trailing white space is
     * trimmed. If the value is empty, the empty string (rather than null) is
     * returned.
     * <p/>
     * Note: host and app are ignored.
     *
     * @param host the host associated with this configuration item
     * @param app the application associated with this configuration item
     * @param key the name of this configuration item
     * @return the value of the configuration item with this key
     * @throws ConfigException if the key does not exist
     */
    private String getString(String host, String app, String key)
            throws ConfigException {

        if (properties.containsKey(key)) {
            return properties.getProperty(key).trim();
        } else {
            throw new ConfigException("The key '" + key + "' does not exist");
        }
    }

}
