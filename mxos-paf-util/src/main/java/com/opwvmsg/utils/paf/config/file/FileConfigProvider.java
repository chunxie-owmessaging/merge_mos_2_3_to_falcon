/*
 * FileConfigProvider.java
 *
 * Copyright (c) 2002 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/config/file/FileConfigProvider.java#1 $
 *
 */

package com.opwvmsg.utils.paf.config.file;

import java.util.Map;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.config.spi.ConfigProvider;

/**
 * This class implements a file-based service provider that reads
 * the configuration data from a Properties instance or properties file.
 *
 * <p>Configuration data is normally looked up using a combination of host,
 * application, and key. For this provider, however, the host and application
 * are not used. Put another way, a host of "*" and an app of "common" are
 * always assumed. In the properties file, only the key is provided.
 *
 * @author Conrad Damon
 * @author Mark Abbott
 * @version $Revision: #1 $
 */

public class FileConfigProvider implements ConfigProvider {

    /**
     * Gets a FileConfig object using the given setup information.
     *
     * @param setup a map with one element, either "path" mapped to the
     *     path of the properties file, or "props" mapped to an
     *     instance of a Properties
     *
     * @return a FileConfig object
     *
     * @throws ConfigException if the properties file can't be loaded
     */
    public Config getConfig (Map setup) throws ConfigException {
        return new FileConfig(setup);
    }

    /**
     * Tests whether this provider is capable of providing the
     * Config API for the "file" provider type.
     *
     * @param type the desired type of provider
     *
     * @return <code>true</code> if the named type is handled by
     *         this provider, else <code>false</code>
     */
    public boolean isType(String type) {
        return type.equalsIgnoreCase("file");
    }
}
