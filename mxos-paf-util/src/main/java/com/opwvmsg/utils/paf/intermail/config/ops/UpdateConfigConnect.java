/*
 * Copyright 2005 Openwave Systems, Inc. All Rights Reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems, Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems, Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id:
 */
package com.opwvmsg.utils.paf.intermail.config.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.config.intermail.ConfigDict;
import com.opwvmsg.utils.paf.config.intermail.IntermailConfig;
import com.opwvmsg.utils.paf.intermail.config.AbstractConfigOperation;
import com.opwvmsg.utils.paf.intermail.config.ConfigChangeListener;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;

/**
 * This RME op will contact the config server, and ask if we need any updates
 * (based on the timestamp of our local config. This operation will then keep
 * the socket open as a server socket that will accept connections from the
 * confserver about further updates.
 */
public class UpdateConfigConnect extends AbstractConfigOperation {

    private boolean needToChange = false;
    private ConfigDict rmeDict = null;
    private ConfigChangeListener configListener = null;
    private boolean startListener = true;

    
    /**
     * A constructor to build the UpdateConfig operation. This constructor takes
     * an IntermailConfig object in, as it shouldn't be used with any other
     * config provider.
     * 
     * @param config the current IntermailConfig object
     * @param confHost the config server host
     * @param confPort the config server port
     * @param defaultHost the default hostname for this app
     * @param defaultApp the defualt application name
     * @param startListener if true, the connection to the conf server will
     *         be maintained, and a configListener will be intialized.
     *         Otherwise, the connection will be dropped.
     */
    public UpdateConfigConnect(IntermailConfig config, String confHost,
            int confPort, String defaultHost, String defaultApp, 
            boolean startListener) {
        initCfgOps(confHost, confPort, defaultHost, defaultApp);
        this.config = config;
        
    }

    /* (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {

        callRme(UPDATE_CONN, RME_OMIT_SENDING_CLIENT_INDEX, true);

        // update config if we have to
        if (needToChange && rmeDict != null && rmeDict.getDictSize() > 0) {
            rmeDict.updateIntermailConfig(config, false);
        }

        // start a listener thread to listen for config traffic
        if (startListener) {
            configListener = new ConfigChangeListener(rmeSocket);
            configListener.start();
        }
    }

    /**
     * Gets the listener thread that has taken over the socket.
     * 
     * @return config listener thread
     */
    public ConfigChangeListener getListenerThread() {
        return configListener;
    }

    /**
     * Gets the conifg.db returned from the request. This may be a partial
     * config.db.
     * 
     * @return ConfigDict with the data from the config.db returned by this
     *         request.
     */
    public ConfigDict getConfigDb() {
        return rmeDict;
    }

    // RME FUNCTIONS

    /*
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.rme.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeString(defaultHost);
        outStream.writeString(defaultApplication);
        outStream.writeString(configTimeStamp);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.rme.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        logEvents = inStream.readLogEvent();
        needToChange = inStream.readBoolean();
        if (needToChange) {
            rmeDict = new ConfigDict(inStream);
        }

    }

	@Override
	protected void constructHttpData() throws IOException {
	}

	@Override
	protected void receiveHttpData() throws IOException {
		// TODO Auto-generated method stub
	}

}

