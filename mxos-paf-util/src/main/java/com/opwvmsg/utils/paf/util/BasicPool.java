/*
 * Copyright (c) 2000-2003 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/BasicPool.java#1 $
 */

package com.opwvmsg.utils.paf.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * This class implements the <code>Pool</code> interface.
 * <p>
 * This class was originally part of net.mobility.util
 * @author Forrest Girouard
 */
public class BasicPool implements Pool {

    /**
     * The items currently retained in this pool.
     */
    private Set items;

    /**
     * The capacity of this pool
     */
    private int capacity;

    /**
     * The current iterator for the items retained in this pool.
     */
    private Iterator iterator;

    /**
     * Constructs a new, empty pool.
     */
    public BasicPool() {
        items = new HashSet();
    }

    /**
     * Constructs a new, empty pool with the specified capacity.
     *
     * @param capacity the maximum number of items to be retained
     *          by this pool.
     */
    public BasicPool(int capacity) {
        if (capacity > 0) {
            this.capacity = capacity;
            items = new HashSet(capacity);
        } else {
            items = new HashSet();
        }
    }

    /**
     * Returns (and removes) an item from this pool.
     *
     * @return Object an available item or <code>null</code> if
     *          this pool is exhausted.
     */
    public Object getItem() {
        Object item = null;
        synchronized (getLock()) {
            if (iterator == null) {
                iterator = items.iterator();
            }
            try {
                item = iterator.next();
                iterator.remove();
            } catch (NoSuchElementException e) {
            }
        }
        return item;
    }

    /**
     * Adds the specified item to this pool.
     *
     * If the item cannot be added to this pool without exceeding
     * the capacity then the item is returned.
     *
     * @param item an <code>Object</code> to be placed into this pool
     *          for later use.
     *
     * @return Object the specified item if the capacity of the
     *          pool would be exceeded by adding it and
     *          <code>null</code> otherwise.
     */
    public Object putItem(Object item) {
        synchronized (getLock()) {
            if (item != null && (capacity == 0 || items.size() < capacity)) {
                iterator = null;
                items.add(item);
                return null;
            }
        }
        return item;
    }

    /**
     * Removes all items from this pool.
     */
    public void clear() {
        synchronized (getLock()) {
            iterator = null;
            items.clear();
        }
    }

    /**
     * Returns the number of items currently in this pool.
     *
     * @return int the number of items currently in this pool.
     */
    public int size() {
        synchronized (getLock()) {
            return items.size();
        }
    }

    /**
     * Returns the maximum number of items to be retained by this
     * pool.  
     *
     * When the size is equal to the capacity no further items are
     * retained by this pool.
     *
     * A value of zero indicates that this pool does not limit the
     * number of items retained.
     *
     * @return int the maximum number of items to be retained by this pool.
     */
    public int capacity() {
        return capacity;
    }

    /**
     * Returns the object used for synchronization by this pool.
     *
     * @return Object the object used by this pool for synchronization.
     */
    public Object getLock() {
        return this;
    }

}
