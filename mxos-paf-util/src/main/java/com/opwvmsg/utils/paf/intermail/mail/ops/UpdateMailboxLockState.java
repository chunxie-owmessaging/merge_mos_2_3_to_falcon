/*
 * Copyright (c) 2005 Openwave Systems Inc. All rights reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/ops/UpdateMailboxLockState.java#1 $
 *  
 */
package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This RME operation will either lock or unlock a mailbox
 */
public class UpdateMailboxLockState extends AbstractMssOperation {

    // input
    private String name;
    boolean lock;

    // output
    // -- no output --

    // internal
    private String url;

    /**
     * Constructor for UpdateMailboxLockState
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param lock if true, lock the mailbox; if false, unlock
     */
    public UpdateMailboxLockState(String host, String name, boolean lock) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_P_LOCKMAILBOX);
        this.lock = lock;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (rmeDataModel == RmeDataModel.CL) {
            callRme(MSS_P_LOCKMAILBOX);
        } else {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_P_LOCKMAILBOX", host }));
        }

    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        inStream.readString(); // the url is returned, no need to keep it
        logEvents = inStream.readLogEvent();
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeString(url);
        outStream.writeBoolean(lock);
    }

    @Override
    protected void constructHttpData() throws IOException {
        // RME operation not supported
    }

    @Override
    protected void receiveHttpData() throws IOException {
        // RME operation not supported
    }
}
