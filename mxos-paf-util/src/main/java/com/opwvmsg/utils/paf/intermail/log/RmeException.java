/*
 * Copyright (c) 2006 Openwave Systems Inc. All rights reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/log/RmeException.java#1 $
 *  
 */
package com.opwvmsg.utils.paf.intermail.log;

import org.apache.log4j.Level;

/**
 * This interface is used for exceptions that hold server side log events.
 */
public interface RmeException {
    
    /**
     * Gets the formatted detail message (log message) for this exception.
     * 
     * @return formatted log string associated with this exception.
     */
    public boolean hasLogEvents();
    
    /**
     * Gets the server generated log events for this exception
     *
     * @return an array of LogEvent objects.
     * 
     */
    public LogEvent[] getLogEvents();

    /**
     * Gets the suggested log4j level for this exception
     * 
     * @return log4j Level
     */
    public Level getLog4jLevel();
}
