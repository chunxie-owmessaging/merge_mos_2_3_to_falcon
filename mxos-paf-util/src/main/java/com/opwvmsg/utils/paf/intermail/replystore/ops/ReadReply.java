/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.replystore.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.replystore.Reply;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

public class ReadReply extends AbstractMssOperation {

    // input
    private String mssId;
    private int type;

    // output
    private Reply reply;

    // internal
    private String url;
    private String hostHeader;

    /**
     * For MSS_P_FETCHAUTOREPLY & MSS_SL_FETCHAUTOREPLY RME operation to read an
     * autoreply field
     * 
     * @param host the autoreply host
     * @param mssId the mss ID of the user
     * @param realm The mailRealm of the user
     * @param type the autoreply type
     * @param rmeDataModel RME data access model. CL or Leopard
     */
    public ReadReply(String host, String mssId, String realm, int type,
            final RmeDataModel rmeDataModel) {
        super(rmeDataModel);
        this.host = resolveClusterToMSS(host, mssId);
        this.mssId = mssId;
        this.type = type;
        this.hostHeader = getHostHeader(host, mssId, realm);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || mssId == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }

        if (rmeDataModel == RmeDataModel.CL) {
            if ((type <= 0) || (type >= 10)) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "type", "ReadReply" }));
            }
            callRme(MSS_P_FETCHAUTOREPLY);
        } else {
            if ((type <= 0) || (type >= 16)) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "type", "ReadReply" }));
            }
            url = getMsUrl(host, mssId, MSS_SL_FETCHAUTOREPLY);
            callRme(MSS_SL_FETCHAUTOREPLY);
        }
    }

    /**
     * Returns the Reply field
     * 
     * @return Reply
     */
    public Reply getReply() {
        return reply;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeStringArray(new String[] { mssId });
        outStream.writeInt(type);
        outStream.writeInt(0); // options - not implemented
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        reply = new Reply();
        reply.readReply(inStream, type);
        logEvents = inStream.readLogEvent();
    }

    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_FETCHAUTOREPLY));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeString(url);

        outStream.writeInt(type);
        outStream.writeInt(0); // options - not implemented
        outStream.flush();
    }

    @Override
    protected void receiveHttpData() throws IOException {
        // debugReceive();
        logEvents = inStream.readLogEvent();
        reply = new Reply();
        reply.readSLReply(inStream, type);
    }

}