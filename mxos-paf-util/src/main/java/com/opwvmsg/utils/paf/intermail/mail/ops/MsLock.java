/*
 * Copyright (c) 2014 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;

/**
 * This operation manipulates message store lock in MSS
 */
public class MsLock extends AbstractMssOperation {
    
    public final static String COMMAND_QUERY = "query";
    public final static String COMMAND_LOCK = "lock";
    public final static String COMMAND_TEMPLOCK = "templock";
    public final static String COMMAND_UNLOCK = "unlock";

    // input
    private final String msName;
    private final Map<String, List<String>> inputParams;
    
    // output
    private Map<String, List<String>> outputParams = null;

    // internal
    private final String url;
    private final String hostHeader;
    
    private final static String NAME_URL = "msurl";
    private final static String NAME_TYPE = "type";
    private final static String NAME_SOFTDEL = "ignoreSoftDelete";
    
    private final static String NAME_STATUS = "status";
    private final static String NAME_SOFTDELETED = "softdeleted";
    
    private final static String STATE_LOCKED = "locked";
    private final static String STATE_TEMPLOCKED = "templocked";
    private final static String STATE_UNLOCKED = "unlocked";
    
    /**
     * Constructor.
     * 
     * @param host Message store host name.
     * @param msName Mailbox ID in MSS.
     * @param realm 
     * @param command Lock operation type.
     * @param inputParams Generic parameter container for options.
     */
    public MsLock(String host, String msName, String realm,
            final String command, final Map<String, List<String>> inputParams) {
        super(RmeDataModel.Leopard);
        this.msName = msName;
        this.host = resolveClusterToMSS(host, msName);
        url = getMsUrl(host, msName, MSS_SL_MSLOCK);
        this.hostHeader = getHostHeader(host, msName, realm);

        // build call parameters
        this.inputParams = new LinkedHashMap<String, List<String>>();
        List<String> values = new ArrayList<String>();
        values.add(url);
        this.inputParams.put(NAME_URL, values);

        values = new ArrayList<String>();
        values.add(command);
        this.inputParams.put(NAME_TYPE, values);

        if (inputParams.containsKey(NAME_SOFTDEL)) {
            this.inputParams.put(NAME_SOFTDEL, inputParams.get(NAME_SOFTDEL));
        }
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || msName == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (rmeDataModel != RmeDataModel.Leopard) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_SL_MAILBOXACCESSINFO", host,
                            "dataModel" + rmeDataModel }));
        } else {
            callRme(MSS_SL_MSLOCK);
        }

    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        // Legacy RME operation is not supported
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // Legacy RME operation is not supported
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_MSLOCK));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeGenericParametersMap(inputParams);

        outStream.flush();
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    @Override
    protected void receiveHttpData() throws IOException {
        outputParams = inStream.readGenericParametersMap();
        logEvents = inStream.readLogEvent();
    }
    
    /**
     * @return Message store lock or accessControl status.
     */
    public String getLockStatus() {
        String value = null;
        if (outputParams != null && outputParams.containsKey(NAME_STATUS)) {
            value = outputParams.get(NAME_STATUS).get(0);
        }
        return value;
    }

    /**
     * @return Message store soft delete status. Returns null if MSS does not
     *         return the corresponding parameter.
     */
    public Boolean getMsSoftDeleted() {
        Boolean value = null;
        if (outputParams != null && outputParams.containsKey(NAME_SOFTDELETED)) {
            value = Boolean.valueOf(outputParams.get(NAME_SOFTDELETED).get(0));
        }
        return value;
    }

    /**
     * @return Generic output parameters map.
     */
    public Map<String, List<String>> getOutputParams() {
        return outputParams;
    }
    
    /**
     * Resolves command name by target state.
     * 
     * @param targetState The state to aim
     * @return MSS_SL_MSLOCK command name (operation type)
     */
    public static String resolveCommand(final String targetState) {
        String result = null;
        
        if (targetState.equals(STATE_LOCKED)) {
            result = COMMAND_LOCK;
        } else if (targetState.equals(STATE_TEMPLOCKED)) {
            result = COMMAND_TEMPLOCK;
        } else if (targetState.equals(STATE_UNLOCKED)) {
            result = COMMAND_UNLOCK;
        }
        
        return result;
    }
}
