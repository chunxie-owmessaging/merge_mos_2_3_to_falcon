/*
 * Copyright (c) 2003 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/BasicStackPool.java#1 $
 */

package com.opwvmsg.utils.paf.util;

import java.util.ArrayList;
import java.util.List;

/**
 * This class implements the <code>Pool</code> interface using a stack as its
 * Collection.  getItem will return the item most recently added by putItem.
 */
public class BasicStackPool implements Pool {

    /**
     * The items currently retained in this pool.
     */
    private List items;

    /**
     * The capacity of this pool
     */
    private int capacity;

    /**
     * Constructs a new, empty pool.
     */
    public BasicStackPool() {
        items = new ArrayList();
    }

    /**
     * Constructs a new, empty pool with the specified capacity.
     *
     * @param capacity the maximum number of items to be retained
     *          by this pool.
     */
    public BasicStackPool(int capacity) {
        if (capacity > 0) {
            this.capacity = capacity;
            items = new ArrayList(capacity);
        } else {
            items = new ArrayList();
        }
    }

    /**
     * Returns (and removes) an item from this pool.
     *
     * @return Object an available item or <code>null</code> if
     *          this pool is exhausted.
     */
    public Object getItem() {
        Object item = null;
        synchronized (getLock()) {
            if (items.size() > 0) {
                item = items.remove(items.size() - 1);
            }
        }
        return item;
    }

    /**
     * Adds the specified item to this pool.
     *
     * If the item cannot be added to this pool without exceeding
     * the capacity then the item is returned.
     *
     * @param item an <code>Object</code> to be placed into this pool
     *          for later use.
     *
     * @return Object the specified item if the capacity of the
     *          pool would be exceeded by adding it and
     *          <code>null</code> otherwise.
     */
    public Object putItem(Object item) {
        synchronized (getLock()) {
            if (item != null && (capacity == 0 || items.size() < capacity)) {
                items.add(item);
                return null;
            }
        }
        return item;
    }

    /**
     * Removes all items from this pool.
     */
    public void clear() {
        synchronized (getLock()) {
            items.clear();
        }
    }

    /**
     * Returns the number of items currently in this pool.
     *
     * @return int the number of items currently in this pool.
     */
    public int size() {
        synchronized (getLock()) {
            return items.size();
        }
    }

    /**
     * Returns the maximum number of items to be retained by this
     * pool.  
     *
     * When the size is equal to the capacity no further items are
     * retained by this pool.
     *
     * A value of zero indicates that this pool does not limit the
     * number of items retained.
     *
     * @return int the maximum number of items to be retained by this pool.
     */
    public int capacity() {
        return capacity;
    }

    /**
     * Returns the object used for synchronization by this pool.
     *
     * @return Object the object used by this pool for synchronization.
     */
    public Object getLock() {
        return this;
    }
    
    /**
     * Get the internal list of items; allowing subclasses to 
     * perform expanded operations on the stack.
     * 
     * @return List of items
     */
    protected List getItems() {
        return items;
    }
 
}
