/*
 *      ConfigProvider.java
 *
 *      Copyright (c) 2002 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/config/spi/ConfigProvider.java#1 $
 *
 */

package com.opwvmsg.utils.paf.config.spi;

import java.util.Map;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;

/**
 * This interface defines the methods that each service provider must
 * implement.
 *
 * Its two methods are small. One is a wrapper that returns a Config object,
 * and the other is used to test the provider type.
 *
 * @author Conrad Damon
 * @author Mark Abbott
 * @version $Revision: #1 $
 */

public interface ConfigProvider {

  /**
   * Gets a Config object using the given setup information.
   *
   * @param setup a map of setup information needed by the provider
   * @throws ConfigException if there was a problem getting to the config
   *   data source, for example a failed read of a properties file.
   *
   * @return a Config object
   */
  public Config getConfig(Map setup) throws ConfigException;

  /**
   * Tests whether this provider is capable of providing the 
   * Config API for the specified provider type.
   *
   * @param type the desired type of provider
   *
   * @return <code>true</code> if the named type is handled by
   *     this provider, else <code>false</code>
   */
  public boolean isType(String type);
}
