/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/extsrv/clients/MessageExamine.java#1 $ 
 *  H- */
package com.opwvmsg.utils.paf.intermail.extsrv.clients;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.intermail.extsrv.AbstractExtSrvOperation;
import com.opwvmsg.utils.paf.intermail.extsrv.ExtensionService;
import com.opwvmsg.utils.paf.intermail.extsrv.ImxArray;
import com.opwvmsg.utils.paf.intermail.extsrv.ImxData;
import com.opwvmsg.utils.paf.intermail.extsrv.ImxStruct;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.Msg;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This operation represents the message examine (legal intercept) client. It is
 * not really an RME operation (although it uses RME as the transport protocol).
 * A specific schema is hard coded into this implementation. If it ever changes,
 * this operation will need to change, too.
 */
public class MessageExamine extends AbstractExtSrvOperation {

    // input data
    private String account;
    private String serverName;
    private String peerIp;
    private String command;
    private int offset;
    private int length;
    private String pathname;
    private String mssHost;
    private String mssId;

    //output results
    private String globalDisposition;

    // Note: Extension services do not use LogEvents
    private boolean isFault;
    private int faultCode;
    private String faultString;
    private Msg[] msgs;

    //internal
    private ImxStruct clientInfo;
    private ImxArray mailgrams;
    private ImxArray dispArray;

    // message examine
    private final static String MESSAGE_EXAMINE = "MessageExamine";
    private final static String MEDISPOSITIONS = "MEDispositions";
    private final static String MECLIENTINFO = "MEClientInfo";
    private final static String MEMAILGRAMS = "MEMailgrams";
    public final static String MESSAGE_READ_RESULTS = "MessageReadResults";
    public final static String MESSAGE_WRITE_REQUEST = "MessageWriteRequest";

    private final static String[] rankedDispositions = { "keep", "drop",
            "drop.anonymous_log", "drop.no_log" };
    private static final String DISPOSITION_KEEP = "keep";
    private static final String DISPOSITION_DROP = "drop";
    private static final String DISPOSITION_ANONYMOUS_DROP = "drop.anonymous_log";
    private static final String DISPOSITION_NO_LOG_DROP = "drop.no_log";
    private static HashMap globalDispositionRanks = new HashMap(4, 1);
    static {
        globalDispositionRanks.put(DISPOSITION_KEEP, new Integer(0));
        globalDispositionRanks.put(DISPOSITION_DROP, new Integer(1));
        globalDispositionRanks.put(DISPOSITION_ANONYMOUS_DROP, new Integer(2));
        globalDispositionRanks.put(DISPOSITION_NO_LOG_DROP, new Integer(3));
    }

    //me mailgram
    private final static String MESSAGE_DATA_HEADERS = "MessageDataHeaders";
    private final static String MESSAGE_DATA_BODY = "MessageDataBody";
    private final static String MESSAGE_DATA_ID = "MessageDataId";
    private final static String MESSAGE_DATA_FOLDER_PATH = "MessageDataFolderPath";
    private final static String MESSAGE_DATA_UID = "MessageDataUid";
    private final static String MESSAGE_DATA_OFFSET = "MessageDataOffset";
    private final static String MESSAGE_DATA_LENGTH = "MessageDataLength";

    //me client info
    private final static String USER_ACCOUNT = "UserAccount";
    private final static String SERVER_NAME = "ServerName";
    private final static String PEER_IP = "PeerIP";
    private final static String COMMAND = "Command";
    private final static String MSS_HOST = "MSSHost";
    private final static String MAILBOX_ID = "MailboxID";

    private static final Logger logger = Logger.getLogger(MessageExamine.class);

    /**
     * Constructor for MessageExamine Client
     * 
     * @param account the name of the user account to report
     * @param serverName the name of the application (i.e. "webedge")
     * @param peerIp the IP address of the client
     * @param command a String representation of the current operation
     * @param msgs the messages that the client may be sending or receiving
     * @param offset if the messages are partial, this is the offset
     * @param length the requested length of the message
     * @param readRequest - true if this should use the MESSAGE_READ_REQUEST
     *            hook false if this should use MESSAGE_WRITE_RESPONSE
     * @param pathname the folder pathname for the message(s)
     * @param mssHost the mss hostname where the account is located
     * @param mssId the mailbox id of the account
     * 
     */
    public MessageExamine(String account, String serverName, String peerIp,
            String command, Msg[] msgs, int offset, int length, boolean readRequest,
            String pathname, String mssHost, String mssId) {

        this.account = account;
        this.serverName = serverName;
        this.peerIp = peerIp;
        this.command = command;
        this.msgs = msgs;
        this.length = length;
        if (this.length == 0) {
            // -1 indicates full message requested
            this.length = -1;
        }
        if (readRequest) {
            hookName = MESSAGE_READ_RESULTS;
        } else {
            hookName = MESSAGE_WRITE_REQUEST;
        }
        this.offset = offset;
        this.pathname = pathname;
        this.mssHost = mssHost;
        this.mssId = mssId;
        initializeHook(hookName);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        // construct client info
        if (msgs.length == 0) {
            return;
        }
        clientInfo = new ImxStruct(4);
        clientInfo.put(USER_ACCOUNT, account);
        clientInfo.put(SERVER_NAME, serverName);
        clientInfo.put(PEER_IP, peerIp);
        clientInfo.put(COMMAND, command);
        clientInfo.put(MSS_HOST, mssHost);
        clientInfo.put(MAILBOX_ID, mssId);

        mailgrams = buildMailgrams();

        Iterator servicesIterator = getServicesIterator();

        while (servicesIterator.hasNext()) {
            currentService = (ExtensionService) servicesIterator.next();
            boolean error = false;
            try {
                callRme(EXT_RME_FASTCALL);
            } catch (IntermailException ie) {
                logger.log(ie.getLog4jLevel(), ie.getFormattedString());
                error = true;
            } catch (ClassCastException c) {
                logger.log(Level.ERROR,
                           LogEvent.formatLogString("Rme.ProtocolErr",
                                                    new String[0]));
                error = true;
            }
            // if RME error, or extension fault, and was mandatory,
            // fail with drop, don't contact other services.
            if (error || isFault) {
                error = true;
            } else {
                if (dispArray == null || dispArray.size() != msgs.length) {
                    logger.log(Level.ERROR,
                               LogEvent.formatLogString("Ext.ServiceResponseWrongNumberOfDispositions",
                                                        new String[] { currentService.getName() }));
                    error = true;
                } else {
                    // get disposition from reply.. add to global
                    // disposition
                    for (int i = 0; i < dispArray.size(); i++) {
                        try {
                            String disp = dispArray.getUtf8String(i);
                            if (!updateGlobalDisposition(disp)) {
                                logger.log(Level.ERROR,
                                           LogEvent.formatLogString("Ext.ServiceResponseInvalidDisposition",
                                                                    new String[] {
                                                                            currentService.getName(),
                                                                            disp }));
                                error = true;
                            }
                        } catch (ClassCastException c) {
                            logger.log(Level.ERROR,
                                       LogEvent.formatLogString("Rme.ProtocolErr",
                                                                new String[0]));
                            error = true;
                        }
                    }
                }
            }

            if (error) {
                if (currentService.isMandatory()) {
                    // make sure we are dropping
                    if (DISPOSITION_KEEP.equals(globalDisposition)) {
                        globalDisposition = DISPOSITION_DROP;
                    }
                    // do not continue
                    break;
                } else {
                    // just keep going
                    globalDisposition = DISPOSITION_KEEP; // as if it was
                    // keep
                }
            }
            if (!DISPOSITION_KEEP.equals(globalDisposition)) { // Keep
                // don't call other services
                break;
            }
        }
    }

    /**
     * All reponses from each extension service are collected into one global
     * dispostion. This function returns the String value of the disposition.
     * 
     * @return the String value of the global disposition.
     */
    public String getGlobalDisposition() {
        return globalDisposition;
    }

    /**
     * Updates the global disposition based on the disposition string passed in.
     * If the disposition is ranked higer than the current global disposition,
     * the global value will be updated.
     * 
     * @param dispIn the disposition
     * @return true if disposition was valid, false if not
     */
    private boolean updateGlobalDisposition(String dispIn) {
        Integer currentRank = ((Integer) globalDispositionRanks.get(globalDisposition));
        Integer testRank = ((Integer) globalDispositionRanks.get(dispIn));

        if (testRank == null) {
            return false;
        }
        if (currentRank == null || (testRank.intValue() > currentRank.intValue())) {
            globalDisposition = dispIn;
        }
        return true;
    }

    /**
     * A private convenience method that builds the MEMailgram for the request.
     * 
     * @return ImxArray that represents the mailgram
     */
    private ImxArray buildMailgrams() {
        ImxArray mailgrams = new ImxArray(msgs.length);
        for (int i = 0; i < msgs.length; i++) {
            // for each message, build the MEMailgram and
            // add it to the array
            Msg msg = msgs[i];
            String[] hdrs = msg.getHeadersArray();
            StringBuffer headers = new StringBuffer(1024);
            if (hdrs.length > 0) {
                for (int j = 0; j < hdrs.length; j++) {
                    if (hdrs[j] != null && hdrs[j].length() > 0) {
                        headers.append(hdrs[j]);
                        headers.append("\r\n");
                    }
                }
            }

            // convert the message byte array to a string
            // which is what the ImxStruct expects
            byte[] byteArr = msg.getText();
            String text = null;
            if (byteArr != null) {
                char[] charArr = new char[byteArr.length];
                for (int j = 0; j < byteArr.length; j++) {
                    charArr[j] = (char) byteArr[j];
                }
                text = new String(charArr);
            } else {
                text = "";
            }

            ImxStruct mailgram = new ImxStruct(6);
            mailgram.put(MESSAGE_DATA_HEADERS, headers.toString());
            mailgram.put(MESSAGE_DATA_BODY, text);
            mailgram.put(MESSAGE_DATA_ID, msg.getMsgId());
            mailgram.put(MESSAGE_DATA_FOLDER_PATH, pathname);
            mailgram.put(MESSAGE_DATA_UID, msg.getUid());
            mailgram.put(MESSAGE_DATA_OFFSET, offset);
            mailgram.put(MESSAGE_DATA_LENGTH, length);
            mailgrams.add(mailgram);
        }
        return mailgrams;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        // send IMX Procedure call
        outStream.writeString(MESSAGE_EXAMINE);
        //send params, (IMX_ARRAY of IMX_DATA)
        ImxArray params = new ImxArray(3);
        // always send the lowercase service name
        params.add(currentService.getName().toLowerCase());
        params.add(hookName);
        ImxStruct data = new ImxStruct(2);
        data.put(MECLIENTINFO, clientInfo);
        data.put(MEMAILGRAMS, mailgrams);
        params.add(data);
        ImxData paramsData = new ImxData(params);
        paramsData.writeImxData(outStream);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        isFault = inStream.readBoolean();
        if (isFault) {
            faultCode = inStream.readInt();
            faultString = inStream.readString();
            //even though there is a fault, the rme itself was ok
        } else {
            try {
                ImxArray resp = (new ImxData(inStream)).getArray();
                ImxStruct imxParams = resp.getStruct(0);
                dispArray = imxParams.getArray(MEDISPOSITIONS);
            } catch (NullPointerException npe) {
                // this will report an error when the # of dispositions
                // is checked later.
                dispArray = null;
            }
        }
    }

	@Override
	protected void constructHttpData() throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void receiveHttpData() throws IOException {
		// TODO Auto-generated method stub
		
	}
}
