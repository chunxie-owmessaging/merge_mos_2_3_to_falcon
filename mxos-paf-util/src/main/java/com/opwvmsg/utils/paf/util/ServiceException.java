/*
 *      ServiceException.java
 *
 *      Copyright (c) 2002 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/ServiceException.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util;


/**
 * An exception indicating that an error has occured while
 * processing provider configuration files.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public class ServiceException extends RuntimeException {

    /**
     * Constructs an instance with the specified message.
     */
    public ServiceException(String message) {
        super(message);
    }

}
