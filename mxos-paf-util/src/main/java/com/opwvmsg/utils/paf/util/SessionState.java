/*
 * Copyright (c) 2004-2005 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/SessionState.java#1 $
 */
package com.opwvmsg.utils.paf.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;


import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.util.Cache;
import com.opwvmsg.utils.paf.util.KeyInternCache;

/**
 * Object containing state information that is stored in session scope.
 *
 * @author Forrest Girouard
 * @version $Revision: #1 $
 */
public abstract class SessionState {

    private static final Logger logger =
            Logger.getLogger(SessionState.class);

    private static class TransientWrapper implements Serializable {
        transient Object content;

        public TransientWrapper() {
            content = null;
        }

        public TransientWrapper(Object content) {
            this.content = content;
        }

        public Object getContent() {
            return content;
        }
    }

    /**
     * The name of the session attribute which contains the
     * data for this session state.
     */
    private final String name;

    /**
     * A reference to the session for this session state.
     */
    private final HttpSession session;

    /**
     * The data, name/value map, for this session state.
     */
    private Cache data;

    private static final String SESSIONSTATE_CACHE = "sessionStateCache";
    private static final String DEFAULT_CACHE =
            "com.openwave.paf.util.KeyInternCache";

    /**
     * The <code>Cache</code> implementation currently in use.
     */
    private static Class cacheClass;

    static {
        try {
            String className = Config.getInstance().get(
                    SESSIONSTATE_CACHE, DEFAULT_CACHE);
            if (className != null && className.length() > 0) {
                cacheClass = Class.forName(className);
                if (!Cache.class.isAssignableFrom(cacheClass)) {
                    throw new ConfigException(
                            SESSIONSTATE_CACHE + ": " + className +
                            ": does not implement required Cache interface" +
                            ": using default: " + DEFAULT_CACHE);
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Using " + className +
                                " for session state data.");
                    }
                }
            }
        } catch (ClassNotFoundException cnf) {
            logger.warn(cnf);
        } catch (ConfigException ce) {
            logger.warn(ce);
        } finally {
            if (cacheClass == null) {
                try {
                    cacheClass = Class.forName(DEFAULT_CACHE);
                } catch (ClassNotFoundException cnf) {
                    logger.error(cnf);
                    throw new IllegalStateException(cnf.getMessage());
                }
            }
        }
    }

    /**
     * Constructor for a basic session state.
     *
     * @param name the name of the <code>HttpSession</code> attribute
     * @param session the <code>HttpSession</code> in which to store
     *        the data for this session state as an attribute
     */
    protected SessionState(String name, HttpSession session) {
        this.name = name;
        this.session = session;
        this.data = (Cache)session.getAttribute(name);
    }

    /**
     * True if the data already exists in the session.
     *
     * @return <code>true</code> if the data already exists in the
     *        session and <code>false</code> otherwise
     */
    public boolean exists() {
        return data != null;
    }

    /**
     * Returns the name of the session attribute which contains the
     * data for this session state.
     *
     * @return the name of the <code>HttpSession</code> attibute in
     *        which the data for this session state is stored
     */
    protected String getName() {
        return name;
    }

    /**
     * Returns the object bound with the specified name in this
     * session state, or <code>null</code> if no object is bound under the
     * name.
     *
     * @param name a string specifying the name of the object
     * @return the object with the specified name
     */
    public Object getProperty(String name) {
        Object value = null;
        if (data != null) {
            value = data.get(name);
        }
        if (value instanceof TransientWrapper) {
            value = ((TransientWrapper)value).getContent();
        }
        if (value == null) {
            removeProperty(name);
        }
        return value;
    }

    /**
     * Returns an <code>Enumeration</code> of <code>String</code>
     * objects containing the names of all the objects bound to this
     * state.
     *
     * @return an <code>Enumeration</code> of <code>String</code>
     *        objects specifying the names of all the objects bound
     *        to this state
     */
    public Enumeration getPropertyNames() {
        if (data != null) {
            return Collections.enumeration(data.getMap().keySet());
        }
        return Collections.enumeration(new HashSet());
    }

    /**
     * Returns a <code>Map</code> of this state's data.
     *
     * @return an <code>Map</code> of this state's data
     */
    public Map getMap() {
        if (data == null) {
            data = getCache();
        }
        return data.getMap();
    }

    /**
     * Removes the object bound with the specified name from this
     * state.
     *
     * @param name the name of the object to remove from this state
     */
    public void removeProperty(String name) {
        if (data != null) {
            data.remove(name);
            session.setAttribute(getName(), data);
        }
    }

    /**
     * Removes all objects bound to this state.
     */
    public void clear() {
        if (data != null) {
            data.clear();
            session.setAttribute(getName(), data);
        }
    }

    /**
     * Binds an object to this state, using the name specified.
     *
     * @param name the name to which the object is bound
     * @param value the object to be bound
     */
    public void setProperty(String name, Object value) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        }
        if (data == null) {
            data = getCache();
        }
        if (value != null) {
            if (!(value instanceof Serializable)) {
                value = new TransientWrapper(value);
            }
            data.put(name, value);
            if (value instanceof Collection) {
                data.put(name + "Size", "" + ((Collection)value).size());
            }
        } else {
            data.remove(name);
            data.remove(name + "Size");
        }
        session.setAttribute(getName(), data);
    }

    /**
     * Informs the state that the given property has been modified.
     * In other words, the value of the given property has been
     * modified and, for example, should persisted as appropriate.
     *
     * @param name the name to which the object is bound
     */
    public void modifiedProperty(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        }
        setProperty(name, getProperty(name));
    }

    /**
     * Binds the objects in the map to this state.
     *
     * @param properties a <code>Map</code> of (name, object) pairs
     */
    public void setProperties(Map properties) {
        if (properties != null && properties.size() > 0) {
            data = getCache();
            Iterator iterator = properties.keySet().iterator();
            while (iterator.hasNext()) {
                String name = (String)iterator.next();
                setProperty(name, properties.get(name));
            }
            session.setAttribute(getName(), data);
        }
    }

    /**
     * Allocate a new <code>Cache</code> instance.
     */
    protected Cache getCache() {
        Cache cache = null;
        try {
            cache = (Cache)cacheClass.newInstance();
        } catch (IllegalAccessException iae) {
            logger.debug(iae);
        } catch (InstantiationException ie) {
            logger.debug(ie);
        } finally {
            if (cache == null) {
                try {
                    cacheClass = Class.forName(DEFAULT_CACHE);
                    cache = (Cache)cacheClass.newInstance();
                } catch (Throwable throwable) {
                    logger.error(throwable);
                    throw new IllegalStateException(throwable.getMessage());
                }
            }
        }
        return cache;
    }
}
