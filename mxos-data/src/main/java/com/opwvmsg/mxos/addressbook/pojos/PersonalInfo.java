
package com.opwvmsg.mxos.addressbook.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * PersonalInfo object of Contact
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "maritalStatus",
    "spouseName",
    "homeWebpage",
    "children",
    "address",
    "communication",
    "events"
})
public class PersonalInfo implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("maritalStatus")
    private com.opwvmsg.mxos.data.enums.MxosEnums.MaritalStatus maritalStatus;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("spouseName")
    private java.lang.String spouseName;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("homeWebpage")
    private java.lang.String homeWebpage;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("children")
    private java.lang.String children;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("address")
    private Address address;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("communication")
    private Communication communication;
    @org.codehaus.jackson.annotate.JsonProperty("events")
    private List<Event> events;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("maritalStatus")
    public com.opwvmsg.mxos.data.enums.MxosEnums.MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("maritalStatus")
    public void setMaritalStatus(com.opwvmsg.mxos.data.enums.MxosEnums.MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("spouseName")
    public java.lang.String getSpouseName() {
        return spouseName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("spouseName")
    public void setSpouseName(java.lang.String spouseName) {
        this.spouseName = spouseName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("homeWebpage")
    public java.lang.String getHomeWebpage() {
        return homeWebpage;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("homeWebpage")
    public void setHomeWebpage(java.lang.String homeWebpage) {
        this.homeWebpage = homeWebpage;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("children")
    public java.lang.String getChildren() {
        return children;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("children")
    public void setChildren(java.lang.String children) {
        this.children = children;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("communication")
    public Communication getCommunication() {
        return communication;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("communication")
    public void setCommunication(Communication communication) {
        this.communication = communication;
    }

    @org.codehaus.jackson.annotate.JsonProperty("events")
    public List<Event> getEvents() {
        return events;
    }

    @org.codehaus.jackson.annotate.JsonProperty("events")
    public void setEvents(List<Event> events) {
        this.events = events;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
