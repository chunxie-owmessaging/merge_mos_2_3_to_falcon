
package com.opwvmsg.mxos.addressbook.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Communication object of Contact
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "preference",
    "phone1",
    "phone2",
    "voip",
    "mobile",
    "fax",
    "email",
    "imAddress"
})
public class Communication implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("preference")
    private java.lang.String preference;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("phone1")
    private java.lang.String phone1;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("phone2")
    private java.lang.String phone2;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("voip")
    private java.lang.String voip;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mobile")
    private java.lang.String mobile;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("fax")
    private java.lang.String fax;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("email")
    private java.lang.String email;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("imAddress")
    private java.lang.String imAddress;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("preference")
    public java.lang.String getPreference() {
        return preference;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("preference")
    public void setPreference(java.lang.String preference) {
        this.preference = preference;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("phone1")
    public java.lang.String getPhone1() {
        return phone1;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("phone1")
    public void setPhone1(java.lang.String phone1) {
        this.phone1 = phone1;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("phone2")
    public java.lang.String getPhone2() {
        return phone2;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("phone2")
    public void setPhone2(java.lang.String phone2) {
        this.phone2 = phone2;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("voip")
    public java.lang.String getVoip() {
        return voip;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("voip")
    public void setVoip(java.lang.String voip) {
        this.voip = voip;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mobile")
    public java.lang.String getMobile() {
        return mobile;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mobile")
    public void setMobile(java.lang.String mobile) {
        this.mobile = mobile;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("fax")
    public java.lang.String getFax() {
        return fax;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("fax")
    public void setFax(java.lang.String fax) {
        this.fax = fax;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("email")
    public java.lang.String getEmail() {
        return email;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("email")
    public void setEmail(java.lang.String email) {
        this.email = email;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("imAddress")
    public java.lang.String getImAddress() {
        return imAddress;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("imAddress")
    public void setImAddress(java.lang.String imAddress) {
        this.imAddress = imAddress;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
