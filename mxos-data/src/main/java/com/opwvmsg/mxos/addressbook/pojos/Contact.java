
package com.opwvmsg.mxos.addressbook.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Contact object of mxos
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "contactBase",
    "name",
    "workInfo",
    "personalInfo",
    "image",
    "subscriptionLinks",
    "groupLinks"
})
public class Contact implements Serializable
{

    /**
     * Base object of contact
     * (Required)
     * 
     */
    @JsonProperty("contactBase")
    private ContactBase contactBase;
    /**
     * Name object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("name")
    private Name name;
    /**
     * WorkInfo object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("workInfo")
    private WorkInfo workInfo;
    /**
     * PersonalInfo object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("personalInfo")
    private PersonalInfo personalInfo;
    /**
     * Image object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("image")
    private Image image;
    /**
     * subscriptionLinks
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("subscriptionLinks")
    private Set<SubscriptionLink> subscriptionLinks = new HashSet<SubscriptionLink>();
    /**
     * groupLinks
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("groupLinks")
    private Set<GroupLink> groupLinks = new HashSet<GroupLink>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Base object of contact
     * (Required)
     * 
     */
    @JsonProperty("contactBase")
    public ContactBase getContactBase() {
        return contactBase;
    }

    /**
     * Base object of contact
     * (Required)
     * 
     */
    @JsonProperty("contactBase")
    public void setContactBase(ContactBase contactBase) {
        this.contactBase = contactBase;
    }

    /**
     * Name object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("name")
    public Name getName() {
        return name;
    }

    /**
     * Name object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("name")
    public void setName(Name name) {
        this.name = name;
    }

    /**
     * WorkInfo object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("workInfo")
    public WorkInfo getWorkInfo() {
        return workInfo;
    }

    /**
     * WorkInfo object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("workInfo")
    public void setWorkInfo(WorkInfo workInfo) {
        this.workInfo = workInfo;
    }

    /**
     * PersonalInfo object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("personalInfo")
    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    /**
     * PersonalInfo object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("personalInfo")
    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    /**
     * Image object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("image")
    public Image getImage() {
        return image;
    }

    /**
     * Image object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("image")
    public void setImage(Image image) {
        this.image = image;
    }

    /**
     * subscriptionLinks
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("subscriptionLinks")
    public Set<SubscriptionLink> getSubscriptionLinks() {
        return subscriptionLinks;
    }

    /**
     * subscriptionLinks
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("subscriptionLinks")
    public void setSubscriptionLinks(Set<SubscriptionLink> subscriptionLinks) {
        this.subscriptionLinks = subscriptionLinks;
    }

    /**
     * groupLinks
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("groupLinks")
    public Set<GroupLink> getGroupLinks() {
        return groupLinks;
    }

    /**
     * groupLinks
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("groupLinks")
    public void setGroupLinks(Set<GroupLink> groupLinks) {
        this.groupLinks = groupLinks;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
