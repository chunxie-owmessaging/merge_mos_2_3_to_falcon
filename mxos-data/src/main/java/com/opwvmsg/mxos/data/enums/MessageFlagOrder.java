/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

/**
 * Message flag order.
 *
 * @author mxos-dev
 */
public enum MessageFlagOrder {
    /**
     * <tt>FlagRecent</tt>: Specifies flagsRecent.
     *
     * @since mxos 2.0
     */
    flagrecent,

    /**
     * <tt>FlagSeen</tt>: Specifies flagSeen.
     *
     * @since mxos 2.0
     */
    flagseen,

    /**
     * <tt>FlagUnread</tt>: Specifies flagUnread.
     *
     * @since mxos 2.0
     */
    flagunread,

    /**
     * <tt>FlagAns</tt>: Specifies flagAns.
     *
     * @since mxos 2.0
     */
    flagans,

    /**
     * <tt>FlagFlagged</tt>: Specifies flagFlagged.
     *
     * @since mxos 2.0
     */
    flagflagged,

    /**
     * <tt>FlagDel</tt>: Specifies flagDel.
     *
     * @since mxos 2.0
     */
    flagdel,

    /**
     * <tt>FlagRes</tt>: Specifies flagRes.
     *
     * @since mxos 2.0
     */
    flagres,

    /**
     * <tt>FlagBounce</tt>: Specifies flagBounce.
     *
     * @since mxos 2.0
     */
    flagbounce,

    /**
     * <tt>FlagPriv</tt>: Specifies flagPriv.
     *
     * @since mxos 2.0
     */
    flagpriv,

    /**
     * <tt>FlagDraft</tt>: Specifies flagDraft.
     *
     * @since mxos 2.0
     */
    flagdraft
}
