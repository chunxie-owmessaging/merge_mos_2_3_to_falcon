
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * SieveFilters object of mailReceipt filters
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "sieveFilteringEnabled",
    "mtaFilter",
    "rmFilter",
    "blockedSenders",
    "blockedSenderAction",
    "blockedSenderMessage",
    "rejectBouncedMessage"
})
public class SieveFilters implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("sieveFilteringEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType sieveFilteringEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("mtaFilter")
    private java.lang.String mtaFilter;
    @org.codehaus.jackson.annotate.JsonProperty("rmFilter")
    private java.lang.String rmFilter;
    @org.codehaus.jackson.annotate.JsonProperty("blockedSenders")
    private List<java.lang.String> blockedSenders;
    @org.codehaus.jackson.annotate.JsonProperty("blockedSenderAction")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BlockedSenderAction blockedSenderAction;
    @org.codehaus.jackson.annotate.JsonProperty("blockedSenderMessage")
    private java.lang.String blockedSenderMessage;
    @org.codehaus.jackson.annotate.JsonProperty("rejectBouncedMessage")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType rejectBouncedMessage;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("sieveFilteringEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSieveFilteringEnabled() {
        return sieveFilteringEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("sieveFilteringEnabled")
    public void setSieveFilteringEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType sieveFilteringEnabled) {
        this.sieveFilteringEnabled = sieveFilteringEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mtaFilter")
    public java.lang.String getMtaFilter() {
        return mtaFilter;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mtaFilter")
    public void setMtaFilter(java.lang.String mtaFilter) {
        this.mtaFilter = mtaFilter;
    }

    @org.codehaus.jackson.annotate.JsonProperty("rmFilter")
    public java.lang.String getRmFilter() {
        return rmFilter;
    }

    @org.codehaus.jackson.annotate.JsonProperty("rmFilter")
    public void setRmFilter(java.lang.String rmFilter) {
        this.rmFilter = rmFilter;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blockedSenders")
    public List<java.lang.String> getBlockedSenders() {
        return blockedSenders;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blockedSenders")
    public void setBlockedSenders(List<java.lang.String> blockedSenders) {
        this.blockedSenders = blockedSenders;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blockedSenderAction")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BlockedSenderAction getBlockedSenderAction() {
        return blockedSenderAction;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blockedSenderAction")
    public void setBlockedSenderAction(com.opwvmsg.mxos.data.enums.MxosEnums.BlockedSenderAction blockedSenderAction) {
        this.blockedSenderAction = blockedSenderAction;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blockedSenderMessage")
    public java.lang.String getBlockedSenderMessage() {
        return blockedSenderMessage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blockedSenderMessage")
    public void setBlockedSenderMessage(java.lang.String blockedSenderMessage) {
        this.blockedSenderMessage = blockedSenderMessage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("rejectBouncedMessage")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getRejectBouncedMessage() {
        return rejectBouncedMessage;
    }

    @org.codehaus.jackson.annotate.JsonProperty("rejectBouncedMessage")
    public void setRejectBouncedMessage(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType rejectBouncedMessage) {
        this.rejectBouncedMessage = rejectBouncedMessage;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
