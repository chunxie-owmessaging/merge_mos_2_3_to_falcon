/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All LDAP attributes used by this application.
 *
 * @see <i>Email Mx LDAP schema Reference Guide</i>
 * @author mxos-dev
 */
public enum FolderProperty implements DataMap.Property {

    /**
     * <tt>FolderId</tt>: Specifies Folderid for the Openwave Email application.
     * This id along with mailboxid uniquely identifies folder.
     *
     * @since mxos 2.0
     */
    folderId,

    /**
     * <tt>FolderName</tt>: Specifies FolderName for the Openwave Email
     * application.
     * This folderName along with mailboxid uniquely identifies folder.
     *
     * @since mxos 2.0
     */
    folderName,

    /**
     * <tt>Status</tt>: Specifies folder Status. For now this is reserved, in
     * future it can be used to mark folder soft deleted.
     *
     * @since mxos 2.0
     */
    status,

    /**
     * <tt>parentFolderId</tt>: Specifies parent FolderId. This is useful to
     * create tree hierarchy for folders.
     *
     * @since mxos 2.0
     */
    parentFolderId,

    /**
     * <tt>parentFolderName</tt>: Specifies parent FolderName. This is useful to
     * create tree hierarchy for folders.
     *
     * @since mxos 2.0
     */
    parentFolderName,

    /**
     * <tt>Name</tt>: Specifies folder Name.
     *
     * @since mxos 2.0
     */
    name,

    /**
     * <tt>nextUid</tt>: Specifies NextUID value.
     *
     * @since mxos 2.0
     */
    nextUid,

    /**
     * <tt>MsgArrTimeLB</tt>: Specifies message arrival time lower bound. This
     * is useful in maintenance. For now kept this attribute, in future we may
     * not need this. TODO: Check whether this attribute is required or not in
     * future.
     *
     * @since mxos 2.0
     */
    msgArrTimeLB,

    /**
     * <tt>MsgFistAccessedTimeLB</tt>: Specifies message fist access time lower
     * bound. This is useful in maintenance. For now kept this attribute, in
     * future we may not need this. TODO: Check whether this attribute is
     * required or not in future.
     *
     * @since mxos 2.0
     */
    msgFistAccessedTimeLB,

    /**
     * <tt>MsgLastAccessedTimeLB</tt>: Specifies message last access time lower
     * bound. This is useful in maintenance. For now kept this attribute, in
     * future we may not need this. TODO: Check whether this attribute is
     * required or not in future.
     *
     * @since mxos 2.0
     */
    msgLastAccessedTimeLB,

    /**
     * <tt>MsgExpTimeLB</tt>: Specifies message expiry time lower bound. This is
     * useful in maintenance. For now kept this attribute, in future we may not
     * need this. TODO: Check whether this attribute is required or not in
     * future.
     *
     * @since mxos 2.0
     */
    msgExpTimeLB,

    /**
     * <tt>NumMsgRead</tt>: Specifies count for read messages present inside a
     * folder.
     *
     * @since mxos 2.0
     */
    numMsgRead,

    /**
     * <tt>NumMsgs</tt>: Specifies count for total messages present inside a
     * folder.
     *
     * @since mxos 2.0
     */
    numMsgs,

    /**
     * <tt>SizeMsgRead</tt>: Specifies total size for read messages present
     * inside a folder.
     *
     * @since mxos 2.0
     */
    sizeMsgRead,

    /**
     * <tt>SizeMsgs</tt>: Specifies total size for messages present inside a
     * folder.
     *
     * @since mxos 2.0
     */
    sizeMsgs,

    /**
     * <tt>UidValidity</tt>: Specifies UID validity.
     *
     * @since mxos 2.0
     */
    uidValidity,

    /**
     * <tt>srcFolderId</tt>: Specifies source folderName.
     *
     * @since mxos 2.0
     */
    srcFolderName,

    /**
     * <tt>toFolderId</tt>: Specifies target folderName.
     *
     * @since mxos 2.0
     */
    toFolderName,

    /**
     * <tt>force</tt>: Specifies the force option
     *
     * @since mxos 2.0
     */
    force,
    
    /**
     * <tt>optionSupressMers</tt>: Specifies to suppress MERS events
     *
     * @since mxos 2.0
     */
     optionSupressMers, 
     
     /**
      * <tt>folderSubscribed</tt>: Specifies to folder Subscribed
      *
      * @since mxos 2.0
      */
     folderSubscribed,

     /**
      * <tt>ignorePopDeleted</tt>: Specifies the ignore pop deleted
      *
      * @since mxos 2.0
      */
     ignorePopDeleted
    
    /* Required for copy message operation */
}
