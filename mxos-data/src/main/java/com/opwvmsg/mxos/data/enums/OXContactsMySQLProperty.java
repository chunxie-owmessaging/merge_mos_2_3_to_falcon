package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All Contacts attributes referenced in OX MySQL by this application. The table that referred
 * is prg_contacts from OX MySQL database.
 * This can be used to map mOS attributes with actual DB attributes of Contacts. 
 * @author mxos_dev
 *
 */
public enum OXContactsMySQLProperty implements DataMap.Property {
	
	//OX workInfo attributes
	field65,
	
	//OX personalInfo attributes
	field66,
	
	//OX email3 attribute
	field67

}
