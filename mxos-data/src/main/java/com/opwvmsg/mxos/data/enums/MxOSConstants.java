/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

/**
 * Constants for MxOS.
 *
 * @author mxos-dev
 */
public interface MxOSConstants {
    String MXOS_HOME = "MXOS_HOME";
    String UTF8 = "UTF-8";
    int ROOT_FOLDER_ID = 0;
    int INBOX_ID = 1;
    long BASE_UID = 999;
    long INVALID_UID = -1;
    short TOTAL_MSG_FLAGS = 10;
    int SOFT_DELETE = 1;
    int NOT_SOFT_DELETE = 0;
    int INVALID_MSG_SHARING_COUNT = -1;
    int MAX_SEARCH_ROWS = 1;
    int SEC_TO_MILLISEC = 1000;
    int MIN_TO_MILLISEC = 60000;
    String SPACE = " ";
    String SINGLE_QUOTE = "'";
    String FLAG_NOT_SET = "0";
    String COLON = ":";
    String PIPE = "\\|";
    String DOUBLE_QUOTE = "\"";
    String ESCAPED_DOUBLE_QUOTE = "\"\"";
    String AT_THE_RATE = "@";
    String PERIOD = ".";
    String EQUALS = "=";
    String AND = "&&";
    String OR = "||";
    String STRING = "string";
    String INTEGER = "integer";
    String DATE = "date";
    String OP_EQUALS = "==";
    String OP_LESS_EQUAL = "<=";
    String OP_GREATER_EQUAL = ">=";
    String OP_LESS = "<";
    String OP_GREATER = ">";
    String OP_CONTAINS = "~";
    String COMMA = ",";
    String YES = "yes";
    String NO = "no";
    String TRUE = "true";
    String FALSE = "false";
    String QUARANTINE = "quarantine";
    String TAG = "tag";
    String HEADER = "header";
    String CARRIAGE_RETURN = "\r\n";
    String LINE_REGEXP = "\\r?\\n";
    String FIELD_REGEXP = "(?<!\\\\)&";
    String EQUAL_REGEXP = "(?<!\\\\)=";
    // Mx 8.0 - support large quotas, header summaries, and folder sort orders.
    int RME_MX8_VER = 163;
    // Mx 9.0 - SL MSS Search and Sort, RME over HTTP
    int RME_MX9_VER = 175;
    String INBOX = "INBOX";
    String FORWARD_SLASH="/";
    String list="list";
    String ROOT_FOLDER_UUID = "00000000-0000-1000-0000-000000000000";
    // SQL Query constants.
    String SQL_QUERY_PARAM = " = ? ";
    String SQL_COMMA = " , ";
    String SQL_AND = "AND";
    String SQL_WHERE = "WHERE";
    String SQL_LIKE = "LIKE";
    String SQL_ORDERBY = "ORDER BY";
    String SQL_AND_OPERATOR = "&";
    String SQL_EQUAL_OPERATOR = "=";
    String APP_URLENCODED =
            "application/x-www-form-urlencoded";
   
    String GENERIC_APP_NAME = "mxos";
    
    String DEFAULT_APP = "defaultApp";
   
    String LDAP_DATE_FORMAT = "yyyyMMddHHmmss'Z'";

    String IS_ADMIN = "isAdmin";

    String COS_BASE_ENTITY = "CosBase";

    String QUERY_SEARCH_CRITERIA_USING_MSISDN =
        "(|(smsCredentialMSISDN={msisdn})(smsPasswordRecoveryMSISDN={msisdn})"
        + "(smsServicesMSISDN={msisdn}))";
    String QUERY_SEARCH_CRITERIA_USING_MAILLOGIN = "(maillogin={userName})";
    String QUERY_SEARCH_CRITERIA_USING_MAIL = "(mail={email})";
    String QUERY_SEARCH_CRITERIA_USING_MAILORMAILALTERNATEADDRESS = "(|(mail={email})(mailalternateaddress={email}))";
    String LDAP_MAILBOX_DN = "mail={email},{domain}";
    
    String OX_DEFAULT_CONTACTS_FOLDER_ID = "22";
    String OX_DEFAULT_TASKS_FOLDER_ID = "23";
    String OX_CLIENT_ID = "com.owmessaging.appsuite.mos.gui.dhtml";
    String OX_AUTH_BYPASS_HEADER = "origin_ip";

    String QUERY = "query";
    String DEFAULT_MESSAGEFLAGS = "FFFFFFF";
    String WE_CONTACT_ENTRY_TYPE = "C";    
    String WE_GROUP_ENTRY_TYPE = "L";
    String WE_IM_PROVIDER_DOMAIN = ".com";
    String WE_DEFAULT_TIMEZONE = "GMT";
    
    String QUERY_SEARCH_CRITERIA_FOR_CONTACTS = "(?<=&&|\\|\\|)|(?=&&|\\|\\|)";
    
    String MAILBOXES_STRING = "mailboxes";
    
    String NOTIFY_MULTIMAP_ID = "subscriptions";
    String EXTENSION = "tmp";
    String SEPARATOR = "&";
    long KB = 1024;
    String ACS_URL = "acsURL";
    String SAML_ASSERTION = "samlAssertion";
    String LDAPSEARCHMAILBOXCRUD = "LDAPSEARCH";
    String LDAPMAILBOXCRUD = "LDAP";
    
    String LINE_FEED_ADRESSBOOK = "##LINEFEED##";
    
}
