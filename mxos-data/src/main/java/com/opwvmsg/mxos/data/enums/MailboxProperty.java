/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All LDAP attributes used by this application.
 * 
 * @see <i>Email Mx LDAP schema Reference Guide</i>
 * @author mxos-dev
 */
public enum MailboxProperty implements DataMap.Property {
    userName, domain, firstName, lastName, email, mailboxId, cosId, extensionAttributes, maxNumAliases, Sn, Cn, charset, locale, timezone, filteringEnabled, largeMailboxPlatformEnabled, maxMessages, maxStorageSizeKB, mobileMaxMessages, mobileMaxStorageSizeKB,maxMessagesSoftLimit, maxStorageSizeKBSoftLimit,mobileMaxMessagesSoftLimit,mobileMaxStorageSizeKBSoftLimit, quotaWarningThreshold, quotaBounceNotify, folderQuota,smtpAuthUser, // Check
                                                                                                                                                                                                         // //
                                                                                                                                                                                                         // this
    externalStoreAccessAllowed, maxExternalStoreSizeMB, maxAdminStorageSizeKB, maxAdminUsers, allowedDomain, newAllowedDomain, oldAllowedDomain, emailAlias, oldEmailAlias, newEmailAlias, oldForwardingAddress, newForwardingAddress, msisdn, msisdnStatus, lastMsisdnStatusChangeDate, currentMSISDN, newMSISDN, toAddress, message, type, maxNumAllowedDomains, customFields, billingId,
    // Base Search API
    query, sortKey, sortOrder, maxRows, scope,
    // Message metadata Search API
    sliceStart, sliceEnd, sliceCount,
    // Common param (between mailbox and meta)
    status, lastStatusChangeDate, notificationMailSentStatus,
    // Start of Meta param
    lastaccessdate, nummsgread, nummsgs, sizemsgread, sizemsgs, systemfolders,

    // WebMailFeatures
    businessFeaturesEnabled, fullFeaturesEnabled, lastFullFeaturesConnectionDate, allowPasswordChange,
    
    // WebMailFeaturesAddressBook
    maxContacts, maxGroups, maxContactsPerGroup, maxContactsPerPage, createContactsFromOutgoingEmails, 

    // general preferences
    parentalControlEnabled, recycleBinEnabled, preferredUserExperience, userThemes, preferredTheme, webmailMTA,

    // credential
    password, passwordStoreType, passwordRecoveryAllowed, passwordRecoveryPreference, passwordRecoveryMsisdn, passwordRecoveryMsisdnStatus, lastPasswordRecoveryMsisdnStatusChangeDate, passwordRecoveryEmail, passwordRecoveryEmailStatus, lastLoginAttemptDate, lastSuccessfulLoginDate, failedLoginAttempts, maxFailedLoginAttempts, preEncryptedPasswordAllowed, failedCaptchaLoginAttempts, maxFailedCaptchaLoginAttempts, lastFailedLoginDate, unlockMailbox, protocolType, accessControlStatus, ignoreSoftDelete,

    // mailaccess
    allowedIP, oldAllowedIP, newAllowedIP, popAuthenticationType, popSSLAccessType, imapSSLAccessType, mobileMailAccessType, webmailSSLAccessType, popAccessType, imapAccessType, smtpAccessEnabled, smtpAuthenticationEnabled, smtpSSLAccessEnabled, webmailAccessType, mobileActiveSyncAllowed,

    // MailSend
    fromAddress, futureDeliveryEnabled, maxFutureDeliveryDaysAllowed, maxFutureDeliveryMessagesAllowed, alternateFromAddress, replyToAddress, useRichTextEditor, includeOriginalMailInReply, originalMailSeperatorCharacter, autoSaveSentMessages, addSignatureForNewMails, signature, addSignatureInReplyType, autoSpellCheckEnabled, confirmPromptOnDelete, includeReturnReceiptReq, numDelayedDeliveryMessagesPending, oldNumDelayedDeliveryMessagesPending, newNumDelayedDeliveryMessagesPending, maxSendMessageSizeKB, maxAttachmentSizeKB, maxAttachmentsInSession, maxAttachmentsToMessage, maxCharactersPerPage,
    
    //Mail Send signature
    signatureId, userId, signatureName, isDefault,
    
    // MailSend BmiFilters
    msSpamAction, msSpamMessageIndicator,
    // MailSend CommtouchFilters
    msCommtouchSpamAction, msCommtouchVirusAction,
    // MailSend McAfeeFilters
    msMcAfeeVirusAction,

    // MailReceipt
    receiveMessages, forwardingEnabled, copyOnForward, forwardingAddress,maxNumForwardingAddresses, filterHTMLContent, displayHeaders, webmailDisplayWidth, webmailDisplayFields, maxMailsPerPage, previewPaneEnabled, ackReturnReceiptReq, deliveryScope, addressesForLocalDelivery, autoReplyMode, autoReplyMessage, maxReceiveMessageSizeKB, senderBlocking, senderBlockingAllowed, senderBlockingEnabled, rejectAction, rejectFolder, allowedSendersList, blockedSendersList, blockSendersPABActive, blockSendersPABAccess, sieveFilteringEnabled, mtaFilter, rmFilter, blockedSenders, blockedSenderAction, blockedSenderMessage, rejectBouncedMessage, bmiSpamAction, commtouchSpamAction, cloudmarkSpamAction, spamMessageIndicator, commtouchVirusAction, spamfilterEnabled, spamPolicy, cleanAction, suspectAction, mcAfeeVirusAction, spamAction, mailSendBmiSpamAction, mailSendCommtouchSpamAction, mailSendCommtouchVirusAction, mailSendMcAfeeVirusAction, mailReceiptBmiSpamAction, mailReceiptCommtouchSpamAction, virusAction, mailReceiptCommtouchVirusAction, mailReceiptMcAfeeVirusAction, mailReceiptCloudmarkSpamAction, sieveFilterBlockedSender, mobileMaxReceiveMessageSizeKB,

    // MailReceipt Multi valued
    allowedSender, oldAllowedSender, newAllowedSender,

    blockedSender, oldBlockedSender, newBlockedSender,

    addressForLocalDelivery, oldAddressForLocalDelivery, newAddressForLocalDelivery,

    // AdminControl
    adminBlockedSendersList, adminApprovedSendersList, adminRejectAction, adminRejectInfo, adminDefaultDisposition,oldAdminBlockedSender,newAdminBlockedSender,oldAdminApprovedSender,newAdminApprovedSender,adminControl,
    
    // SmsServices
    smsServicesAllowed, smsServicesMsisdn, smsServicesMsisdnStatus, lastSmsServicesMsisdnStatusChangeDate, smsOnlineEnabled, internationalSMSAllowed, internationalSMSEnabled, maxSMSPerDay, concatenatedSMSAllowed, concatenatedSMSEnabled, maxConcatenatedSMSSegments, maxPerCaptchaSMS, maxPerCaptchaDurationMins, smsBasicNotificationsEnabled, smsAdvancedNotificationsEnabled,

    // InternalInfo
    webmailVersion, selfCareAccessEnabled, selfCareSSLAccessEnabled, messageStoreHost, smtpProxyHost, imapProxyHost, popProxyHost, autoReplyHost, realm, imapProxyAuthenticationEnabled, remoteCallTracingEnabled, interManagerAccessEnabled, interManagerSSLAccessEnabled, voiceMailAccessEnabled, faxAccessEnabled, ldapUtilitiesAccessType, addressBookProvider, eventRecordingEnabled, maxHeaderLength, maxFilesSizeKB,

    // SocialNetworks
    socialNetworkIntegrationAllowed, socialNetworkSite, socialNetworkSiteAccessEnabled,

    // ExternalAccounts
    externalMailAccountsAllowed, promptForExternalAccountSync,
    // Mail Account
    accountId, accountName, accountType, serverAddress, serverPort, timeoutSecs, emailAddress, accountUserName, accountPassword, displayImage, fetchMailsEnabled, autoFetchMails, fetchedMailsFolderName, leaveEmailsOnServer, fromAddressInReply, mailSendEnabled, mailSendSMTPAddress, mailSendSMTPPort, smtpSSLEnabled, useSmtpLogin, smtpLogin, smtpPassword, sendFolderName, draftsFolderName, trashFolderName, spamFolderName, mailAccount,
    
    headerText, tagText, folderName,
    
    // Family Mailbox
    groupName, groupAdmin, adminrealmdn,
    
    // Mailbox Operations
    deleteOnlyOnMss,
    recreateOnMss,
    disableBlobHardDelete,
    keepAutoReply,
    suppressMers,
    
    // CAPTCHA
    isValid,
    
    // AppSuite Provisioning
    bulkUploaderImapURL,
    bulkUploaderSmtpURL,
    
    // can be email or mailboxId or other uid for mailbox
    uid
}
