
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * SocialNetworkSite object of socialNetworks
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "socialNetworkSite",
    "socialNetworkSiteAccessEnabled"
})
public class SocialNetworkSite implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("socialNetworkSite")
    private java.lang.String socialNetworkSite;
    @org.codehaus.jackson.annotate.JsonProperty("socialNetworkSiteAccessEnabled")
    private java.lang.String socialNetworkSiteAccessEnabled;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("socialNetworkSite")
    public java.lang.String getSocialNetworkSite() {
        return socialNetworkSite;
    }

    @org.codehaus.jackson.annotate.JsonProperty("socialNetworkSite")
    public void setSocialNetworkSite(java.lang.String socialNetworkSite) {
        this.socialNetworkSite = socialNetworkSite;
    }

    @org.codehaus.jackson.annotate.JsonProperty("socialNetworkSiteAccessEnabled")
    public java.lang.String getSocialNetworkSiteAccessEnabled() {
        return socialNetworkSiteAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("socialNetworkSiteAccessEnabled")
    public void setSocialNetworkSiteAccessEnabled(java.lang.String socialNetworkSiteAccessEnabled) {
        this.socialNetworkSiteAccessEnabled = socialNetworkSiteAccessEnabled;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
