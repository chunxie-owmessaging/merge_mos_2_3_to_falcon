
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * MailAccess object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "allowedIPs",
    "popAccessType",
    "popAuthenticationType",
    "popSSLAccessType",
    "imapAccessType",
    "imapSSLAccessType",
    "smtpAccessEnabled",
    "smtpSSLAccessEnabled",
    "smtpAuthenticationEnabled",
    "webmailAccessType",
    "webmailSSLAccessType",
    "mobileMailAccessType",
    "mobileActiveSyncAllowed"
})
public class MailAccess implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("allowedIPs")
    private List<String> allowedIPs;
    @org.codehaus.jackson.annotate.JsonProperty("popAccessType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AccessType popAccessType;
    @org.codehaus.jackson.annotate.JsonProperty("popAuthenticationType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.IntAccessType popAuthenticationType;
    @org.codehaus.jackson.annotate.JsonProperty("popSSLAccessType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AccessType popSSLAccessType;
    @org.codehaus.jackson.annotate.JsonProperty("imapAccessType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AccessType imapAccessType;
    @org.codehaus.jackson.annotate.JsonProperty("imapSSLAccessType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AccessType imapSSLAccessType;
    @org.codehaus.jackson.annotate.JsonProperty("smtpAccessEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smtpAccessEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("smtpSSLAccessEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smtpSSLAccessEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("smtpAuthenticationEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smtpAuthenticationEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("webmailAccessType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AccessType webmailAccessType;
    @org.codehaus.jackson.annotate.JsonProperty("webmailSSLAccessType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AccessType webmailSSLAccessType;
    @org.codehaus.jackson.annotate.JsonProperty("mobileMailAccessType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AccessType mobileMailAccessType;
    @org.codehaus.jackson.annotate.JsonProperty("mobileActiveSyncAllowed")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType mobileActiveSyncAllowed;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("allowedIPs")
    public List<String> getAllowedIPs() {
        return allowedIPs;
    }

    @org.codehaus.jackson.annotate.JsonProperty("allowedIPs")
    public void setAllowedIPs(List<String> allowedIPs) {
        this.allowedIPs = allowedIPs;
    }

    @org.codehaus.jackson.annotate.JsonProperty("popAccessType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AccessType getPopAccessType() {
        return popAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("popAccessType")
    public void setPopAccessType(com.opwvmsg.mxos.data.enums.MxosEnums.AccessType popAccessType) {
        this.popAccessType = popAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("popAuthenticationType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.IntAccessType getPopAuthenticationType() {
        return popAuthenticationType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("popAuthenticationType")
    public void setPopAuthenticationType(com.opwvmsg.mxos.data.enums.MxosEnums.IntAccessType popAuthenticationType) {
        this.popAuthenticationType = popAuthenticationType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("popSSLAccessType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AccessType getPopSSLAccessType() {
        return popSSLAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("popSSLAccessType")
    public void setPopSSLAccessType(com.opwvmsg.mxos.data.enums.MxosEnums.AccessType popSSLAccessType) {
        this.popSSLAccessType = popSSLAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("imapAccessType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AccessType getImapAccessType() {
        return imapAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("imapAccessType")
    public void setImapAccessType(com.opwvmsg.mxos.data.enums.MxosEnums.AccessType imapAccessType) {
        this.imapAccessType = imapAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("imapSSLAccessType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AccessType getImapSSLAccessType() {
        return imapSSLAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("imapSSLAccessType")
    public void setImapSSLAccessType(com.opwvmsg.mxos.data.enums.MxosEnums.AccessType imapSSLAccessType) {
        this.imapSSLAccessType = imapSSLAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpAccessEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSmtpAccessEnabled() {
        return smtpAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpAccessEnabled")
    public void setSmtpAccessEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smtpAccessEnabled) {
        this.smtpAccessEnabled = smtpAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpSSLAccessEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSmtpSSLAccessEnabled() {
        return smtpSSLAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpSSLAccessEnabled")
    public void setSmtpSSLAccessEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smtpSSLAccessEnabled) {
        this.smtpSSLAccessEnabled = smtpSSLAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpAuthenticationEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSmtpAuthenticationEnabled() {
        return smtpAuthenticationEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpAuthenticationEnabled")
    public void setSmtpAuthenticationEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smtpAuthenticationEnabled) {
        this.smtpAuthenticationEnabled = smtpAuthenticationEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("webmailAccessType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AccessType getWebmailAccessType() {
        return webmailAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("webmailAccessType")
    public void setWebmailAccessType(com.opwvmsg.mxos.data.enums.MxosEnums.AccessType webmailAccessType) {
        this.webmailAccessType = webmailAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("webmailSSLAccessType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AccessType getWebmailSSLAccessType() {
        return webmailSSLAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("webmailSSLAccessType")
    public void setWebmailSSLAccessType(com.opwvmsg.mxos.data.enums.MxosEnums.AccessType webmailSSLAccessType) {
        this.webmailSSLAccessType = webmailSSLAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mobileMailAccessType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AccessType getMobileMailAccessType() {
        return mobileMailAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mobileMailAccessType")
    public void setMobileMailAccessType(com.opwvmsg.mxos.data.enums.MxosEnums.AccessType mobileMailAccessType) {
        this.mobileMailAccessType = mobileMailAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mobileActiveSyncAllowed")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getMobileActiveSyncAllowed() {
        return mobileActiveSyncAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mobileActiveSyncAllowed")
    public void setMobileActiveSyncAllowed(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType mobileActiveSyncAllowed) {
        this.mobileActiveSyncAllowed = mobileActiveSyncAllowed;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
