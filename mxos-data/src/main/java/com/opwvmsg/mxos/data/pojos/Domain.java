
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Domain object of mxos
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "domain",
    "isSubDomain",
    "type",
    "relayHost",
    "alternateDomain",
    "defaultMailbox",
    "domainOwner",
    "customFields"
})
public class Domain implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("domain")
    private String domain;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("isSubDomain")
    private String isSubDomain;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    private com.opwvmsg.mxos.data.enums.MxosEnums.DomainType type;
    @org.codehaus.jackson.annotate.JsonProperty("relayHost")
    private String relayHost;
    @org.codehaus.jackson.annotate.JsonProperty("alternateDomain")
    private String alternateDomain;
    @org.codehaus.jackson.annotate.JsonProperty("defaultMailbox")
    private String defaultMailbox;
    @org.codehaus.jackson.annotate.JsonProperty("domainOwner")
    private String domainOwner;
    @org.codehaus.jackson.annotate.JsonProperty("customFields")
    private String customFields;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("domain")
    public String getDomain() {
        return domain;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("domain")
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("isSubDomain")
    public String getIsSubDomain() {
        return isSubDomain;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("isSubDomain")
    public void setIsSubDomain(String isSubDomain) {
        this.isSubDomain = isSubDomain;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    public com.opwvmsg.mxos.data.enums.MxosEnums.DomainType getType() {
        return type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    public void setType(com.opwvmsg.mxos.data.enums.MxosEnums.DomainType type) {
        this.type = type;
    }

    @org.codehaus.jackson.annotate.JsonProperty("relayHost")
    public String getRelayHost() {
        return relayHost;
    }

    @org.codehaus.jackson.annotate.JsonProperty("relayHost")
    public void setRelayHost(String relayHost) {
        this.relayHost = relayHost;
    }

    @org.codehaus.jackson.annotate.JsonProperty("alternateDomain")
    public String getAlternateDomain() {
        return alternateDomain;
    }

    @org.codehaus.jackson.annotate.JsonProperty("alternateDomain")
    public void setAlternateDomain(String alternateDomain) {
        this.alternateDomain = alternateDomain;
    }

    @org.codehaus.jackson.annotate.JsonProperty("defaultMailbox")
    public String getDefaultMailbox() {
        return defaultMailbox;
    }

    @org.codehaus.jackson.annotate.JsonProperty("defaultMailbox")
    public void setDefaultMailbox(String defaultMailbox) {
        this.defaultMailbox = defaultMailbox;
    }

    @org.codehaus.jackson.annotate.JsonProperty("domainOwner")
    public String getDomainOwner() {
        return domainOwner;
    }

    @org.codehaus.jackson.annotate.JsonProperty("domainOwner")
    public void setDomainOwner(String domainOwner) {
        this.domainOwner = domainOwner;
    }

    @org.codehaus.jackson.annotate.JsonProperty("customFields")
    public String getCustomFields() {
        return customFields;
    }

    @org.codehaus.jackson.annotate.JsonProperty("customFields")
    public void setCustomFields(String customFields) {
        this.customFields = customFields;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
