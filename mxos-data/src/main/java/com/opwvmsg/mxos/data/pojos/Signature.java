
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Signature object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "signatureId",
    "signatureName",
    "isDefault",
    "addSignatureInReplyType",
    "signature"
})
public class Signature implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("signatureId")
    private Integer signatureId;
    @org.codehaus.jackson.annotate.JsonProperty("signatureName")
    private String signatureName;
    @org.codehaus.jackson.annotate.JsonProperty("isDefault")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType isDefault;
    @org.codehaus.jackson.annotate.JsonProperty("addSignatureInReplyType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.SignatureInReplyType addSignatureInReplyType;
    @org.codehaus.jackson.annotate.JsonProperty("signature")
    private String signature;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("signatureId")
    public Integer getSignatureId() {
        return signatureId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("signatureId")
    public void setSignatureId(Integer signatureId) {
        this.signatureId = signatureId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("signatureName")
    public String getSignatureName() {
        return signatureName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("signatureName")
    public void setSignatureName(String signatureName) {
        this.signatureName = signatureName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("isDefault")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getIsDefault() {
        return isDefault;
    }

    @org.codehaus.jackson.annotate.JsonProperty("isDefault")
    public void setIsDefault(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType isDefault) {
        this.isDefault = isDefault;
    }

    @org.codehaus.jackson.annotate.JsonProperty("addSignatureInReplyType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.SignatureInReplyType getAddSignatureInReplyType() {
        return addSignatureInReplyType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("addSignatureInReplyType")
    public void setAddSignatureInReplyType(com.opwvmsg.mxos.data.enums.MxosEnums.SignatureInReplyType addSignatureInReplyType) {
        this.addSignatureInReplyType = addSignatureInReplyType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("signature")
    public String getSignature() {
        return signature;
    }

    @org.codehaus.jackson.annotate.JsonProperty("signature")
    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
