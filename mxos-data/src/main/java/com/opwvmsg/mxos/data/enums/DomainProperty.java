/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All LDAP attributes used by this application.
 *
 * @see <i>Email Mx LDAP schema Reference Guide</i>
 * @author mxos-dev
 */
public enum DomainProperty implements DataMap.Property {

    /**
     * <tt>Domain</tt>: Specifies a domain name for the Openwave Email
     * application. This name uniquely identifies the domain. mxos -
     * domain:domain
     *
     * @since mxos 1.0
     */
    domain,

    /**
     * <tt>DomainType</tt>: Specifies domain type associated with a domain
     * object. mxos - domain:domainType
     *
     * @since mxos 1.0
     */
    type,

    /**
     * <tt>MailRelayHost</tt>: Specifies domain mailRelayHost associated with a
     * domain object. mxos - domain:mailRelayHost
     *
     * @since mxos 1.0
     */
    relayHost,

    /**
     * <tt>MailRewriteDomain</tt>: Specifies domain mailRewriteDomain associated
     * with a domain object. mxos - domain:mailRewriteDomain
     *
     * @since mxos 1.0
     */
    alternateDomain,

    /**
     * <tt>MailWildcardAccount</tt>: Specifies the default mailbox of domain
     * with a domain object. mxos - domain:defaultMailbox
     *
     * @since mxos 1.0
     */
    defaultMailbox

}
