/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

/**
 * This class implements the match method for epoch time in milliseconds.
 * 
 */
public abstract class EpochTimeTerm extends SearchTerm {

	private static final long serialVersionUID = 4173440549243580245L;

	/**
	 * The epoch time.
	 * 
	 * @serial
	 */
	final protected String epochTimeStr;
	final protected char op;
	final protected String opStr;

	protected EpochTimeTerm(char op, final String epochTimeStr) {
		this.op = op;
		this.epochTimeStr = epochTimeStr;
		if (op == SearchOperator.GreaterThanOrEqual) {
			this.opStr = ">=";
		} else if (op == SearchOperator.LessThanOrEqual) {
			this.opStr = "<=";
		} else {
			this.opStr = "" + op;
		}
	}

	public boolean match(long et) {
		long epochTime = Long.parseLong(epochTimeStr);
		if (op == SearchOperator.Equals) {
			return et == epochTime;
		} else if (op == SearchOperator.GreaterThan) {
			return et > epochTime;
		} else if (op == SearchOperator.LessThan) {
			return et < epochTime;
		} else if (op == SearchOperator.GreaterThanOrEqual) {
			return et >= epochTime;
		} else if (op == SearchOperator.LessThanOrEqual) {
			return et <= epochTime;
		}
		return false;
	}

	/**
	 * Equality comparison.
	 */
	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof EpochTimeTerm))
			return false;
		EpochTimeTerm that = (EpochTimeTerm) o;
		return this.op == that.op && this.epochTimeStr.equals(that.epochTimeStr);
	}

	/**
	 * Compute a hashCode for this object.
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + (int) op;
		result = 31 * result + epochTimeStr.hashCode();
		return result;
	}

}
