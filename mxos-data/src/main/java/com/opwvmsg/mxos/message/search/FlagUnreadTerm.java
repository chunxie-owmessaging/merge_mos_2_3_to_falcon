/**
 * 
 */
package com.opwvmsg.mxos.message.search;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * This class implements comparisons for the search key flagUnread.
 * 
 * @author mxos-dev
 * 
 */
public class FlagUnreadTerm extends SearchTerm {

    private static final long serialVersionUID = 2557514143770942721L;

    protected final boolean flag;
    protected final char op;

    public FlagUnreadTerm(char op, boolean flag) {
        this.flag = flag;
        this.op = op;
    }

    /*
     * (non-Javadoc)
     * @see
     * com.opwvmsg.mxos.message.search.SearchTerm#match(com.opwvmsg.mxos.message
     * .pojos.Metadata)
     */
    @Override
    public boolean match(Message message) {
        Metadata metadata = message.getMetadata();
        if (op == SearchOperator.Equals) {
            return metadata.getFlagUnread().equals(flag);
        } else {
            return false;
        }
    }

   

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (flag ? 1231 : 1237);
        result = prime * result + op;
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FlagUnreadTerm other = (FlagUnreadTerm) obj;
        if (flag != other.flag)
            return false;
        if (op != other.op)
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * @see com.opwvmsg.mxos.message.search.SearchTerm#toString()
     */
    @Override
    public String toString() {
        return new StringBuffer("(").append(MessageProperty.flagUnread.name())
                .append(op).append(flag).append(")").toString();
    }

}
