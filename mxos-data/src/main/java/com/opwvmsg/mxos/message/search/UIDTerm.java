/*
 * Copyright (c) 2014 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * This class implements comparisons for the search key uid.
 * 
 */
public class UIDTerm extends SearchTerm {

	private static final long serialVersionUID = -4465650855803883302L;

	protected final String input;
    protected final char op;
	
	public UIDTerm(char op, String input) {
		this.op = op;
		this.input = input;
    }
	
	@Override
    public boolean match(Message message) {
        Metadata metadata = message.getMetadata();
        if (op == SearchOperator.Equals) {
            return metadata.getUid() == Long.parseLong(this.input);
        } else if (op == SearchOperator.Regex) {
            return metadata.getUid().toString().matches(input);
        }
        return false;
	}
	
	/**
	 * Equality comparison.
	 */
	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof UIDTerm))
			return false;
		UIDTerm that = (UIDTerm) o;
		return this.op == that.op && this.input.equals(that.input);
	}

	/**
	 * Compute a hashCode for this object.
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + (int) op;
		result = 31 * result + input.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return new StringBuffer("(").append(MessageProperty.uid.name())
                .append(op).append(input).append(")").toString();
	}

}
