package com.opwvmsg.mxos.message.search;

public class SearchOperator {
    public final static char Contains = '~';
    public final static char Equals   = '=';
    public final static char Regex = '^';
    public final static char GreaterThan   = '>';
    public final static char LessThan   = '<';
    public final static char GreaterThanOrEqual   = '}';
    public final static char LessThanOrEqual   = '{';
    public final static char And      = '&';
    public final static char Or       = '|';
    public final static char Not      = '!';
    public final static char Begin    = '(';
    public final static char End      = ')';
    public final static char Escape   = '\\';
}
