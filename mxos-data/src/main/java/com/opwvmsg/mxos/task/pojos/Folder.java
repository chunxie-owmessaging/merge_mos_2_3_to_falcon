
package com.opwvmsg.mxos.task.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonValue;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Folder object of task mxos
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "title",
    "displayName",
    "module",
    "type",
    "folderId",
    "haveSubFolders",
    "ownRights",
    "permissions",
    "description",
    "isDefault",
    "numTotalObjects",
    "numNewObjects",
    "numUnreadObjects",
    "numDeletedObjects",
    "capabilities",
    "standardFolderType",
    "supportedCapabilities",
    "flags"
})
public class Folder implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("title")
    private java.lang.String title;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("displayName")
    private java.lang.String displayName;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("module")
    private java.lang.String module;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    private Folder.Type type;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("folderId")
    private java.lang.String folderId;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("haveSubFolders")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType haveSubFolders;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("ownRights")
    private java.lang.String ownRights;
    /**
     * Permissions object of Task
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("permissions")
    private Permissions permissions;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("description")
    private java.lang.String description;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("isDefault")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType isDefault;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("numTotalObjects")
    private java.lang.Integer numTotalObjects;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("numNewObjects")
    private java.lang.Integer numNewObjects;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("numUnreadObjects")
    private java.lang.Integer numUnreadObjects;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("numDeletedObjects")
    private java.lang.Integer numDeletedObjects;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("capabilities")
    private java.lang.String capabilities;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("standardFolderType")
    private java.lang.String standardFolderType;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("supportedCapabilities")
    private List<java.lang.String> supportedCapabilities;
    /**
     * flags object of Task
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flags")
    private Flags flags;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("title")
    public java.lang.String getTitle() {
        return title;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("title")
    public void setTitle(java.lang.String title) {
        this.title = title;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("displayName")
    public java.lang.String getDisplayName() {
        return displayName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("displayName")
    public void setDisplayName(java.lang.String displayName) {
        this.displayName = displayName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("module")
    public java.lang.String getModule() {
        return module;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("module")
    public void setModule(java.lang.String module) {
        this.module = module;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    public Folder.Type getType() {
        return type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("type")
    public void setType(Folder.Type type) {
        this.type = type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("folderId")
    public java.lang.String getFolderId() {
        return folderId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("folderId")
    public void setFolderId(java.lang.String folderId) {
        this.folderId = folderId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("haveSubFolders")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getHaveSubFolders() {
        return haveSubFolders;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("haveSubFolders")
    public void setHaveSubFolders(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType haveSubFolders) {
        this.haveSubFolders = haveSubFolders;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("ownRights")
    public java.lang.String getOwnRights() {
        return ownRights;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("ownRights")
    public void setOwnRights(java.lang.String ownRights) {
        this.ownRights = ownRights;
    }

    /**
     * Permissions object of Task
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("permissions")
    public Permissions getPermissions() {
        return permissions;
    }

    /**
     * Permissions object of Task
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("permissions")
    public void setPermissions(Permissions permissions) {
        this.permissions = permissions;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("description")
    public java.lang.String getDescription() {
        return description;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("description")
    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("isDefault")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getIsDefault() {
        return isDefault;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("isDefault")
    public void setIsDefault(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("numTotalObjects")
    public java.lang.Integer getNumTotalObjects() {
        return numTotalObjects;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("numTotalObjects")
    public void setNumTotalObjects(java.lang.Integer numTotalObjects) {
        this.numTotalObjects = numTotalObjects;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("numNewObjects")
    public java.lang.Integer getNumNewObjects() {
        return numNewObjects;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("numNewObjects")
    public void setNumNewObjects(java.lang.Integer numNewObjects) {
        this.numNewObjects = numNewObjects;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("numUnreadObjects")
    public java.lang.Integer getNumUnreadObjects() {
        return numUnreadObjects;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("numUnreadObjects")
    public void setNumUnreadObjects(java.lang.Integer numUnreadObjects) {
        this.numUnreadObjects = numUnreadObjects;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("numDeletedObjects")
    public java.lang.Integer getNumDeletedObjects() {
        return numDeletedObjects;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("numDeletedObjects")
    public void setNumDeletedObjects(java.lang.Integer numDeletedObjects) {
        this.numDeletedObjects = numDeletedObjects;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("capabilities")
    public java.lang.String getCapabilities() {
        return capabilities;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("capabilities")
    public void setCapabilities(java.lang.String capabilities) {
        this.capabilities = capabilities;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("standardFolderType")
    public java.lang.String getStandardFolderType() {
        return standardFolderType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("standardFolderType")
    public void setStandardFolderType(java.lang.String standardFolderType) {
        this.standardFolderType = standardFolderType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("supportedCapabilities")
    public List<java.lang.String> getSupportedCapabilities() {
        return supportedCapabilities;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("supportedCapabilities")
    public void setSupportedCapabilities(List<java.lang.String> supportedCapabilities) {
        this.supportedCapabilities = supportedCapabilities;
    }

    /**
     * flags object of Task
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flags")
    public Flags getFlags() {
        return flags;
    }

    /**
     * flags object of Task
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flags")
    public void setFlags(Flags flags) {
        this.flags = flags;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum Type {

        PRIVATE("private"),
        PUBLIC("public"),
        SHARED("shared"),
        SYSTEM("system");
        private final java.lang.String value;

        private Type(java.lang.String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public java.lang.String toString() {
            return this.value;
        }

        @JsonCreator
        public static Folder.Type fromValue(java.lang.String value) {
            for (Folder.Type c: Folder.Type.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static Folder.Type fromOrdinal(java.lang.Integer value) {
            for (Folder.Type c: Folder.Type.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

}
