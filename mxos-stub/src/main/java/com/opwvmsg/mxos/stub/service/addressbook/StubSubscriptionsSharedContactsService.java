/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.SharedContact;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.ISubscriptionsSharedContactsService;

/**
 * Subscriptions Shared Contacts service exposed to client which is responsible
 * for doing basic Subscriptions related activities e.g create, update, read,
 * search and delete Shared Contacts via stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubSubscriptionsSharedContactsService implements
        ISubscriptionsSharedContactsService {

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long sharedContactsId = 2;
        if (inputParams
                .containsKey(AddressBookProperty.sharedContactsId.name())) {
            sharedContactsId = Long.parseLong(inputParams.get(
                    AddressBookProperty.sharedContactsId.name()).get(0));
        }
        return sharedContactsId;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;

    }

    @Override
    public SharedContact read(Map<String, List<String>> inputParams)
            throws MxOSException {

        SharedContact sharedContact = new SharedContact();
        sharedContact.setCreated("2011-06-28T17:26:54Z");
        sharedContact.setUpdated("2011-06-28T17:26:54Z");
        sharedContact
                .setSharedURL("http://mxos.com:8080/addressbook/v1/{Bob}/contacts/{John}");
        sharedContact.setSharedUserId("ABUserName");

        return sharedContact;
    }

    @Override
    public List<SharedContact> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        List<SharedContact> sharedContacts = new ArrayList<SharedContact>();

        SharedContact sharedContact1 = new SharedContact();
        sharedContact1.setCreated("2011-06-28T17:26:54Z");
        sharedContact1.setUpdated("2011-06-28T17:26:54Z");
        sharedContact1
                .setSharedURL("http://mxos.com:8080/addressbook/v1/{Bob}/contacts/{John}");
        sharedContact1.setSharedUserId("ABUserName");

        SharedContact sharedContact2 = new SharedContact();
        sharedContact2.setCreated("2011-06-28T17:26:54Z");
        sharedContact2.setUpdated("2011-06-28T17:26:54Z");
        sharedContact2
                .setSharedURL("http://mxos.com:8080/addressbook/v1/{Bob}/contacts/{John}");
        sharedContact2.setSharedUserId("ABUserName");

        sharedContacts.add(sharedContact1);
        sharedContacts.add(sharedContact2);

        return sharedContacts;
    }
}
