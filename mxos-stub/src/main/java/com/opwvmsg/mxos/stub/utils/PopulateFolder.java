/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.message.pojos.Folder;

/**
 * Populate the Data.
 * 
 * @author Aricent
 * 
 */
public final class PopulateFolder {

    public static Folder folder;

    private static void loadFolder() {
        InputStream is = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            is = StubUtils.getRource("/sample-folder.json");
            if (is != null) {
                folder = mapper.readValue(is, Folder.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(is);
        }
    }
    /**
     * populate Folder data.
     * 
     * @return Folder Folder object
     */
    public static String create(String name) {
        return name;
    }

    /**
     * populate Folder data.
     * 
     * @return Folder Folder object
     */
    public static Folder getFolder(String folderId, String name) {
        loadFolder();
        folder.setFolderId(folderId);
        folder.setFolderName(name);
        return folder;
    }

    /**
     * Get List of folders.
     *
     * @return
     */
    public static List<Folder> getFolders() {
        loadFolder();
        List<Folder> list = new ArrayList<Folder>();
        list.add(getFolder("1", "INBOX"));
        list.add(getFolder("2", "OUTBOX"));
        return list;
    }
}
