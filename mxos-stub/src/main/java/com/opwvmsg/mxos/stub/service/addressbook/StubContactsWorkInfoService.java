/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.addressbook.pojos.Communication;
import com.opwvmsg.mxos.addressbook.pojos.WorkInfo;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsWorkInfoService;

/**
 * WorkInfo service exposed to client which is responsible for doing basic
 * WorkInfo related activities e.g create, update, read, search and delete
 * WorkInfo via Stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubContactsWorkInfoService implements IContactsWorkInfoService {

    @Override
    public WorkInfo read(Map<String, List<String>> inputParams)
            throws MxOSException {
        WorkInfo workInfo = new WorkInfo();

        Address address = new Address();
        address.setName("611 Lakeview Apartments");
        address.setStreet("2100, Seaport Blvd");
        address.setCity("Redwood city");
        address.setStateOrProvince("California");
        address.setPostalCode("45-765-24");
        address.setCountry("USA");
        address.setCountryCode("ISO 3166 Country code");

        Communication communication = new Communication();
        communication.setPreference("mobile");
        communication.setPhone1("+1223444");
        communication.setPhone2("+1223445");
        communication.setVoip("+1223444");
        communication.setMobile("+212323");
        communication.setFax("+3234234");
        communication.setEmail("bob@openwave.com");
        communication.setImAddress("bob@openwave.com");

        workInfo.setAddress(address);
        workInfo.setCommunication(communication);

        return workInfo;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }
}
