/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.captcha;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.captcha.ICaptchaService;

/**
 * CaptchaService service exposed to client which is responsible validating
 * CAPTCHA request
 * 
 * @author mxos-dev
 */
public class StubCaptchaService implements ICaptchaService {
    @Override
    public void validateCaptcha(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;

    }
}
