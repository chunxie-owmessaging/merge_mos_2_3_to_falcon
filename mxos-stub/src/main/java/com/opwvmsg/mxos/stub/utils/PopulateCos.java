/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.data.pojos.AddressBookFeatures;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;

/**
 * Populate the Data.
 * 
 * @author Aricent
 * 
 */
public final class PopulateCos {
    public static Mailbox cos;
    private static void loadCos() {
        InputStream is = null;
        try {
            // JsonFactory jfactory = new JsonFactory();
            ObjectMapper mapper = new ObjectMapper();
            is = StubUtils.getRource("/sample-cos.json");
            if (is != null) {
                cos = mapper.readValue(is, Mailbox.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    /**
     * Populate create.
     * 
     * @param email email
     * @return String mailboxId.
     */
    public static Long create(String email) {
        return new Long(email.hashCode());
    }

    /**
     * populate mailboxes data.
     * 
     * @return List of mailbox objects
     */
    public static List<Mailbox> getCoses() {
        loadCos();
        List<Mailbox> mailboxes = new ArrayList<Mailbox>();
        mailboxes.add(cos);
        Mailbox mb1 = cos;
        mb1.getBase().setCosId("default_2.0");
        mailboxes.add(mb1);
        return mailboxes;
    }

    /**
     * populate Cos data.
     * 
     * @return Cos (Mailbox) object
     */
    public static Mailbox getCos(String cosId) {
        loadCos();
        cos.getBase().setCosId(cosId);
        return cos;
    }
    
    /**
     * populate WebMailFeatures data
     * 
     * @return WebMailFeatures WebMailFeatures object
     */
    public static WebMailFeatures getWebMailFeatures() {
        loadCos();
        return cos.getWebMailFeatures();
    }
    
    /**
     * 
     * @return
     */
    public static AddressBookFeatures getWebMailFeaturesAddressBook() {
        loadCos();
        return cos.getWebMailFeatures().getAddressBookFeatures();
    }

    /**
     * populate Credentials data
     * 
     * @return Credentials object
     */
    public static Credentials getCredentials() {
        loadCos();
        return cos.getCredentials();
    }

    /**
     * populate GeneralPreferences data
     * 
     * @return
     * 
     * @return GeneralPreferences GeneralPreferences object
     */
    public static GeneralPreferences getGeneralPreferences() {
        loadCos();
        return cos.getGeneralPreferences();
    }

    /**
     * populate MailSend data
     * 
     * @return MailSend MailSend object
     */
    public static MailSend getMailSend() {
        loadCos();
        return cos.getMailSend();
    }

    /**
     * populate MailReceipt data
     * 
     * @return MailReceipt MailReceipt object
     */
    public static MailReceipt getMailReceipt() {
        loadCos();
        return cos.getMailReceipt();
    }

    /**
     * populate MailAccess data
     * 
     * @return MailAccess MailAccess object
     */
    public static MailAccess getMailAccess() {
        loadCos();
        return cos.getMailAccess();
    }

    /**
     * populate MailStore data
     * 
     * @return MailStore MailStore object
     */
    public static MailStore getMailStore() {
        loadCos();
        return cos.getMailStore();
    }

    /**
     * populate ExternalAccounts data
     * 
     * @return ExternalAccounts ExternalAccounts object
     */
    public static ExternalAccounts getExternalAccounts() {
        loadCos();
        return cos.getExternalAccounts();
    }
    
    /**
     * populate SocialNetworks data
     * 
     * @return SocialNetworks SocialNetworks object
     */
    public static SocialNetworks getSocialNetworks() {
        loadCos();
        return cos.getSocialNetworks();
    }

    /**
     * populate SmsServices data
     * 
     * @return SmsServices SmsServices object
     */
    public static SmsServices getSmsServices() {
        loadCos();
        return cos.getSmsServices();
    }

    /**
     * populate InternalInfo data
     * 
     * @return InternalInfo InternalInfo object
     */
    public static InternalInfo getInternalInfo() {
        loadCos();
        return cos.getInternalInfo();
    }
}
