/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Notification;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.INotificationsService;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * Mailbox related activities e.g create, update, read, search and delete
 * Mailbox via stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubNotificationsService implements INotificationsService {

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long notificationId = 2;
        if (inputParams.containsKey(AddressBookProperty.notificationId.name())) {
            notificationId = Long.parseLong(inputParams.get(
                    AddressBookProperty.notificationId.name()).get(0));
        }
        return notificationId;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;

    }

    @Override
    public Notification read(Map<String, List<String>> inputParams)
            throws MxOSException {

        Notification notification = new Notification();
        notification.setCreated("2011-06-28T17:26:54Z");
        notification.setUpdated("2011-06-28T17:26:54Z");
        notification.setApplicationTag("myApp");
        notification.setNotificationFormat("callback");
        notification
                .setNotifyURL("http://application.com/addressbook/notifications");

        return notification;
    }

    @Override
    public List<Notification> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        List<Notification> notifications = new ArrayList<Notification>();

        Notification notification1 = new Notification();
        notification1.setCreated("2011-06-28T17:26:54Z");
        notification1.setUpdated("2011-06-28T17:26:54Z");
        notification1.setApplicationTag("myApp1");
        notification1.setNotificationFormat("callback");
        notification1
                .setNotifyURL("http://application1.com/addressbook/notifications");

        Notification notification2 = new Notification();
        notification2.setCreated("2011-06-28T17:26:54Z");
        notification2.setUpdated("2011-06-28T17:26:54Z");
        notification2.setApplicationTag("myApp2");
        notification2.setNotificationFormat("callback");
        notification2
                .setNotifyURL("http://application2.com/addressbook/notifications");

        notifications.add(notification1);
        notifications.add(notification2);

        return notifications;
    }
}
