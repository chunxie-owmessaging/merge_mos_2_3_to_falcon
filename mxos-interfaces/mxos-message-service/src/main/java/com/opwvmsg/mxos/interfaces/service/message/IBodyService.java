/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.interfaces.service.message;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Message body operations interface which will be exposed to the client. This
 * interface is responsible for reading message body.
 * 
 * @author mxos-dev
 */
public interface IBodyService {

    /**
     * This operation is responsible for reading message body of the given message.
     * 
     * @param inputParams Parameters given by user.
     * @return returns list of messages.
     * @throws MxOSException MxOSException.
     */
    Body read(Map<String, List<String>> inputParams) throws MxOSException;
    /**
     * This operation is responsible for updating message blob of the given
     * message key.
     * 
     * @param inputParams Parameters given by user.
     * @throws MxOSException MxOSException.
     */
    void update(Map<String, List<String>> inputParams) throws MxOSException;
}
