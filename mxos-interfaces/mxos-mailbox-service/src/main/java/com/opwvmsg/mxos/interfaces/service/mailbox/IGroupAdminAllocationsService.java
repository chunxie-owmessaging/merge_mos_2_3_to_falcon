/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.GroupAdminAllocations;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Group Admin Allocations operations interface which will be exposed to the client.
 * This interface is responsible for doing Group Admin Allocations related operations
 * (like Read, Update, etc.).
 *
 * @author mxos-dev
 */
public interface IGroupAdminAllocationsService {

    /**
     * This operation is responsible for reading Group Admin Allocations mail
     * store.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return GroupAdminAllocations POJO of MailStore.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_GROUP_ADMIN_ALLOCATIONS_GET - <i>Unable to perform Group Admin Allocations GET operation.</i>
     * MBX_NOT_GROUP_ADMIN_ACCOUNT - <i>Mailbox is not a group admin account.</i>
     * MBX_NOT_GROUP_ACCOUNT - <i>Mailbox is not a group account.</i>
     * GROUP_MAILBOX_FEATURE_IS_DISABLED - <i>Group Mailbox feature is disabled.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    GroupAdminAllocations read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating Group Admin Allocations mail
     * store.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>maxAdminStorageSizeKB</b>
     *     <i>Maximum storage space that can be allocated to all accounts combined</i>
     *     Type: long, Minimum : -1, Maximum : 9223372036854775807
     *     Example: 0
     *     
     * <b>maxAdminUsers</b>
     *     <i>Maximum number of licensed users</i>
     *     Type: long, Minimum : -1, Maximum : 9223372036854775807
     *     Example: 0
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_MAX_ADMIN_STORAGE_SIZEKB - <i>Invalid max admin storage size kb.</i>
     * MBX_UNABLE_TO_SET_MAX_ADMIN_STORAGE_SIZEKB - <i>Unable to set maxAdminStorageSizeKB.</i>
     * MBX_INVALID_MAX_ADMIN_USERS - <i>Invalid max admin users.</i>
     * MBX_UNABLE_TO_SET_MAX_ADMIN_USERS - <i>Unable to set maxAdminUsers.</i>
     * MBX_NOT_GROUP_ADMIN_ACCOUNT - <i>Mailbox is not a group admin account.</i>
     * MBX_NOT_GROUP_ACCOUNT - <i>Mailbox is not a group account.</i>
     * GROUP_MAILBOX_FEATURE_IS_DISABLED - <i>Group Mailbox feature is disabled.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters."/>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
