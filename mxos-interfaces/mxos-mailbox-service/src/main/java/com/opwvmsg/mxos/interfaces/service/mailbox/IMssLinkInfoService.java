/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mss Link info operations interface which will be exposed to the client.
 * This interface is responsible for MSS link info read operation.
 *
 * @author mxos-dev
 */
public interface IMssLinkInfoService {

    /**
     * This operation is responsible for reading Message Store Link info.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return MssLinkInfo POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MSG_MISSING_MAILBOXID_OR_MESSAGESTOREHOST_PARAM - <i>Unable to perform operation, messageStoreHost and mailboxId both are mandatory params if either is present.</i>
     * MBX_UNABLE_TO_MSSLINKINFO_GET - <i>Unable to perform MssLinkInfo GET operation.</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_FOUND - <i>The given email does not exist.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    MssLinkInfo read(final Map<String, List<String>> inputParams)
        throws MxOSException;

}
