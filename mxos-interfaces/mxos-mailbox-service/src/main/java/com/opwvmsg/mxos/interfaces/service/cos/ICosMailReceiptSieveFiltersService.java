/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.SieveFilters;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mailbox mailReceipt sieve filter operations interface which will be exposed
 * to the client. This interface is responsible for doing mailReceipt sieve
 * filter related operations (like Create, Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface ICosMailReceiptSieveFiltersService {

    /**
     * This operation is responsible for reading mailReceipt sieve filters.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * 
	 * </pre>
	 *  
	 * @return returns list of allowed domains.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_UNABLE_TO_MAILRECEIPT_GET - <i>Unable to perform MailReceipt GET operation</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    SieveFilters read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mailReceipt sieve filters.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * 
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>sieveFilteringEnabled</b>
	 *     <i>Sieve filtering is enabled or disabled for the subscriber</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["no", "yes"]
	 *     Example: no
	 *     
	 * <b>blockedSenderAction</b>
	 *     <i>Blocked sender action</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["bounce", "reject", "sideline", "toss", "none"]
	 *     Example: none
	 *     
	 * <b>blockedSenderMessage</b>
	 *     <i>Message associated with the denied sender actio</i>
	 *     Type: String, Empty Allowed: No, Maximum Length: 255
	 *     Example: message 	
	 *     
	 * <b>rejectBouncedMessage</b>
	 *     <i>Reject bounced message enabled or disabled</i>
	 *     Type: String, Empty Allowed: No
	 *     Enumeration: ["no", "yes"]
	 *     Example: no
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_UNABLE_TO_CREATE - <i>Unable to perform Mailbox Create operation.</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
