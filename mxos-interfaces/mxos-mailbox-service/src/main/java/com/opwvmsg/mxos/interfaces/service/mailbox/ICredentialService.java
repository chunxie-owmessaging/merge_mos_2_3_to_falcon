/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Credential operations interface which will be exposed to the client. This
 * interface is responsible for doing allowed credential related operations
 * (like Read, Update, etc.).
 *
 * @author mxos-dev
 */

public interface ICredentialService {

    /**
     * This operation is responsible for reading credentials. It uses one or
     * more actions to do this activity.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>.
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return Credentials object
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_CREDENTIALS_GET_MISSING_PARAMS - <i>One or more mandatory parameters 
     * are missing for Credentials GET operation.</i>
     * MBX_CREDENTIALS_UNABLE_TO_GET - <i>Unable to perform Credentials GET operation.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Credentials read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating credentials.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>.
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * 
     * <b>inputParams-Optional:</b> <i>(Any one is required)</i>
     * 
     * <b>passwordStoreType</b>
     *     <i>Update of only passwordStoreType is not allowed; it can be updated only along with password;
     *     Note - Currently mOS does not support custom2, custom3, custom4, custom5 types.</i>
     *     Type: String, Enum: clear, unix, ssha1, md5-po, sha1, custom1, custom2, custom3, custom4, custom5, bcrypt
     *     Example: clear
     * <b>preEncryptedPasswordAllowed</b>
     *     <i>PreEncryptedPasswordAllowed, allowed value -true, false.
     *          If it is true  mOS to check for optional attribute passwordStoreType,
     *          If passwordStoreType is not passed in the request, mOS to use passwordStoreType as SHA1,
     *          If passwordStoreType is passed in the request and is not SHA1, mOS to send back the error,
     *          If the preEnryptedPasswordAllowed is not passed in the request or its value is false, it will follow existing behaviour. 
     *          If passwordStoreType is 'bcrypt' and its value is true, it will follow existing behaviour.
     *          If passwordStoreType is 'bcrypt' and its value is false, an exception will be thrown.<i>
     *     Type: String, Enum: "false", "true"
     * <b>password</b>
     *     <i>Subscriber's password, By default, the allowed values are strings between 1 and 65 characters,
     *     containing letters, numbers and the following special characters "+", "/", "\", "=", "-".
     *     An operator can control the allowed characters by modifying the password validator configuration
     *     in $MXOS_HOME/var/mxos/config/mxos/schema/attributes/password</i>
     *     Type: String
     *     Regex Pattern: "[a-zA-Z0-9+/=-]{1,31}"
     *     Example: Pa$$W0rD#1234
     * <b>passwordRecoveryAllowed</b>
     *     <i>Specified whether password recovery enabled, allowed value- yes or no</i>
     *     Type: String, EmptyAllowed: yes, Maximum Length: 255
     * <b>passwordRecoveryPreference</b>
     *     <i>Indicate password recovery - allowed value- none, email, SMS, All</i>
     *     Type: String, Enum: "none", "email", "sms", "all"     
     * <b>passwordRecoveryMsisdn</b>
     *     <i>Valid msisdn number used for recovery, should be between 10 to 15 length, start with(+) and only number supported</i>
     *     Type: String,  
     *     Regex Pattern: "^(({0,})|[+]{1}([0-9]{10,15}))$"  
     * <b>passwordRecoveryMsisdnStatus</b>
     *     <i>Indicates whether msisdn recovery is activated or not, allowed values- activated or deactivated</i>
     *     Type: String, Enum: "deactivated", "activated"
     * <b>lastPasswordRecoveryMsisdnStatusChangeDate</b>
     *     <i>date in format yyyy-MM-dd'T'HH:mm:ss'Z'</i>
     *     Type: date, Format: yyyy-MM-dd'T'HH:mm:ss'Z'
     * <b>passwordRecoveryEmail</b>
     *     <i>Email used in password Recovery, valid email address of format userName@domain</i>
     * <b>passwordRecoveryEmailStatus</b>
     *     <i>Indicates whether email recovery is activated or not, allowed values- activated or deactivated</i>
     *     Type: String, Enum: "deactivated", "activated"
     * <b>maxFailedLoginAttempts</b>
     *     <i>Specified maximum number of failed login attempts for a user, allowed value- positive integer</i>
     *     Type: integer, Minimum: 0, maximum: 1000
     * <b>failedLoginAttempts</b>
     *     <i>Indicates total number of failed login attempts since last successful login</i>
     *     Type: integer, Minimum: 0, maximum: 1000
     * <b>maxFailedCaptchaLoginAttempts</b>
     *     <i>Specified maximum number of failed captcha login attempts for a user, allowed value- positive integer</i>
     *     Type: integer, Minimum: 0, maximum: 1000
     * <b>failedCaptchaLoginAttempts</b>
     *     <i>Indicates total number of failed captcha login attempts since last successful login</i>
     *     Type: integer, Minimum: 0, maximum: 1000    
     * <b>lastLoginAttemptDate</b>
     *     <i>date of last login attempt in format yyyy-MM-dd'T'HH:mm:ss'Z'</i>
     *     Type: date, Format: yyyy-MM-dd'T'HH:mm:ss'Z'
     * <b>lastSuccessfulLoginDate</b>
     *     <i>Last successful login date in format yyyy-MM-dd'T'HH:mm:ss'Z'</i>
     *     Type: date 
     *     Format: yyyy-MM-dd'T'HH:mm:ss'Z'
     * </pre>
     *  
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_BLOCKED_SENDERS_POST - <i>Unable to perform Blocked Senders POST operation.</i>
     * GEN_BAD_REQUEST - <i>email parameter  is missing from the request.</i>
     * MBX_CREDENTIALS_UPDATE_MISSING_PARAMS - <i>One or more mandatory parameters are missing 
        for Credentials POST operation.</i>
     * MBX_UNABLE_TO_SET_PASSWORD - <i>Unable to perform set password.</i>
     * MBX_INVALID_PASSWORD - <i>Invalid password, check the password format.</i>
     * MBX_INVALID_PASSWORD_STORETYPE - <i>Unable to perform set PasswordStoreType, 
        Invalid passwordStoreType parameter.</i>
     * MBX_INVALID_PRENCRYPTED_PASSWORD_ALLOWED - <i>Invalid value for prencrypted password allowed.</i>
     * MBX_INVALID_PASSWORD_RECOVERY_ALLOWED - <i>Unable to perform set passwordRecoveryAllowed, 
        Invalid passwordRecoveryAllowed parameter.</i>
     * MBX_INVALID_PASSWORD_RECOVERY_PREFERENCE - <i>Unable to perform set passwordRecoveryPreference, 
        Invalid passwordRecoveryPreference parameter.</i>
     * MBX_INVALID_PASSWORD_RECOVERY_MSISDN - <i>Unable to perform set passwordRecoveryMsisdn, 
        Invalid passwordRecoveryMsisdn parameter.</i>
     * MBX_INVALID_PASSWORD_RECOVERY_MSISDN_STATUS - <i>Unable to perform set passwordRecoveryMsisdnStatus 
        Invalid passwordRecoveryMsisdnStatus parameter.</i>
     * MBX_INVALID_PASS_REC_MSISDN_STATUS_CHANGE_DATE - <i>Unable to perform set 
        passwordRecoveryMsisdnStatusChangeDate, Invalid passwordRecoveryMsisdnStatusChangeDate parameter.</i>
     * MBX_INVALID_PASSWORD_RECOVERY_EMAIL - <i>Unable to perform set passwordRecoveryEmail,
        Invalid passwordRecoveryEmail paremeter.</i>
     * MBX_INVALID_PASSWORD_RECOVERY_EMAIL_STATUS - <i>Unable to perform set passwordRecoveryEmailStatus,
        Invalid passwordRecoveryEmailStatus parameter</i>
     * MBX_INVALID_MAX_FAILED_LOGIN_ATTEMPTS - <i>Unable to perform set maxFailedLoginAttempts,
        Invalid maxFailedLoginAttempts parameter.</i>
     * MBX_INVALID_FAILED_LOGIN_ATTEMPTS - <i>Unable to perform set failedLoginAttempts,
        Invalid failedLoginAttempts parameter.</i>
     * MBX_INVALID_FAILED_CAPTCHA_LOGIN_ATTEMPTS - <i>Invalid failedCaptchaLoginAttempts parameter.</i>
     * MBX_INVALID_MAX_FAILED_CAPTCHA_LOGIN_ATTEMPTS - <i>Invalid maxFailedCaptchaLoginAttempts parameter.</i>
     * MBX_UNABLE_TO_SET_FAILED_CAPTCHA_LOGIN_ATTEMPTS - <i>Unable to set failedCaptchaLoginAttempts.</i>
     * MBX_UNABLE_TO_SET_MAX_FAILED_CAPTCHA_LOGIN_ATTEMPTS - <i>Unable to set maxFailedCaptchaLoginAttempts.</i>    
     * MBX_INVALID_LAST_LOGIN_ATTEMPTS_DATE - <i>Unable to perform set lastLoginAttemptDate,
     * MBX_UNABLE_TO_SET_LAST_SUCCESSFUL_LOGIN_DATE - <i>Unable to perform set successful login date.</i>
     * MBX_INVALID_LAST_SUCCESSFUL_LOGIN_DATE - <i>Invalid date format for last successful login date.</i>
     * MBX_INVALID_LAST_FAILED_LOGIN_TIME - <i>Invalid last failed login time.</i>

     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for authenticating client.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>password</b>
     *     <i>Subscriber's password, By default, the allowed values are strings between 1 and 65 characters,
     *     containing letters, numbers and the following special characters "+", "/", "\", "=", "-".
     *     An operator can control the allowed characters by modifying the password validator configuration
     *     in $MXOS_HOME/var/mxos/config/mxos/schema/attributes/password</i>
     *     Type: String
     *     Regex Pattern: "[a-zA-Z0-9+/=-]{1,64}"
     *     Example: Pa$$W0rD#1234
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void authenticate(final Map<String, List<String>> inputParams)
        throws MxOSException;/**
     * This operation is responsible for authenticating the user and return Mailbox pojo.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>password</b>
     *     <i>Subscriber's password, By default, the allowed values are strings between 1 and 65 characters,
     *     containing letters, numbers and the following special characters "+", "/", "\", "=", "-".
     *     An operator can control the allowed characters by modifying the password validator configuration
     *     in $MXOS_HOME/var/mxos/config/mxos/schema/attributes/password</i>
     *     Type: String
     *     Regex Pattern: "[a-zA-Z0-9+/=-]{1,64}"
     *     Example: Pa$$W0rD#1234
     * </pre>
     * 
     * @return Mailbox object
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Mailbox authenticateAndGetMailbox(final Map<String, List<String>> inputParams)
        throws MxOSException;

}
