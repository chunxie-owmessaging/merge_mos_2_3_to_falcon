/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to COS for MessageEventRecords Object level operations like Read,
 * Update.
 *
 * @author mxos-dev
 */
public interface ICosMessageEventRecordsService {

    /**
     * This operation is responsible for reading MessageEventRecords for a COS.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * </pre>
     *  
     * @return returns Base POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * COS_INVALID_COSID - <i>Invalid cosId.</i>
     * COS_NOT_FOUND - <i>The given cosId does not exist.</i>
     * MBX_UNABLE_TO_GET_MSG_EVENT_RECORDS - <i>Unable to perform MessageEventRecords GET operation.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    MessageEventRecords read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating MessageEventRecords attributes
     * for a COS.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>eventRecordingEnabled</b>
     *     <i>Specifies whether the Message Event Recording System (MERS) used by the Openwave Notification Manager application is enabled.</i>
     *     Type: String, Empty Allowed: No, Enum: "Always", "True", "False", "Never"
     * <b>maxHeaderLength</b>
     *     <i>MSpecifies the maximum header size of event messages in the Message Event Recording System (MERS).</i>
     *     Type: Integer, Empty Allowed: No, Minimum: 1, Maximum: 2147483647
     * <b>maxFilesSizeKB</b>
     *     <i>Specifies the size, in kilobytes, of the Message Event Recording System (MERS) file.</i>
     *     Type: Integer, Empty Allowed: No, Minimum: 1, Maximum: 2147483647
     * </pre>
     *  
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * COS_INVALID_COSID - <i>Invalid cosId.</i>
     * COS_NOT_FOUND - <i>The given cosId does not exist.</i>
     * MBX_INVALID_EVENT_RECORDING_ENABLED - <i>Invalid eventRecordingEnabled.</i>
     * MBX_INVALID_MAX_HEADER_LENGTH - <i>Invalid maxHeaderLength.</i>
     * MBX_INVALID_MAX_FILES_SIZE - <i>Invalid maxFilesSizeKB.</i>
     * MBX_UNABLE_TO_SET_EVENT_RECORDING_ENABLED - <i>Unable to set eventRecordingEnabled.</i>
     * MBX_UNABLE_TO_SET_MAX_HEADER_LENGTH - <i>Unable to set maxHeaderLength.</i>
     * MBX_UNABLE_TO_SET_MAX_FILES_SIZE - <i>Unable to set maxFilesSizeKB.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

}
