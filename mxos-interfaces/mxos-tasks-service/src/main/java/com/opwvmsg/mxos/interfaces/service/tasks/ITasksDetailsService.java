/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.tasks;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.task.pojos.Details;

/**
 * Interface to Tasks Base Object level operations like Read, Update and List.
 * 
 * @author mxos-dev
 */
public interface ITasksDetailsService {

	/**
     * This operation is responsible for read the task details.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>taskId</b>
     *     <i>Task ID, which is returned after calling create task API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 1,2,3 et...
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     *      
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * @throws MxOSException MxOSException.
     * 
     *  <pre>
     * <b>Error Codes</b>
     * TSK_INVALID_USERNAME - <i>Invalid user name.</i>
     * TSK_INVALID_TASKID - <i>Invalid TaskId.</i>
     * TSK_INVALID_SESSION - <i>Invalid Session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i> 
     * </pre>
     * 
     */
    Details read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating Task Details.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>taskId</b>
     *     <i>Task ID, which is returned after calling create task API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 1,2,3 et...
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     *      
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>actualCost</b> 
     *     <i>Actual cost spent to finish the task</i>
     *     Type: Integer
     *     
     * <b>actualWork</b> 
     *     <i>Actual duration spent to finish the task</i>
     *     Type: Integer
     *     
     * <b>billing</b> 
     *     <i>Billing details of the task</i>
     *     Type: String
     *     
     * <b>targetCost</b> 
     *     <i>Target cost to finish the task</i>
     *     Type: Integer 
     *     
     * <b>targetWork</b> 
     *     <i>Target work duration</i>
     *     Type: Integer
     *     
     * <b>currency</b> 
     *     <i>Currency to mention estimated and actual cost</i>
     *     Type: String
     *     Example: USD
     *     
     * <b>mileage</b> 
     *     <i>Distance details</i>
     *     Type: String
     *     Example: 400
     * <b>companies</b>
     *     <i>Company details</i>
     *     Type: String
     *     Example: openwave
     *       
     * @throws MxOSException MxOSException.
     * 
     * 
     *  <pre>
     * <b>Error Codes</b>
     * TSK_INVALID_USERNAME - <i>Invalid user name.</i>
     * TSK_INVALID_TASKID - <i>Invalid TaskId.</i>
     * TSK_INVALID_SESSION - <i>Invalid Session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i> 
     * TSK_INVALID_DETAILS_ACTUALCOST - <i>Invalid task details actual cost.</i>
     * TSK_INVALID_DETAILS_ACTUALWORK - <i>Invalid task details actual work.</i>
     * TSK_INVALID_DETAILS_BILLING - <i>Invalid task details billing.</i>
     * TSK_INVALID_DETAILS_TARGETCOST - <i>Invalid task details target cost.</i>
     * TSK_INVALID_DETAILS_TARGETWORK - <i>Invalid task details target work.</i>
     * TSK_INVALID_DETAILS_CURRENCY - <i>Invalid task details currency.</i>
     * TSK_INVALID_DETAILS_MILEAGE - <i>Invalid task details mileage.</i>
     * TSK_INVALID_DETAILS_COMPANIES - <i>Invalid task details companies.</i>
     * </pre>
     * 
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
