/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.interfaces.service;

import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Abstract class to define MxOS Service objects. This class must be extended
 * and implement init() method to initialize service objects.
 *
 * @author mxos-dev
 */
public abstract class IMxOSContext {
    private static Logger logger = Logger.getLogger(IMxOSContext.class);
    protected String contextId = null;
    protected Map<String, Object> serviceMap;

    /**
     * Init.
     *
     * @param contextId contextId
     * @param p properties
     * @throws MxOSException on any error.
     */
    public abstract void init(final String contextId, final Properties p)
            throws MxOSException;

    /**
     * Destroy.
     *
     * @throws MxOSException on any error.
     */
    public abstract void destroy() throws MxOSException;
    /**
     * Method to getService by passing serviceName.
     *
     * @param serviceId serviceName
     * @return Service Object
     * @throws MxOSException if any
     */
    public Object getService(String serviceId) throws MxOSException {
        if (serviceId != null && serviceMap.containsKey(serviceId)) {
            return serviceMap.get(serviceId);
        } else {
            logger.error(new StringBuilder("Service ").append(serviceId)
                    .append(" not found."));
            throw new MxOSException(ErrorCode.GEN_BAD_REQUEST.name(),
                    "Service Not found");
        }
    }

    /**
     * @return the contextId
     */
    public String getContextId() {
        return contextId;
    }
}
