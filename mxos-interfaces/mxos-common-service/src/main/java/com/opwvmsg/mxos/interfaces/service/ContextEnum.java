/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.interfaces.service;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * Enum of all the Context's that MxOS Supports.
 *
 * @author mxos-dev
 */
public enum ContextEnum implements DataMap.Property {
    STUB,
    REST,
    BACKEND;
    /**
     * Method to find the given name with ignore case.
     * @param name name
     * @return ContextEnum
     */
    public static ContextEnum find(String name) {
        for (ContextEnum ctx : ContextEnum.values()) {
            if (ctx.name().equalsIgnoreCase(name)) {
                return ctx;
            }
        }
        return null;
    }
}
