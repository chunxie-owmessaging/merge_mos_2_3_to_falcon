/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.interfaces.service;

/**
 * Enum of all the supported services
 * 
 * @author mxos-dev
 */

public class ContextProperty {
    // Context Id
    // Any String - which would be use as Name of the Context
    public static final String MXOS_CONTEXT_ID = "contextId";
    // Context Type
    // Allowed values REST, STUB or BACKEND
    public static final String MXOS_CONTEXT_TYPE = "contextType";
    // Services to be support
    // Allowed Value Mailbox,Message,AddressBook,Process,Notify
    public static final String LOAD_SERVICES = "loadServices";
    // REST context specific keys
    // Base MxOS URL
    // Absent of this param, the default value http://mxos-host:8080/mxos
    public static final String MXOS_BASE_URL = "mxosBaseUrl";
    // Max Number of Connections to MxOS
    // The connection objects towards mxos would be created on demand
    // Absent of this param, the default value is 10
    public static final String MXOS_MAX_CONNECTIONS = "mxosMaxConnections";
    // Connection Timeout
    // Default value as per system level configuration
    public static final String MXOS_CONNECTION_TIMEOUT = "mxosConnectionTimeout";
    // Read Timeout
    // Default value as per system level configuration
    public static final String MXOS_READ_TIMEOUT = "mxosReadTimeout";
    // is custom
    public static final String CUSTOM = "custom";
}
